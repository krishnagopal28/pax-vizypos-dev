package com.vizypay.cashdiscountapp.repository;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.DatabaseManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderItemRepository {

	private DatabaseHelper databaseHelper;
	Dao<OrderItems, Integer> itemDao;

	public OrderItemRepository(Context context) throws SQLException {
		DatabaseManager dbManager = new DatabaseManager();
		databaseHelper = dbManager.getHelper(context);
		itemDao = databaseHelper.getOrderDao();
	}

	public int create(OrderItems item) {
		try {
			return itemDao.create(item);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int update(OrderItems item) {
		try {
			return itemDao.update(item);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int delete(OrderItems item) {
		try {
			return itemDao.delete(item);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	public int getCountItem(){
		int count = 0;
		try{
			count=(int) itemDao.countOf();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return count;
	}

	public double getTotalPrice(){
		double price = 0.0;
		try {
			GenericRawResults<String[]> rawResults = itemDao.queryRaw("select sum(total_amount) from OrderItems where isDPercent='false'");
			//price=price+;
			for (String[] resultArray : rawResults) {
				price = Double.parseDouble(resultArray[0].toString());
			}

			rawResults.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		//price=price-cashD;

		try{
			double discount=0.00;
			GenericRawResults<String[]> rawResultsDiscount =itemDao.queryRaw("select sum(total_amount) from OrderItems where isDPercent='true'");
			//price=price+;
			for (String[] resultArray : rawResultsDiscount) {
				discount=Double.parseDouble(resultArray[0].toString().replace("-",""));

				//price=Double.parseDouble(resultArray[0].toString());
			}
			double discountV=((price * discount)/100);
			price=price-discountV;
			rawResultsDiscount.close();


		}catch(Exception ex){
			//ex.printStackTrace();
		}
		return price;
	}

	public double getTotalPriceAll(){
		double price = 0.0;
		try {
			GenericRawResults<String[]> rawResults = itemDao.queryRaw("select sum(total_amount) from OrderItems ");
			//price=price+;
			for (String[] resultArray : rawResults) {
				price = Double.parseDouble(resultArray[0].toString());
			}

			rawResults.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		//price=price-cashD;
		return price;
	}



	public double getTotalPrice(double cashD){
		double price = 0.0;
		try {
			GenericRawResults<String[]> rawResults = itemDao.queryRaw("select sum(total_amount) from OrderItems where isDPercent='false'");
			//price=price+;
			for (String[] resultArray : rawResults) {
				price = Double.parseDouble(resultArray[0].toString());
			}

			rawResults.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		price=price-cashD;

		try{
			double discount=0.00;
			GenericRawResults<String[]> rawResultsDiscount =itemDao.queryRaw("select sum(total_amount) from OrderItems where isDPercent='true'");
			//price=price+;
			for (String[] resultArray : rawResultsDiscount) {
				discount=Double.parseDouble(resultArray[0].toString().replace("-",""));

				//price=Double.parseDouble(resultArray[0].toString());
			}
			double discountV=((price * discount)/100);
			price=price-discountV;
			rawResultsDiscount.close();


		}catch(Exception ex){
			ex.printStackTrace();
		}
		return price;
	}

	public double getSubTotal(){
		double price = 0.0;
		try {
			GenericRawResults<String[]> rawResults = itemDao.queryRaw("select sum(total_amount) from OrderItems where isDiscount='false'");
			//price=price+;
			for (String[] resultArray : rawResults) {
				price = Double.parseDouble(resultArray[0].toString());
			}

			//price=rawResults[0];
			rawResults.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return price;
	}

	public double getAllDiscountPercent(){
		double price = 0.0;
		try {
			GenericRawResults<String[]> rawResults = itemDao.queryRaw("select sum(total_amount) from OrderItems where isDPercent='true'");
			//price=price+;
			for (String[] resultArray : rawResults) {
				price = Double.parseDouble(resultArray[0].toString());
			}

			//price=rawResults[0];
			rawResults.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return price;
	}
public double getAllDiscountFixed(){
		double price = 0.0;
		try {
			GenericRawResults<String[]> rawResults = itemDao.queryRaw("select sum(total_amount) from OrderItems where isDPercent='false' AND isDiscount='true'");
			//price=price+;
			for (String[] resultArray : rawResults) {
				price = Double.parseDouble(resultArray[0].toString());
			}

			//price=rawResults[0];
			rawResults.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return price;
	}


	public List<String> getDiscountIds(){
			List<String> idList = new ArrayList<String>();
			try {
				GenericRawResults<Object[]> rawResults = itemDao.queryRaw("select 'ad_id' from OrderItems OrderItems ",new DataType[] { DataType.INTEGER });
				for (Object[] resultArray : rawResults){
					String id = (resultArray[0].toString().replace("-",""));
					idList.add(id);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return idList;

	}


	public int getQuantityById(int productId) throws SQLException{
		try{
			// return the orders with the sum of their amounts per account
			String totalAmount=null;
			GenericRawResults<String[]> rawResults =itemDao.queryRaw("select quantity from OrderItems where product_id='"+productId+"' ");
			// page through the results
			for (String[] resultArray : rawResults) {
				totalAmount=resultArray[0].toString();
			}
			int total=Integer.parseInt(totalAmount);
			rawResults.close();
			return total;

		}catch(Exception ex){
			//ex.printStackTrace();
			return 0;
		}
	}

	public int getMinQuantityById(int productId) throws SQLException{
		try{
			// return the orders with the sum of their amounts per account
			String totalAmount=null;
			GenericRawResults<String[]> rawResults =itemDao.queryRaw("select min_qty from OrderItems where product_id='"+productId+"' ");
			// page through the results
			for (String[] resultArray : rawResults) {
				totalAmount=resultArray[0].toString();
			}
			int total=Integer.parseInt(totalAmount);
			rawResults.close();
			return total;

		}catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
	}
	public String getISDiscountPercentByID(String productId) throws SQLException{
		try{
			// return the orders with the sum of their amounts per account
			String isPercent=null;
			GenericRawResults<String[]> rawResults =itemDao.queryRaw("select isDPercent from OrderItems where product_id='"+productId+"' ");
			// page through the results
			for (String[] resultArray : rawResults) {
				isPercent=resultArray[0].toString();
			}

			rawResults.close();
			return isPercent;

		}catch(Exception ex){
			ex.printStackTrace();
			return "false";
		}
	}


	public double getQuickSaleAmount(){
		try{
			// return the orders with the sum of their amounts per account
			String saleAmount=null;
			GenericRawResults<String[]> rawResults =itemDao.queryRaw("select quickSale from OrderItems");
			// page through the results
			for (String[] resultArray : rawResults) {
				saleAmount=resultArray[0].toString();
			}
			double total=Double.parseDouble(saleAmount);
			rawResults.close();
			return total;

		}catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
	}

	public double getPriceById(String productId) throws SQLException{
		try{
			// return the orders with the sum of their amounts per account
			String totalAmount=null;
			GenericRawResults<String[]> rawResults =itemDao.queryRaw("select total_amount from OrderItems where product_id='"+productId+"' ");
			// page through the results
			for (String[] resultArray : rawResults) {
				totalAmount=resultArray[0].toString();
			}
			double total=Double.parseDouble(totalAmount);
			rawResults.close();
			return total;

		}catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
	}


	public int containsId(String productId) throws SQLException{
		try{
			// return the orders with the sum of their amounts per account
			int rows=0;
			GenericRawResults<String[]> rawResults =itemDao.queryRaw("select * from OrderItems where product_id='"+productId+"' ");
			// page through the results
			for (String[] resultArray : rawResults) {
				rows=rows+1;
			}
			rawResults.close();
			return rows;

		}catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
	}


	public List<Integer> getAllId() throws SQLException {
		List<Integer> idList = new ArrayList<Integer>();
		try {
			GenericRawResults<Object[]> rawResults = itemDao.queryRaw("select product_id from OrderItems",new DataType[] { DataType.INTEGER });
			for (Object[] resultArray : rawResults){
				int id = Integer.parseInt(resultArray[0].toString());
				idList.add(id);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return idList;
	}

	public List<OrderItems> getAll() {
		try {
			QueryBuilder<OrderItems, Integer> qb = itemDao.queryBuilder();

			qb.orderBy("isDPercent", true);
			return qb.query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAll() throws SQLException{
		try{
			try
			{
				DeleteBuilder<OrderItems, Integer> deleteBuilder = itemDao.deleteBuilder();
				deleteBuilder.where().isNotNull("product_id");
				deleteBuilder.delete();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void updateQuantityById(String productID,int quantity,double updateTotalPrice, double totalCost, double totalProfit) throws SQLException{
		try{
			UpdateBuilder<OrderItems, Integer> updateBuilder = itemDao.updateBuilder();
			// set the criteria like you would a QueryBuilder
			updateBuilder.where().eq("product_id", productID);
			// update the value of your field(s)
			updateBuilder.updateColumnValue("quantity", quantity);
			updateBuilder.updateColumnValue("item_total_cost", totalCost);
			updateBuilder.updateColumnValue("item_total_profit", totalProfit);
			updateBuilder.updateColumnValue("total_amount", updateTotalPrice);

			updateBuilder.update();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
public void updatePriceById(String productID,double updateTotalPrice) throws SQLException{
		try{
			UpdateBuilder<OrderItems, Integer> updateBuilder = itemDao.updateBuilder();
			// set the criteria like you would a QueryBuilder
			updateBuilder.where().eq("product_id", productID);
			// update the value of your field(s)
			//updateBuilder.updateColumnValue("quantity", quantity);
			updateBuilder.updateColumnValue("new_total_amount", updateTotalPrice);

			updateBuilder.update();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void deleteById(String productId) throws SQLException{
		try{
			DeleteBuilder<OrderItems, Integer> deleteBuilder=itemDao.deleteBuilder();
			Where<OrderItems, Integer> where=deleteBuilder.where();
			where.eq("product_id", productId);
			PreparedDelete<OrderItems> preparedDelete=deleteBuilder.prepare();
			itemDao.delete(preparedDelete);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/*public void updateQuickSale(double quickSale) {
		try{
			UpdateBuilder<OrderItems, Integer> updateBuilder = itemDao.updateBuilder();
			// set the criteria like you would a QueryBuilder
		//	updateBuilder.where().eq("product_id", productID);
			// update the value of your field(s)
			updateBuilder.updateColumnValue("quickSale", quickSale);
			//updateBuilder.updateColumnValue("total_amount", updateTotalPrice);

			updateBuilder.update();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}*/

	public void updateQuickSalePrice(String price) {
		try{
			UpdateBuilder<OrderItems, Integer> updateBuilder = itemDao.updateBuilder();
			// set the criteria like you would a QueryBuilder
			updateBuilder.where().eq("product_id", "0");
			// update the value of your field(s)
			updateBuilder.updateColumnValue("quantity", 1);
			updateBuilder.updateColumnValue("item_price", price);
			updateBuilder.updateColumnValue("total_amount", price);

			updateBuilder.update();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
