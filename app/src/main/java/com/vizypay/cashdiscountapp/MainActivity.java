package com.vizypay.cashdiscountapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.pax.poslink.PaymentRequest;
import com.pax.poslink.PaymentResponse;
import com.pax.poslink.PosLink;
import com.pax.poslink.ProcessTransResult;
import com.vizypay.cashdiscountapp.activity.LoginActivity;
import com.vizypay.cashdiscountapp.adapter.MainScreenTabAdapter;
import com.vizypay.cashdiscountapp.fragment.AllOrders;
import com.vizypay.cashdiscountapp.fragment.DashboardFragment;
import com.vizypay.cashdiscountapp.fragment.MainTabFragment;
import com.vizypay.cashdiscountapp.utility.AppThreadPool;
import com.vizypay.cashdiscountapp.utility.POSLinkCreatorWrapper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.AppController;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.Menu;
import android.widget.TextView;

import static com.vizypay.cashdiscountapp.volley.AppController.getContext;

public class MainActivity extends AppCompatActivity
       {

    private MainScreenTabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Fragment fragment;
    protected PosLink poslink;
           private Menu menu;
           Bundle bundle;
    String fragmentToCall;

           @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bundle=getIntent().getExtras();
        if (bundle!=null){
            fragmentToCall=bundle.getString("Fragment");
            fragment = new AllOrders();
        }else {
            fragment = new MainTabFragment();
        }
       // PosLink
        initPOSLink();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.home1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*  getSupportActionBar().setTitle(Preferences.get(this, Preferences.APP_HEADER));
        setActionBarTitle(Preferences.get(this, Preferences.APP_HEADER));*/

        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(Preferences.get(this, Preferences.APP_HEADER));
        // Toast.makeText(this,Preferences.get(this, Preferences.APP_HEADER),Toast.LENGTH_LONG).show();


       /* viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        adapter = new MainScreenTabAdapter(getSupportFragmentManager());
        adapter.addFragment(new ItemFragment(), "Tab 1");

        adapter.addFragment(new ItemFragment(), "Tab 2");
        adapter.addFragment(new ItemFragment(), "Tab 3");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);*/


        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, fragment, "AllProduct");
            ft.commit();
        }


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
            /*   DrawerLayout drawer = findViewById(R.id.drawer_layout);
               drawer.setVisibility(View.GONE);
*/
      /*  DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);*/


    }

           @Override
           public boolean onKeyDown(int keyCode, KeyEvent event) {
               fragment=new DashboardFragment();
               if (fragment != null) {
                   FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                   ft.replace(R.id.frame_layout, fragment, "AllProduct");
                   ft.commit();
               }
               return false;

           }

           public void setActionBarTitle(String title) {
          getSupportActionBar().setTitle(title);
    }


    @Override
    public void onBackPressed() {
       /* DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else */{
            super.onBackPressed();
        }
    }
           public boolean onCreateOptionsMenu(Menu menu) {
               this.menu = menu;
               getMenuInflater().inflate(R.menu.main, menu);
               return true;
           }

           @Override
           public boolean onOptionsItemSelected(MenuItem item) {
               switch (item.getItemId()) {
                   // Respond to the action bar's Up/Home button
                   case android.R.id.home:
                       fragment = new DashboardFragment();
                       if (fragment != null) {
                           FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                           ft.replace(R.id.frame_layout, fragment, "AllProduct");
                           ft.commit();
                       }
                       //handle the home button onClick event here.
                       return true;
                   case R.id.action_logout:
                       showAlertDialog("Logout", "Are you sure you want to logout?");


               }

               return super.onOptionsItemSelected(item);
           }
           public void showAlertDialog( String title, String message) {
               AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                       MainActivity.this);

               // set title
               alertDialogBuilder.setTitle(title);

               // set dialog message
               alertDialogBuilder
                       .setMessage(
                               message)
                       .setCancelable(false)
                       .setPositiveButton("Yes",
                               new DialogInterface.OnClickListener() {
                                   public void onClick(DialogInterface dialog,
                                                       int id) {
                                       dialog.dismiss();
                                       Preferences.clearPreference(MainActivity.this);
                                       Intent intent=new Intent(MainActivity.this, LoginActivity.class);
                                       startActivity(intent);
                                       finish();

                                       //((Activity)oContext).finish();
                                   }
                               }).setNegativeButton("No",
                       new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog,
                                               int id) {
                               dialog.cancel();
                               //((Activity)oContext).finish();
                           }
                       });

               // create alert dialog
               AlertDialog alertDialog = alertDialogBuilder.create();

               // show it
               alertDialog.show();
           }

/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Bundle bundle=new Bundle();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_new_charge) {


            fragment = new MainTabFragment();
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "MainScreen");
                ft.addToBackStack("history");
                ft.commit();
            }
            // Handle the camera action
        } else if (id == R.id.nav_dashboard) {
            fragment = new DashboardFragment();
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "MainScreen");
                ft.addToBackStack("history");
                ft.commit();
            }
        }else if (id == R.id.nav_acc_overview) {
            fragment = new ReportFragment();
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "MainScreen");
                ft.addToBackStack("history");
                ft.commit();
            }
        } else if (id == R.id.nav_item_catalog) {
            fragment = new ItemCatalogFragment();
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "MainScreen");
                ft.addToBackStack("history");
                ft.commit();
            }
        } else if (id == R.id.nav_settings) {
            fragment = new SettingsFragment();
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "MainScreen");
                ft.addToBackStack("history");
                ft.commit();
            }

        } else if (id == R.id.nav_transaction_hist) {
            fragment = new AllOrders();
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "Orders");
                ft.addToBackStack("history");
                ft.commit();
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
*/


    private void initPOSLink() {
        POSLinkCreatorWrapper.createSync(getContext(), new AppThreadPool.FinishInMainThreadCallback<PosLink>() {
            @Override
            public void onFinish(PosLink result) {

                poslink = result;
                poslink.SetCommSetting(AppController.setupSetting());
                Log.d("MainActivity", "PosLink : "+poslink);
                Log.d("MainActivity", "PosLink comm: port "+poslink.GetCommSetting().getDestPort());
                Log.d("MainActivity", "PosLink comm timeout: "+poslink.GetCommSetting().getTimeOut());

             //  paymentMethod();
             //  paymentMethod();
             //  paymentMethod();
             //  paymentMethod();

            }
        });
    }



    public void paymentMethod(){


        new AsyncTask<Void, Void, String>(){
            String msg;
            @Override
            protected String doInBackground(Void... voids) {

                PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.TransType = 2;
                paymentRequest.TenderType = 1;
                paymentRequest.ECRRefNum= "TestData";
                paymentRequest.Amount = String.valueOf(100);
                paymentRequest.InvNum = "TEST";
                poslink.PaymentRequest = paymentRequest;




                //Launch it
                ProcessTransResult  result = poslink.ProcessTrans();

                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK)
                {
                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                    if (paymentResponse != null && paymentResponse.ResultCode != null)
                    {

                        // lblMsg.Text =  paymentResponse.ResultTxt;

                        //Log.e("RESULT", paymentResponse.ResultTxt);
                        msg=paymentResponse.ResultTxt;

                    }
                    else
                    {
                        //   Log.e("RESULT ERROR", "Unknown error: paxProcess.PaymentResponse is null.");
                        msg="Unknown error: paxProcess.PaymentResponse is null.";
                        // MessageBox.Show("Unknown error: paxProcess.PaymentResponse is null.");
                    }
                    // RegisterCard();
                }
                else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut)
                {
                    //lblMsg.Text = "Action Timeout.";
                    // Log.e("TimeOut", "Action Timeout.");
                    msg="Action TimeOut";
                }
                else
                {
                    //Log.e("Result MSG", result.Msg);
                    msg=result.Msg;
                    // lblMsg.Text = result.Msg;
                }
                msg=result.Msg+"_"+result.Response+"_"+result.Code;

                return msg;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.e("PAX_RESP", s);
            }
        }.execute();

    }


}
