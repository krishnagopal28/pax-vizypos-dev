package com.vizypay.cashdiscountapp.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pax.poslink.PaymentRequest;
import com.pax.poslink.PaymentResponse;
import com.pax.poslink.PosLink;
import com.pax.poslink.ProcessTransResult;
import com.pax.poslink.peripheries.POSLinkPrinter;
import com.pax.poslink.peripheries.ProcessResult;
import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.activity.LoginActivity;
import com.vizypay.cashdiscountapp.adapter.AdditionalDAdapter;
import com.vizypay.cashdiscountapp.adapter.AdditionalDiscountAdapter;
import com.vizypay.cashdiscountapp.adapter.CartListAdapter;
import com.vizypay.cashdiscountapp.adapter.Cash_CardSummaryAdditionalDiscountAdapter;
import com.vizypay.cashdiscountapp.adapter.ExpProductListAdapter;
import com.vizypay.cashdiscountapp.adapter.ProductListAdapter;
import com.vizypay.cashdiscountapp.adapter.SummaryAdditionalDiscountAdapter;
import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
import com.vizypay.cashdiscountapp.model.CategoryWithProducts;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.AppThreadPool;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.CustomView;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.POSLinkCreatorWrapper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.AlertManager;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainTabFragmentBackup extends Fragment implements POSLinkPrinter.PrintListener, IApiResponse {

    public static double newPercentTotal = 0.0;
    ViewPager viewPager;
    RelativeLayout rlDiscount, rlQuicksale, rlItem, rlCart;
    TextView txtDiscount, txtQuickSale, txtItem, txtCart, txtCartTotalPrice, txtCartTotalEnterPrice;
    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    EditText edtQuickSale;
    List<Products> mProductResponseList;
    List<OrderItems> mCartList;
    ProductListAdapter productListAdapter;
    CartListAdapter cartListAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    View v0, v1, v2, vAD;
    TextView txtMessage;
    static OrderItemRepository orderItemRepository;
    LinearLayout linearCart, linearQuickSale;
    //Button btnOrderNow;
    // double newPercentTotal=0.0;
    LinearLayout fabClearCardList, linear_expandable;
    Button btnCharge, btnAddtocart, btnRefund;
    Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnPlus, btnClear, btnReset;
    protected PosLink poslink;
    int isCash = 0, trans_type = 0;//trans_type=0 for sale, 1 for Refund
    static Button btnOrderNowCash;
    static Button btnOrderNowCard;
    static Button btnOrderNowCard_cash;
    double paid1, change;
    double subtCash, discPerC, discFixedC;
    List<AdditionalDiscount> madiscountResponseList;
    private Map<String, String> cardPaymentResponse = null;
    LinearLayout linearCashCard;
    RelativeLayout txtRelativeCardTotal;
    boolean stateError;
    boolean lastNumeric;
    View view;
    Button btnTestVoid;
    ExpandableListView expandableListView;
    List<CategoryWithProducts> mExProductResponseList;
    private int[] numericButtons = {R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9};
    private ExpProductListAdapter expProductListAdapter;
    EditText edtSearch;
    Button btnSearch;
    LinearLayout linearSearch;


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        // PosLink
        initPOSLink();
        // printTestData();

        try {
            if (Preferences.get(mContext, Preferences.CASD_Discount).equals(null)) {
                Preferences.save(getActivity(), Preferences.CASD_Discount, "3.85");
            }
        } catch (Exception e) {
            Preferences.save(getActivity(), Preferences.CASD_Discount, "3.85");
            e.printStackTrace();
        }
        view = inflater.inflate(R.layout.main_tabs_layout, container, false);
        // Setting ViewPager for each Tabs

        setNumericOnClickListener();
        pb = view.findViewById(R.id.pb);
        edtSearch=view.findViewById(R.id.edtSearch);
        linearSearch=view.findViewById(R.id.linearSearch);
        btnSearch=view.findViewById(R.id.btnSearch);
        linearSearch.setVisibility(View.VISIBLE);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } catch(Exception ignored) {
                }
                callProductsWithCategoryAPI(edtSearch.getText().toString());
            }
        });
        txtMessage = view.findViewById(R.id.txtMessage);
        edtQuickSale = view.findViewById(R.id.edtQuickSale);
        edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
        edtQuickSale.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                edtQuickSale.setCursorVisible(true);
                edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                return false;
            }
        });
        edtQuickSale.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                    }
                }
        );
      /*  if (Build.MODEL.startsWith("A8")){
            edtQuickSale.setEnabled(true);
        }  else {
            edtQuickSale.setEnabled(false);
        }*/
        //paymentMethod();
        btn0 = view.findViewById(R.id.btn0);
        btn1 = view.findViewById(R.id.btn1);
        btn2 = view.findViewById(R.id.btn2);
        btn3 = view.findViewById(R.id.btn3);
        btn4 = view.findViewById(R.id.btn4);
        btn5 = view.findViewById(R.id.btn5);
        btn6 = view.findViewById(R.id.btn6);
        btn7 = view.findViewById(R.id.btn7);
        btn8 = view.findViewById(R.id.btn8);
        btn9 = view.findViewById(R.id.btn9);
        btnPlus = view.findViewById(R.id.charge);
        btnClear = view.findViewById(R.id.btnErase);
        v1 = view.findViewById(R.id.view1);
        v2 = view.findViewById(R.id.view2);
        v0 = view.findViewById(R.id.view0);
        vAD = view.findViewById(R.id.viewAD);
        mContext = getActivity();
        recyclerView = view.findViewById(R.id.recycleView);
        linear_expandable = view.findViewById(R.id.linear_expandable);
        expandableListView = view.findViewById(R.id.expandableListView);
        expandableListView.setDividerHeight(2);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if(edtSearch.getText().toString().equals("")) {
                    if (groupPosition != previousGroup)
                        expandableListView.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            }
        });

        linearCart = view.findViewById(R.id.linearCart);
        linearCashCard = view.findViewById(R.id.linearcashcard);
        linearQuickSale = view.findViewById(R.id.linearQuickSale);
        //btnOrderNow=view.findViewById(R.id.btnOrderNow);
        btnOrderNowCash = view.findViewById(R.id.btnOrderNowCash);
        btnOrderNowCard = view.findViewById(R.id.btnOrderNowCard);
        btnOrderNowCard_cash = view.findViewById(R.id.btnOrderNowCard_cash);
        btnAddtocart = view.findViewById(R.id.btnAddtoCart);
        btnCharge = view.findViewById(R.id.charge);
        btnRefund = view.findViewById(R.id.btnRefund);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        rlDiscount = view.findViewById(R.id.rel_AditionalD);
        rlQuicksale = view.findViewById(R.id.rel_quicksale);
        rlCart = view.findViewById(R.id.rel_cart);
        rlItem = view.findViewById(R.id.rel_items);
        txtCart = view.findViewById(R.id.tv_cart);
        txtCartTotalPrice = view.findViewById(R.id.txtCartTotalPrice);
        txtItem = view.findViewById(R.id.tv_items);
        txtDiscount = view.findViewById(R.id.tv_AD);
        txtQuickSale = view.findViewById(R.id.tv_quicksale);
        btnReset = view.findViewById(R.id.btnReset);
        btnTestVoid = view.findViewById(R.id.btnTestVoid);
        fabClearCardList = view.findViewById(R.id.fabClearCardList);
        txtCartTotalEnterPrice = view.findViewById(R.id.txtCartTotalEnterPrice);
        txtRelativeCardTotal = view.findViewById(R.id.txtRelativeCardTotal);

        //pb.setVisibility(View.VISIBLE);
        mCartList = new ArrayList<>();

        //txtCartTotalEnterPrice.setText("");
        //txtCartTotalPrice.setText( " 0.00");
      /*  edtQuickSale.setVisibility(View.VISIBLE);
        linearQuickSale.setVisibility(View.VISIBLE);
        rlQuicksale.setVisibility(View.VISIBLE);
        txtCartTotalPrice.setVisibility(View.INVISIBLE);*/
        txtCartTotalPrice.setVisibility(View.INVISIBLE);
        edtQuickSale.setVisibility(View.VISIBLE);

        linearCart.setVisibility(View.GONE);
        linearQuickSale.setVisibility(View.VISIBLE);
        txtMessage.setVisibility(View.GONE);

        try {
            orderItemRepository = new OrderItemRepository(getActivity());
            //list=storeRepository.getAll();
            Log.e("LISTING COUNT", orderItemRepository.getAll().size() + "");
            if (orderItemRepository.getAll().size() > 0) {
                linearCashCard.setVisibility(View.VISIBLE);
                mCartList = orderItemRepository.getAll();
                // txtCartTotalPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice() + orderItemRepository.getQuickSaleAmount()));
               /* storeAdapter=new StoreAdapter(SelectShopActivity.this, list);
                recyclerView.setAdapter(storeAdapter);*/
            } else {
                linearCashCard.setVisibility(View.GONE);
                //Toast.makeText(SelectShopActivity.this, "Please add shops", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        v0.setVisibility(View.VISIBLE);
        v1.setVisibility(View.GONE);
        vAD.setVisibility(View.GONE);

        callSettingsGETAPI();

        callAdditionalDiscountAPI();
        callGETTaxAPI();
        btnTestVoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncTask<Void, Void, String>() {
                    String msg;

                    @Override
                    protected String doInBackground(Void... voids) {

                        PaymentRequest paymentRequest = new PaymentRequest();
                        paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                        paymentRequest.TransType = paymentRequest.ParseTransType("VOID");
                        paymentRequest.ECRRefNum = "1";
                        paymentRequest.OrigRefNum = "1";
                        poslink.PaymentRequest = paymentRequest;

                        ProcessTransResult result = poslink.ProcessTrans();
                        if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                            PaymentResponse paymentResponse = poslink.PaymentResponse;
                            Log.e("RESP", paymentResponse.ResultTxt);
                        } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                            //lblMsg.Text = "Action Timeout.";
                            // Log.e("TimeOut", "Action Timeout.");
                            msg = "Action TimeOut";
                        } else {
                            //Log.e("Result MSG", result.Msg);
                            msg = result.Msg;
                            // lblMsg.Text = result.Msg;
                        }
                        try {
                            Log.e("PAX_RESP", msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }
        });

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewCart.setLayoutManager(mLayoutManager);
        cartListAdapter = new CartListAdapter(mCartList, mContext, txtCartTotalPrice);
        recyclerViewCart.setAdapter(cartListAdapter);

        callDiscountAPI();
        callAdditionalDiscountAPI();
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewCart.setLayoutManager(mLayoutManager);
        cartListAdapter = new CartListAdapter(mCartList, mContext, txtCartTotalPrice);
        recyclerViewCart.setAdapter(cartListAdapter);


        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    edtQuickSale.setText(edtQuickSale.getText().toString().substring(0, edtQuickSale.getText().length() - 1));
                   /* int charIndex = 0;
                    String text = txtCartTotalEnterPrice.getText().toString();
                    text = text.substring(0, text.length() - 1);// + text.substring(charIndex + 1);
                    if(text.length() == 0){
                        txtCartTotalEnterPrice.setText("");
                    }else{
                        txtCartTotalEnterPrice.setText(text);
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //txtCartTotalEnterPrice.setText("");

                edtQuickSale.setText("$ 0.00");
            }
        });
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCartTotalPrice.setText(edtQuickSale.getText().toString().replaceAll("[$,]", ""));
                cardPaymentResponse = null;
                //f(orderItemRepository.getAll().size()>0){
                {
                    paymentPopup("", "", 0);
                    //completeCashOrder();
                }
            }
        });

        /** Add Card List Clear Button **/

        btnRefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtQuickSale.getText().toString().equals("$0.00"))
                    paymentPopup("", "0", 3);
                else
                    AlertManager.showAlertDialog(mContext, "No Amount", "Please enter valid amount!! ");

            }
        });
      /*  edtQuickSale.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                return false;
            }
        });*/
        edtQuickSale.setEllipsize(TextUtils.TruncateAt.END);
        edtQuickSale.setMovementMethod(null);
        edtQuickSale.setShowSoftInputOnFocus(false);
        edtQuickSale.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                return false;
            }
        });
        edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
        edtQuickSale.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
            }
        });

        edtQuickSale.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                if (edtQuickSale.getText().toString().equals("$0.00")) {
                    edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                }
            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    edtQuickSale.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));

                    current = formatted;
                    edtQuickSale.setText(formatted);
                    edtQuickSale.setSelection(formatted.length());

                    edtQuickSale.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        fabClearCardList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                    orderItemRepository.deleteAll();
                    Preferences.save(mContext, Preferences.QSaleID, String.valueOf(00));

//                    rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
//                    rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
//                    rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
//                    // callProductsAPI();
//                    //txtCartTotalEnterPrice.setText("");
//                    txtCartTotalPrice.setText(" 0.00");
//                    vAD.setVisibility(View.GONE);
//                    v2.setVisibility(View.GONE);
//                    v1.setVisibility(View.GONE);
//                    v0.setVisibility(View.VISIBLE);
//                    btnCharge.setVisibility(View.GONE);
//                    btnReset.setVisibility(View.VISIBLE);
//                    // btnOrderNow.setVisibility(View.GONE);
//                    linearCashCard.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.GONE);
//                    linearCart.setVisibility(View.GONE);
//                    linearQuickSale.setVisibility(View.VISIBLE);
//                    txtMessage.setVisibility(View.GONE);
//                    txtCartTotalPrice.setVisibility(View.GONE);
//                    txtQuickSale.setVisibility(View.VISIBLE);
//                    txtRelativeCardTotal.setVisibility(View.GONE);
                    rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                    rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                    rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                    // callProductsAPI();
                    //txtCartTotalEnterPrice.setText("");
                    txtCartTotalPrice.setText(" 0.00");
                    vAD.setVisibility(View.GONE);
                    v2.setVisibility(View.GONE);
                    v1.setVisibility(View.GONE);
                    v0.setVisibility(View.VISIBLE);
                    btnCharge.setVisibility(View.GONE);
                    btnReset.setVisibility(View.VISIBLE);
                    // btnOrderNow.setVisibility(View.GONE);
                    linearCashCard.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    linearCart.setVisibility(View.GONE);
                    linearQuickSale.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    txtCartTotalPrice.setVisibility(View.INVISIBLE);
                    edtQuickSale.setVisibility(View.VISIBLE);
                    edtQuickSale.setText("0.00");
                    txtQuickSale.setVisibility(View.VISIBLE);
                    txtRelativeCardTotal.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    recyclerViewCart.setVisibility(View.GONE);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        btnAddtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCartTotalPrice.setText(edtQuickSale.getText().toString().replaceAll("[$,]", ""));
                if (Double.valueOf(txtCartTotalPrice.getText().toString()) > 0.00)
                    try {
                 /*   try {
                        orderItemRepository = new OrderItemRepository(mContext);

                        orderItemRepository.updateQuickSale(Double.parseDouble(txtCartTotalPrice.getText().toString()));
                        // notifyItemChanged(position);

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }*/

                        try {
                            orderItemRepository = new OrderItemRepository(mContext);

                     /*   if (orderItemRepository.containsId("0") > 0) {
                            orderItemRepository.updateQuickSalePrice(Utility.decimal2Values(orderItemRepository.getPriceById("0") + Double.parseDouble(txtCartTotalPrice.getText().toString())));
                            //orderItemRepository.updateQuantityById(mList.get(position).getId(), (orderItemRepository.getQuantityById(mList.get(position).getId())+1), (orderItemRepository.getQuantityById(mList.get(position).getId())+1)*Double.valueOf(mList.get(position).getPrice()));
                        } else*/

                            List<Integer> ids = orderItemRepository.getAllId();

                            {
                                OrderItems orderItems = new OrderItems();
                                orderItems.setProduct_id("Q_" + Preferences.get(mContext, Preferences.QSaleID));
                                orderItems.setItem_price(txtCartTotalPrice.getText().toString());
                                orderItems.setItem_cost(txtCartTotalPrice.getText().toString());
                                orderItems.setItem_profit(txtCartTotalPrice.getText().toString());
                                orderItems.setItem_total_cost(txtCartTotalPrice.getText().toString());
                                orderItems.setItem_total_profit(txtCartTotalPrice.getText().toString());
                                //orderItems.setItem_price(txtCartTotalEnterPrice.getText().toString());
                                orderItems.setProduct_name("Quick Sale");
                                orderItems.setTax(0);
                                orderItems.setIsDPercent("false");
                                orderItems.setIsDiscount("false");
                                orderItems.setQuantity("1");

                                orderItems.setTotal_amount(txtCartTotalPrice.getText().toString());
                                //orderItems.setTotal_amount(txtCartTotalEnterPrice.getText().toString());
                                orderItemRepository.create(orderItems);
                                Preferences.save(mContext, Preferences.QSaleID, String.valueOf(Integer.parseInt(Preferences.get(mContext, Preferences.QSaleID)) + 1));
                            }
                            //txtCartTotal.setText(context.getResources().getString(R.string.currency)+ Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                            //holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));

                            //holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(mList.get(position).getId())));

                            // notifyItemChanged(position);

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        txtCartTotalPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                        // txtCartTotalEnterPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                    } catch (Exception throwables) {
                        throwables.printStackTrace();
                    }
                rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                edtQuickSale.setText("0.00");
                //v1.setVisibility(View.VISIBLE);
                //linearQuickSale.setVisibility(View.GONE);
                txtMessage.setVisibility(View.GONE);
                //txtRelativeCardTotal.setVisibility(View.GONE);
                //v2.setVisibility(View.GONE);
                //vAD.setVisibility(View.GONE);
                //v0.setVisibility(View.GONE);
                //linearCart.setVisibility(View.GONE);
                //recyclerView.setVisibility(View.GONE);
                //expandableListView.setVisibility(View.VISIBLE);
                //edtQuickSale.setVisibility(View.INVISIBLE);
                //txtCartTotalPrice.setVisibility(View.VISIBLE);
                //callProductsAPI();
                //callProductsWithCategoryAPI("");
            }
        });
        rlDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txtCartTotalPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice() + orderItemRepository.getQuickSaleAmount()));
                } catch (Exception throwables) {
                    throwables.printStackTrace();
                }
                rlDiscount.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                vAD.setVisibility(View.VISIBLE);
                v1.setVisibility(View.GONE);
                linearQuickSale.setVisibility(View.GONE);
                txtMessage.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);
                v0.setVisibility(View.GONE);
                linearCart.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                linear_expandable.setVisibility(View.GONE);
                // expandableListView.setVisibility(View.GONE);
                txtRelativeCardTotal.setVisibility(View.GONE);
                edtQuickSale.setVisibility(View.INVISIBLE);
                txtCartTotalPrice.setVisibility(View.VISIBLE);
                callAdditionalDiscountAPI();
            }
        });

        rlItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtCartTotalPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice() + orderItemRepository.getQuickSaleAmount()));
                } catch (Exception throwables) {
                    throwables.printStackTrace();
                }
                rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                v1.setVisibility(View.VISIBLE);
                linearQuickSale.setVisibility(View.GONE);
                txtMessage.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);
                vAD.setVisibility(View.GONE);
                v0.setVisibility(View.GONE);
                linearCart.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                linear_expandable.setVisibility(View.VISIBLE);
                //  v.setVisibility(View.VISIBLE);
                txtRelativeCardTotal.setVisibility(View.GONE);
                edtQuickSale.setVisibility(View.INVISIBLE);
                txtCartTotalPrice.setVisibility(View.VISIBLE);
                //callProductsAPI();
                callProductsWithCategoryAPI("");
            }
        });

        rlCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtCartTotalPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice() + orderItemRepository.getQuickSaleAmount()));
                rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                // callProductsAPI();
                v0.setVisibility(View.GONE);
                vAD.setVisibility(View.GONE);
                v1.setVisibility(View.GONE);
                v2.setVisibility(View.VISIBLE);
                txtMessage.setVisibility(View.GONE);
                //btnOrderNow.setVisibility(View.VISIBLE);
                linearCashCard.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                recyclerViewCart.setVisibility(View.VISIBLE);
                //expandableListView.setVisibility(View.GONE);
                linear_expandable.setVisibility(View.GONE);
                linearCart.setVisibility(View.VISIBLE);
                linearQuickSale.setVisibility(View.GONE);
                txtRelativeCardTotal.setVisibility(View.GONE);

                edtQuickSale.setVisibility(View.INVISIBLE);
                txtCartTotalPrice.setVisibility(View.VISIBLE);
                mCartList = new ArrayList<>();
                mCartList = orderItemRepository.getAll();
                mLayoutManager = new GridLayoutManager(getActivity(), 1);
                recyclerViewCart.setLayoutManager(mLayoutManager);
                cartListAdapter = new CartListAdapter(mCartList, mContext, txtCartTotalPrice);
                //cartListAdapter.notifyDataSetChanged();
                btnTestVoid.setVisibility(View.GONE);
                recyclerViewCart.setAdapter(cartListAdapter);
                if (mCartList.size() == 0) {
                    fabClearCardList.setVisibility(View.GONE);
                } else {
                    fabClearCardList.setVisibility(View.VISIBLE);
                }


                calculateCashOrder();
                buttonCalculation(mContext, txtCartTotalPrice);

                // btnOrderNowCard.setText("Credit \n" + txtCartTotalPrice.getText().toString());
                // txtMessage.setText("Coming Soon");
                // txtMessage.setVisibility(View.VISIBLE);
            }
        });
        rlQuicksale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlItem.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlCart.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                rlQuicksale.setBackgroundColor(mContext.getResources().getColor(R.color.tab_0));
                // callProductsAPI();
                txtCartTotalPrice.setText(" 0.00");
                //txtCartTotalEnterPrice.setText("");

               /* try {
                    txtCartTotalPrice.setText(getActivity().getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice() + orderItemRepository.getQuickSaleAmount()));
                } catch (Exception throwables) {
                    throwables.printStackTrace();
                }*/
                txtMessage.setVisibility(View.GONE);
                vAD.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);
                v1.setVisibility(View.GONE);
                v0.setVisibility(View.VISIBLE);
                btnCharge.setVisibility(View.VISIBLE);
                btnReset.setVisibility(View.VISIBLE);
                // btnOrderNow.setVisibility(View.GONE);
                linearCashCard.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                linear_expandable.setVisibility(View.GONE);
                //expandableListView.setVisibility(View.GONE);
                linearCart.setVisibility(View.GONE);
                linearQuickSale.setVisibility(View.VISIBLE);
                txtRelativeCardTotal.setVisibility(View.GONE);
                edtQuickSale.setVisibility(View.VISIBLE);
                if (edtQuickSale.getText().toString().equals("$0.00")) {
                    edtQuickSale.setSelection(edtQuickSale.getText().toString().length());
                }
                txtCartTotalPrice.setVisibility(View.INVISIBLE);
            }
        });

        btnCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edtQuickSale.getText().toString().equals("$0.00")){
                    if (!Preferences.get(mContext, Preferences.DEVICE_ID).isEmpty()) {

                        txtCartTotalPrice.setText(edtQuickSale.getText().toString().replaceAll("[$,]", ""));
                        cardPaymentResponse = null;
                        //f(orderItemRepository.getAll().size()>0){
                        {
                            paymentPopup("", "", 0);
                            //completeCashOrder();
                        }
                    } else {
                        Toast.makeText(mContext, "Please link device to complete transaction", Toast.LENGTH_SHORT).show();
                    }
                }else {

                    Utility.openCustomDialog(mContext, "Please enter amount greater than $0.00");
                }
                // completeOrder();
                //openOrderDetailPopUp();
            }
        });

        buttonCalculation(mContext, txtCartTotalPrice);

        btnOrderNowCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Preferences.get(mContext, Preferences.DEVICE_ID).isEmpty()) {
                    callGETTaxAPI();
                    cardPaymentResponse = null;
                    try {
                        double orderTotal = (orderItemRepository.getSubTotal());
                        double subAmount = ((orderTotal));
                        double Dper = orderItemRepository.getAllDiscountPercent();
                        discFixedC = orderItemRepository.getAllDiscountFixed();
                        discPerC = (((subAmount + discFixedC) * Dper) / 100);
                        subAmount = subAmount + discFixedC + discPerC;

                        double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04

                        Double creditFees = (subAmount * creditfee_value) - subAmount;

                        double totalAmount = subAmount;//Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
                        double tax;
                        if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                            tax = (orderItemRepository.getSubTotal() + discFixedC + discPerC) * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            totalAmount = orderItemRepository.getSubTotal() + discFixedC + discPerC + tax;
                        } else {
                            tax = 0.00;
                            totalAmount = orderItemRepository.getSubTotal() + discFixedC + discPerC;
                        }
                        if (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) > 0.00)
                            if (orderItemRepository.getAll().size() > 0) {
                           /* if(madiscountResponseList.size()>0) {
                                discountPOPUP(1);
                            }else*/
                                {
                                    isCash = 0;
                                    openSummaryPopup("Card", "Card Order", totalAmount, orderItemRepository.getSubTotal(), tax, 0.00, orderItemRepository.getSubTotal() + discFixedC);
                                    //paymentMethod(1,"");
                                    //paymentPopup("", 1);
                                }
                                //completeCashOrder();
                            }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, "Please link device to complete transaction", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnOrderNowCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Preferences.get(mContext, Preferences.DEVICE_ID).isEmpty()) {
                    callGETTaxAPI();
                    cardPaymentResponse = null;
                    try {
                        double cashD = Double.valueOf(Utility.decimal2Values((Double.parseDouble(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (orderItemRepository.getSubTotal()) / (100)));
                        double orderTotal = (orderItemRepository.getSubTotal());
                        double subAmount = ((orderTotal));
                        double Dper = orderItemRepository.getAllDiscountPercent();
                        discFixedC = orderItemRepository.getAllDiscountFixed();
                        discPerC = (((subAmount - cashD + discFixedC) * Dper) / 100);
                        double totalAmount = Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
                        totalAmount = subAmount - cashD + discFixedC + discPerC;

                        double tax;
                        if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                            // if (orderItems.get(0).getProduct_id() == "0") {
                            tax = totalAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            totalAmount = totalAmount + tax;
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                        } else {
                            tax = 0.00;
                        }
                        // map.put("order_total", Utility.decimal2Values(subtCash));

                        if (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) > 0.00)
                            if (orderItemRepository.getAll().size() > 0) {
                           /* if(madiscountResponseList.size()>0) {
                                discountPOPUP(1);
                            }else*/
                                {
                                    isCash = 1;
                                    openSummaryPopup("Cash", "Cash Order", totalAmount, orderItemRepository.getSubTotal(), tax, cashD, (orderItemRepository.getSubTotal() - cashD + discFixedC));
                                    //completeCashOrder("",1);
                                    // paymentPopup("", 1);
                                }
                                //completeCashOrder();
                            }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, "Please link device to complete transaction", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnOrderNowCard_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Preferences.get(mContext, Preferences.DEVICE_ID).isEmpty()) {
                    callGETTaxAPI();
                    cardPaymentResponse = null;
                    try {
                        double orderTotal = (orderItemRepository.getSubTotal());
                        double subAmount = ((orderTotal));
                        double Dper = orderItemRepository.getAllDiscountPercent();
                        discFixedC = orderItemRepository.getAllDiscountFixed();
                        discPerC = (((subAmount + discFixedC) * Dper) / 100);
                        subAmount = subAmount + discFixedC + discPerC;


                        double totalAmount = subAmount;//Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
                        double tax;
                        if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                            tax = (orderItemRepository.getSubTotal() + discFixedC + discPerC) * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            totalAmount = orderItemRepository.getSubTotal() + discFixedC + discPerC + tax;
                        } else {
                            tax = 0.00;
                            totalAmount = orderItemRepository.getSubTotal() + discFixedC + discPerC;
                        }
                        if (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) > 0.00)
                            if (orderItemRepository.getAll().size() > 0) {
                           /* if(madiscountResponseList.size()>0) {
                                discountPOPUP(1);
                            }else*/
                                {
                                    isCash = 0;
                                    openSummaryPopupCashCredit("Cash/Credit Order", totalAmount, orderItemRepository.getSubTotal(), tax, 0.00, orderItemRepository.getSubTotal() + discFixedC);
                                    //paymentMethod(1,"");
                                    //paymentPopup("", 1);
                                }
                                //completeCashOrder();
                            }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, "Please link device to complete transaction", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    public void callProductsWithCategoryAPI(String search) {
        pb.setVisibility(View.VISIBLE);
        if (search!=""){
            Preferences.save(mContext,"search", search );
        }
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsWithCategoryAPI + "?limit=200&search="+search,
                "Cproducts", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    private void printTestData() {
        POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_print_data, null);


      /*  PrintController printController = new PrintController(rootView);
        printController.setCutMode(cutMode);
        printController.setPrintType(printType);
        printController.print(BitmapFactory.decodeResource(rootView.getResources(), R.drawable.img_sign), new OtherInfoEntity("1",
                "D00201", "039761", "0019", "1220"), orderSelected, total, new Runnable() {
            @Override
            public void run() {
            }
        });*/

        POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
        posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
        posLinkPrinter.print(BitmapFactory.decodeResource(rootView.getResources(), R.drawable.cart), new POSLinkPrinter.PrintListener() {
            @Override
            public void onSuccess() {
                // dismissDialog(processingDialog, printFinish);
                Log.e("SuccessPrint", "Done");
            }

            @Override
            public void onError(ProcessResult processResult) {
                // dismissDialog(processingDialog, printFinish);
                Log.e("FailedPrint", processResult.getMessage());
            }
        });
    }

    ;


    private void printReceiptSplitTender(JSONObject orderResponse, String copyName) {

        try {

            POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();


            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_NAME).toUpperCase());
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS1));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS2));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_CONTACT));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();


            printDataFormatter.addContent("Transaction");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("#" + orderResponse.getString("id"));
            printDataFormatter.addLineSeparator();


            System.out.println("Payment Details : " + orderResponse.getString("payment_details"));
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(orderResponse.getString("created_at").split(" ")[0]);
            printDataFormatter.addRightAlign();
            printDataFormatter.addSmallFont();
            try {
                //  printDataFormatter.addContent(orderResponse.getString("created_at").split(" ")[1]);
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyy-MM-dd h:mm:ss");
                Date time = localDateFormat.parse(orderResponse.getString("created_at"));
                //holder.txtTime.setText("Time: "+String.valueOf(new SimpleDateFormat("h:mm aa").format(time)));
                printDataFormatter.addContent("" + (new SimpleDateFormat("h:mm aa").format(time)));
            }catch (Exception e){
                e.printStackTrace();
            }
            printDataFormatter.addLineSeparator();

            JSONObject paymentObj = new JSONObject(orderResponse.getString("payment_details").replaceAll("\\\\", ""));
            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                try {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(("Result"));
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent(("Approved"));
                    printDataFormatter.addLineSeparator();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (orderResponse.getString("order_pay_type").equals("Cash_Card")) {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("Transaction Method: ");
                    //printDataFormatter.addBigFont();
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent("Cash/Card");
                    printDataFormatter.addLineSeparator();
                } else {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("Transaction Method:");
                    //printDataFormatter.addBigFont();
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent(paymentObj.getString("payment_type"));
                    printDataFormatter.addLineSeparator();
                }
                //printDataFormatter.addBigFont();


                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                if (orderResponse.getString("order_status").equals("Refund"))
                    printDataFormatter.addContent("Refund");
                else
                    printDataFormatter.addContent("Sale");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card No : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("account_num"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Type : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("card_type"));
                printDataFormatter.addLineSeparator();

            } else {
                if (orderResponse.getString("order_pay_type").equals("Cash_Card")) {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("Transaction Method: ");
                    //printDataFormatter.addBigFont();
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent("Cash/Card");
                    printDataFormatter.addLineSeparator();
                } else {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("Transaction Method:");
                    //printDataFormatter.addBigFont();
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent(paymentObj.getString("payment_type"));
                    printDataFormatter.addLineSeparator();
                }

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                if (orderResponse.getString("order_status").equals("Refund"))
                    printDataFormatter.addContent("Refund");
                else {
                    printDataFormatter.addContent("Sale");
                }
                printDataFormatter.addLineSeparator();
            }
            printDataFormatter.addLineSeparator();


            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent("Items:");
            printDataFormatter.addLineSeparator();
            try {
                JSONArray itemArr = orderResponse.getJSONArray("order_items");
                for (int i = 0; i < itemArr.length(); i++) {
                    JSONObject itemObj = itemArr.getJSONObject(i);
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(itemObj.getInt("quantity") + " X " + itemObj.getString("product_name"));
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("$" + itemObj.getString("total_amount"));
                    printDataFormatter.addLineSeparator();

                }
            } catch (Exception e) {
                e.printStackTrace();
                printDataFormatter.addNormalFont();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Quick Sale (Charge)");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("$" + Utility.decimal2Values(Double.valueOf(orderResponse.getString("order_total"))));
                printDataFormatter.addLineSeparator();
            }
            printDataFormatter.addHeader();
            printDataFormatter.addLineSeparator();

            printDataFormatter.addContent("Subtotal : ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values((Double.valueOf(orderResponse.getString("total_amount")) + Double.valueOf(orderResponse.getString("cash_discount")) - Double.valueOf(orderResponse.getString("tax")))) + " ");
            printDataFormatter.addLineSeparator();


//            if (!paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
//                printDataFormatter.addContent("Cash Discount : ");
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("($" + Utility.decimal2Values(Double.valueOf(orderResponse.getString("cash_discount"))) + ")");
//                printDataFormatter.addLineSeparator();
//            }
          /*  printDataFormatter.addContent("Credit fee : ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values(Double.valueOf(orderResponse.getString("credit_fees"))) + " ");
            printDataFormatter.addLineSeparator();*/
            try {
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Paid : ");
                // printDataFormatter.addSmallFont();
                printDataFormatter.addRightAlign();
                // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
                printDataFormatter.addContent("$" + orderResponse.getString("card_amount") + " ");
                printDataFormatter.addLineSeparator();
            } catch (Exception e) {
                e.printStackTrace();
            }

            printDataFormatter.addContent("Cash Discount : ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("cash_discount"))) + " ");
            printDataFormatter.addLineSeparator();

            printDataFormatter.addContent("Cash Paid :");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("cash_amount"))) + " ");
            printDataFormatter.addLineSeparator();

            // if (orderResponse.getString("order_pay_type").equalsIgnoreCase("Cash_Card")) {

            //  }

        /*    printDataFormatter.addContent("Credit Paid :");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values(Double.valueOf(orderResponse.getString("card_amount"))) + " ");
            printDataFormatter.addLineSeparator();*/
            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                printDataFormatter.addContent("Tax (" + Preferences.get(mContext, Preferences.TAX) + "%) :");
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("tax"))) + " ");
                printDataFormatter.addLineSeparator();
            }

            printDataFormatter.addHeader();

            printDataFormatter.addBigFont();
            printDataFormatter.addContent("Order Total : ");
            printDataFormatter.addBigFont();
            printDataFormatter.addRightAlign();
            // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
            printDataFormatter.addContent("$" + orderResponse.getString("total_amount") + " ");
            printDataFormatter.addLineSeparator();
//            if(Preferences.get(mContext, Preferences.ADD_TIP).equals("1")){
//                printDataFormatter.addNormalFont();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addContent("Add a Tip: ");
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLeftAlign();
//
//                double totalAmount=Double.valueOf(orderResponse.getString("total_amount"));
//                double fifteenP=Double.valueOf(Utility.decimal2Values((totalAmount*15)/100));
//                double eighteenP=Double.valueOf(Utility.decimal2Values((totalAmount*18)/100));
//                double twentyP=Double.valueOf(Utility.decimal2Values((totalAmount*20)/100));
//                double twentyfiveP=Double.valueOf(Utility.decimal2Values((totalAmount*25)/100));
//                //double newTotal=totalAmount+
//                printDataFormatter.addLineSeparator();
//                // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
//                printDataFormatter.addContent("[  ] 15%: (Tip $"+Utility.decimal2Values_new(fifteenP)+" Total $"+Utility.decimal2Values_new(totalAmount+fifteenP)+")");
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addContent("[  ] 18%: (Tip $"+Utility.decimal2Values_new(eighteenP)+" Total $"+Utility.decimal2Values_new(totalAmount+eighteenP)+")");
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addContent("[  ] 20%: (Tip $"+Utility.decimal2Values_new(twentyP)+" Total $"+Utility.decimal2Values_new(totalAmount+twentyP)+")");
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addContent("[  ] 25%: (Tip $"+Utility.decimal2Values_new(twentyfiveP)+" Total $"+Utility.decimal2Values_new(totalAmount+twentyfiveP)+")");
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addContent("[  ] $ ____________"+" $ ____________");
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addContent("        Custom Tip  "+"    Total      ");
//                printDataFormatter.addLineSeparator();
//
//
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//
//                printDataFormatter.addLineSeparator();
//            }
//
//


            if (copyName.equals("Merchant Copy")) {
                if (Preferences.get(mContext, Preferences.ADD_TIP).equals("1")) {

                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addContent("X");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addTrailer();
                    printDataFormatter.addCenterAlign();
                    // printDataFormatter.addContent("_____________________________________________");

                    printDataFormatter.addContent("Signature");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addLineSeparator();
                } else {
                    try {
                        if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                            printDataFormatter.addLineSeparator();
                            printDataFormatter.addContent("X");
                            printDataFormatter.addLineSeparator();
                            printDataFormatter.addTrailer();
                            printDataFormatter.addCenterAlign();
                            // printDataFormatter.addContent("_____________________________________________");

                            printDataFormatter.addContent("Signature");
                            printDataFormatter.addLineSeparator();
                            printDataFormatter.addLineSeparator();
                        } else if (orderResponse.getString("order_pay_type").equalsIgnoreCase("Cash_Card")) {
                            {
                                printDataFormatter.addLineSeparator();
                                printDataFormatter.addContent("X");
                                printDataFormatter.addLineSeparator();
                                printDataFormatter.addTrailer();
                                printDataFormatter.addCenterAlign();
                                // printDataFormatter.addContent("_____________________________________________");

                                printDataFormatter.addContent("Signature");
                                printDataFormatter.addLineSeparator();
                                printDataFormatter.addLineSeparator();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }


            if (copyName.equals("Customer Copy")) {
                if (Preferences.get(mContext, Preferences.ADD_TIP).equals("0")) {
                    try {
                        OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                        orderItemRepository.deleteAll();
                        ((AppCompatActivity) mContext).recreate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                    orderItemRepository.deleteAll();
                    ((AppCompatActivity) mContext).recreate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                openConfirmPopup(orderResponse);
                //   printDataFormatter.addTrailer();
            }


            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(copyName);
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_FOOTER));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            Log.d("PrintFragment", printDataFormatter.build());
            POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
            posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
            posLinkPrinter.print(printDataFormatter.build(), this);


         /*   if(copyName.equals("Customer Copy"))
                if(Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                    printReceipt(orderResponse, "Merchant Copy");
                }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void buttonCalculation(Context mContext, TextView txtCartTotalPrice) {
        double taxCard, taxCash;
        double subtCash = 0;
        // Float cashD=Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount)))*((orderItemRepository.getSubTotal()))/(100)));
        try {
           /* double orderTotal =(orderItemRepository.getSubTotal());
            double subAmount=((orderTotal-cashD));
            Log.e("SubAmount",subAmount+" "+orderItemRepository.getAllDiscountPercent());
*/
            Float cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(mContext, Preferences.CASD_Discount))) * ((orderItemRepository.getSubTotal())) / (100)));
            double orderTotal = (orderItemRepository.getSubTotal());
            double subAmount = ((orderTotal - cashD));
            double Dper = orderItemRepository.getAllDiscountPercent();
            double discFixedC = orderItemRepository.getAllDiscountFixed();
            double discPerC = (((subAmount + discFixedC) * Dper) / 100);
            subAmount = subAmount + discPerC + orderItemRepository.getAllDiscountFixed();

            // calculateCashOrder();

            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                taxCash = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                taxCard = (Double.parseDouble(txtCartTotalPrice.getText().toString().replace("$", ""))) * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                // totalAmount =orderItemRepository.getSubTotal()+discFixedC+discPerC+ tax;
            } else {
                taxCard = 0.00;
                taxCash = 0.00;
                //totalAmount =orderItemRepository.getSubTotal()+discFixedC+discPerC;
            }

            btnOrderNowCash.setText("Cash\n$" + Utility.decimal2Values(subAmount + taxCash));
            try {
                btnOrderNowCard.setText("Credit\n$" + Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().isEmpty() ? "0.00" : txtCartTotalPrice.getText().toString().replace("$", "")) + taxCard));

                btnOrderNowCard_cash.setText("Cash/Credit\n" + txtCartTotalPrice.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculateCashOrder() {
        try {
            Float cashD = Float.valueOf(Utility.decimal2Values(Double.valueOf(Preferences.get(getActivity(), Preferences.CASD_Discount)) * orderItemRepository.getSubTotal() / 100));
            double orderTotal = (orderItemRepository.getSubTotal());
            double subAmount = ((orderTotal - cashD));
            double Dper = orderItemRepository.getAllDiscountPercent();
            discFixedC = orderItemRepository.getAllDiscountFixed();
            discPerC = (((subAmount + discFixedC) * Dper) / 100);
            subAmount = subAmount + discPerC + discFixedC;
            subtCash = subAmount;
            //  Log.e("SubAmount", subAmount + " " + orderItemRepository.getAllDiscountPercent() + " __ " + subtCash);
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnOrderNowCash.setText("Cash\n$" + Utility.decimal2Values(subtCash));
    }

    private void initPOSLink() {
        POSLinkCreatorWrapper.createSync(getContext(), new AppThreadPool.FinishInMainThreadCallback<PosLink>() {
            @Override
            public void onFinish(PosLink result) {

                poslink = result;
                poslink.SetCommSetting(AppController.setupSetting());
                Log.d("MainActivity", "PosLink : " + poslink);
                Log.d("MainActivity", "PosLink comm: port " + poslink.GetCommSetting().getDestPort());
                Log.d("MainActivity", "PosLink comm timeout: " + poslink.GetCommSetting().getTimeOut());

                //paymentMethod();
            }
        });
    }

    public void receiptPopup(final String ad_id, final int type) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnCard = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCash = 0;
                    dialog.dismiss();
                }
            });
            dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCash = 1;
                    completeCashOrder("order_pay_type", ad_id, type);
                    dialog.dismiss();
                }
            });
            final EditText edtName = dialog.findViewById(R.id.edtName);
            final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);
            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isCash = 0;
                    paymentMethod("", type, ad_id, 0, 0.00, 0.00, 0.00, 0.00,
                            0.00, 0.00, 0.00);
                    //callOrderAPI(map);
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paymentPopup(final String order_pay_type, final String ad_id, final int type) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnCard = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button btnCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
            if (type == 3) {
                btnCardCash.setVisibility(View.GONE);
            } else {
                btnCardCash.setVisibility(View.VISIBLE);
            }
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCash = 0;
                    dialog.dismiss();
                }
            });
            dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCash = 1;
                    if (type == 3) {
                        showAlertDialog(mContext, "Cash Refund", "Are you sure you want to refund amount: $ " + Utility.decimal2Values_new(Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]", "")) )+ "?", "cash");
                      /*  trans_type=1;
                        completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, "Refund");*/
                    } else {
                        trans_type = 0;

                        double totalAmount=Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""));
                      /*  double Dper = orderItemRepository.getAllDiscountPercent();
                        discFixedC = orderItemRepository.getAllDiscountFixed();
                        discPerC = (((subTotal + discFixedC - cashDiscount) * Dper) / 100);
                        double nsubAmount = subTotal + discFixedC + discPerC;
                        txtSubtotalV.setText("$" + Utility.decimal2Values((nsubAmount)));*/

                        double cashD = Double.valueOf(Utility.decimal2Values_new((Double.parseDouble(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (totalAmount) / (100)));
                        double tax;
                        if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                            // if (orderItems.get(0).getProduct_id() == "0") {
                            tax = totalAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            totalAmount = totalAmount + Double.valueOf(Utility.decimal2Values_new(tax));
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                        } else {
                            tax = 0.00;
                        }
                        //completeCashOrder(order_pay_type, ad_id, type);
                        openCashPopup(order_pay_type, Double.valueOf(Utility.decimal2Values_new(totalAmount))-cashD, type);
                    }
                    dialog.dismiss();
                }
            });
            final EditText edtName = dialog.findViewById(R.id.edtName);
            final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);
            btnCardCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type == 3) {
                        double total = Double.valueOf(edtQuickSale.getText().toString().replace("$", ""));
                        openSummaryPopupCashCreditRefund(total);
                        //showAlertDialog(mContext, "Cash Refund", "Are you sure you want to refund amount: $ "+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "cash");
                      /*  trans_type=1;
                        completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, "Refund");*/
                    } else {
                        double total = Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
                        //Double.valueOf(edtQuickSale.getText().toString().replaceAll("[$,]","")
                        openSummaryPopupCashCredit("Cash/Credit Order", total, total, 0.00, 0.00, total + discFixedC);
                    }

                    dialog.dismiss();
                }
            });
            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isCash = 0;
                    if (type == 3) {
                        showAlertDialog(mContext, "Card Refund", "Are you sure you want to refund amount: $" + Utility.decimal2Values_new(Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]", ""))) + "?", "card");
                    } else {
                        trans_type = 0;
                        double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04

                        double subAmount = Double.parseDouble(txtCartTotalPrice.getText().toString().replaceAll("[$,]", ""));
                        Double creditFees = (subAmount * creditfee_value) - subAmount;

                        paymentMethod(order_pay_type, type, ad_id, 0, 0.00, 0.00, 0.00,
                                0.00, 0.00, 0.00, creditFees);
                        //callOrderAPI(map);
                    }
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void discountPOPUP(final int type) {
        try {
            final String ad_id = "";
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_additional_discount, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            GridView gridView = dialog.findViewById(R.id.grid);
            gridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
            final AdditionalDAdapter adapter = new AdditionalDAdapter(madiscountResponseList, getContext());
            gridView.setAdapter(adapter);


            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {

                    int selectedIndex = adapter.selectedPositions.indexOf(position);
                    if (selectedIndex > -1) {
                        ad_id.replace("," + String.valueOf(madiscountResponseList.get(position).getId()), "");
                        adapter.selectedPositions.remove(selectedIndex);
                        adapter.selectedIDs.remove(selectedIndex);
                        ((CustomView) v).display(false);
                        Log.e("Selected", adapter.selectedPositions.toString() + " ::: " + ad_id);
                    } else {
                        ad_id.concat("," + String.valueOf(madiscountResponseList.get(position).getId()));
                        adapter.selectedPositions.add(position);
                        adapter.selectedIDs.add(madiscountResponseList.get(position).getId());
                        // ad_id=TextUtils.join(",", adapter.selectedPositions);
                        ((CustomView) v).display(true);
                        Log.e("Selected", adapter.selectedPositions.toString() + "__" + ad_id + " ::: " + TextUtils.join(",", adapter.selectedIDs));
                    }

                }
            });

          /*  for (int i=0;i<adapter.selectedPositions.size();i++) {
                if (i==0){
                    ad_id.concat( String.valueOf(madiscountResponseList.get(adapter.selectedPositions.get(i)).getId()));
                }else {
                    ad_id.concat("," + String.valueOf(madiscountResponseList.get(adapter.selectedPositions.get(i)).getId()));
                }
            }


*/
            Button btnOK = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button btnCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            // Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //isCash=0;
                    dialog.dismiss();
                }
            });
            // final String finalAd_id = ad_id;
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // isCash=1;

                    Log.e("ADDID", TextUtils.join(",", adapter.selectedPositions));
                    paymentPopup("", TextUtils.join(",", adapter.selectedIDs), type);
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void refund() {
        final double amountSplit = Double.valueOf(edtQuickSale.getText().toString().replaceAll("[$,]", ""));
        final String amount;
        if (amountSplit > 0) {
            amount = String.valueOf(Utility.decimal2Values(amountSplit));
        } else {
            final String amount1 = txtCartTotalPrice.getText().toString().replace("$", "");
            Double amountValue = Double.valueOf(amount1);
            Double tax;
            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                {
                    tax = amountValue * Double.parseDouble(Preferences.get(mContext, Preferences.TAX)) / 100;
                }
            } else {
                tax = 0.00;
            }
            tax=0.00;
            Double finalAmount = amountValue + tax;
            amount = String.valueOf(Utility.decimal2Values(finalAmount));
        }
        Log.e("AMountRefund", amount);

        new AsyncTask<Void, Void, Boolean>() {
            String msg;

            @Override
            protected Boolean doInBackground(Void... voids) {

                PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                paymentRequest.TransType = paymentRequest.ParseTransType("RETURN");
                paymentRequest.ECRRefNum = "1";
                //  paymentRequest.OrigRefNum="123";
                //  double amount =orderItemRepository.getTotalPrice();

                paymentRequest.Amount = amount;
                //paymentRequest.InvNum = "TEST";
                poslink.PaymentRequest = paymentRequest;

                //Launch it
                ProcessTransResult result = poslink.ProcessTrans();
                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                    if (paymentResponse != null && paymentResponse.ResultCode != null) {
                        // lblMsg.Text =  paymentResponse.ResultTxt;

                        //Log.e("RESULT", paymentResponse.ResultTxt);
                        Log.e("PAX_RESP", "Payment auth code ; " + paymentResponse.AuthCode);
                        Log.e("PAX_RESP", "Payment Result text; " + paymentResponse.ResultTxt);
                        Log.e("PAX_RESP", "Payment approved amount ; " + paymentResponse.ApprovedAmount);
                        msg = paymentResponse.ResultTxt;

                        if (!paymentResponse.ApprovedAmount.equals("0") || !paymentResponse.ApprovedAmount.equals("") || !paymentResponse.ApprovedAmount.isEmpty()) {
                            if (!(paymentResponse.ApprovedAmount).equals("")) {
                                if (Double.valueOf(paymentResponse.ApprovedAmount) > 0) {
                                    //paymentResponse.AddlRspData.TransID;
                                    cardPaymentResponse = new HashMap<>();
                                    cardPaymentResponse.put("TransID", paymentResponse.AddlRspData.TransID);
                                    cardPaymentResponse.put("card_type", paymentResponse.CardType);
                                    cardPaymentResponse.put("approved_amount", paymentResponse.ApprovedAmount);
                                    cardPaymentResponse.put("account_num", "****" + paymentResponse.BogusAccountNum);
                                    cardPaymentResponse.put("ref_num", paymentResponse.RefNum);
                                    Log.e("Payment map", cardPaymentResponse.toString());

                                    return true;
                                }
                            }

                        }

                    } else {
                        //   Log.e("RESULT ERROR", "Unknown error: paxProcess.PaymentResponse is null.");
                        msg = "Unknown error: paxProcess.PaymentResponse is null.";
                        // MessageBox.Show("Unknown error: paxProcess.PaymentResponse is null.");
                    }
                    // RegisterCard();
                } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                    //lblMsg.Text = "Action Timeout.";
                    // Log.e("TimeOut", "Action Timeout.");
                    msg = "Action TimeOut";
                } else {
                    //Log.e("Result MSG", result.Msg);
                    msg = result.Msg;
                    // lblMsg.Text = result.Msg;
                }
                Log.e("PAX_RESP", msg);

                return false;
            }

            @Override
            protected void onPostExecute(Boolean resp) {
                super.onPostExecute(resp);
                if (resp) {
                    completeOrder("Credit", 0, "0", 0, amountSplit, 0, 0, 0, amountSplit, 0, "Refund");
                } else {
                    Toast.makeText(getActivity(), " Transaction " + msg, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

    }

    @SuppressLint("StaticFieldLeak")
    public void paymentMethod(final String order_pay_type, final int type, final String adID, final double subtotal, double amountSplit,
                              final double total_amount, final double taxCC, final double discount, final double cash,
                              final double cardPay, final double credit_fees) {
        final String amount;
        if (amountSplit > 0) {
            amount = String.valueOf(Utility.decimal2Values_new(amountSplit));
        } else if (total_amount > 0) {
            amount = String.valueOf(Utility.decimal2Values_new(total_amount));
        } else {
            final String amount1 = txtCartTotalPrice.getText().toString().replace("$", "");
            Double amountValue = Double.valueOf(amount1);
            Double tax;
            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                {
                    tax = amountValue * Double.parseDouble(Preferences.get(mContext, Preferences.TAX)) / 100;
                    if (tax<0.009){
                        tax=0.01;
                    }else {
                        tax=tax;
                    }
                }
            } else {
                tax = 0.00;
            }
            Double finalAmount = amountValue + tax;
            amount = String.valueOf(Utility.decimal2Values_new(finalAmount));
            Log.e("AMT", amount);
        }

        new AsyncTask<Void, Void, Boolean>() {
            String msg;

            @Override
            protected Boolean doInBackground(Void... voids) {

                PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.TransType = paymentRequest.ParseTransType("SALE");
                paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                paymentRequest.ECRRefNum = "1";
                //  double amount =orderItemRepository.getTotalPrice();

                paymentRequest.Amount = amount;
                // paymentRequest.InvNum = "TEST";
                poslink.PaymentRequest = paymentRequest;

                //Launch it
                ProcessTransResult result = poslink.ProcessTrans();
                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                    if (paymentResponse != null && paymentResponse.ResultCode != null) {
                        // lblMsg.Text =  paymentResponse.ResultTxt;

                        //Log.e("RESULT", paymentResponse.ResultTxt);
                        Log.e("PAX_RESP", "Payment auth code ; " + paymentResponse.AuthCode);
                        Log.e("PAX_RESP", "Payment Result text; " + paymentResponse.ResultTxt);
                        Log.e("PAX_RESP", "Payment approved amount ; " + paymentResponse.ApprovedAmount);
                        msg = paymentResponse.ResultTxt;

                        if (!paymentResponse.ApprovedAmount.equals("0") || !paymentResponse.ApprovedAmount.equals("") || !paymentResponse.ApprovedAmount.isEmpty()) {
                            if (!(paymentResponse.ApprovedAmount).equals("")) {
                                if (Double.valueOf(paymentResponse.ApprovedAmount) > 0) {
                                    //paymentResponse.AddlRspData.TransID;
                                    cardPaymentResponse = new HashMap<>();
                                    cardPaymentResponse.put("TransID", paymentResponse.AddlRspData.TransID);
                                    cardPaymentResponse.put("card_type", paymentResponse.CardType);
                                    cardPaymentResponse.put("approved_amount", paymentResponse.ApprovedAmount);
                                    cardPaymentResponse.put("account_num", "****" + paymentResponse.BogusAccountNum);
                                    cardPaymentResponse.put("ref_num", paymentResponse.RefNum);
                                    Log.e("Payment map", cardPaymentResponse.toString());

                                    return true;
                                }
                            }

                        }

                    } else {
                        //   Log.e("RESULT ERROR", "Unknown error: paxProcess.PaymentResponse is null.");
                        msg = "Unknown error: paxProcess.PaymentResponse is null.";
                        // MessageBox.Show("Unknown error: paxProcess.PaymentResponse is null.");
                    }
                    // RegisterCard();
                } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                    //lblMsg.Text = "Action Timeout.";
                    // Log.e("TimeOut", "Action Timeout.");
                    msg = "Action TimeOut";
                } else {
                    //Log.e("Result MSG", result.Msg);
                    msg = result.Msg;
                    // lblMsg.Text = result.Msg;
                }
                Log.e("PAX_RESP", msg);

                return false;
            }

            @Override
            protected void onPostExecute(Boolean resp) {
                super.onPostExecute(resp);
                if (resp) {
                    completeOrder(order_pay_type, type, adID, subtotal, total_amount, taxCC, discount, cash, cardPay, credit_fees, "Completed");
                } else {
                    Toast.makeText(getActivity(), " Transaction " + msg, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

    }


    public void completeOrder(String order_pay_type, int type, String adID, double subtotal, double total_amount,
                              double taxCC, double discount, double cash, double cardPay, double credit_fees, String status) {
        pb.setVisibility(View.VISIBLE);
        btnOrderNowCard.setEnabled(false);
        HashMap<String, Object> map = new HashMap<>();
        map.put("serial_number", Preferences.get(mContext, Preferences.DEVICE_SN));
        map.put("device_id", Preferences.get(mContext, Preferences.DEVICE_ID));

        HashMap<String, Object> mapCard = new HashMap<>();
        mapCard.put("serial_number", Preferences.get(mContext, Preferences.DEVICE_SN));
        mapCard.put("device_id", Preferences.get(mContext, Preferences.DEVICE_ID));
       /* map.put("name", edtName.getText().toString().trim());
        map.put("mobile", edtMobile.getText().toString().trim());
        map.put("email", edtEmail.getText().toString().trim());*/
        //map.put("order_total", Utility.decimal2Values(orderItemRepository.getSubTotal()));
        map.put("order_total", Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")));
        map.put("cash_discount", "0.00");
        map.put("order_status", status);
        map.put("order_pay_type", order_pay_type);
        try {
            map.put("ad_info", (orderItemRepository.getAllDiscountFixed()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            map.put("transaction_no", cardPaymentResponse.get("ref_num"));
            map.put("operation_json", cardPaymentResponse.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("ad_id", TextUtils.join(",", orderItemRepository.getDiscountIds()));
        double orderTotal = Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
        double subAmount = ((orderTotal));

        // created by Krishna Gopal 23 Feb 2021
        switch (type) {
            case 0:
                try {
                    double tax;
                    JSONArray array = new JSONArray();
                    orderItemRepository = new OrderItemRepository(mContext);
                    List<OrderItems> orderItems = orderItemRepository.getAll();
                    Log.e("GETTax", Preferences.get(mContext, Preferences.TAX));
                    if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                        //if (orderItems.get(0).getProduct_id() == "0") {
                        tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                        tax = Double.parseDouble(Utility.decimal2Values(tax));
                        if(tax<0.009){
                            tax=0.01;
                        }
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                    } else {
                        tax = 0.00;
                    }
                    for (int i = 0; i < orderItems.size(); i++) {
                        OrderItems item = orderItems.get(i);

                        JSONObject jsonObjectOrder = new JSONObject();
                        jsonObjectOrder.put("product_id", item.getProduct_id());
                        jsonObjectOrder.put("product_name", item.getProduct_name());
                        jsonObjectOrder.put("quantity", item.getQuantity());
                        jsonObjectOrder.put("item_price", item.getItem_price());
                        jsonObjectOrder.put("item_cost", item.getItem_cost());
                        jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                        jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                        if (item.getProduct_id().contains("-")) {
                            jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                            jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                        }
                        array.put(jsonObjectOrder);
                    }
                    if (credit_fees > 0.00) {
                        map.put("credit_fees", credit_fees);
                    }
                    if (status.equals("Refund")) {
                        map.put("total_amount", total_amount);
                    } else

                        map.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) + tax)));

                    //  map.put("tax", tax);
                /*}else {
                    map.put("tax", 0.00);
                }*/
                    Log.e("TTTax", Utility.decimal2Values(tax<=0.009?0.01:tax));
                    map.put("tax", Utility.decimal2Values(tax!=0.00&&tax<=0.009?0.01:tax));
                    map.put("order_items", array);
                    map.put("ad_id", adID);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Charge", e.getMessage()+"");
                }
                try {
                    JSONObject paymentObject = new JSONObject();
                    paymentObject.put("payment_type", "Card");
                    if (cardPaymentResponse != null) {
                        for (String key : cardPaymentResponse.keySet()) {
                            paymentObject.put(key, cardPaymentResponse.get(key));
                        }
                        map.put("transaction_no", cardPaymentResponse.get("ref_num"));
                    }

                    map.put("payment_details", paymentObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("RAW_JSON", map.toString());
                callOrderAPI(map);
                break;
            case 1:
                try {
                    double tax;
                    JSONArray array = new JSONArray();
                    orderItemRepository = new OrderItemRepository(mContext);
                    List<OrderItems> orderItems = orderItemRepository.getAll();
/*
                if(orderItems.get(0).getProduct_id()==0){
                     tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                }else {
                     tax = subAmount * orderItems.get(0).getTax() / 100;
                }*/
                    if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                        // if (orderItems.get(0).getProduct_id() == "0") {
                        tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                    } else {
                        tax = 0.00;
                    }
                    for (int i = 0; i < orderItems.size(); i++) {
                        OrderItems item = orderItems.get(i);
                        JSONObject jsonObjectOrder = new JSONObject();
                        jsonObjectOrder.put("product_id", item.getProduct_id());
                        jsonObjectOrder.put("product_name", item.getProduct_name());
                        jsonObjectOrder.put("quantity", item.getQuantity());
                        jsonObjectOrder.put("item_price", item.getItem_price());
                        jsonObjectOrder.put("item_cost", item.getItem_cost());
                        jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                        jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                        if (item.getProduct_id().contains("-")) {
                            jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                            jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                        }
                        array.put(jsonObjectOrder);
                    }
                    map.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) + tax)));
                    // if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
                    map.put("tax",Utility.decimal2Values( tax!=0.00&&tax<0.009?0.01:tax));
                /*}else {
                    map.put("tax", 0.00);
                }
*/
                    map.put("credit_fees", credit_fees);
                    map.put("order_items", array);
                    map.put("ad_id", adID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject paymentObject = new JSONObject();
                    paymentObject.put("payment_type", "Card");
                    if (cardPaymentResponse != null) {
                        for (String key : cardPaymentResponse.keySet()) {
                            paymentObject.put(key, cardPaymentResponse.get(key));
                        }
                        map.put("transaction_no", cardPaymentResponse.get("ref_num"));
                    }
                    map.put("payment_details", paymentObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("RAW_JSON", map.toString());
                callOrderAPI(map);
                break;
            case 2:
                try {
                    mapCard.put("order_total", Double.valueOf(subtotal));
                    mapCard.put("cash_discount", discount);
                    mapCard.put("order_status", "Completed");
                    mapCard.put("cash_amount", cash);
                    mapCard.put("card_amount", cardPay);
                    mapCard.put("credit_fees", credit_fees);
                    mapCard.put("order_pay_type", order_pay_type);
                    map.put("transaction_no", cardPaymentResponse.get("ref_num"));
                    mapCard.put("ad_info", (orderItemRepository.getAllDiscountFixed()));
                    // mapCard.put("ad_id", TextUtils.join(",", orderItemRepository.getDiscountIds()));

                    JSONArray array = new JSONArray();
                    orderItemRepository = new OrderItemRepository(mContext);
                    List<OrderItems> orderItems = orderItemRepository.getAll();

                    for (int i = 0; i < orderItems.size(); i++) {
                        OrderItems item = orderItems.get(i);
                        JSONObject jsonObjectOrder = new JSONObject();
                        jsonObjectOrder.put("product_id", item.getProduct_id());
                        jsonObjectOrder.put("product_name", item.getProduct_name());
                        jsonObjectOrder.put("quantity", item.getQuantity());
                        jsonObjectOrder.put("item_price", item.getItem_price());
                        jsonObjectOrder.put("item_cost", item.getItem_cost());
                        jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                        jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                        if (item.getProduct_id().contains("-")) {
                            jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                            jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                        }
                        array.put(jsonObjectOrder);
                    }
                    mapCard.put("total_amount", String.valueOf(Utility.decimal2Values(total_amount)));
                    mapCard.put("tax", Utility.decimal2Values(taxCC<=0.009?0.01:taxCC));
                    mapCard.put("order_items", array);
                    // mapCard.put("ad_id", adID);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject paymentObject = new JSONObject();
                    paymentObject.put("payment_type", "Card");
                    if (cardPaymentResponse != null) {
                        for (String key : cardPaymentResponse.keySet()) {
                            paymentObject.put(key, cardPaymentResponse.get(key));
                        }
                        map.put("transaction_no", cardPaymentResponse.get("ref_num"));
                    }
                    mapCard.put("payment_details", paymentObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("RAW_JSON", mapCard.toString());
                callOrderAPI(mapCard);
                break;
            case 3:
                try {
                    double tax;
                    JSONArray array = new JSONArray();
                    orderItemRepository = new OrderItemRepository(mContext);
                    List<OrderItems> orderItems = orderItemRepository.getAll();
                    Log.e("GETTax", Preferences.get(mContext, Preferences.TAX));
                    if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                        //if (orderItems.get(0).getProduct_id() == "0") {
                        tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                    } else {
                        tax = 0.00;
                    }
                    for (int i = 0; i < orderItems.size(); i++) {
                        OrderItems item = orderItems.get(i);

                        JSONObject jsonObjectOrder = new JSONObject();
                        jsonObjectOrder.put("product_id", item.getProduct_id());
                        jsonObjectOrder.put("product_name", item.getProduct_name());
                        jsonObjectOrder.put("quantity", item.getQuantity());
                        jsonObjectOrder.put("item_price", item.getItem_price());
                        jsonObjectOrder.put("item_cost", item.getItem_cost());
                        jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                        jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                        if (item.getProduct_id().contains("-")) {
                            jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                            jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                        }
                        array.put(jsonObjectOrder);
                    }
                    if (status.equals("Refund")) {
                        map.put("total_amount", total_amount);
                    } else
                        map.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) + tax)));

                    //  map.put("tax", tax);
                /*}else {
                    map.put("tax", 0.00);
                }*/
                    map.put("tax", Utility.decimal2Values(tax!=0.00&&tax<0.009?0.01:tax));
                    map.put("order_items", array);
                    map.put("ad_id", adID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject paymentObject = new JSONObject();
                    paymentObject.put("payment_type", "Cash");
                    if (cardPaymentResponse != null) {
                        for (String key : cardPaymentResponse.keySet()) {
                            paymentObject.put(key, cardPaymentResponse.get(key));
                        }
                        map.put("transaction_no", cardPaymentResponse.get("ref_num"));
                    }

                    map.put("payment_details", paymentObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("RAW_JSON", map.toString());
                callOrderAPI(map);
                break;
            default:
                Toast.makeText(getActivity(), ".", Toast.LENGTH_LONG).show();
        }


//        if (type == 0) {
//            try {
//                double tax;
//                JSONArray array = new JSONArray();
//                orderItemRepository = new OrderItemRepository(mContext);
//                List<OrderItems> orderItems = orderItemRepository.getAll();
//                Log.e("GETTax", Preferences.get(mContext, Preferences.TAX));
//                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
//                    //if (orderItems.get(0).getProduct_id() == "0") {
//                    tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
//                    /*} else {
//                        tax = subAmount * orderItems.get(0).getTax() / 100;
//                    }*/
//                } else {
//                    tax = 0.00;
//                }
//                for (int i = 0; i < orderItems.size(); i++) {
//                    OrderItems item = orderItems.get(i);
//
//                    JSONObject jsonObjectOrder = new JSONObject();
//                    jsonObjectOrder.put("product_id", item.getProduct_id());
//                    jsonObjectOrder.put("product_name", item.getProduct_name());
//                    jsonObjectOrder.put("quantity", item.getQuantity());
//                    jsonObjectOrder.put("item_price", item.getItem_price());
//                    jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
//                    if (item.getProduct_id().contains("-")) {
//                        jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
//                    }
//                    array.put(jsonObjectOrder);
//                }
//                map.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) + tax)));
//
//                //  map.put("tax", tax);
//                /*}else {
//                    map.put("tax", 0.00);
//                }*/
//                map.put("tax", tax);
//                map.put("order_items", array);
//                map.put("ad_id", adID);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else {
//            try {
//                double tax;
//                JSONArray array = new JSONArray();
//                orderItemRepository = new OrderItemRepository(mContext);
//                List<OrderItems> orderItems = orderItemRepository.getAll();
///*
//                if(orderItems.get(0).getProduct_id()==0){
//                     tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
//                }else {
//                     tax = subAmount * orderItems.get(0).getTax() / 100;
//                }*/
//                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
//                    // if (orderItems.get(0).getProduct_id() == "0") {
//                    tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
//                    /*} else {
//                        tax = subAmount * orderItems.get(0).getTax() / 100;
//                    }*/
//                } else {
//                    tax = 0.00;
//                }
//                for (int i = 0; i < orderItems.size(); i++) {
//                    OrderItems item = orderItems.get(i);
//
//                    JSONObject jsonObjectOrder = new JSONObject();
//                    jsonObjectOrder.put("product_id", item.getProduct_id());
//                    jsonObjectOrder.put("product_name", item.getProduct_name());
//                    jsonObjectOrder.put("quantity", item.getQuantity());
//                    jsonObjectOrder.put("item_price", item.getItem_price());
//                    jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
//                    if (item.getProduct_id().contains("-")) {
//                        jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
//                    }
//                    array.put(jsonObjectOrder);
//                }
//                map.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) + tax)));
//                // if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
//                map.put("tax", tax);
//                /*}else {
//                    map.put("tax", 0.00);
//                }
//*/
//                map.put("order_items", array);
//                map.put("ad_id", adID);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        try {
//            JSONObject paymentObject = new JSONObject();
//            paymentObject.put("payment_type", "Card");
//            if (cardPaymentResponse != null) {
//                for (String key : cardPaymentResponse.keySet()) {
//                    paymentObject.put(key, cardPaymentResponse.get(key));
//                }
//            }
//            map.put("payment_details", paymentObject.toString());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.e("RAW_JSON", map.toString());
//        callOrderAPI(map);
    }

    @SuppressLint("NewApi")
    public void openSummaryPopupCashCreditRefund(final double orderTotal) {
        try {
            final double[] credit = new double[1];
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_summary_new_refund, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            final TextView txtOrderType = dialog.findViewById(R.id.txtOrderType);
            final TextView txtSubtotalV = dialog.findViewById(R.id.txtSubtotalV);
            final TextView txtOrderTotal = dialog.findViewById(R.id.txtOrderTotal);
            final TextView txtOrderTotal1 = dialog.findViewById(R.id.txtOrderTotal1);
            txtOrderTotal1.setText(txtCartTotalPrice.getText().toString());
            final EditText edtPaid = dialog.findViewById(R.id.edtPaid);
            final TextView txtcashValue = dialog.findViewById(R.id.txtcashValue);
            final TextView txtCreditfee = dialog.findViewById(R.id.txtCreditfee);
            txtCreditfee.setVisibility(View.GONE);
           /* final TextView txtCashdiscount = dialog.findViewById(R.id.txtCashdiscount);
            txtCashdiscount.setText(View.GONE);*/
            final TextView txtTaxesvalue = dialog.findViewById(R.id.txtTaxesvalue);
            txtTaxesvalue.setVisibility(View.GONE);
            final TextView txtcreditToPay = dialog.findViewById(R.id.txtcreditToPay);

            final TextView txtcashTopay = dialog.findViewById(R.id.txtcashTopay);
            final TextView txtTaxes = dialog.findViewById(R.id.txtTaxes);
            txtTaxes.setVisibility(View.GONE);
            final Button dialogCalculate = dialog.findViewById(R.id.dialogCalculate);
            final RecyclerView recycleViewAdditionalDiscount = dialog.findViewById(R.id.recycleViewAdditionalDiscount);

            final double[] cashdiscount = new double[1];
            final double[] creditTopay = new double[1];
            final double[] odear_total_amount = new double[1];
            final double[] cashAmount = new double[1];
            final double[] credit_fee = new double[1];
            final double[] tax_pay = new double[1];

            final double[] newOrderTotal = new double[1];
            final double[] newCashPaid = new double[1];


//            Double d = new Double(Utility.decimal2Values(Double.valueOf(Preferences.get(mContext,Preferences.TAX))));
//            int int_tax = d.intValue();
            txtTaxes.setText("Taxes (" + Preferences.get(mContext, Preferences.TAX) + "%)");
            txtOrderType.setText("Refund");
            txtSubtotalV.setText("$" + Utility.decimal2Values(orderTotal));
            // txtOrderTotal.setText("$" + Utility.decimal2Values(orderTotal));
//            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
//                txtTaxesvalue.setText("$"+Utility.decimal2Values(tax));
//            }
            //txtdiscount.setText("(-) $" + Utility.decimal2Values(cashDiscount));

           /* final ArrayList<AdditionalDiscount> list = new ArrayList<>();
            List<OrderItems> cartItems = orderItemRepository.getAll();

            for (int i = 0; i < cartItems.size(); i++) {
                if (cartItems.get(i).getIsDiscount().equals("true")) {

                    list.add(new AdditionalDiscount(cartItems.get(i).getProduct_name(), String.valueOf(orderItemRepository.getPriceById(String.valueOf(cartItems.get(i).getProduct_id()))), cartItems.get(i).getIsDPercent().equals("false") ? "Fixed" : "Percent", cartItems.get(i)));
                }
            }

            if (list.size() <= 0) {
                recycleViewAdditionalDiscount.setVisibility(View.GONE);
            } else {
                recycleViewAdditionalDiscount.setVisibility(View.VISIBLE);
            }
            mLayoutManager = new GridLayoutManager(getActivity(), 1);
            recycleViewAdditionalDiscount.setLayoutManager(mLayoutManager);
            final Cash_CardSummaryAdditionalDiscountAdapter[] adapter = new Cash_CardSummaryAdditionalDiscountAdapter[1];
            if (orderType.equals("Cash Order")) {
                adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository, newSubtotal);
            } else {
                adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository, newSubtotal);
            }
            recycleViewAdditionalDiscount.setAdapter(adapter[0]);*/
            recycleViewAdditionalDiscount.setVisibility(View.GONE);
            edtPaid.setShowSoftInputOnFocus(false);
            edtPaid.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    int length = edtPaid.length();
                    if (length > 0) {
                        txtcashValue.setText("$00.00");
                        txtCreditfee.setText("$00.00");
                        //
                        // txtCashdiscount.setText("-$00.00");
                        txtOrderTotal.setText("$00.00");
                        txtcashTopay.setText("$00.00");
                        txtcreditToPay.setText("$00.00");
                    }
                }

                private String current = "";

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals(current)) {
                        edtPaid.removeTextChangedListener(this);

                        String cleanString = s.toString().replaceAll("[$,.]", "");

                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));

                        current = formatted;
                        edtPaid.setText(formatted);
                        edtPaid.setSelection(formatted.length());

                        edtPaid.addTextChangedListener(this);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            edtPaid.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        // do something
                        String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                        if (str_cash != null && !str_cash.isEmpty()) {
                            cashAmount[0] = Double.parseDouble(str_cash);
                            if (cashAmount[0] <= orderTotal) {
                                newPercentTotal = 0.0;
                                edtPaid.setError(null);

                                double credit = (orderTotal - cashAmount[0]);
                                // total Amount credit To Pay
                                txtcashValue.setText("$" + Utility.decimal2Values(cashAmount[0])); //Cash value
                                //txtCreditfee.setText("$" + Utility.decimal2Values(credit_fee[0]));  // print Credit fees
                                //txtCashdiscount.setText("-$" + Utility.decimal2Values(cashdiscount[0])); // print CashDiscount
                                txtcashTopay.setText("$" + Utility.decimal2Values(cashAmount[0])); // print Cash Value
                                txtcreditToPay.setText("$" + Utility.decimal2Values(credit)); // print credit to pay
                                txtOrderTotal1.setText("$" + Utility.decimal2Values(orderTotal));  // print order To Pay Value
                                txtOrderTotal.setText("$" + Utility.decimal2Values(orderTotal));  // print order To Pay Value
                            } else {
                                txtcashValue.setText("$00.00");
                                edtPaid.setError("Please enter amount less than due");
                            }
                        } else {
                            Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                        }
                    } else {

                    }
                    return false;
                }
            });
            dialogCalculate.setVisibility(View.GONE);
            dialogCalculate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                    if (str_cash != null && !str_cash.isEmpty()) {
                        cashAmount[0] = Double.parseDouble(str_cash);
                        if (cashAmount[0] <= orderTotal) {
                            newPercentTotal = 0.0;
                            edtPaid.setError(null);

                            credit[0] = (orderTotal - cashAmount[0]);
                            // total Amount credit To Pay
                            txtcashValue.setText("$" + Utility.decimal2Values(cashAmount[0])); //Cash value
                            //txtCreditfee.setText("$" + Utility.decimal2Values(credit_fee[0]));  // print Credit fees
                            // txtCashdiscount.setText("-$" + Utility.decimal2Values(cashdiscount[0])); // print CashDiscount
                            txtcashTopay.setText("$" + Utility.decimal2Values(cashAmount[0])); // print Cash Value
                            txtcreditToPay.setText("$" + Utility.decimal2Values(credit[0])); // print credit to pay
                            txtOrderTotal1.setText("$" + Utility.decimal2Values(orderTotal));  // print order To Pay Value
                            txtOrderTotal.setText("$" + Utility.decimal2Values(orderTotal));  // print order To Pay Value
                        } else {
                            txtcashValue.setText("$00.00");
                            edtPaid.setError("Please enter amount less than due");
                        }
                    } else {
                        Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                    }
                }
            });


            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonConfirm);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);

            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                    String str_pay = txtcreditToPay.getText().toString();
                    if (str_cash != null && !str_cash.isEmpty() && !str_pay.equals("$00.00")) {
                        if (Double.valueOf(str_pay.replace("$", "")) > 0.00) {
                           /* if (orderType.equals("Cash Order")) {
                                dialog.dismiss();
                            } else */
                            {
                                showAlertDialog(mContext, "Credit Refund", "Are you sure you want to refund amount: $" + Double.parseDouble(str_pay.replaceAll("[$,]", "")) + "?", "card");
//                            completeOrder("Cash_Card" ,2, "",Double.parseDouble(Utility.decimal2Values(subTotal))
//                                    ,Double.parseDouble(Utility.decimal2Values(odear_total1[0]))
//                                    ,Double.parseDouble(Utility.decimal2Values(tax))
//                                    ,Double.parseDouble(Utility.decimal2Values(discount[0]))
//                                    ,Double.parseDouble(Utility.decimal2Values(cash[0]))
//                                            ,Double.parseDouble(Utility.decimal2Values(creditTopay[0]))
//                                            ,Double.parseDouble(Utility.decimal2Values(credit_fee[0])));
                                dialog.dismiss();
                            }
                        } else {
                            Toast.makeText(mContext, "please enter amount less than dUE", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                    }
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void openSummaryPopupCashCredit(final String orderType, final double orderTotal, final double subTotal, final double tax, final double cash_Discount, final double newSubtotal) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_summary_new, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            final TextView txtOrderType = dialog.findViewById(R.id.txtOrderType);
            final TextView txtSubtotalV = dialog.findViewById(R.id.txtSubtotalV);
            final TextView txtOrderTotal = dialog.findViewById(R.id.txtOrderTotal);
            final TextView txtOrderTotal1 = dialog.findViewById(R.id.txtOrderTotal1);
            txtOrderTotal1.setText(txtCartTotalPrice.getText().toString());
            final EditText edtPaid = dialog.findViewById(R.id.edtPaid);
            final TextView txtcashValue = dialog.findViewById(R.id.txtcashValue);
            final TextView txtCreditfee = dialog.findViewById(R.id.txtCreditfee);
            txtCreditfee.setVisibility(View.GONE);
            final TextView txtCashdiscount = dialog.findViewById(R.id.txtCashdiscount);
            final TextView txtTaxesvalue = dialog.findViewById(R.id.txtTaxesvalue);
            final TextView txtcreditToPay = dialog.findViewById(R.id.txtcreditToPay);
            final TextView txtcashTopay = dialog.findViewById(R.id.txtcashTopay);
            final TextView txtTaxes = dialog.findViewById(R.id.txtTaxes);
            final Button dialogCalculate = dialog.findViewById(R.id.dialogCalculate);
            final RecyclerView recycleViewAdditionalDiscount = dialog.findViewById(R.id.recycleViewAdditionalDiscount);

            final double[] cashdiscount = new double[1];
            final double[] creditTopay = new double[1];
            final double[] odear_total_amount = new double[1];
            final double[] cashAmount = new double[1];
            final double[] credit_fee = new double[1];
            final double[] tax_pay = new double[1];

            final double[] newOrderTotal = new double[1];
            final double[] newCashPaid = new double[1];


//            Double d = new Double(Utility.decimal2Values(Double.valueOf(Preferences.get(mContext,Preferences.TAX))));
//            int int_tax = d.intValue();
            txtTaxes.setText("Taxes (" + Preferences.get(mContext, Preferences.TAX) + "%)");
            txtOrderType.setText(orderType);
            txtSubtotalV.setText("$" + Utility.decimal2Values(newSubtotal));
            // txtOrderTotal.setText("$" + Utility.decimal2Values(orderTotal));
//            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
//                txtTaxesvalue.setText("$"+Utility.decimal2Values(tax));
//            }
            //txtdiscount.setText("(-) $" + Utility.decimal2Values(cashDiscount));

            final ArrayList<AdditionalDiscount> list = new ArrayList<>();
            List<OrderItems> cartItems = orderItemRepository.getAll();

            for (int i = 0; i < cartItems.size(); i++) {
                if (cartItems.get(i).getIsDiscount().equals("true")) {

                    list.add(new AdditionalDiscount(cartItems.get(i).getProduct_name(), String.valueOf(orderItemRepository.getPriceById(String.valueOf(cartItems.get(i).getProduct_id()))), cartItems.get(i).getIsDPercent().equals("false") ? "Fixed" : "Percent", cartItems.get(i)));
                }
            }

            if (list.size() <= 0) {
                recycleViewAdditionalDiscount.setVisibility(View.GONE);
            } else {
                recycleViewAdditionalDiscount.setVisibility(View.VISIBLE);
            }
            mLayoutManager = new GridLayoutManager(getActivity(), 1);
            recycleViewAdditionalDiscount.setLayoutManager(mLayoutManager);
            final Cash_CardSummaryAdditionalDiscountAdapter[] adapter = new Cash_CardSummaryAdditionalDiscountAdapter[1];
            if (orderType.equals("Cash Order")) {
                adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository, newSubtotal);
            } else {
                adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository, newSubtotal);
            }
            recycleViewAdditionalDiscount.setAdapter(adapter[0]);
            edtPaid.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    int length = edtPaid.length();
                    if (length > 0) {
                        txtcashValue.setText("$00.00");
                        txtCreditfee.setText("$00.00");
                        txtCashdiscount.setText("-$00.00");
                        txtOrderTotal.setText("$00.00");
                        txtcashTopay.setText("$00.00");
                        txtcreditToPay.setText("$00.00");
                    }
                }

                private String current = "";

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals(current)) {
                        edtPaid.removeTextChangedListener(this);

                        String cleanString = s.toString().replaceAll("[$,.]", "");

                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));

                        current = formatted;
                        edtPaid.setText(formatted);
                        edtPaid.setSelection(formatted.length());

                        edtPaid.addTextChangedListener(this);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                    // do something
                    String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                    if (str_cash != null && !str_cash.isEmpty()) {
                        cashAmount[0] = Double.parseDouble(str_cash);
                        if (cashAmount[0] <= subTotal) {
                            newPercentTotal = 0.0;
                            edtPaid.setError(null);


                            double credit = (subTotal - cashAmount[0]);
                            double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04
                            //credit_fee[0] = (credit * creditfee_value ) - credit; // credit fees calculation  ;
                            // credit fees calculation  ;a
                            double fees_paid = Double.parseDouble(Preferences.get(mContext, Preferences.CASD_Discount));
                            double fees_paid_value = fees_paid / 100;  // feesPaid calculation 0.0385
                            //cashdiscount[0] = (cashAmount[0] * creditfee_value ) * fees_paid_value ;  // Cash Discount
                            cashdiscount[0] = (cashAmount[0] * creditfee_value) * fees_paid_value;
                            credit_fee[0] = ((subTotal - cashdiscount[0]) * creditfee_value) - subTotal;
                            /**  credit Amount  **/
                            Double Dint = credit_fee[0];
                            Double Dcash = cashdiscount[0];
                            int newCreditFees = Integer.valueOf(Dint.intValue());
                            int newcash_d = Integer.valueOf(Dcash.intValue());
                            credit = credit - newcash_d - newCreditFees;
                            /** ---------- **/
                            double newCredit = credit - cashdiscount[0];
                            double cash_With_CreditFees = (cashAmount[0] * creditfee_value);
                            double credit_With_CreditFees = (newCredit * creditfee_value);
                            double cashDiscountProgram = (-cashdiscount[0]);
                            // All Discount get order Fixed and Percent
                            double getallDiscount_Fixed = (-orderItemRepository.getAllDiscountFixed()); // All fixed Discount
                            double getallDiscount_Percent = (-orderItemRepository.getAllDiscountPercent()) / 100; // All fixed Percent

                            // new calculation
                            double newCreditamount = credit * creditfee_value;
                            newCashPaid[0] = ((cashAmount[0] * creditfee_value) - (cashAmount[0] * creditfee_value) * fees_paid_value);
                            double newtotalafter_cashdiscount = newCreditamount + newCashPaid[0];
                            double newApplyfixed_discount = newtotalafter_cashdiscount + -getallDiscount_Fixed;
                            double newDiscountPercent = newApplyfixed_discount * getallDiscount_Percent;
                            double newApplyPercentdiscount = newApplyfixed_discount - newDiscountPercent; // Apply % discount
                            double newtax_value;
                            try {
                                newtax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            } catch (Exception e) {
                                e.printStackTrace();
                                newtax_value = 0.00;
                            }
                            //double newFinalTotalAmountwithTax = (newApplyPercentdiscount * (1 + newtax_value));
                            odear_total_amount[0] = (newApplyPercentdiscount * (1 + newtax_value));
                            /*************************/

                            double newSubtotalPercent = ((((((cashAmount[0] * creditfee_value)) - (cashAmount[0] * creditfee_value) * fees_paid_value) + (newtotalafter_cashdiscount - cashdiscount[0]) * creditfee_value) + -newApplyfixed_discount));

//                            double newSubtotalPercent = (((((cashdiscount[0]* creditfee_value))-(cashdiscount[0]*creditfee_value)*fees_paid_value)+credit*creditfee_value)+ -getallDiscount_Fixed);


                            adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,
                                    newSubtotalPercent);
                            newPercentTotal = newSubtotalPercent * getallDiscount_Percent;
                            //adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,((cash[0]*fees_paid)-cash[0]*fees_paid*cashDiscount*100/100+clu_fees_paid*fees_paid+orderItemRepository.getAllDiscountFixed())*orderItemRepository.getAllDiscountPercent()*100/100);
                            // adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,(cash_With_CreditFees+credit_fee[0]+cashDiscountProgram+credit_With_CreditFees+orderItemRepository.getAllDiscountFixed()));
                            recycleViewAdditionalDiscount.setAdapter(adapter[0]);

                            // tax calculation
                            // double taxcalculationstep1 = (((((cash_With_CreditFees))-(cash_With_CreditFees) * fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed); // tax calculation step 1
                            double taxcalculationstep1 = (newSubtotalPercent + -getallDiscount_Fixed); // tax calculation step 1
                            // double taxcalculationstep2 = ((((((cash_With_CreditFees))-(cash_With_CreditFees)* fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed)* getallDiscount_Percent); // tax calculation step 2
                            double taxcalculationstep2 = (newSubtotalPercent - adapter[0].getAllPercent());
                            /*double newSubT=newSubtotalPercent+adapter[0].getAllPercent();
                            double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            tax_pay[0] = newSubT * tax_value;*/// tax calculation step 2
                            // double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            double tax_value;
                            try {
                                tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            } catch (Exception e) {
                                e.printStackTrace();
                                tax_value = 0.00;
                            }
                            tax_pay[0] = (subTotal - cashdiscount[0] + orderItemRepository.getAllDiscountFixed() - newPercentTotal) * tax_value;
                            // tax_pay[0] = ((((((cashAmount[0]*creditfee_value))-(cashAmount[0]*creditfee_value)*fees_paid_value)+(newtotalafter_cashdiscount-cashdiscount[0])*creditfee_value)+ -newApplyfixed_discount)-adapter[0].getAllPercent()) * tax_value;
                            // tax_pay[0] = (taxcalculationstep1 - taxcalculationstep2) * tax_value;


                            txtSubtotalV.setText("$" + Utility.decimal2Values(subTotal + orderItemRepository.getAllDiscountFixed() - newPercentTotal));
                            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                                txtTaxesvalue.setText("$" + Utility.decimal2Values(tax_pay[0]));
                            } else {
                                txtTaxesvalue.setText("$" + Utility.decimal2Values(tax_pay[0]));
                            }


                            // order Total
//                            double subtotalvalueDiscount = ((cash_With_CreditFees-cashdiscount[0]+credit_With_CreditFees)+ - getallDiscount_Fixed);
//                            subtotalvalueDiscount = subtotalvalueDiscount * getallDiscount_Percent;
//                            odear_total_amount[0] = cashAmount[0]+credit- getallDiscount_Fixed - subtotalvalueDiscount +tax_pay[0]+credit_fee[0]; // final order Total amount

                            newOrderTotal[0] = (subTotal - cashdiscount[0] + orderItemRepository.getAllDiscountFixed() - newPercentTotal) + tax_pay[0];
                            creditTopay[0] = odear_total_amount[0] - newCashPaid[0];  // total Amount credit To Pay
                            txtcashValue.setText("$" + Utility.decimal2Values(cashAmount[0])); //Cash value
                            txtCreditfee.setText("$" + Utility.decimal2Values(credit_fee[0]));  // print Credit fees
                            txtCashdiscount.setText("-$" + Utility.decimal2Values(cashdiscount[0])); // print CashDiscount
                            txtcashTopay.setText("$" + Utility.decimal2Values(cashAmount[0])); // print Cash Value
                            txtcreditToPay.setText("$" + Utility.decimal2Values(newOrderTotal[0] - newCashPaid[0])); // print credit to pay
                            txtOrderTotal1.setText("$" + Utility.decimal2Values(newOrderTotal[0]));  // print order To Pay Value
                            txtOrderTotal.setText("$" + Utility.decimal2Values(newOrderTotal[0]));  // print order To Pay Value
                        } else {
                            txtcashValue.setText("$00.00");
                            edtPaid.setError("Please enter amount less than due");
                        }
                    } else {
                        Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                    }
                }
            });
            edtPaid.setFocusable(true);

            edtPaid.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        // do something
                        String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                        if (str_cash != null && !str_cash.isEmpty()) {
                            cashAmount[0] = Double.parseDouble(str_cash);
                            if (cashAmount[0] <= subTotal) {
                                newPercentTotal = 0.0;
                                edtPaid.setError(null);

                                double credit = (subTotal - cashAmount[0]);
                                double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04
                                //credit_fee[0] = (credit * creditfee_value ) - credit; // credit fees calculation  ;
                                // credit fees calculation  ;a
                                double fees_paid = Double.parseDouble(Preferences.get(mContext, Preferences.CASD_Discount));
                                double fees_paid_value = fees_paid / 100;  // feesPaid calculation 0.0385
                                //cashdiscount[0] = (cashAmount[0] * creditfee_value ) * fees_paid_value ;  // Cash Discount
                                cashdiscount[0] = (cashAmount[0] * creditfee_value) * fees_paid_value;
                                credit_fee[0] = ((subTotal - cashdiscount[0]) * creditfee_value) - subTotal;
                                /**  credit Amount  **/
                                Double Dint = credit_fee[0];
                                Double Dcash = cashdiscount[0];
                                int newCreditFees = Integer.valueOf(Dint.intValue());
                                int newcash_d = Integer.valueOf(Dcash.intValue());
                                credit = credit - newcash_d - newCreditFees;
                                /** ---------- **/
                                double newCredit = credit - cashdiscount[0];
                                double cash_With_CreditFees = (cashAmount[0] * creditfee_value);
                                double credit_With_CreditFees = (newCredit * creditfee_value);
                                double cashDiscountProgram = (-cashdiscount[0]);
                                // All Discount get order Fixed and Percent
                                double getallDiscount_Fixed = (-orderItemRepository.getAllDiscountFixed()); // All fixed Discount
                                double getallDiscount_Percent = (-orderItemRepository.getAllDiscountPercent()) / 100; // All fixed Percent

                                // new calculation
                                double newCreditamount = credit * creditfee_value;
                                newCashPaid[0] = ((cashAmount[0] * creditfee_value) - (cashAmount[0] * creditfee_value) * fees_paid_value);
                                double newtotalafter_cashdiscount = newCreditamount + newCashPaid[0];
                                double newApplyfixed_discount = newtotalafter_cashdiscount + -getallDiscount_Fixed;
                                double newDiscountPercent = newApplyfixed_discount * getallDiscount_Percent;
                                double newApplyPercentdiscount = newApplyfixed_discount - newDiscountPercent; // Apply % discount
                                double newtax_value;
                                try {
                                    newtax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    newtax_value = 0.00;
                                }
                                // double newtax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                                //double newFinalTotalAmountwithTax = (newApplyPercentdiscount * (1 + newtax_value));
                                odear_total_amount[0] = (newApplyPercentdiscount * (1 + newtax_value));
                                /*************************/

                                double newSubtotalPercent = ((((((cashAmount[0] * creditfee_value)) - (cashAmount[0] * creditfee_value) * fees_paid_value) + (newtotalafter_cashdiscount - cashdiscount[0]) * creditfee_value) + -newApplyfixed_discount));

//                            double newSubtotalPercent = (((((cashdiscount[0]* creditfee_value))-(cashdiscount[0]*creditfee_value)*fees_paid_value)+credit*creditfee_value)+ -getallDiscount_Fixed);


                                adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,
                                        newSubtotalPercent);
                                newPercentTotal = newSubtotalPercent * getallDiscount_Percent;
                                //adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,((cash[0]*fees_paid)-cash[0]*fees_paid*cashDiscount*100/100+clu_fees_paid*fees_paid+orderItemRepository.getAllDiscountFixed())*orderItemRepository.getAllDiscountPercent()*100/100);
                                // adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,(cash_With_CreditFees+credit_fee[0]+cashDiscountProgram+credit_With_CreditFees+orderItemRepository.getAllDiscountFixed()));
                                recycleViewAdditionalDiscount.setAdapter(adapter[0]);

                                // tax calculation
                                // double taxcalculationstep1 = (((((cash_With_CreditFees))-(cash_With_CreditFees) * fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed); // tax calculation step 1
                                double taxcalculationstep1 = (newSubtotalPercent + -getallDiscount_Fixed); // tax calculation step 1
                                // double taxcalculationstep2 = ((((((cash_With_CreditFees))-(cash_With_CreditFees)* fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed)* getallDiscount_Percent); // tax calculation step 2
                                double taxcalculationstep2 = (newSubtotalPercent - adapter[0].getAllPercent());
                            /*double newSubT=newSubtotalPercent+adapter[0].getAllPercent();
                            double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            tax_pay[0] = newSubT * tax_value;*/// tax calculation step 2
                                double tax_value;
                                try {
                                    tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    tax_value = 0.00;
                                }
                                //double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                                tax_pay[0] = (subTotal - cashdiscount[0] + orderItemRepository.getAllDiscountFixed() - newPercentTotal) * tax_value;
                                // tax_pay[0] = ((((((cashAmount[0]*creditfee_value))-(cashAmount[0]*creditfee_value)*fees_paid_value)+(newtotalafter_cashdiscount-cashdiscount[0])*creditfee_value)+ -newApplyfixed_discount)-adapter[0].getAllPercent()) * tax_value;
                                // tax_pay[0] = (taxcalculationstep1 - taxcalculationstep2) * tax_value;


                                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                                    txtTaxesvalue.setText("$" + Utility.decimal2Values(tax_pay[0]));
                                } else {
                                    txtTaxesvalue.setText("$" + Utility.decimal2Values(tax_pay[0]));
                                }


                                // order Total
//                            double subtotalvalueDiscount = ((cash_With_CreditFees-cashdiscount[0]+credit_With_CreditFees)+ - getallDiscount_Fixed);
//                            subtotalvalueDiscount = subtotalvalueDiscount * getallDiscount_Percent;
//                            odear_total_amount[0] = cashAmount[0]+credit- getallDiscount_Fixed - subtotalvalueDiscount +tax_pay[0]+credit_fee[0]; // final order Total amount

                                newOrderTotal[0] = (subTotal - cashdiscount[0] + orderItemRepository.getAllDiscountFixed() - newPercentTotal) + tax_pay[0];
                                creditTopay[0] = odear_total_amount[0] - newCashPaid[0];  // total Amount credit To Pay
                                txtcashValue.setText("$" + Utility.decimal2Values(cashAmount[0])); //Cash value
                                txtCreditfee.setText("$" + Utility.decimal2Values(credit_fee[0]));  // print Credit fees
                                txtCashdiscount.setText("-$" + Utility.decimal2Values(cashdiscount[0])); // print CashDiscount
                                txtcashTopay.setText("$" + Utility.decimal2Values(cashAmount[0])); // print Cash Value
                                txtcreditToPay.setText("$" + Utility.decimal2Values(newOrderTotal[0] - newCashPaid[0])); // print credit to pay
                                txtOrderTotal1.setText("$" + Utility.decimal2Values(newOrderTotal[0]));  // print order To Pay Value
                                txtOrderTotal.setText("$" + Utility.decimal2Values(newOrderTotal[0]));  // print order To Pay Value
                            } else {
                                txtcashValue.setText("$00.00");
                                edtPaid.setError("Please enter amount less than due");
                            }
                        } else {
                            Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                        }
                    } else {

                    }
                    return false;
                }
            });
            dialogCalculate.setVisibility(View.GONE);
            dialogCalculate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                    if (str_cash != null && !str_cash.isEmpty()) {
                        cashAmount[0] = Double.parseDouble(str_cash);
                        if (cashAmount[0] <= subTotal) {
                            newPercentTotal = 0.0;
                            edtPaid.setError(null);

                            double credit = (subTotal - cashAmount[0]);
                            double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04
                            //credit_fee[0] = (credit * creditfee_value ) - credit; // credit fees calculation  ;
                            // credit fees calculation  ;a
                            double fees_paid = Double.parseDouble(Preferences.get(mContext, Preferences.CASD_Discount));
                            double fees_paid_value = fees_paid / 100;  // feesPaid calculation 0.0385
                            //cashdiscount[0] = (cashAmount[0] * creditfee_value ) * fees_paid_value ;  // Cash Discount
                            cashdiscount[0] = (cashAmount[0] * creditfee_value) * fees_paid_value;
                            credit_fee[0] = ((subTotal - cashdiscount[0]) * creditfee_value) - subTotal;
                            /**  credit Amount  **/
                            Double Dint = credit_fee[0];
                            Double Dcash = cashdiscount[0];
                            int newCreditFees = Integer.valueOf(Dint.intValue());
                            int newcash_d = Integer.valueOf(Dcash.intValue());
                            credit = credit - newcash_d - newCreditFees;
                            /** ---------- **/
                            double newCredit = credit - cashdiscount[0];
                            double cash_With_CreditFees = (cashAmount[0] * creditfee_value);
                            double credit_With_CreditFees = (newCredit * creditfee_value);
                            double cashDiscountProgram = (-cashdiscount[0]);
                            // All Discount get order Fixed and Percent
                            double getallDiscount_Fixed = (-orderItemRepository.getAllDiscountFixed()); // All fixed Discount
                            double getallDiscount_Percent = (-orderItemRepository.getAllDiscountPercent()) / 100; // All fixed Percent

                            // new calculation
                            double newCreditamount = credit * creditfee_value;
                            newCashPaid[0] = ((cashAmount[0] * creditfee_value) - (cashAmount[0] * creditfee_value) * fees_paid_value);
                            double newtotalafter_cashdiscount = newCreditamount + newCashPaid[0];
                            double newApplyfixed_discount = newtotalafter_cashdiscount + -getallDiscount_Fixed;
                            double newDiscountPercent = newApplyfixed_discount * getallDiscount_Percent;
                            double newApplyPercentdiscount = newApplyfixed_discount - newDiscountPercent; // Apply % discount
                            double newtax_value;
                            try {
                                newtax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            } catch (Exception e) {
                                e.printStackTrace();
                                newtax_value = 0.00;
                            }
                            // double newtax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            //double newFinalTotalAmountwithTax = (newApplyPercentdiscount * (1 + newtax_value));
                            odear_total_amount[0] = (newApplyPercentdiscount * (1 + newtax_value));
                            /*************************/

                            double newSubtotalPercent = ((((((cashAmount[0] * creditfee_value)) - (cashAmount[0] * creditfee_value) * fees_paid_value) + (newtotalafter_cashdiscount - cashdiscount[0]) * creditfee_value) + -newApplyfixed_discount));

//                            double newSubtotalPercent = (((((cashdiscount[0]* creditfee_value))-(cashdiscount[0]*creditfee_value)*fees_paid_value)+credit*creditfee_value)+ -getallDiscount_Fixed);


                            adapter[0] = new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,
                                    newSubtotalPercent);
                            newPercentTotal = newSubtotalPercent * getallDiscount_Percent;
                            //adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,((cash[0]*fees_paid)-cash[0]*fees_paid*cashDiscount*100/100+clu_fees_paid*fees_paid+orderItemRepository.getAllDiscountFixed())*orderItemRepository.getAllDiscountPercent()*100/100);
                            // adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,(cash_With_CreditFees+credit_fee[0]+cashDiscountProgram+credit_With_CreditFees+orderItemRepository.getAllDiscountFixed()));
                            recycleViewAdditionalDiscount.setAdapter(adapter[0]);

                            // tax calculation
                            // double taxcalculationstep1 = (((((cash_With_CreditFees))-(cash_With_CreditFees) * fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed); // tax calculation step 1
                            double taxcalculationstep1 = (newSubtotalPercent + -getallDiscount_Fixed); // tax calculation step 1
                            // double taxcalculationstep2 = ((((((cash_With_CreditFees))-(cash_With_CreditFees)* fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed)* getallDiscount_Percent); // tax calculation step 2
                            double taxcalculationstep2 = (newSubtotalPercent - adapter[0].getAllPercent());
                            /*double newSubT=newSubtotalPercent+adapter[0].getAllPercent();
                            double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            tax_pay[0] = newSubT * tax_value;*/// tax calculation step 2
                            double tax_value;
                            try {
                                tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            } catch (Exception e) {
                                e.printStackTrace();
                                tax_value = 0.00;
                            }
                            // double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                            tax_pay[0] = (subTotal - cashdiscount[0] + orderItemRepository.getAllDiscountFixed() - newPercentTotal) * tax_value;
                            // tax_pay[0] = ((((((cashAmount[0]*creditfee_value))-(cashAmount[0]*creditfee_value)*fees_paid_value)+(newtotalafter_cashdiscount-cashdiscount[0])*creditfee_value)+ -newApplyfixed_discount)-adapter[0].getAllPercent()) * tax_value;
                            // tax_pay[0] = (taxcalculationstep1 - taxcalculationstep2) * tax_value;


                            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                                txtTaxesvalue.setText("$" + Utility.decimal2Values(tax_pay[0]));
                            } else {
                                txtTaxesvalue.setText("$" + Utility.decimal2Values(tax_pay[0]));
                            }


                            // order Total
//                            double subtotalvalueDiscount = ((cash_With_CreditFees-cashdiscount[0]+credit_With_CreditFees)+ - getallDiscount_Fixed);
//                            subtotalvalueDiscount = subtotalvalueDiscount * getallDiscount_Percent;
//                            odear_total_amount[0] = cashAmount[0]+credit- getallDiscount_Fixed - subtotalvalueDiscount +tax_pay[0]+credit_fee[0]; // final order Total amount

                            newOrderTotal[0] = (subTotal - cashdiscount[0] + orderItemRepository.getAllDiscountFixed() - newPercentTotal) + tax_pay[0];
                            creditTopay[0] = odear_total_amount[0] - newCashPaid[0];  // total Amount credit To Pay
                            txtcashValue.setText("$" + Utility.decimal2Values(cashAmount[0])); //Cash value
                            txtCreditfee.setText("$" + Utility.decimal2Values(credit_fee[0]));  // print Credit fees
                            txtCashdiscount.setText("-$" + Utility.decimal2Values(cashdiscount[0])); // print CashDiscount
                            txtcashTopay.setText("$" + Utility.decimal2Values(cashAmount[0])); // print Cash Value
                            txtcreditToPay.setText("$" + Utility.decimal2Values(newOrderTotal[0] - newCashPaid[0])); // print credit to pay
                            txtOrderTotal1.setText("$" + Utility.decimal2Values(newOrderTotal[0]));  // print order To Pay Value
                            txtOrderTotal.setText("$" + Utility.decimal2Values(newOrderTotal[0]));  // print order To Pay Value
                        } else {
                            txtcashValue.setText("$00.00");
                            edtPaid.setError("Please enter amount less than due");
                        }
                    } else {
                        Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                    }
                }
            });


            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonConfirm);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);

            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnOk.callOnClick();
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str_cash = edtPaid.getText().toString().replaceAll("[$,]", "");
                    String str_pay = txtcreditToPay.getText().toString();
                    if (str_cash != null && !str_cash.isEmpty() && !str_pay.equals("$00.00")) {
                        if (Double.valueOf(str_pay.replace("$", "")) > 0.00) {
                            if (orderType.equals("Cash Order")) {
                                dialog.dismiss();
                            } else {
                                paymentMethod("Cash_Card", 2, "", Double.parseDouble(Utility.decimal2Values(subTotal)),
                                        Double.parseDouble(Utility.decimal2Values(Double.valueOf(str_pay.replace("$", ""))))/*credittopay*/,
                                        Double.parseDouble(Utility.decimal2Values(newOrderTotal[0])), Double.parseDouble(Utility.decimal2Values(tax_pay[0])),
                                        Double.parseDouble(Utility.decimal2Values(cashdiscount[0])), Double.parseDouble(Utility.decimal2Values(cashAmount[0])),
                                        Double.parseDouble(Utility.decimal2Values(newOrderTotal[0] - newCashPaid[0])), Double.parseDouble(Utility.decimal2Values(credit_fee[0])));
//                            completeOrder("Cash_Card" ,2, "",Double.parseDouble(Utility.decimal2Values(subTotal))
//                                    ,Double.parseDouble(Utility.decimal2Values(odear_total1[0]))
//                                    ,Double.parseDouble(Utility.decimal2Values(tax))
//                                    ,Double.parseDouble(Utility.decimal2Values(discount[0]))
//                                    ,Double.parseDouble(Utility.decimal2Values(cash[0]))
//                                            ,Double.parseDouble(Utility.decimal2Values(creditTopay[0]))
//                                            ,Double.parseDouble(Utility.decimal2Values(credit_fee[0])));
                                dialog.dismiss();
                            }
                        } else {
                            Toast.makeText(mContext, "please enter amount less than dUE", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please enter cash amount..", Toast.LENGTH_LONG).show();
                    }
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//
//    public void openSummaryPopupCashCredit(final String orderType, final double orderTotal, final double subTotal, final double tax, final double cash_Discount, final double newSubtotal) {
//        try {
//            // Create custom dialog object
//            final Dialog dialog = new Dialog(getActivity());
//            // hide to default title for Dialog
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View view = inflater.inflate(R.layout.dialog_summary_new, null, false);
//            dialog.setCanceledOnTouchOutside(true);
//            dialog.setCancelable(true);
//            dialog.setContentView(view);
//            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//            final double[] cashdiscount = new double[1];
//            final TextView txtOrderType = dialog.findViewById(R.id.txtOrderType);
//            final TextView txtSubtotalV = dialog.findViewById(R.id.txtSubtotalV);
//            final TextView txtOrderTotal = dialog.findViewById(R.id.txtOrderTotal);
//            final EditText edtPaid = dialog.findViewById(R.id.edtPaid);
//            final TextView txtcashValue  = dialog.findViewById(R.id.txtcashValue);
//            final TextView txtCreditfee  = dialog.findViewById(R.id.txtCreditfee);
//            final TextView txtCashdiscount = dialog.findViewById(R.id.txtCashdiscount);
//            final TextView txtTaxesvalue = dialog.findViewById(R.id.txtTaxesvalue);
//            final TextView txtcreditToPay = dialog.findViewById(R.id.txtcreditToPay);
//            final TextView txtcashTopay = dialog.findViewById(R.id.txtcashTopay);
//            final TextView txtTaxes = dialog.findViewById(R.id.txtTaxes);
//            final Button dialogCalculate = dialog.findViewById(R.id.dialogCalculate);
//            final RecyclerView recycleViewAdditionalDiscount=dialog.findViewById(R.id.recycleViewAdditionalDiscount);
//
//            final double[] creditTopay = new double[1];
//            final double[] odear_total_amount = new double[1];
//            final double[] cashAmount = new double[1];
//            final double[] credit_fee = new double[1];
//            final double[] tax_pay = new double[1];
//
////            Double d = new Double(Utility.decimal2Values(Double.valueOf(Preferences.get(mContext,Preferences.TAX))));
////            int int_tax = d.intValue();
//            txtTaxes.setText("Taxes ("+Preferences.get(mContext,Preferences.TAX)+"%)");
//            txtOrderType.setText(orderType);
//            txtSubtotalV.setText("$" + Utility.decimal2Values(subTotal));
//            // txtOrderTotal.setText("$" + Utility.decimal2Values(orderTotal));
////            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
////                txtTaxesvalue.setText("$"+Utility.decimal2Values(tax));
////            }
//            //txtdiscount.setText("(-) $" + Utility.decimal2Values(cashDiscount));
//
//            final ArrayList<AdditionalDiscount> list=new ArrayList<>();
//            List<OrderItems> cartItems=orderItemRepository.getAll();
//
//            for (int i=0;i<cartItems.size();i++){
//                if(cartItems.get(i).getIsDiscount().equals("true")){
//                    list.add(new AdditionalDiscount(cartItems.get(i).getProduct_name(),String.valueOf( orderItemRepository.getPriceById(String.valueOf(cartItems.get(i).getProduct_id()))), cartItems.get(i).getIsDPercent().equals("false")?"Fixed":"Percent"));
//                }
//            }
//
//            if(list.size()<=0){
//                recycleViewAdditionalDiscount.setVisibility(View.GONE);
//            }else {
//                recycleViewAdditionalDiscount.setVisibility(View.VISIBLE);
//            }
//            mLayoutManager = new GridLayoutManager(getActivity(), 1);
//            recycleViewAdditionalDiscount.setLayoutManager(mLayoutManager);
//            final Cash_CardSummaryAdditionalDiscountAdapter[] adapter = new Cash_CardSummaryAdditionalDiscountAdapter[1];
//            if(orderType.equals("Cash Order")) {
//                adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,newSubtotal);
//            }else {
//                adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,newSubtotal);
//            }
//            recycleViewAdditionalDiscount.setAdapter(adapter[0]);
//            edtPaid.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    int length = edtPaid.length();
//                    if(length > 0){
//                        txtcashValue.setText("$00.00");
//                        txtCreditfee.setText("$00.00");
//                        txtCashdiscount.setText("-$00.00");
//                        txtOrderTotal.setText("$00.00");
//                        txtcashTopay.setText("$00.00");
//                        txtcreditToPay.setText("$00.00");
//                    }
//                }
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                }
//                @Override
//                public void afterTextChanged(Editable s) { }
//            });
//
//
//
//            dialogCalculate.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String str_cash = edtPaid.getText().toString();
//                    if(str_cash != null && !str_cash.isEmpty()){
//                        cashAmount[0] = Double.parseDouble(str_cash);
//                        if(cashAmount[0] <= orderTotal){
//                            edtPaid.setError(null);
//
//                            double credit = subTotal - cashAmount[0];
//                            double  creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04
//                            //credit_fee[0] = (credit * creditfee_value ) - credit; // credit fees calculation  ;
//                            // credit fees calculation  ;
//                            double fees_paid = Double.parseDouble(Preferences.get(mContext, Preferences.FEES_PAID));
//                            double fees_paid_value = fees_paid/100;  // feesPaid calculation 0.0385
//                            cashdiscount[0] =  (cashAmount[0] * creditfee_value ) * fees_paid_value ;  // Cash Discount
//                            credit_fee[0] = ((subTotal-cashdiscount[0])*creditfee_value)-subTotal;
//
//                            double newcashDiscount = Double.parseDouble(Utility.decimal2Values(credit_fee[0]));
//
//                            /**  credit Amount  **/
//                            credit = credit - cashdiscount[0] - credit_fee[0];
//                            credit = credit + credit_fee[0];
//
//                            double newCredit=credit-cashdiscount[0];
//                            double cash_With_CreditFees=(cashAmount[0] * creditfee_value);
//                            double credit_With_CreditFees=(newCredit * creditfee_value);
//                            double cashDiscountProgram=(-cashdiscount[0]);
//                            // All Discount get order Fixed and Percent
//                            double getallDiscount_Fixed = (-orderItemRepository.getAllDiscountFixed()); // All fixed Discount
//                            double getallDiscount_Percent = (-orderItemRepository.getAllDiscountPercent())  / 100; // All fixed Percent
//
//                            // new calculation
//                            double newCreditamount = credit * creditfee_value;
//                            double newCashPaid =((cashAmount[0]*creditfee_value)-(cashAmount[0]*creditfee_value)*fees_paid_value);
//                            double newtotalafter_cashdiscount = newCreditamount + newCashPaid;
//                            double newApplyfixed_discount = newtotalafter_cashdiscount + - getallDiscount_Fixed;
//                            double newDiscountPercent = newApplyfixed_discount * getallDiscount_Percent;
//                            double newApplyPercentdiscount = newApplyfixed_discount - newDiscountPercent; // Apply % discount
//                            double newtax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
//                            //double newFinalTotalAmountwithTax = (newApplyPercentdiscount * (1 + newtax_value));
//                            odear_total_amount[0] = (newApplyPercentdiscount * (1 + newtax_value));
//                            /*************************/
//
//
//                            adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,
//                                    ((cash_With_CreditFees-cashdiscount[0]+credit_With_CreditFees)+ - getallDiscount_Fixed));
//                            //adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,((cash[0]*fees_paid)-cash[0]*fees_paid*cashDiscount*100/100+clu_fees_paid*fees_paid+orderItemRepository.getAllDiscountFixed())*orderItemRepository.getAllDiscountPercent()*100/100);
//                            // adapter[0] =new Cash_CardSummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository,(cash_With_CreditFees+credit_fee[0]+cashDiscountProgram+credit_With_CreditFees+orderItemRepository.getAllDiscountFixed()));
//                            recycleViewAdditionalDiscount.setAdapter(adapter[0]);
//
//                            // tax calculation
//                            double taxcalculationstep1 = (((((cash_With_CreditFees))-(cash_With_CreditFees) * fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed); // tax calculation step 1
//                            double taxcalculationstep2 = ((((((cash_With_CreditFees))-(cash_With_CreditFees)* fees_paid_value)+ credit * creditfee_value)+ - getallDiscount_Fixed)* getallDiscount_Percent); // tax calculation step 2
//                            double tax_value = Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
//                            tax_pay[0] = (taxcalculationstep1 - taxcalculationstep2) * tax_value;
//                            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
//                                txtTaxesvalue.setText("$"+Utility.decimal2Values(tax_pay[0]));
//                            }else{
//                                txtTaxesvalue.setText("$"+Utility.decimal2Values(tax_pay[0]));
//                            }
//
//
//                            // order Total
////                            double subtotalvalueDiscount = ((cash_With_CreditFees-cashdiscount[0]+credit_With_CreditFees)+ - getallDiscount_Fixed);
////                            subtotalvalueDiscount = subtotalvalueDiscount * getallDiscount_Percent;
////                            odear_total_amount[0] = cashAmount[0]+credit- getallDiscount_Fixed - subtotalvalueDiscount +tax_pay[0]+credit_fee[0]; // final order Total amount
//
//                            creditTopay[0] = odear_total_amount[0] - cashAmount[0] ;  // total Amount credit To Pay
//                            txtcashValue.setText("$"+Utility.decimal2Values(cashAmount[0])); //Cash value
//                            txtCreditfee.setText("$" + Utility.decimal2Values(credit_fee[0]));  // print Credit fees
//                            txtCashdiscount.setText("-$" + Utility.decimal2Values(cashdiscount[0])); // print CashDiscount
//                            txtcashTopay.setText("$"+Utility.decimal2Values(cashAmount[0])); // print Cash Value
//                            txtcreditToPay.setText("$"+Utility.decimal2Values(creditTopay[0])); // print credit to pay
//                            txtOrderTotal.setText("$" + Utility.decimal2Values(odear_total_amount[0]));  // print order To Pay Value
//                        }else {
//                            txtcashValue.setText("$00.00");
//                            edtPaid.setError("Please enter amount less than due");
//                        }
//                    }else {
//                        Toast.makeText(mContext,"Please enter cash amount..",Toast.LENGTH_LONG).show();
//                    }
//                }
//            });
//
//
//            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonConfirm);
//            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
//
//            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });
//            btnOk.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String str_cash = edtPaid.getText().toString();
//                    String str_pay = txtcreditToPay.getText().toString();
//                    if(str_cash != null && !str_cash.isEmpty() && !str_pay.equals("$00.00")){
//                        if (orderType.equals("Cash Order")) {
//                            dialog.dismiss();
//                        } else {
//                            paymentMethod("Cash_Card" ,2, "",Double.parseDouble(Utility.decimal2Values(subTotal)),
//                                    Double.parseDouble(Utility.decimal2Values(creditTopay[0])),
//                                    Double.parseDouble(Utility.decimal2Values(odear_total_amount[0])),Double.parseDouble(Utility.decimal2Values(tax_pay[0])),
//                                    Double.parseDouble(Utility.decimal2Values(cashdiscount[0])),Double.parseDouble(Utility.decimal2Values(cashAmount[0])),
//                                    Double.parseDouble(Utility.decimal2Values(creditTopay[0])),Double.parseDouble(Utility.decimal2Values(credit_fee[0])));
////                            completeOrder("Cash_Card" ,2, "",Double.parseDouble(Utility.decimal2Values(subTotal))
////                                    ,Double.parseDouble(Utility.decimal2Values(odear_total1[0]))
////                                    ,Double.parseDouble(Utility.decimal2Values(tax))
////                                    ,Double.parseDouble(Utility.decimal2Values(discount[0]))
////                                    ,Double.parseDouble(Utility.decimal2Values(cash[0]))
////                                            ,Double.parseDouble(Utility.decimal2Values(creditTopay[0]))
////                                            ,Double.parseDouble(Utility.decimal2Values(credit_fee[0])));
//                            dialog.dismiss();
//                        }
//                    }else {
//                        Toast.makeText(mContext,"Please enter cash amount..",Toast.LENGTH_LONG).show();
//                    }
//                }
//            });
//            // Display the dialog
//            dialog.show();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }


    public void openSummaryPopup(final String order_pay_type, final String orderType, final double orderTotal, double subTotal, double tax, double cashDiscount, double newSubtotal) {


        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_summary, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            final TextView txtOrderType = dialog.findViewById(R.id.txtOrderType);
            final TextView txtSubtotalV = dialog.findViewById(R.id.txtSubtotalV);
            final TextView txtTax = dialog.findViewById(R.id.txtTax);
            final TextView txtTaxV = dialog.findViewById(R.id.txtTaxV);
            final TextView txtOrderTotalV = dialog.findViewById(R.id.txtOrderTotalV);
            final TextView txtCashD = dialog.findViewById(R.id.txtCashD);
            final TextView txtCashDV = dialog.findViewById(R.id.txtCashDV);
            final TextView txtCreditFees = dialog.findViewById(R.id.txtCreditFees);
            final TextView txtCreditFeesV = dialog.findViewById(R.id.txtCreditFeesV);
            final LinearLayout linearCreditFees = dialog.findViewById(R.id.linearCreditFees);

            //linearCreditFees.setVisibility(View.VISIBLE);

            double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04

            final Double creditFees = (subTotal * creditfee_value) - subTotal;
            txtCreditFees.append(" (" + creditfee_value + "%)");
            txtCreditFeesV.setText("$ " + Utility.decimal2Values(creditFees));

            RecyclerView recycleViewAdditionalDiscount = dialog.findViewById(R.id.recycleViewAdditionalDiscount);
            LinearLayout linearLayoutCashD = dialog.findViewById(R.id.linearCashD);
            LinearLayout linearLayoutTax = dialog.findViewById(R.id.linearTax);

            txtOrderType.setText(orderType);
            txtSubtotalV.setText("$" + Utility.decimal2Values(subTotal));
            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                txtTaxV.setText("$" + Utility.decimal2Values(tax));
                linearLayoutTax.setVisibility(View.VISIBLE);
                txtTax.append(" (" + Preferences.get(mContext, Preferences.TAX) + "%)");
            } else {
                linearLayoutTax.setVisibility(View.GONE);
            }
            txtOrderTotalV.setText("$" + Utility.decimal2Values(orderTotal));
            ArrayList<AdditionalDiscount> list = new ArrayList<>();
            List<OrderItems> cartItems = orderItemRepository.getAll();

            for (int i = 0; i < cartItems.size(); i++) {
                if (cartItems.get(i).getIsDiscount().equals("true")) {
                    list.add(new AdditionalDiscount(cartItems.get(i).getProduct_name(), String.valueOf(orderItemRepository.getPriceById(String.valueOf(cartItems.get(i).getProduct_id()))), cartItems.get(i).getIsDPercent().equals("false") ? "Fixed" : "Percent", cartItems.get(i)));
                }
            }
            if (list.size() <= 0) {
                recycleViewAdditionalDiscount.setVisibility(View.GONE);
            } else {
                recycleViewAdditionalDiscount.setVisibility(View.VISIBLE);
            }
            mLayoutManager = new GridLayoutManager(getActivity(), 1);
            recycleViewAdditionalDiscount.setLayoutManager(mLayoutManager);
            SummaryAdditionalDiscountAdapter adapter;
            if (orderType.equals("Cash Order")) {
                adapter = new SummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository, newSubtotal);
            } else {
                adapter = new SummaryAdditionalDiscountAdapter(list, mContext, orderItemRepository, newSubtotal);
            }
            recycleViewAdditionalDiscount.setAdapter(adapter);

            if (orderType.equals("Cash Order")) {
                linearLayoutCashD.setVisibility(View.VISIBLE);
                txtCashD.append("(" + Preferences.get(mContext, Preferences.CASD_Discount) + "%)");
                txtCashDV.setText("-$" + Utility.decimal2Values(cashDiscount));
            } else {
                linearLayoutCashD.setVisibility(View.GONE);
            }
            if (orderType.equals("Cash Order")) {

                //txtCartTotalPrice.setText("$" +Utility.decimal2Values(orderTotal));

                double Dper = orderItemRepository.getAllDiscountPercent();
                discFixedC = orderItemRepository.getAllDiscountFixed();
                discPerC = (((subTotal + discFixedC - cashDiscount) * Dper) / 100);
                double nsubAmount = subTotal + discFixedC + discPerC;
                txtSubtotalV.setText("$" + Utility.decimal2Values((nsubAmount)));
            } else {
                double Dper = orderItemRepository.getAllDiscountPercent();
                discFixedC = orderItemRepository.getAllDiscountFixed();
                discPerC = (((subTotal + discFixedC) * Dper) / 100);
                double nsubAmount = subTotal + discFixedC + discPerC;
                txtSubtotalV.setText("$" + Utility.decimal2Values((nsubAmount)));
            }
            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonConfirm);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);

            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (orderType.equals("Cash Order")) {
                        //completeCashOrder(order_pay_type, "", 1);
                        double cashD1=0.00;
                        try {
                            cashD1 = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (orderItemRepository.getSubTotal()) / (100)));
                        } catch (Exception e) {
                            cashD1  = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))) / (100)));
                            e.printStackTrace();
                        }
                        openCashPopup(order_pay_type,(Double.valueOf(txtOrderTotalV.getText().toString().replace("$", ""))), 1);
                        dialog.dismiss();
                    } else {
                        paymentMethod(order_pay_type, 1, "", 0, 0.00, orderTotal,
                                0.00, 0.00, 0.00, 0.00, creditFees);
                        dialog.dismiss();
                    }
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void completeCashOrder(String order_pay_type, String adId, int type) {
        pb.setVisibility(View.VISIBLE);
        // btnOrderNowCash.setEnabled(false);

        HashMap<String, Object> map = new HashMap<>();
        map.put("serial_number", Preferences.get(mContext, Preferences.DEVICE_SN));
        map.put("device_id", Preferences.get(mContext, Preferences.DEVICE_ID));

       /* map.put("name", edtName.getText().toString().trim());
        map.put("mobile", edtMobile.getText().toString().trim());
        map.put("email", edtEmail.getText().toString().trim());*/
        //map.put("order_total", Utility.decimal2Values(orderItemRepository.getSubTotal()));
        Float cashD;
        if (type == 0) {
            cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))) / (100)));
            map.put("order_total", Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")));
        } else {
            try {
                cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (orderItemRepository.getSubTotal()) / (100)));
            } catch (Exception e) {
                cashD = cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))) / (100)));
                e.printStackTrace();
            }
            map.put("order_total", Utility.decimal2Values(Float.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))));
        }

        map.put("cash_discount", Utility.decimal2Values(cashD));
        map.put("ad_total", Utility.decimal2Values(-discPerC - discFixedC));
        map.put("ad_id", TextUtils.join(",", orderItemRepository.getDiscountIds()));
        map.put("total_amount", Utility.decimal2Values(Float.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) - (cashD)));
        map.put("order_status", "Completed");
        map.put("order_pay_type", order_pay_type);
        map.put("ad_info", (orderItemRepository.getAllDiscountFixed()));

        double orderTotal = Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
        double subAmount = ((orderTotal - cashD));
        Log.e("GETTax", Preferences.get(mContext, Preferences.TAX));
        if (type == 0) {
            double tax;
            try {
                JSONArray array = new JSONArray();
                orderItemRepository = new OrderItemRepository(mContext);
                List<OrderItems> orderItems = orderItemRepository.getAll();
               /* if(orderItems.get(0).getProduct_id()==0){
                     tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                }else {
                     tax = subAmount * orderItems.get(0).getTax() / 100;
                }*/
                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                    tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                    if(tax<0.009){
                        tax=0.01;
                    }
                } else {
                    tax = 0.00;
                }

                for (int i = 0; i < orderItems.size(); i++) {
                    OrderItems item = orderItems.get(i);

             /*   if(item.getTax()!=0.00){
                    tax=tax+ Double.valueOf((((Double.valueOf(item.getItem_price())*Integer.parseInt(item.getQuantity()))*item.getTax())/100));
                }*/
                    JSONObject jsonObjectOrder = new JSONObject();
                    jsonObjectOrder.put("product_id", item.getProduct_id());
                    jsonObjectOrder.put("product_name", item.getProduct_name());
                    jsonObjectOrder.put("quantity", item.getQuantity());
                    jsonObjectOrder.put("item_price", item.getItem_price());
                    jsonObjectOrder.put("item_cost", item.getItem_cost());
                    jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                    jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                    jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                    if (item.getProduct_id().contains("-")) {
                        // jsonObjectOrder.put("ad_info", (orderItemRepository.getAllDiscountFixed()));
                        jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                    }
                    array.put(jsonObjectOrder);
                }
                map.put("total_amount", Utility.decimal2Values(subAmount + tax));
                // if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
                map.put("tax", String.valueOf(Utility.decimal2Values(tax!=0.00&&tax<0.009?0.01:tax)));
                /*}else {
                    map.put("tax", 0.00);
                }*/
                // map.put("tax", String.valueOf(Utility.decimal2Values(tax)));

                map.put("order_items", array);
                map.put("ad_id", adId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                double tax;
                JSONArray array = new JSONArray();
                orderItemRepository = new OrderItemRepository(mContext);
                List<OrderItems> orderItems = orderItemRepository.getAll();
               /* if(orderItems.get(0).getProduct_id()==0){
                     tax =( subAmount * (Double.valueOf(Preferences.get(mContext, Preferences.TAX))) )/ 100;
                }else {
                     tax = subAmount * orderItems.get(0).getTax() / 100;
                }*/
                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                    // if (orderItems.get(0).getProduct_id() == "0") {
                    tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                } else {
                    tax = 0.00;
                }
                for (int i = 0; i < orderItems.size(); i++) {
                    OrderItems item = orderItems.get(i);

             /*   if(item.getTax()!=0.00){
                    tax=tax+ Double.valueOf((((Double.valueOf(item.getItem_price())*Integer.parseInt(item.getQuantity()))*item.getTax())/100));
                }*/

                    JSONObject jsonObjectOrder = new JSONObject();
                    jsonObjectOrder.put("product_id", item.getProduct_id());
                    jsonObjectOrder.put("product_name", item.getProduct_name());
                    jsonObjectOrder.put("quantity", item.getQuantity());
                    jsonObjectOrder.put("item_price", item.getItem_price());
                    jsonObjectOrder.put("item_cost", item.getItem_cost());
                    jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                    jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                    jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                    if (item.getProduct_id().contains("-")) {

                        jsonObjectOrder.put("ad_info", (orderItemRepository.getAllDiscountFixed()));
                        jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                    }
                    array.put(jsonObjectOrder);
                }
                map.put("total_amount", Utility.decimal2Values(subAmount + tax));
                // if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
                map.put("tax", String.valueOf(Utility.decimal2Values(tax!=0.00&&tax<0.009?0.01:tax)));
                /*}else {
                    map.put("tax", 0.00);
                }*/
                map.put("order_items", array);
                map.put("ad_id", adId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {

            map.put("payment_details", new JSONObject().put("payment_type", "Cash").toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("RAW_JSON", map.toString());

        callOrderAPI(map);
    }

    public void completeCashOrderNew(String order_pay_type, String adId, int type, double paid1, double change ) {
        pb.setVisibility(View.VISIBLE);
        // btnOrderNowCash.setEnabled(false);

        HashMap<String, Object> map = new HashMap<>();
        map.put("serial_number", Preferences.get(mContext, Preferences.DEVICE_SN));
        map.put("device_id", Preferences.get(mContext, Preferences.DEVICE_ID));
        map.put("amount_paid", paid1);
        map.put("change_due", change);


       /* map.put("name", edtName.getText().toString().trim());
        map.put("mobile", edtMobile.getText().toString().trim());
        map.put("email", edtEmail.getText().toString().trim());*/
        //map.put("order_total", Utility.decimal2Values(orderItemRepository.getSubTotal()));
        Float cashD;
        if (type == 0) {
            cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))) / (100)));
            map.put("order_total", Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")));
        } else {
            try {
                cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (orderItemRepository.getSubTotal()) / (100)));
            } catch (Exception e) {
                cashD = cashD = Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(getActivity(), Preferences.CASD_Discount))) * (Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))) / (100)));
                e.printStackTrace();
            }
            map.put("order_total", Utility.decimal2Values(Float.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))));
        }

        map.put("cash_discount", Utility.decimal2Values(cashD));
        map.put("ad_total", Utility.decimal2Values(-discPerC - discFixedC));
        map.put("ad_id", TextUtils.join(",", orderItemRepository.getDiscountIds()));
        map.put("total_amount", Utility.decimal2Values(Float.valueOf(txtCartTotalPrice.getText().toString().replace("$", "")) - (cashD)));
        map.put("order_status", "Completed");
        map.put("order_pay_type", order_pay_type);
        map.put("ad_info", (orderItemRepository.getAllDiscountFixed()));

        double orderTotal = Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
        double subAmount = ((orderTotal - cashD));
        Log.e("GETTax", Preferences.get(mContext, Preferences.TAX));
        if (type == 0) {
            double tax;
            try {
                JSONArray array = new JSONArray();
                orderItemRepository = new OrderItemRepository(mContext);
                List<OrderItems> orderItems = orderItemRepository.getAll();
               /* if(orderItems.get(0).getProduct_id()==0){
                     tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                }else {
                     tax = subAmount * orderItems.get(0).getTax() / 100;
                }*/
                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                    tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                    if(tax<0.009){
                        tax=0.01;
                    }
                } else {
                    tax = 0.00;
                }

                for (int i = 0; i < orderItems.size(); i++) {
                    OrderItems item = orderItems.get(i);

             /*   if(item.getTax()!=0.00){
                    tax=tax+ Double.valueOf((((Double.valueOf(item.getItem_price())*Integer.parseInt(item.getQuantity()))*item.getTax())/100));
                }*/
                    JSONObject jsonObjectOrder = new JSONObject();
                    jsonObjectOrder.put("product_id", item.getProduct_id());
                    jsonObjectOrder.put("product_name", item.getProduct_name());
                    jsonObjectOrder.put("quantity", item.getQuantity());
                    jsonObjectOrder.put("item_price", item.getItem_price());
                    jsonObjectOrder.put("item_cost", item.getItem_cost());
                    jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                    jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                    jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                    if (item.getProduct_id().contains("-")) {
                        // jsonObjectOrder.put("ad_info", (orderItemRepository.getAllDiscountFixed()));
                        jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                    }
                    array.put(jsonObjectOrder);
                }
                map.put("total_amount", Utility.decimal2Values(subAmount + tax));
                // if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
                map.put("tax", String.valueOf(Utility.decimal2Values(tax!=0.00&&tax<0.009?0.01:tax)));
                /*}else {
                    map.put("tax", 0.00);
                }*/
                // map.put("tax", String.valueOf(Utility.decimal2Values(tax)));

                map.put("order_items", array);
                map.put("ad_id", adId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                double tax;
                JSONArray array = new JSONArray();
                orderItemRepository = new OrderItemRepository(mContext);
                List<OrderItems> orderItems = orderItemRepository.getAll();
               /* if(orderItems.get(0).getProduct_id()==0){
                     tax =( subAmount * (Double.valueOf(Preferences.get(mContext, Preferences.TAX))) )/ 100;
                }else {
                     tax = subAmount * orderItems.get(0).getTax() / 100;
                }*/
                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                    // if (orderItems.get(0).getProduct_id() == "0") {
                    tax = subAmount * Double.valueOf(Preferences.get(mContext, Preferences.TAX)) / 100;
                    /*} else {
                        tax = subAmount * orderItems.get(0).getTax() / 100;
                    }*/
                } else {
                    tax = 0.00;
                }
                for (int i = 0; i < orderItems.size(); i++) {
                    OrderItems item = orderItems.get(i);

             /*   if(item.getTax()!=0.00){
                    tax=tax+ Double.valueOf((((Double.valueOf(item.getItem_price())*Integer.parseInt(item.getQuantity()))*item.getTax())/100));
                }*/

                    JSONObject jsonObjectOrder = new JSONObject();
                    jsonObjectOrder.put("product_id", item.getProduct_id());
                    jsonObjectOrder.put("product_name", item.getProduct_name());
                    jsonObjectOrder.put("quantity", item.getQuantity());
                    jsonObjectOrder.put("item_price", item.getItem_price());
                    jsonObjectOrder.put("item_cost", item.getItem_cost());
                    jsonObjectOrder.put("item_total_cost", item.getItem_total_cost());
                    jsonObjectOrder.put("item_total_profit", item.getItem_total_profit());
                    jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getTotal_amount()))));
                    if (item.getProduct_id().contains("-")) {

                        jsonObjectOrder.put("ad_info", (orderItemRepository.getAllDiscountFixed()));
                        jsonObjectOrder.put("ad_id", (item.getProduct_id()).replace("-", ""));
                        jsonObjectOrder.put("total_amount", String.valueOf(Utility.decimal2Values(Double.valueOf(item.getNew_total_amount()))));
                    }
                    array.put(jsonObjectOrder);
                }
                map.put("total_amount", Utility.decimal2Values(subAmount + tax));
                // if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
                map.put("tax", String.valueOf(Utility.decimal2Values(tax!=0.00&&tax<0.009?0.01:tax)));
                /*}else {
                    map.put("tax", 0.00);
                }*/
                map.put("order_items", array);
                map.put("ad_id", adId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {

            map.put("payment_details", new JSONObject().put("payment_type", "Cash").toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("RAW_JSON", map.toString());

        callOrderAPI(map);
    }

    /* private void openOrderDetailPopUp() {
         try {
             // Create custom dialog object
             final Dialog dialog = new Dialog(getActivity());
             // hide to default title for Dialog
             dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

             LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             View view = inflater.inflate(R.layout.dialog_order_now, null, false);
             dialog.setCanceledOnTouchOutside(false);
             dialog.setCancelable(false);
             dialog.setContentView(view);
             dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
             //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

             Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
             Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
             dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     dialog.dismiss();
                 }
             });
             final EditText edtName = dialog.findViewById(R.id.edtName);
             final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
             final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
             final TextView txtHead =  dialog.findViewById(R.id.text);
             btnOk.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     pb.setVisibility(View.VISIBLE);

                     HashMap<String, Object> map = new HashMap<>();

                     double tax=0.00;

                     map.put("name", edtName.getText().toString().trim());
                     map.put("mobile", edtMobile.getText().toString().trim());
                     map.put("email", edtEmail.getText().toString().trim());
                     map.put("order_total",  Utility.decimal2Values(Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""))));
                     map.put("cash_discount", "0.00");
                     try{
                         JSONArray array=new JSONArray();
                         orderItemRepository=new OrderItemRepository(mContext);
                         List<OrderItems> orderItems =orderItemRepository.getAll();
                         for(int i=0;i<orderItems.size();i++){
                             OrderItems item= orderItems.get(i);

                             if(item.getTax()!=0.00){
                                 tax=tax+ Double.valueOf((((Double.valueOf(item.getItem_price())*Integer.parseInt(item.getQuantity()))*item.getTax())/100));
                             }

                             JSONObject jsonObjectOrder=  new JSONObject();
                             jsonObjectOrder.put("product_id", item.getProduct_id());
                             jsonObjectOrder.put("product_name", item.getProduct_name());
                             jsonObjectOrder.put("quantity", item.getQuantity());
                             jsonObjectOrder.put("item_price", item.getItem_price());
                             jsonObjectOrder.put("total_amount", item.getTotal_amount());

                             array.put(jsonObjectOrder);
                         }
                         if(Preferences.get(mContext, Preferences.ISTAX).equals("1")){
                             map.put("tax", String.valueOf(Utility.decimal2Values(tax)));
                         }else {
                             map.put("tax", 0.00);
                         }
                       //  map.put("tax",String.valueOf(Utility.decimal2Values(tax)));
                         map.put("total_amount", txtCartTotalPrice.getText().toString().replace("$", ""));
                         map.put("status", "Completed");



                         map.put("order_items",array);
                     }catch (Exception e){
                         e.printStackTrace();
                     }
                     try {

                         map.put("payment_details", new JSONObject().put("payment_type", "Cash").toString());

                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                     Log.e("RAW_JSON", map.toString());

                     //callOrderAPI(map);
                     dialog.dismiss();
                 }
             });

             // Display the dialog
             dialog.show();
         } catch (Exception ex) {
             ex.printStackTrace();
         }
     }
 */
    private void callOrderAPI(HashMap<String, Object> map) {
        map.put("created_at", new Date());
        map.put("serial_number", Preferences.get(mContext, Preferences.DEVICE_SN));
        map.put("device_id", Preferences.get(mContext, Preferences.DEVICE_ID));
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postJSONRequest(Constants.BaseURL + Constants.OrdersAddAPI,
                "order_add", map, Request.Method.POST, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    public void callProductsAPI() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI + "?limit=200",
                "products", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    public void callDiscountAPI() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.DiscountAPI,
                "discount", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("order_add")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.has("data")) {

                    /*{"data":{"total_amount":"2.20","cash_discount":"0.08","order_total":"2.08","tax":"0.20","order_status":"Completed","created_by_id":4,"updated_at":"2020-10-30 16:46:34","created_at":"2020-10-30 16:46:34","id":80,"payment_details":"{\"payment_type\":\"Cash\"}","order_item":[{"id":114,"order_id":80,"product_id":1,"product_name":"Sticky Notes","quantity":1,"item_price":"2.08","total_amount":"2.08"}]}}
I/System.out: Payment Details : {"payment_type":"Cash"}*/
                    //if(result.getJSONArray("data").length()>0){
                    //Toast.makeText(mContext, "Order Approved.", Toast.LENGTH_LONG).show();


                    if (isCash == 1) {

                        // if (trans_type == 1) {
                        if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                            printReceipt(result.getJSONObject("data"), "Merchant Copy");
                            //openConfirmPopup(result.getJSONObject("data"));

                            // printReceipt(result.getJSONObject("data"), "Customer Copy");
                        } else {
                            printReceipt(result.getJSONObject("data"), "Customer Copy");
                        }
                        //openTipsPopup("Tips", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!");
                        //
                        // showAlertDialog(mContext, "Cash Refund", "Refund Amount: "+result.getJSONObject("data").getString("total_amount"), result);
                       /* } else {
                            // openTipsPopup("Tips", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!");
                            //openCashPopup(result);


                        }
*/

                        // isCash=0;
                    } else {

//                        if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
//                            printReceipt(result.getJSONObject("data"), "Merchant Copy");
//
//                            openConfirmPopup(result.getJSONObject("data"));
//                            // printReceipt(result.getJSONObject("data"), "Customer Copy");
//                        } else {
//                            printReceipt(result.getJSONObject("data"), "Customer Copy");
//                        }
                        /*printReceiptSplitTender type Cash_Card*/
                        if (result.getJSONObject("data").getString("order_pay_type").equals("Cash_Card")) {
                            if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                                printReceiptSplitTender(result.getJSONObject("data"), "Merchant Copy");
                                //openConfirmPopup(result.getJSONObject("data"));
                            } else {
                                printReceiptSplitTender(result.getJSONObject("data"), "Customer Copy");
                            }
                            // Toast.makeText(getActivity(),result.getJSONObject("data").getString("order_pay_type"),Toast.LENGTH_LONG).show();
                        } else {
                            if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                                printReceipt(result.getJSONObject("data"), "Merchant Copy");

                                // openConfirmPopup(result.getJSONObject("data"));
                                // printReceipt(result.getJSONObject("data"), "Customer Copy");
                            } else {
                                printReceipt(result.getJSONObject("data"), "Customer Copy");
                            }
                        }
                    }
                }else {
                    Toast.makeText(mContext, "Please try again", Toast.LENGTH_LONG).show();
                }


                ///;>:'[}
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pb.setVisibility(View.INVISIBLE);
        }

        if (tag_json_obj.equals("discount")) {
            Log.e("RES_DIS", response);
            try {
                JSONObject result = new JSONObject(response);
                String discount = result.getString("discount");
                Preferences.save(mContext, Preferences.CASD_Discount, discount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("GetTax")) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                Log.e("TAX Value", jsonObject.optString("tax"));
                Preferences.save(mContext, Preferences.TAX, jsonObject.optString("tax"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (tag_json_obj.equals("SettingsGet")) {
            try {
                JSONObject result = new JSONObject(response);
                /*{"is_tax":1,"print_name":"Vizypay","print_address_line1":"144 29th ST. #208",
                "print_address_line2":"West Des Moines, IA 50266","print_contact_no":"8559994142",
                "print_footer_text":"THANK YOU!","merchant_copy_in_print":0}*/

                Preferences.save(mContext, Preferences.ISTAX, result.getString("is_tax").equals("null")?"0.00":result.getString("is_tax"));
                Preferences.save(mContext, Preferences.ISPRINT_MERCHANTCOPY, result.getString("merchant_copy_in_print"));
                Preferences.save(mContext, Preferences.PRINT_NAME, result.getString("print_name"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS1, result.getString("print_address_line1"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS2, result.getString("print_address_line2"));
                Preferences.save(mContext, Preferences.PRINT_CONTACT, result.getString("print_contact_no"));
                Preferences.save(mContext, Preferences.PRINT_FOOTER, result.getString("print_footer_text"));
                Preferences.save(mContext, Preferences.APP_HEADER, result.getString("app_header"));
//                Preferences.save(mContext, Preferences.CREDIT_FEES, result.getString("credit_fees"));
                Preferences.save(mContext, Preferences.FEES_PAID, result.getString("fees_paid"));
                Preferences.save(mContext, Preferences.STOCK_TRACKING, result.getString("stock_tracking"));
                Preferences.save(mContext, Preferences.STOCK_NOTIFICATION, result.getString("stock_notification"));
                Preferences.save(mContext, Preferences.ADD_TIP, result.getString("tip_enable"));


               /* switchCompatTax.setChecked(result.getString("is_tax").equals("1")?true:false);
                switchCompatMerchantCopy.setChecked(result.getString("merchant_copy_in_print").equals("1")?true:false);
                edtName.setText(result.getString("print_name"));
                edtFooterText.setText(result.getString("print_footer_text"));
                edtAddressLine1.setText(result.getString("print_address_line1"));
                edtAddressLine2.setText(result.getString("print_address_line2"));
                edtContact.setText(result.getString("print_contact_no"));
                view.findViewById(R.id.progressbar).setVisibility(View.GONE);*/
                /*if(result.getString("type").equals("success")){
                    clearData();
                    Toast.makeText(mContext, "Saved Successfully", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }else {
                    Toast.makeText(mContext, "Please try Again", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("Cproducts")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                Log.e("Product", mProductList.toString());
                // final JSONArray mProductList1 = result.getJSONArray("data").getJSONArray();
                if (mProductList.length() > 0) {
                    for (int i = 0; i < mProductList.length(); i++) {
                        JSONObject jsonObj = mProductList.getJSONObject(i);
                        JSONObject created_byresult = new JSONObject(jsonObj.getString("created_by"));
                        String str = created_byresult.getString("credit_fees");
                        Preferences.save(mContext, Preferences.CREDIT_FEES, str);
                    }
                    linear_expandable.setVisibility(View.VISIBLE);
                    expandableListView.setVisibility(View.VISIBLE);

                    recyclerView.setVisibility(View.GONE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mExProductResponseList = gson.fromJson(mProductList.toString(), new TypeToken<List<CategoryWithProducts>>() {
                    }.getType());


                    expProductListAdapter = new ExpProductListAdapter(mExProductResponseList, mContext, txtCartTotalPrice, mCartList);
                    expandableListView.setAdapter(expProductListAdapter);

                    if (Preferences.get(mContext, "search")!="")
                    {
                        try {
                            for (int i = 0; i <=mExProductResponseList.size(); i++) {
                                //for(int j=0;j<=mExProductResponseList.get(i).getListOfProducts().size();j++) {
                                //expandableListView.isGroupExpanded(i);
                                expandableListView.expandGroup(i);
                                //}
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        if (tag_json_obj.equals("products")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    for (int i = 0; i < mProductList.length(); i++) {
                        JSONObject jsonObj = mProductList.getJSONObject(i);
                        JSONObject created_byresult = new JSONObject(jsonObj.getString("created_by"));
                        String str = created_byresult.getString("credit_fees");
                        Preferences.save(mContext, Preferences.CREDIT_FEES, str);
                    }
                    recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();
                    mProductResponseList = gson.fromJson(mProductList.toString(), new TypeToken<List<Products>>() {
                    }.getType());

                    productListAdapter = new ProductListAdapter(mProductResponseList, mContext, txtCartTotalPrice, mCartList);
                    recyclerView.setAdapter(productListAdapter);
                } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("AdditionalDGet")) {
            try {
                JSONObject result = new JSONObject(response);
                // JSONArray array=result.getJSONArray("data");

                final JSONArray mADiscountList = result.getJSONArray("data");
                if (mADiscountList.length() > 0) {
                    // recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    madiscountResponseList = gson.fromJson(mADiscountList.toString(), new TypeToken<List<AdditionalDiscount>>() {
                    }.getType());
                    AdditionalDiscountAdapter additionalDAdapter = new AdditionalDiscountAdapter(madiscountResponseList, mContext, txtCartTotalPrice);
                    recyclerView.setAdapter(additionalDAdapter);

                  /*  productListAdapter = new ProductListAdapter( mProductResponseList, mContext, txtCartTotalPrice, mCartList);
                    recyclerView.setAdapter(productListAdapter);*/
                } else {
                    // txtMessage.setText("No Data Found");
                    // txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onSuccess() {

        Log.e("PrintFragment", "Printed successfully. ");

    }

    @Override
    public void onError(ProcessResult processResult) {
        Log.e("PrintFragment", "Print Error. " + processResult.getMessage());

    }

    private void showContent(EditText contentView, POSLinkPrinter.PrintDataFormatter printDataFormatter) {
        contentView.setText(printDataFormatter.build());
        contentView.setSelection(contentView.length());
    }

    private void printReceipt(JSONObject orderResponse, String copyName) {

        try {
            POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();

            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_NAME).toUpperCase());
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS1));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS2));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_CONTACT));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();


            printDataFormatter.addContent("Transaction");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("#" + orderResponse.getString("id"));
            printDataFormatter.addLineSeparator();


            System.out.println("Payment Details : " + orderResponse.getString("payment_details"));
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(orderResponse.getString("created_at").split(" ")[0]);
            printDataFormatter.addRightAlign();
            printDataFormatter.addSmallFont();
            try {
                //  printDataFormatter.addContent(orderResponse.getString("created_at").split(" ")[1]);
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyy-MM-dd h:mm:ss");
                Date time = localDateFormat.parse(orderResponse.getString("created_at"));
                //holder.txtTime.setText("Time: "+String.valueOf(new SimpleDateFormat("h:mm aa").format(time)));
                printDataFormatter.addContent("" + (new SimpleDateFormat("h:mm aa").format(time)));
            }catch (Exception e){
                e.printStackTrace();
            }
            printDataFormatter.addLineSeparator();

            JSONObject paymentObj = new JSONObject(orderResponse.getString("payment_details").replaceAll("\\\\", ""));
            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                try {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(("Result"));
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent(("Approved"));
                    printDataFormatter.addLineSeparator();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //printDataFormatter.addBigFont();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Method:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent(paymentObj.getString("payment_type"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                if (orderResponse.getString("order_status").equals("Refund"))
                    printDataFormatter.addContent("Refund");
                else
                    printDataFormatter.addContent("Sale");

                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card No : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("account_num"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Type : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("card_type"));
                printDataFormatter.addLineSeparator();


            } else {
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Method:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent(paymentObj.getString("payment_type"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                if (orderResponse.getString("order_status").equals("Refund"))
                    printDataFormatter.addContent("Refund");
                else
                    printDataFormatter.addContent("Sale");
                printDataFormatter.addLineSeparator();
            }
            printDataFormatter.addLineSeparator();


            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent("Items:");
            printDataFormatter.addLineSeparator();
            try {
                JSONArray itemArr = orderResponse.getJSONArray("order_items");
                for (int i = 0; i < itemArr.length(); i++) {
                    JSONObject itemObj = itemArr.getJSONObject(i);
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(itemObj.getInt("quantity") + " X " + itemObj.getString("product_name"));
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(itemObj.getString("total_amount"))));
                    printDataFormatter.addLineSeparator();

                }
            } catch (Exception e) {
                if (orderResponse.getString("order_status").equals("Refund")) {
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(1 + " X " + "Refund");
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("total_amount"))) + "");
                    printDataFormatter.addLineSeparator();
                } else {
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("Quick Sale (Charge)");
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("order_total"))));
                    printDataFormatter.addLineSeparator();
                }
                e.printStackTrace();
            }
            printDataFormatter.addHeader();
            printDataFormatter.addLineSeparator();

            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                printDataFormatter.addContent("Subtotal : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("order_total"))) + " ");
                printDataFormatter.addLineSeparator();

            } else {
                printDataFormatter.addContent("Subtotal : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new((Double.valueOf(orderResponse.getString("total_amount")) + Double.valueOf(orderResponse.getString("cash_discount")) - Double.valueOf(orderResponse.getString("tax")))) + " ");
                printDataFormatter.addLineSeparator();

            }

            try {
                if (orderResponse.has("ad_info")) {
                    /*[{\"name\":\"10% off\",\"type_value\":null}]"*/

                    JSONArray array = new JSONArray(orderResponse.getString("ad_info"));
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        printDataFormatter.addContent(obj.getString("name") + " (" + obj.getString("type_value") + ")");
                        printDataFormatter.addRightAlign();
                        printDataFormatter.addContent("-$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("ad_total"))) + "");
                        printDataFormatter.addLineSeparator();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {


                printDataFormatter.addContent("Cash Discount : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("($" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("cash_discount"))) + ")");
                printDataFormatter.addLineSeparator();
            }
            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                printDataFormatter.addContent("Tax (" + Preferences.get(mContext, Preferences.TAX) + "%) :");
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("tax"))) + " ");
                printDataFormatter.addLineSeparator();
            }
            printDataFormatter.addHeader();

            printDataFormatter.addBigFont();
            printDataFormatter.addContent("Order Total : ");
            printDataFormatter.addBigFont();
            printDataFormatter.addRightAlign();
            // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("total_amount"))) + " ");
            printDataFormatter.addLineSeparator();


            if (!paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                printDataFormatter.addBigFont();
                printDataFormatter.addContent("Amount Paid : ");
                printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(paid1) + " ");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addBigFont();
                printDataFormatter.addContent("Change Due : ");
                printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + (Utility.decimal2Values_new(change)) + " ");
                printDataFormatter.addLineSeparator();
            }
            if (orderResponse.getString("order_pay_type").equalsIgnoreCase("Cash_Card")) {
                try {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("Card Paid : ");
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addRightAlign();
                    // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
                    printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderResponse.getString("card_amount"))) + " ");
                    printDataFormatter.addLineSeparator();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (Preferences.get(mContext, Preferences.ADD_TIP).equals("1")) {
                if (!orderResponse.getString("order_status").equals("Refund"))
                // if (!paymentObj.getString("payment_type").equalsIgnoreCase("Cash_Card")) {
                {
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addCenterAlign();
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addContent("Add a Tip: ");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addLeftAlign();

                    double totalAmount = Double.valueOf(orderResponse.getString("total_amount"));
                    double fifteenP = Double.valueOf(Utility.decimal2Values_new((totalAmount * 15) / 100));
                    double eighteenP = Double.valueOf(Utility.decimal2Values_new((totalAmount * 18) / 100));
                    double twentyP = Double.valueOf(Utility.decimal2Values_new((totalAmount * 20) / 100));
                    double twentyfiveP = Double.valueOf(Utility.decimal2Values_new((totalAmount * 25) / 100));
                    //double newTotal=totalAmount+
                    printDataFormatter.addLineSeparator();
                    // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
                    printDataFormatter.addContent("[   ] 15%: (Tip $" + Utility.decimal2Values_new(fifteenP) + " Total $" + Utility.decimal2Values_new(totalAmount + fifteenP) + ")");
                    printDataFormatter.addLineSeparator();
                   /* printDataFormatter.addContent("[  ] 18%: (Tip $" + Utility.decimal2Values_new(eighteenP) + " Total $" + Utility.decimal2Values_new(totalAmount + eighteenP) + ")");
                    printDataFormatter.addLineSeparator();*/
                    printDataFormatter.addContent("[   ] 20%: (Tip $" + Utility.decimal2Values_new(twentyP) + " Total $" + Utility.decimal2Values_new(totalAmount + twentyP) + ")");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addContent("[   ] 25%: (Tip $" + Utility.decimal2Values_new(twentyfiveP) + " Total $" + Utility.decimal2Values_new(totalAmount + twentyfiveP) + ")");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addContent("[   ] $ ____________" + " $ ____________");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addContent("        Custom Tip  " + "    Total      ");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addCenterAlign();
                    printDataFormatter.addContent("Tip percentage are based on the check price after taxes");
                    printDataFormatter.addLineSeparator();
                    printDataFormatter.addLineSeparator();
                }

            }


            if (copyName.equals("Merchant Copy")) {
                if (!orderResponse.getString("order_status").equals("Refund"))
                    if (Preferences.get(mContext, Preferences.ADD_TIP).equals("1")) {
                        printDataFormatter.addContent("X");
                        printDataFormatter.addTrailer();
                        printDataFormatter.addCenterAlign();
                        printDataFormatter.addContent("Signature");
                        printDataFormatter.addLineSeparator();
                        printDataFormatter.addLineSeparator();
                    } else {

                        try {
                            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                                printDataFormatter.addLineSeparator();
                                printDataFormatter.addContent("X");
                                printDataFormatter.addLineSeparator();
                                printDataFormatter.addTrailer();
                                printDataFormatter.addCenterAlign();
                                // printDataFormatter.addContent("_____________________________________________");

                                printDataFormatter.addContent("Signature");
                                printDataFormatter.addLineSeparator();
                                printDataFormatter.addLineSeparator();
                            } else if (orderResponse.getString("order_pay_type").equalsIgnoreCase("Cash_Card")) {
                                {
                                    printDataFormatter.addLineSeparator();
                                    printDataFormatter.addContent("X");
                                    printDataFormatter.addLineSeparator();
                                    printDataFormatter.addTrailer();
                                    printDataFormatter.addCenterAlign();
                                    // printDataFormatter.addContent("_____________________________________________");

                                    printDataFormatter.addContent("Signature");
                                    printDataFormatter.addLineSeparator();
                                    printDataFormatter.addLineSeparator();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
            }


            //  printDataFormatter.addTrailer();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(copyName);
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_FOOTER));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            Log.d("PrintFragment", printDataFormatter.build());
            POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
            posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
            posLinkPrinter.print(printDataFormatter.build(), this);

            // if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
            if (copyName.equals("Customer Copy")) {
                if (Preferences.get(mContext, Preferences.ADD_TIP).equals("0")) {
                    try {
                        OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                        orderItemRepository.deleteAll();
                        Preferences.save(mContext, Preferences.QSaleID, String.valueOf(00));
                        ((AppCompatActivity) mContext).recreate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else {

                }
                try {
                    OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                    orderItemRepository.deleteAll();
                    ((AppCompatActivity) mContext).recreate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {

                openConfirmPopup(orderResponse);
                /*try {
                    OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                    orderItemRepository.deleteAll();
                    ((AppCompatActivity) mContext).recreate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }*/
            }
            //}
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void callSettingsGETAPI() {
        //  view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.SettingsAPI,
                "SettingsGet", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void callGETTaxAPI() {
        //  view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.GetTax,
                "GetTax", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));
    }

    private void openCashPopup(final String order_pay_type, double totalAmount, final int type) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_cash, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            final EditText edtPaid = dialog.findViewById(R.id.edtPaid);
            final TextView texAmount = dialog.findViewById(R.id.textAMT);
            final TextView txtChange = dialog.findViewById(R.id.textChange);
            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            Button dialogButtonDismiss = (Button) dialog.findViewById(R.id.dialogButtonDismiss);
            final Button dialogButton1 = (Button) dialog.findViewById(R.id.dialogButton1);
            final Button dialogButton2 = (Button) dialog.findViewById(R.id.dialogButton2);
            final Button dialogButton3 = (Button) dialog.findViewById(R.id.dialogButton3);
            dialogButtonDismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            edtPaid.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                private String current = "";

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals(current)) {
                        edtPaid.removeTextChangedListener(this);

                        String cleanString = s.toString().replaceAll("[$,.]", "");

                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));

                        current = formatted;
                        edtPaid.setText(formatted);
                        edtPaid.setSelection(formatted.length());

                        edtPaid.addTextChangedListener(this);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            // double subtotal = 0.00, tax = 0.00, cashD = 0.00, ;
            double total = 0.00;
            try {
                //subtotal = Double.parseDouble(jsonObject.getJSONObject("data").getString("order_total"));
                total = totalAmount;//Double.valueOf(String.format("%.2f", totalAmount));
                //cashD = Double.parseDouble(jsonObject.getJSONObject("data").getString("cash_discount"));
                /*try {
                    tax = Double.parseDouble(jsonObject.getJSONObject("data").getString("tax"));

                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                final double finalTotal1 = total;
                dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!edtPaid.getText().toString().isEmpty() && !edtPaid.getText().toString().equals("0.00")) {
                            try {
                                //dialog.dismiss();
                      /*  printReceipt(jsonObject.getJSONObject("data"), "Customer Copy");
                        if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                            printReceipt(jsonObject.getJSONObject("data"), "Merchant Copy");
                        }*/
                           /* if (Double.valueOf(edtPaid.getText().toString()) >= (finalTotal1)) {
                                try {
                                    double paid = Double.parseDouble(edtPaid.getText().toString().replace("$", ""));
                                    calculate(paid, finalTotal1, txtChange);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/
                                if (Double.valueOf(edtPaid.getText().toString().replaceAll("[$,]", "")) != 0.00) {

                                    dialog.dismiss();
                                    if (Double.valueOf(edtPaid.getText().toString().replaceAll("[$,]", "")) >= (finalTotal1)) {
                                        try {
                                            double paid = Double.parseDouble(edtPaid.getText().toString().replace("$", ""));
                                            calculate(paid, finalTotal1, txtChange);
                                            // completeCashOrder("Cash","", 1);
                                            isCash = 101;
                                            HashMap<String, Object> map = new HashMap<>();
                                            // String id = (jsonObject.getJSONObject("data").getString("id"));
                                            //map.put("id", id);
                                            map.put("amount_paid", paid1);
                                            map.put("change_due", change);

                                            //callUpdateOrderAPI(map, id);
                                            //calculateCashOrder();
                                            completeCashOrderNew(order_pay_type, "", type, paid1, change);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
/*
                                        if (Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                                            printReceipt(jsonObject.getJSONObject("data"), "Merchant Copy");

                                            //openConfirmPopup(jsonObject.getJSONObject("data"));
                                            // printReceipt(result.getJSONObject("data"), "Customer Copy");
                                        } else {
                                            printReceipt(jsonObject.getJSONObject("data"), "Customer Copy");
                                        }*/
                               /* try {
                                    OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                                    orderItemRepository.deleteAll();
                                    ((AppCompatActivity) mContext).recreate();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }*/

                                    } else {
                                        edtPaid.setError("Please enter valid amount");
                                        Toast.makeText(mContext, "Please enter valid amount", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    edtPaid.setError("Required");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            edtPaid.setError("Required");
                        }


                    }

                });

             /*   double Dper = orderItemRepository.getAllDiscountPercent();
                discFixedC = orderItemRepository.getAllDiscountFixed();
                discPerC = (((subTotal + discFixedC - cashDiscount) * Dper) / 100);
                double nsubAmount = subTotal + discFixedC + discPerC;
                txtSubtotalV.setText("$" + Utility.decimal2Values((nsubAmount)));*/


                texAmount.setText("Amount Due: $" + Utility.decimal2Values_new(total));
                double number = (Math.ceil((total) / 5.00) * 5.00);
//                dialogButton1.setText("$" + Utility.decimal2Values(total));
//                dialogButton2.setText("$" + Utility.decimal2Values((number + 5.0)));
//                dialogButton3.setText("$" + Utility.decimal2Values((number + 5.0 + 5.0)));
                dialogButton1.setText("$" + Utility.decimal2Values_new(total));
                dialogButton2.setText("$" + Utility.decimal2Values_new((number + 5.0)));
                dialogButton3.setText("$" + Utility.decimal2Values_new((number + 5.0 + 5.0)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            // final TextView txtHead =  dialog.findViewById(R.id.text);
            final double finalOrderAmount = total;

            dialogButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    edtPaid.setError(null);
                    edtPaid.setText(dialogButton1.getText().toString().replace("$", ""));
                    calculate(Double.parseDouble(dialogButton1.getText().toString().replace("$", "")), finalOrderAmount, txtChange);
                }
            });
            dialogButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    edtPaid.setError(null);
                    edtPaid.setText(dialogButton2.getText().toString().replace("$", ""));
                    calculate(Double.parseDouble(dialogButton2.getText().toString().replace("$", "")), finalOrderAmount, txtChange);
                }
            });
            dialogButton3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    edtPaid.setError(null);
                    edtPaid.setText(dialogButton3.getText().toString().replace("$", ""));
                    calculate(Double.parseDouble(dialogButton3.getText().toString().replace("$", "")), finalOrderAmount, txtChange);
                }
            });
            final double finalTotal = total;
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!edtPaid.getText().toString().isEmpty() && !edtPaid.getText().toString().equals("0.00")) {
                        pb.setVisibility(View.VISIBLE);


                        if (Double.valueOf(edtPaid.getText().toString().replaceAll("[$,]", "")) >= (finalTotal)) {
                            try {
                                double paid = Double.parseDouble(edtPaid.getText().toString().replace("$", ""));
                                calculate(paid, finalOrderAmount, txtChange);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            edtPaid.setError("Please enter valid amount");
                            Toast.makeText(mContext, "Please enter valid amount", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        edtPaid.setError("Required");
                    }

                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void openConfirmPopup(final JSONObject jsonObject) {

        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_confirm, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            TextView textView = dialog.findViewById(R.id.text);
            textView.setText("Print Customer Receipt?");
            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            btnOk.setText("Yes");

            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setText("No");
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dialog.dismiss();
                        if (Preferences.get(mContext, Preferences.ADD_TIP).equals("0")) {
                            try {


                                OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                                orderItemRepository.deleteAll();
                                ((AppCompatActivity) mContext).recreate();
                                // openTipsPopup("Tips", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!");
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                        /*try {
                            OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                            orderItemRepository.deleteAll();
                            ((AppCompatActivity) mContext).recreate();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }*/
                        try {
                            OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                            orderItemRepository.deleteAll();
                            Preferences.save(mContext, Preferences.QSaleID, String.valueOf(00));
                            ((AppCompatActivity) mContext).recreate();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //pb.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                    try {
                        if (jsonObject.getString("order_pay_type").equals("Cash_Card")) {
                            printReceiptSplitTender(jsonObject, "Customer Copy");
                        } else {
                            printReceipt(jsonObject, "Customer Copy");

                        }
                        //openTipsPopup("Tips", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!");

                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }

                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void calculate(double paid, double orderAmount, TextView txtChange) {

        Log.e("CALc", "AMOUNT: " + orderAmount + "  Change: " + paid);
        change = Double.valueOf(Utility.decimal2Values(paid - orderAmount));
        paid1 = paid;
        txtChange.setText("Change Back Amount: $" + Utility.decimal2Values(paid - orderAmount));
    }

    public void callAdditionalDiscountAPI() {
        //  view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.AdditionalDiscountAPIAdd,
                "AdditionalDGet", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity())
                .setActionBarTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));

    }

    private void callUpdateOrderAPI(HashMap<String, Object> map, String id) {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postJSONRequest(Constants.BaseURL + Constants.OrdersAddAPI + "/" + id,
                "order_addU", map, Request.Method.PUT, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    private void setNumericOnClickListener() {
        // Create a common OnClickListener
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Just append/set the text of clicked button
                Button button = (Button) v;
                if (stateError) {
                    // If current state is Error, replace the error message
                    edtQuickSale.setText(button.getText());
                    stateError = false;
                } else {
                    // If not, already there is a valid expression so append to it
                    edtQuickSale.append(button.getText());
                }
                // Set the flag
                lastNumeric = true;
            }
        };
        // Assign the listener to all the numeric buttons
        for (int id : numericButtons) {
            view.findViewById(id).setOnClickListener(listener);
        }
    }

    public void showAlertDialog(final Context oContext, String title, String message, final String type) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                                if (type.equals("cash")) {
                                    trans_type = 1;
                                    completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]", "")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]", "")), 0, "Refund");
                                } else {
                                    trans_type = 1;
                                    refund();
                                }
                              /*  try {
                                if (Preferences.get(oContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {

                                        printReceipt(result.getJSONObject("data"), "Merchant Copy");


                                    openConfirmPopup(result.getJSONObject("data"));
                                    // printReceipt(result.getJSONObject("data"), "Customer Copy");
                                } else {
                                    printReceipt(result.getJSONObject("data"), "Customer Copy");
                                }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }*/
                                //((Activity)oContext).finish();
                            }
                        }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                        //openTipsPopup("Tips", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!");
                       /* try {
                            if (Preferences.get(oContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {

                                printReceipt(result.getJSONObject("data"), "Merchant Copy");


                                openConfirmPopup(result.getJSONObject("data"));
                                // printReceipt(result.getJSONObject("data"), "Customer Copy");
                            } else {
                                printReceipt(result.getJSONObject("data"), "Customer Copy");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        //((Activity)oContext).finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void openTipsPopup(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Do It Now",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.dismiss();
                               /* Preferences.clearPreference(getActivity());
                                Intent intent=new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                finish();*/

                            /*    Fragment fragment = new AllOrders();
                                // AskOptionDialog().show();
                                FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                                //Bundle bundle=new Bundle();
                                //bundle.putSerializable("Order", mList.get(position));
                                //bundle.putInt("OrderID", mList.get(position).getId());
                                //fragment.setArguments(bundle);
                                ft.replace(R.id.frame_layout, fragment, "hisory");
                                ft.addToBackStack("history1");
                                ft.commit();*/

                                //((Activity)oContext).finish();
                            }
                        }).setNegativeButton("Later",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                        try {


                            OrderItemRepository orderItemRepository = new OrderItemRepository(mContext);
                            orderItemRepository.deleteAll();
                            ((AppCompatActivity) mContext).recreate();
                            // openTipsPopup("Tips", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!");
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        //((Activity)oContext).finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}