package com.vizypay.cashdiscountapp.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.pax.poslink.PosLink;
import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.HomeAdapter;
import com.vizypay.cashdiscountapp.model.MenuPojo;
import com.vizypay.cashdiscountapp.utility.AppThreadPool;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.POSLinkCreatorWrapper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment  extends Fragment implements IApiResponse {

    RecyclerView gridView;
    HomeAdapter adapter;
    List<MenuPojo> mList;
    MenuPojo menuPojo;
    GridLayoutManager layoutManager;
    protected PosLink poslink;
    Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_fragment, container, false);
       // getActivity().setTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));

        initPOSLink();
        mContext=getActivity();
        callSettingsGETAPI();

        mList=new ArrayList<>();
    /*    menuPojo=new MenuPojo(R.drawable.register, "Register");
        menuPojo=new MenuPojo(R.drawable.reports, "Reports");
        menuPojo=new MenuPojo(R.drawable.translog, "Activity");
        menuPojo=new MenuPojo(R.drawable.inventory, "Inventory");
        menuPojo=new MenuPojo(R.drawable.settings, "Settings");
*/
        mList.add(new MenuPojo(R.drawable.register1, "Register"));
        mList.add(new MenuPojo(R.drawable.report1, "Reports"));
        mList.add(new MenuPojo(R.drawable.activity1, "Activity"));
        mList.add(new MenuPojo(R.drawable.inventory1, "Inventory"));
        mList.add(new MenuPojo(R.drawable.setting1, "Settings"));
        mList.add(new MenuPojo(R.drawable.batch2, "Batch"));
        gridView=view.findViewById(R.id.home_grid);
        layoutManager = new GridLayoutManager(getActivity(), 3);
        gridView.setLayoutManager(layoutManager);
        adapter=new HomeAdapter(mList,getActivity());

        gridView.setAdapter(adapter);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN )
                {

                    AskOptionDialog().show();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }
    private AlertDialog AskOptionDialog() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_name)
                .setMessage("Are you sure you want to exit?")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finishAffinity();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("SettingsGet")) {
            Log.d("Setting>>>", " : " + response);
            try {
                JSONObject result = new JSONObject(response);
                /*{"is_tax":1,"print_name":"Vizypay","print_address_line1":"144 29th ST. #208",
                "print_address_line2":"West Des Moines, IA 50266","print_contact_no":"8559994142",
                "print_footer_text":"THANK YOU!","merchant_copy_in_print":0}*/

                Preferences.save(mContext, Preferences.ISTAX, result.getString("is_tax").equals("null")?"0.00":result.getString("is_tax"));
                Preferences.save(mContext, Preferences.ISPRINT_MERCHANTCOPY, result.getString("merchant_copy_in_print"));
                Preferences.save(mContext, Preferences.PRINT_NAME, result.getString("print_name"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS1, result.getString("print_address_line1"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS2, result.getString("print_address_line2"));
                Preferences.save(mContext, Preferences.PRINT_CONTACT, result.getString("print_contact_no"));
                Preferences.save(mContext, Preferences.PRINT_FOOTER, result.getString("print_footer_text"));
                Preferences.save(mContext, Preferences.APP_HEADER, result.getString("app_header"));
//                Preferences.save(mContext, Preferences.CREDIT_FEES, result.getString("credit_fees"));
                Preferences.save(mContext, Preferences.FEES_PAID, result.getString("fees_paid"));
                Preferences.save(mContext, Preferences.STOCK_TRACKING, result.getString("stock_tracking"));
                Preferences.save(mContext, Preferences.STOCK_NOTIFICATION, result.getString("stock_notification"));
                Preferences.save(mContext, Preferences.ADD_TIP, result.getString("tip_enable"));
                Preferences.save(mContext, Preferences.TIP_SUGGESTION_ENABLE, result.getJSONObject("user_details").getString("tip_suggestion_enable"));
                Preferences.save(mContext, Preferences.CUSTOME_TIP_SUGGESTION_ENABLE, result.getJSONObject("user_details").getString("tip_custom_enable"));
                String user_details = result.getString("user_details");
                String program_type = result.getJSONObject("user_details").getString("program_type");
                Preferences.save(mContext, Preferences.USER_DETAIL, user_details);
                Preferences.save(mContext, Preferences.PROGRAM_TYPE, program_type);
                Preferences.save(getActivity(), Preferences.NON_CASH_CHARGE, result.getJSONObject("user_details").getString("ncc_percent"));
               /* switchCompatTax.setChecked(result.getString("is_tax").equals("1")?true:false);
                switchCompatMerchantCopy.setChecked(result.getString("merchant_copy_in_print").equals("1")?true:false);
                edtName.setText(result.getString("print_name"));
                edtFooterText.setText(result.getString("print_footer_text"));
                edtAddressLine1.setText(result.getString("print_address_line1"));
                edtAddressLine2.setText(result.getString("print_address_line2"));
                edtContact.setText(result.getString("print_contact_no"));
                view.findViewById(R.id.progressbar).setVisibility(View.GONE);*/
                /*if(result.getString("type").equals("success")){
                    clearData();
                    Toast.makeText(mContext, "Saved Successfully", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }else {
                    Toast.makeText(mContext, "Please try Again", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity())
                .setActionBarTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));

    }
    public void callSettingsGETAPI() {
        //  view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.SettingsAPI,
                "SettingsGet", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    private void initPOSLink() {
        POSLinkCreatorWrapper.createSync(getContext(), new AppThreadPool.FinishInMainThreadCallback<PosLink>() {
            @Override
            public void onFinish(PosLink result) {

                poslink = result;
                poslink.SetCommSetting(AppController.setupSetting());
                Log.d("MainActivity", "PosLink : " + poslink);
                Log.d("MainActivity", "PosLink comm: port " + poslink.GetCommSetting().getDestPort());
                Log.d("MainActivity", "PosLink comm timeout: " + poslink.GetCommSetting().getTimeOut());

                //paymentMethod();
            }
        });
    }
}
