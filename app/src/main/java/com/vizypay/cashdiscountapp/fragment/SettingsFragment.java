package com.vizypay.cashdiscountapp.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.Transliterator;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.activity.LoginActivity;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class SettingsFragment extends Fragment implements IApiResponse {

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4, floatingActionButton5, searchFAB;
    Dialog dialog;
    ListView listView;
    TextView tv_price_increase,tv_cash_discount,tv_merchant_setting;
    View view_1,view_2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.settting_tab, container, false);
        listView=view.findViewById(R.id.listView);
        final String program_type = Preferences.get(getContext(), Preferences.PROGRAM_TYPE);

        tv_price_increase = view.findViewById(R.id.tv_price_increase);
        tv_cash_discount = view.findViewById(R.id.tv_cash_discount);
        tv_merchant_setting = view.findViewById(R.id.tv_merchant_setting);
        view_1 = view.findViewById(R.id.view_1);
        view_2 = view.findViewById(R.id.view_2);

        if (program_type.equalsIgnoreCase("Traditional") || program_type.equalsIgnoreCase("CDP 1.0")){
            tv_price_increase.setVisibility(View.GONE);
            view_1.setVisibility(View.GONE);
//            tv_price_increase.setClickable(false);
            tv_price_increase.setTextColor(Color.parseColor("#D3D3D3"));
        }else {
            tv_price_increase.setVisibility(View.VISIBLE);
            view_1.setVisibility(View.VISIBLE);
//            tv_price_increase.setClickable(true);
            tv_price_increase.setTextColor(Color.parseColor("#000000"));
            tv_price_increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    priceIncreaseDialog();
                }
            });
        }

        if (program_type.equalsIgnoreCase("Traditional") || program_type.equalsIgnoreCase("CDP 1.0")){
            tv_cash_discount.setVisibility(View.GONE);
            view_2.setVisibility(View.GONE);
//            tv_cash_discount.setClickable(false);
            tv_cash_discount.setTextColor(Color.parseColor("#D3D3D3"));
        }else {
            tv_cash_discount.setVisibility(View.VISIBLE);
            view_2.setVisibility(View.VISIBLE);
//            tv_cash_discount.setClickable(true);
            tv_cash_discount.setTextColor(Color.parseColor("#000000"));
            tv_cash_discount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cashDiscountDialog();
                }
            });
        }

        tv_merchant_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment fragment = new MerchantSettingsFragment();
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "Settings");
                    ft.addToBackStack("ItemCatalog");
                    ft.commit();
                }
            }
        });




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i){
                    case 0:

                        if (program_type.equalsIgnoreCase("Traditional") || program_type.equalsIgnoreCase("CDP 1.0")){
                            view.setClickable(false);
                            view.setEnabled(false);

                        }else {
                            priceIncreaseDialog();
                        }
                        break;
                    case 1:
                        if (program_type.equalsIgnoreCase("Traditional") || program_type.equalsIgnoreCase("CDP 1.0")){
                            view.setClickable(false);
                            view.setEnabled(false);
                        }else {
                            cashDiscountDialog();
                        }
                        break;
//                    case 2:
//                        if (program_type.equalsIgnoreCase("CDP 1.0")){
//                            //view.setClickable(false);
//                            //view.setEnabled(false);
//                            NCCDialog();
//                        }else {
//                            view.setEnabled(false);
//                            view.setClickable(false);
//                            //NCCDialog();
//                        }
//                        break;
                   /* case 2:
                        additionalDiscountDialog();
                        break;*/
                  /*  case 2:
                        additionalDiscountDialog();
                        break;*/
                    case 2:
                        //if (program_type.equalsIgnoreCase("Traditional")){
                        //view.setClickable(false);
                        //view.setEnabled(false);
                        //}else {
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        Fragment fragment = new MerchantSettingsFragment();
                        if (fragment != null) {

                            ft.replace(R.id.frame_layout, fragment, "Settings");
                            ft.addToBackStack("ItemCatalog");
                            ft.commit();
                        }
                        //}

                        break;
                }
            }

        });

        materialDesignFAM = (FloatingActionMenu) view.findViewById(R.id.material_design_android_floating_action_menu);
        materialDesignFAM.setVisibility(View.GONE);
        //materialDesignFAM.setBackgroundResource(R.drawable.my_list);
        floatingActionButton1 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton1.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton2 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton2.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton3 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton3.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton4 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton4.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton5 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item5);
        floatingActionButton5.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment fragment = new MerchantSettingsFragment();
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "Settings");
                    ft.addToBackStack("ItemCatalog");
                    ft.commit();
                }
            }
        });

        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment fragment = new ItemFragment();
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                    ft.addToBackStack("Brand");
                    ft.commit();
                }
            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priceIncreaseDialog();
            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cashDiscountDialog();
            }
        });

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCategoryDialog();
            }
        });
        return view;
    }


    public void callCategoryAdd(HashMap<String, String> map){
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.CategoriesAddAPI,
                "product_add", map, Request.Method.POST, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void callPriceAPI(String percent, String type){
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.ProductsPriceAPI+"?percent="+percent+"&type="+type,
                "price_change", null, Request.Method.GET,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
    public void callDiscountAPI(String percent, String type){
        HashMap<String, String> map = new HashMap<>();
        map.put("discount", percent );
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.DiscountAPI,
                "discount", map, Request.Method.POST,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
    public void callNCC_postAPI(String ncc){
        HashMap<String, Object> map = new HashMap<>();
        map.put("ncc_percent", Double.valueOf(ncc));
        Log.e("mapmapmap", String.valueOf(map));
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postJSONRequest(Constants.BaseURL+Constants.SettingsAPI,
                "Ncc_post", map, Request.Method.POST,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("product_add")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.getJSONObject("data").has("name")) {
                    /*{"data":{"price":"123","name":"test66","units_in_stok":"23","sku":"123456","created_by_id":3,"updated_at":"2020-05-14 10:33:00","created_at":"2020-05-14 10:33:00","id":33,"main_photo":null,"additional_photos":[],"media":[]}}*/
                    JSONObject res=result.getJSONObject("data");
                    String id=res.getString("id");
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please try later", Toast.LENGTH_SHORT).show();
                    //clearData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("price_change")){
            Toast.makeText(getActivity(), "Successfully updated", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }
        if(tag_json_obj.equals("additional_discount")){

        }
        if (tag_json_obj.equals("Ncc_post")) {
            try {
                JSONObject result = new JSONObject(response);
                Log.e("result----", String.valueOf(result));
                if(result.has("name")){
                    Preferences.save(getActivity(), Preferences.NON_CASH_CHARGE, result.getString("ncc_percent"));
                    Toast.makeText(getContext(), "Saved Successfully", Toast.LENGTH_LONG);
                    dialog.dismiss();
                }else {
                    Toast.makeText(getActivity(), "Please try Again", Toast.LENGTH_LONG);
                    dialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void addCategoryDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            //dialog.setCanceledOnTouchOutside(false);
            //dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", edtCategoryName.getText().toString().trim());
                    callCategoryAdd(map);
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void priceIncreaseDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            //dialog.setCanceledOnTouchOutside(false);
            // dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Price Increase (In %)");
            edtCategoryName.setText(Preferences.get(getActivity(), Preferences.PriceInc));
            edtCategoryName.setHint("Enter Increase %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    // set title
                    alertDialogBuilder.setTitle("Price Increase");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(
                                    "Are you sure you want to increase price?")
                            .setCancelable(false)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog1,
                                                            int id) {
                                            HashMap<String, String> map = new HashMap<>();
                                            Preferences.save(getActivity(), Preferences.PriceInc, edtCategoryName.getText().toString());
                                            //  map.put("percent", edtCategoryName.getText().toString().trim());
                                            //  map.put("type", );
                                            callPriceAPI(edtCategoryName.getText().toString().trim(), "increase");
                                            //dialog.dismiss();
                                            dialog1.dismiss();
                                            dialog.dismiss();
                                        }
                                    }).setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void cashDiscountDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            //dialog.setCanceledOnTouchOutside(false);
            // dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Cash Discount (In %)");
            edtCategoryName.setText(Preferences.get(getActivity(), Preferences.CASD_Discount));
            edtCategoryName.setHint("Enter Cash Discount %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();

                    if (edtCategoryName.getText().toString()!=null)
                        Preferences.save(getActivity(), Preferences.CASD_Discount, edtCategoryName.getText().toString());
                    //  map.put("percent", edtCategoryName.getText().toString().trim());
                    //  map.put("type", );
                    callDiscountAPI(edtCategoryName.getText().toString().trim(), "increase");
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
    public void NCCDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            //dialog.setCanceledOnTouchOutside(false);
            // dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Non Cash Charge (In %)");
            int inType = edtCategoryName.getInputType(); // backup the input type
            edtCategoryName.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL); // disable soft input
            //edtCategoryName.onTouchEvent(event); // call native handler

            edtCategoryName.setText(Preferences.get(getActivity(), Preferences.NON_CASH_CHARGE));
            edtCategoryName.setHint("Enter NCC %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();

                    //if (edtCategoryName.getText().toString()!=null)
                    //Preferences.save(getActivity(), Preferences.NON_CASH_CHARGE, edtCategoryName.getText().toString());
                    Log.e("NCC valuw--",edtCategoryName.getText().toString().trim());
                    callNCC_postAPI(edtCategoryName.getText().toString().trim());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        edtCategoryName.setShowSoftInputOnFocus(false);
                    }
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void callAdditionalDiscountAPI(String percent, String name){
        HashMap<String, String> map = new HashMap<>();
        map.put("name", name );
        map.put("type_value", percent );
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.AdditionalDiscountAPIAdd,
                "additional_discount", map, Request.Method.POST,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
    public void additionalDiscountDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            //dialog.setCanceledOnTouchOutside(false);
            // dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final EditText edtPercent = (EditText) dialog.findViewById(R.id.edtPercent);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Additional Discount (In %)");
            //edtCategoryName.setText(Preferences.get(getActivity(), Preferences.CASD_Discount));
            edtCategoryName.setHint("Enter Discount Name");
            //edtPercent.setText(Preferences.get(getActivity(), Preferences.CASD_Discount));
            edtPercent.setHint("Enter Additional Discount %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();

                    if (edtCategoryName.getText().toString()!=null)
                        Preferences.save(getActivity(), Preferences.CASD_Discount, edtCategoryName.getText().toString());
                    //  map.put("percent", edtCategoryName.getText().toString().trim());
                    //  map.put("type", );
                    callAdditionalDiscountAPI(edtPercent.getText().toString() ,edtCategoryName.getText().toString().trim());
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}