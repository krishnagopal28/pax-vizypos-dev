package com.vizypay.cashdiscountapp.fragment;

import static com.vizypay.cashdiscountapp.utility.Constants.BaseURLImage;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.Order_Item_Adapter;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.model.OrderItems;

import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Signature_photo;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class Order_Detail_View extends Fragment implements IApiResponse {
    TextView tv_date, trnsactio_id, tv_time, order_result, payment_methode, transaction_type,
            card_no, card_type, tv_subtotal, tv_card_paid, tv_card_discount, tv_cash_paid, tv_tax, tv_ordeer_total,
            tv_tip, tv_tip_text, tv_15_percent, tv_18_percent, tv_20_percent, tv_25_percent, quuick_sale_amt,
            tv_ncc, tv_tax_ncc, iv_cash_paid, iv_card_paid, card_type_title, card_no_title, iv_card_dis_title,
            tv_cash_discount, iv_cash_dis_title,tv_signature;
    LinearLayout tip_layout, thnk_layout, quick_sale_amt_layout;
    RecyclerView recycleView_order_item;

    ImageView iv_dot;
    Integer Order_id;
    String tip_value, tip_suggeston_enable;
    ArrayList<OrderItems> orderItems = new ArrayList<>();
    Order_Item_Adapter order_item_adapter;
    Context mContext;
    OrderHistory orderHistory;
    Button btnSms,btnEmail,btnExit;
    ProgressBar pb;
    boolean validEmail = false;
    ImageView iv_signature;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_order_detail_view, container, false);
        mContext = getActivity();
        tv_date = view.findViewById(R.id.tv_date);
        trnsactio_id = view.findViewById(R.id.trnsactio_id);
        tv_time = view.findViewById(R.id.tv_time);
        order_result = view.findViewById(R.id.order_result);
        payment_methode = view.findViewById(R.id.payment_methode);
        transaction_type = view.findViewById(R.id.transaction_type);
        card_no = view.findViewById(R.id.card_no);
        card_type = view.findViewById(R.id.card_type);
        tv_subtotal = view.findViewById(R.id.tv_subtotal);
        tv_card_paid = view.findViewById(R.id.tv_card_paid);
        tv_card_discount = view.findViewById(R.id.tv_card_discount);
        tv_cash_paid = view.findViewById(R.id.tv_cash_paid);
        tv_tax = view.findViewById(R.id.tv_tax);
        tv_ordeer_total = view.findViewById(R.id.tv_ordeer_total);
        tv_tip = view.findViewById(R.id.tv_tip);
        tv_signature = view.findViewById(R.id.tv_signature);
        tv_tip_text = view.findViewById(R.id.tv_tip_text);
        tip_layout = view.findViewById(R.id.tip_layout);
        thnk_layout = view.findViewById(R.id.thnk_layout);
        iv_cash_dis_title = view.findViewById(R.id.iv_cash_dis_title);
        tv_cash_discount = view.findViewById(R.id.tv_cash_discount);
        tv_ncc = view.findViewById(R.id.tv_ncc);
        tv_tax_ncc = view.findViewById(R.id.tv_tax_ncc);
        iv_cash_paid = view.findViewById(R.id.iv_cash_paid);
        iv_card_paid = view.findViewById(R.id.iv_card_paid);
        card_type_title = view.findViewById(R.id.card_type_title);
        iv_card_dis_title = view.findViewById(R.id.iv_card_dis_title);
        card_no_title = view.findViewById(R.id.card_no_title);
        quuick_sale_amt = view.findViewById(R.id.quuick_sale_amt);
        quick_sale_amt_layout = view.findViewById(R.id.quick_sale_amt_layout);
        iv_dot = view.findViewById(R.id.iv_dot);
        tv_15_percent = view.findViewById(R.id.tv_15_percent);
        tv_18_percent = view.findViewById(R.id.tv_18_percent);
        tv_20_percent = view.findViewById(R.id.tv_20_percent);
        tv_25_percent = view.findViewById(R.id.tv_25_percent);
        btnSms = view.findViewById(R.id.btnSms);
        btnEmail = view.findViewById(R.id.btnEmail);
        btnExit = view.findViewById(R.id.btnExit);
        pb = view.findViewById(R.id.pb);
        iv_signature = view.findViewById(R.id.iv_signature);


        recycleView_order_item = view.findViewById(R.id.recycleView_order_item);
        recycleView_order_item.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));

        Order_id = Integer.valueOf(Preferences.get(mContext, Preferences.ORDER_ID));
        tip_value = Preferences.get(mContext, Preferences.ADD_TIP);
        tip_suggeston_enable = Preferences.get(mContext, Preferences.TIP_SUGGESTION_ENABLE);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new OrderDetail();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    bundle.putInt("OrderID", Integer.parseInt(orderHistory.getId()));
                    fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "Order");
                    ft.addToBackStack("Order1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new OrderDetail();
                // AskOptionDialog().show();
                FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                //bundle.putSerializable("Order", mList.get(position));
                bundle.putInt("OrderID", Integer.parseInt(orderHistory.getId()));
                fragment.setArguments(bundle);
                ft.replace(R.id.frame_layout, fragment, "Order");
                ft.addToBackStack("Order1");
                ft.commit();
            }
        });

        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openReceiptSMSPopup();
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openReceiptEmailPopup();
            }
        });

//        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(mContext);
//        String previouslyEncodedImage = shre.getString("image_data", "");
//        File imgFile = new  File(previouslyEncodedImage);
//
//        if(imgFile.exists()){
//
//            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//
//
//            iv_signature.setImageBitmap(myBitmap);
//        if (orderHistory.getUrl() == null){
//
//
//
//        }else {


//        Glide.with(getActivity()).load(BaseURLImage+ orderHistory.getSignaturephoto().get(0).getUrl()).into(iv_signature);
//        }



//        }
        CallApi();
        return view;
    }

    public void openReceiptEmailPopup(){
        try {
            //System.out.println("orderHistoryID--"+orderHistoryID);
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_receipt_email, null, false);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txt=dialog.findViewById(R.id.text);
            txt.setText("Customer Detail");

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final EditText edtCustname = dialog.findViewById(R.id.edtName);
            final EditText edtCustMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtComnt = dialog.findViewById(R.id.edtCommnt);
            final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
                    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                            "\\@" +
                            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                            "(" +
                            "\\." +
                            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                            ")+"
            );
            edtEmail.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if (EMAIL_ADDRESS_PATTERN.matcher(edtEmail.getText().toString()).matches() && s.length() > 0)
                    {
                        //Toast.makeText(mContext,"valid email address",Toast.LENGTH_SHORT).show();
                        // or
                        //textView.setText("valid email");
                        validEmail = true;
                        edtEmail.setError(null);
                    }
                    else
                    {
                        validEmail = false;
                        edtEmail.setError("Invalid email address");
                        // Toast.makeText(mContext,"Invalid email address",Toast.LENGTH_SHORT).show();
                        //or
                        //textView.setText("invalid email");
                    }
                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // other stuffs
                }
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // other stuffs
                }
            });

            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtEmail.getText().toString().isEmpty() || !validEmail){
                        edtEmail.setError("Please enter valid email address.");
                    }else {
                        dialog.dismiss();
                        HashMap<String, String> map = new HashMap<>();
                        map.put("type", "email");
                        map.put("order_id", String.valueOf(orderHistory.getId()));
                        map.put("email", edtEmail.getText().toString());
                        map.put("customer_name", edtCustname.getText().toString());
                        map.put("message", edtComnt.getText().toString());
                        CallOrderRecEmail(map);
                    }
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void CallOrderRecEmail(HashMap<String, String> map) {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ORDER_RECEIPT,
                "Order_receiptEmail", map, Request.Method.POST, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    private void openReceiptSMSPopup() {

        try {
            //System.out.println("orderHistoryID--"+orderHistoryID);
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_receipt_sms, null, false);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txt=dialog.findViewById(R.id.text);
            txt.setText("Customer Detail");

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final EditText edtCustname = dialog.findViewById(R.id.edtName);
            final EditText edtCustMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtComnt = dialog.findViewById(R.id.edtCommnt);



            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtCustMobile.getText().toString().isEmpty()){
                        edtCustMobile.setError("Please enter mobile number.");
                        edtCustMobile.requestFocus();
                    }else {
                        dialog.dismiss();

                        String to_number = edtCustMobile.getText().toString().trim();
                        DefaultHttpClient httpclient = new DefaultHttpClient();

                        HttpPost httppost = new HttpPost(
                                "https://api.twilio.com/2010-04-01/Accounts/"+Constants.TWILIO_ACCOUNT_SID+"/SMS/Messages.json");
                        String base64EncodedCredentials = "Basic "
                                + Base64.encodeToString(
                                (Constants.TWILIO_ACCOUNT_SID + ":" + Constants
                                        .TWILIO_TOKEN).getBytes(),
                                Base64.NO_WRAP);

                        httppost.setHeader("Authorization",
                                base64EncodedCredentials);
                        try {

                            List<BasicNameValuePair> nameValuePairs = new ArrayList<>();
                            nameValuePairs.add(new BasicNameValuePair("From",
                                    "+18455521804"));
                            nameValuePairs.add(new BasicNameValuePair("To",
                                    to_number));
                            nameValuePairs.add(new BasicNameValuePair("Body",
                                    "Here is your rerceipt "+orderHistory.getReceipt_url()+"\n"+"" +
                                            "Please click on the link"));

                            httppost.setEntity(new UrlEncodedFormEntity(
                                    nameValuePairs));

                            // Execute HTTP Post Request
                            HttpResponse response = httpclient.execute(httppost);
                            HttpEntity entity = response.getEntity();

                            System.out.println("Entity post is: "
                                    + EntityUtils.toString(entity));

                            pb.setVisibility(View.VISIBLE);

                            String status_code = String.valueOf(response.getStatusLine().getStatusCode());

                            System.out.println("status::::::"+response.getStatusLine().getStatusCode());

                            if (status_code.equalsIgnoreCase("201")){

                                pb.setVisibility(View.GONE);
                                opensucessDAilog("sucess");
                                Toast.makeText(mContext, "SMS sent sucessfully", Toast.LENGTH_SHORT).show();
                            }else {

                                pb.setVisibility(View.GONE);
                                opensucessDAilog("Fail");
                                Toast.makeText(mContext, "SMS not sent", Toast.LENGTH_SHORT).show();
                            }

                        } catch (ClientProtocolException e) {

                        } catch (IOException e) {

                        }


                    }

                }

            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void opensucessDAilog(String sucess) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_email_sms_sent, null, false);
        dialog.setContentView(view);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ImageView img_sucess = dialog.findViewById(R.id.img_sucess);
        ImageView img_fail = dialog.findViewById(R.id.img_fail);
        TextView tv_sucess = dialog.findViewById(R.id.tv_sucess);
        TextView tv_failiure = dialog.findViewById(R.id.tv_failiure);
        Button dialogButtonOKl = dialog.findViewById(R.id.dialogButtonOKl);

        if (sucess.equals("sucess")){
            img_fail.setVisibility(View.GONE);
            tv_failiure.setVisibility(View.GONE);
            img_sucess.setVisibility(View.VISIBLE);
            tv_sucess.setVisibility(View.VISIBLE);
        }else if (sucess.equals("Fail")){
            img_sucess.setVisibility(View.GONE);
            tv_sucess.setVisibility(View.GONE);
            img_fail.setVisibility(View.VISIBLE);
            tv_failiure.setVisibility(View.VISIBLE);
        }

        dialogButtonOKl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void CallApi() {

//        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/" + Order_id,
                "orders", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("orders")) {
            JSONObject result = null;
            Log.e("OrdersHistoryView", response+"");
            try {
                result = new JSONObject(response);

                final JSONObject mProductList = result.getJSONObject("data");

                Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                orderHistory = gson.fromJson(mProductList.toString(), new TypeToken<OrderHistory>() {
                }.getType());

//                List<Signature_photo> mList = new ArrayList<>();
//                mList = orderHistory.getSignaturephoto();'
//                Log.e("OrdersHistoryURL",  mList+""); orderItems = (ArrayList<OrderItems>) orderHistory.getOrderItems();
//                JSONObject jsonObj = new JSONObject(orderHistory.getSignaturephoto().toString());

                System.out.println("orderHistory>>>>" + orderHistory);
                //setValues();
                String datetime = orderHistory.getCreated_at();
                System.out.println("datetime====>>>>" + datetime);
                String[] arrayString = orderHistory.getCreated_at().split("");
                String date = arrayString[0];
                String time = arrayString[1];
                tv_date.setText(date);
                tv_time.setText(time);
//                quuick_sale_amt.setText(Utility.decimal2Values(Double.valueOf(orderHistory.getTotal_amount())));





                trnsactio_id.setText(orderHistory.getId());
                order_result.setText(orderHistory.getOrder_status());
                payment_methode.setText(orderHistory.getOrder_pay_type());
                if (orderHistory.getOrder_pay_type().equalsIgnoreCase("Cash")) {
                    card_no.setVisibility(View.GONE);
                    card_type.setVisibility(View.GONE);
                    tv_ncc.setVisibility(View.GONE);
                    tv_tax_ncc.setVisibility(View.GONE);
                    tv_cash_paid.setVisibility(View.VISIBLE);
                    tv_card_paid.setVisibility(View.GONE);
                    iv_cash_paid.setVisibility(View.VISIBLE);
                    iv_card_paid.setVisibility(View.GONE);
                    card_no_title.setVisibility(View.GONE);
                    card_type_title.setVisibility(View.GONE);
                    tv_card_discount.setVisibility(View.GONE);
                    iv_card_dis_title.setVisibility(View.GONE);
                    tv_cash_paid.setText(orderHistory.getCash_amount());
                    tv_card_paid.setText(orderHistory.getCard_amount());
                } else if (orderHistory.getOrder_pay_type().equalsIgnoreCase("Card")) {
                    card_no.setVisibility(View.VISIBLE);
                    card_type.setVisibility(View.VISIBLE);
                    tv_ncc.setVisibility(View.GONE);
                    tv_tax_ncc.setVisibility(View.VISIBLE);
                    tv_card_paid.setVisibility(View.VISIBLE);
                    tv_cash_paid.setVisibility(View.GONE);
                    iv_cash_paid.setVisibility(View.GONE);
                    iv_card_paid.setVisibility(View.VISIBLE);
                    card_no_title.setVisibility(View.VISIBLE);
                    card_type_title.setVisibility(View.VISIBLE);
                    tv_card_discount.setVisibility(View.VISIBLE);
                    iv_card_dis_title.setVisibility(View.VISIBLE);
                    card_no.setText(orderHistory.getCardNumber());
                    card_type.setText(orderHistory.getCardName());
                    tv_tax_ncc.setText(orderHistory.getNcc());
                    tv_card_discount.setText(orderHistory.getCardDiscount());
                } else if (orderHistory.getOrder_pay_type().equalsIgnoreCase("Cash_Card")) {


                }

                tv_subtotal.setText(orderHistory.getOrder_total());

                if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")) {
                    //System.out.println("if orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                    iv_cash_dis_title.setVisibility(View.GONE);
                    tv_cash_discount.setVisibility(View.GONE);


                } else {
                    //System.out.println("else orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                    iv_cash_dis_title.setVisibility(View.VISIBLE);
                    tv_cash_discount.setVisibility(View.VISIBLE);
                }
                tv_cash_discount.setText(orderHistory.getCash_discount());


                tv_tax.setText(orderHistory.getTax());
                tv_tip.setText(orderHistory.getTip_amount());
                if (orderHistory.getTip_amount().equalsIgnoreCase("0.00")) {
                    tv_tip.setVisibility(View.GONE);
                    tv_tip_text.setVisibility(View.GONE);

                } else {
                    tv_tip.setVisibility(View.VISIBLE);
                    tv_tip_text.setVisibility(View.VISIBLE);


                }
                if (tip_suggeston_enable.equalsIgnoreCase("1") || tip_suggeston_enable.equalsIgnoreCase("1")) {
                    tip_layout.setVisibility(View.VISIBLE);
                    iv_dot.setVisibility(View.VISIBLE);
                } else {
                    tip_layout.setVisibility(View.GONE);
                    iv_dot.setVisibility(View.GONE);
                }


                double amount = Double.parseDouble(orderHistory.getOrder_total());
                double res = (amount / 100.0f) * 15;
                res = Double.parseDouble(new DecimalFormat("##.##").format(res));
                tv_15_percent.setText("Tip" + " " + "$" + res + " " + " " + "Total" + " " + "$" + (res + amount));
                tv_ordeer_total.setText(orderHistory.getOrder_total());

                double amount_18 = Double.parseDouble(orderHistory.getOrder_total());
                double res_18 = (amount_18 / 100.0f) * 18;
                res_18 = Double.parseDouble(new DecimalFormat("##.##").format(res_18));
                tv_18_percent.setText("Tip" + " " + "$" + res_18 + " " + " " + "Total" + " " + "$" + (res_18 + amount_18));

                double amount_20 = Double.parseDouble(orderHistory.getOrder_total());
                double res_20 = (amount_20 / 100.0f) * 20;
                res_20 = Double.parseDouble(new DecimalFormat("##.##").format(res_20));
                tv_20_percent.setText("Tip" + " " + "$" + res_20 + " " + " " + "Total" + " " + "$" + (res_20 + amount_20));

                double amount_25 = Double.parseDouble(orderHistory.getOrder_total());
                double res_25 = (amount_25 / 100.0f) * 25;
                res_25 = Double.parseDouble(new DecimalFormat("##.##").format(res_25));
                tv_25_percent.setText("Tip" + " " + "$" + res_25 + " " + " " + "Total" + " " + "$" + (res_25 + amount_25));


                tv_ordeer_total.setText(orderHistory.getTotal_amount());

                orderItems = (ArrayList<OrderItems>) orderHistory.getOrderItems();
                order_item_adapter = new Order_Item_Adapter(mContext
                        , orderItems);
                recycleView_order_item.setAdapter(order_item_adapter);
                if (orderItems.size() == 0) {
                    quick_sale_amt_layout.setVisibility(View.VISIBLE);
                    quuick_sale_amt.setText(Utility.decimal2Values(Double.valueOf(orderHistory.getTotal_amount())));


                } else {
                    quick_sale_amt_layout.setVisibility(View.GONE);
                }


                System.out.println("OrdersHistoryURL>>>>" + mProductList.getJSONObject("signature_photo").getString("url"));
                Glide.with(getActivity()).load(BaseURLImage+ mProductList.getJSONObject("signature_photo").getString("url")).into( iv_signature);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (tag_json_obj.equals("Order_receiptEmail")){
            JSONObject result = null;
            try {
                result = new JSONObject(response);
                final JSONObject Receipt_Email = result.getJSONObject("data");
                Log.e("Receipt_Email", response);

                if (result.has("data")){

                    String sucess = "sucess";
                    opensucessDAilog(sucess);
                }else {

                    String sucess = "Fail";
                    opensucessDAilog(sucess);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

}