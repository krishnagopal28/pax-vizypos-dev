package com.vizypay.cashdiscountapp.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pax.poslink.PaymentRequest;
import com.pax.poslink.PaymentResponse;
import com.pax.poslink.PosLink;
import com.pax.poslink.ProcessTransResult;
import com.pax.poslink.peripheries.POSLinkPrinter;
import com.pax.poslink.peripheries.ProcessResult;
import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.OrderItemAdapter;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.utility.AppThreadPool;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.POSLinkCreatorWrapper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class OrderDetailBackup extends Fragment implements POSLinkPrinter.PrintListener, IApiResponse {


    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<OrderItems> orderItems;
    OrderItemAdapter orderItemAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    // TextView txtMessage;
    Bundle bundle;
    protected PosLink poslink;
    OrderHistory orderHistory;
    TextView txtName,txtMobile, txtEmail, txtOrdertotal, txtSubTotal, txtCashD, txtTax, txtOrderType;
    TextView txtcreditfees,txtcash_amount,txtcard_amount,txtDate,txtTime, txtTIP;
    Button btn_printreceipt, btnCancelTransaction, btnAddTip;
    int orderHistoryID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.all_items, container, false);
        mContext = getActivity();
        bundle=getArguments();
        if ((bundle!=null)){
            orderHistory = (OrderHistory) bundle.get("Order");
            orderHistoryID =  bundle.getInt("OrderID");
        }

        initPOSLink();
        // Setting ViewPager for each Tabs
        pb = view.findViewById(R.id.pb);
        //txtMessage = view.findViewById(R.id.txtMessage);
        view.findViewById(R.id.linearHeader).setVisibility(View.VISIBLE);
        view.findViewById(R.id.linearItemsHeader).setVisibility(View.VISIBLE);

        txtName=view.findViewById(R.id.txtName);
        txtMobile=view.findViewById(R.id.txtMobile);
        txtEmail=view.findViewById(R.id.txtEmail);
        txtOrdertotal=view.findViewById(R.id.txtOrderTotal);
        txtSubTotal=view.findViewById(R.id.txtSubTotal);
        txtCashD=view.findViewById(R.id.txtCashD);
        txtTIP=view.findViewById(R.id.txtTIP);
        txtTax=view.findViewById(R.id.txtTax);
        txtOrderType=view.findViewById(R.id.txtOrderType);
        txtcreditfees=view.findViewById(R.id.txtcreditfees);
        txtcash_amount=view.findViewById(R.id.txtcash_amount);
        txtcard_amount=view.findViewById(R.id.txtcard_amount);
        txtDate =view.findViewById(R.id.txtDate);
        txtTime =view.findViewById(R.id.txtTime);
        btn_printreceipt = view.findViewById(R.id.btn_printreceipt);
        btnCancelTransaction = view.findViewById(R.id.btnCancelTransaction);
        btnAddTip = view.findViewById(R.id.btnAddTip);
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);

        recyclerView.setVisibility(View.VISIBLE);
        //created by Krishna 24-Feb-2021
        getOrderHistoryDetail(orderHistoryID);
        btnCancelTransaction.setText("Void");
        btnCancelTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // cancelTrans();
                showAlertDialog(mContext, "Void Transaction", "Are you sure you want to void transaction?");
            }
        });



        btnAddTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(txtOrderType.getText().toString()){
                    case "Card Order":
                        openTipsPopup("CARD", orderHistory);
                        break;
                    case "Cash Order":
                        openTipsPopup("CASH", orderHistory);
                        break;
                    case "Cash_Card":
                        paymentPopupTIPS("TIPS");
                        break;
                    default:
                        break;
                }
            }
        });
        btn_printreceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printReceiptnew(txtOrderType.getText().toString());
            }
        });
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new AllOrders();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;

    }
    public void showAlertDialog(final Context oContext, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                dialog.dismiss();
                                cancelTrans();
                                //((Activity)oContext).finish();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void paymentPopup(final String order_pay_type, final String ad_id, final int type) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnCard = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button btnCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            btnCardCash.setVisibility(View.GONE);
            Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
            if(type==3){
                btnCardCash.setVisibility(View.GONE);
            }else {
                btnCardCash.setVisibility(View.VISIBLE);
            }
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // isCash = 0;
                    dialog.dismiss();
                }
            });
            dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateAPI();
                  //  isCash = 1;
                   /* if(type==3){
                        showAlertDialog(mContext, "Cash Refund", "Are you sure you want to refund amount: $ "+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "cash");
                      *//*  trans_type=1;
                        completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, "Refund");*//*
                    }else {
                        trans_type=0;
                        completeCashOrder(order_pay_type, ad_id, type);
                    }*/
                    dialog.dismiss();
                }
            });
           // final EditText edtName = dialog.findViewById(R.id.edtName);
           // final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
           // final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);
          /*  btnCardCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type==3){
                        double total= Double.valueOf(edtQuickSale.getText().toString().replace("$", ""));
                        openSummaryPopupCashCreditRefund(total);
                        //showAlertDialog(mContext, "Cash Refund", "Are you sure you want to refund amount: $ "+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "cash");
                      *//*  trans_type=1;
                        completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, "Refund");*//*
                    }else {
                        double total= Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
                        //Double.valueOf(edtQuickSale.getText().toString().replaceAll("[$,]","")
                        openSummaryPopupCashCredit("Cash/Credit Order", total, total, 0.00, 0.00, total + discFixedC);
                    }

                    dialog.dismiss();
                }
            });*/
            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // isCash = 0;
                    /*if(type==3){
                        showAlertDialog(mContext, "Credit Refund", "Are you sure you want to refund amount: $"+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "card");
                    }else {
                        trans_type=0;
                        double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04

                        double subAmount=Double.parseDouble(txtCartTotalPrice.getText().toString().replaceAll("[$,]",""));
                        Double creditFees=(subAmount*creditfee_value)-subAmount;

                        paymentMethod(order_pay_type, type, ad_id, 0, 0.00, 0.00, 0.00,
                                0.00, 0.00, 0.00, creditFees);
                        //callOrderAPI(map);
                    }*/
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paymentPopupTIPS(final String order_pay_type) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnCard = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button btnCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            btnCardCash.setVisibility(View.GONE);
            Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
          {
                btnCardCash.setVisibility(View.GONE);
            }
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // isCash = 0;
                    dialog.dismiss();
                }
            });
            dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openTipsPopup("CASH", orderHistory);

                    dialog.dismiss();
                }
            });
            // final EditText edtName = dialog.findViewById(R.id.edtName);
            // final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            // final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);

            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openTipsPopup("CARD", orderHistory);
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void openTipsPopup(final String order_pay_type, OrderHistory orderHistory ) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_cash_tips, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            TextView txtAmnt= dialog.findViewById(R.id.textAMT);
            final TextView txtChange= dialog.findViewById(R.id.textChange);
            Button btnCalculate = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button btn1 = (Button) dialog.findViewById(R.id.dialogButton1);
            Button btn2 = (Button) dialog.findViewById(R.id.dialogButton2);
            Button btn3 = (Button) dialog.findViewById(R.id.dialogButton3);
            Button btn4 = (Button) dialog.findViewById(R.id.dialogButton4);
            Button btnCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            Button btnSubmit = (Button) dialog.findViewById(R.id.dialogButtonSubmit);
            final EditText editText= dialog.findViewById(R.id.edtPaid);
            final Double amount=Double.valueOf(orderHistory.getTotal_amount());
            final Double[] newAmount = {0.00};
            txtAmnt.setText("$"+amount);
            btnCalculate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*Double.valueOf(editText.getText().toString()))/100);
                    txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*15)/100);
                    txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*18)/100);
                    txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*20)/100);
                    txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*25)/100);
                    txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   //if (txtChange.getText().toString().contains("$"))
                   {
                        if (order_pay_type.equals("Card Order")) {

                            updateTipToPAXServer(Utility.decimal2Values(newAmount[0]), amount + newAmount[0]);
                        } else if (order_pay_type.equals("Cash Order")) {
                            updateOrderAPI(Utility.decimal2Values(newAmount[0]), order_pay_type);
                        } else {
                            updateOrderAPI(Utility.decimal2Values(newAmount[0]), "CASH");
                        }
                    }

                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateTipToPAXServer(final String tipAmount, double newAmount) {
        new AsyncTask<Void, Void , String>(){

            @Override
            protected String doInBackground(Void... voids) {
                addTipAmount(tipAmount);
                return null;
            }
        }.execute();
    }

    public void cancelTrans(){
        String num = null;
        //if (!orderHistory.getOrder_pay_type().equals("Cash"))
        {
            try {
                num = orderHistory.getTransaction_no();
//                Log.e("TransNo", orderHistory.getTransaction_no());
            } catch (Exception e) {
                try {
                    JSONObject obj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
                    num = obj.getString("ref_num");
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                    //updateAPI();
                }
                e.printStackTrace();
            }

            if (num!=null) {
                try {
                    final String finalNum = num;
                    new AsyncTask<Void, Void, String>() {
                        String msg, done = null;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            /*       Intent intent=new Intent(mContext, MainActivity.class);
                             *//* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*//*
                        intent.putExtra("Fragment", "OrderHistory");
                        ((AppCompatActivity)mContext).startActivity(intent);
                        ((AppCompatActivity)mContext).finish();*/
                        }

                        @Override
                        protected String doInBackground(Void... voids) {

                            PaymentRequest paymentRequest = new PaymentRequest();
                            paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                            paymentRequest.TransType = paymentRequest.ParseTransType("VOID");
                            paymentRequest.ECRRefNum = "1";
                            paymentRequest.OrigRefNum = finalNum;
                            poslink.PaymentRequest = paymentRequest;

                            try {
                                ProcessTransResult result = poslink.ProcessTrans();
                                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                                    Log.e("RESP", paymentResponse.ResultTxt);
                                    done = "yes";
                                } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                    //lblMsg.Text = "Action Timeout.";
                                    // Log.e("TimeOut", "Action Timeout.");
                                    msg = "Action TimeOut";
                                    done = msg;
                                } else {
                                    //Log.e("Result MSG", result.Msg);
                                    msg = result.Msg;
                                    done = msg;
                                    // lblMsg.Text = result.Msg;
                                }
                            } catch (Exception e) {
                                done = null;
                                e.printStackTrace();
                            }
                        /*ProcessTransResult result = poslink.ProcessTrans();
                        try {
                            if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                PaymentResponse paymentResponse = poslink.PaymentResponse;
                                Log.e("RESP", paymentResponse.ResultTxt);
                            } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                //lblMsg.Text = "Action Timeout.";
                                // Log.e("TimeOut", "Action Timeout.");
                                msg = "Action TimeOut";
                            } else {
                                //Log.e("Result MSG", result.Msg);
                                msg = result.Msg;
                                // lblMsg.Text = result.Msg;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            Log.e("PAX_RESP", msg);
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                            return done;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            if (s.equals("yes")) {
                                updateAPI();
                            } else {
                                Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
                            }

                        }
                    }.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                updateAPI();
            }
        }
    }

    public void addTipAmount(final String tipAmount){
        String num = null;
        //if (!orderHistory.getOrder_pay_type().equals("Cash"))
        {
            try {
                num = orderHistory.getTransaction_no();
//                Log.e("TransNo", orderHistory.getTransaction_no());
            } catch (Exception e) {
                try {
                    JSONObject obj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
                    num = obj.getString("ref_num");
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                    //updateAPI();
                }
                e.printStackTrace();
            }

            if (num!=null) {
                try {
                    final String finalNum = num;
                    new AsyncTask<Void, Void, String>() {
                        String msg, done = null;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            /*       Intent intent=new Intent(mContext, MainActivity.class);
                             *//* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*//*
                        intent.putExtra("Fragment", "OrderHistory");
                        ((AppCompatActivity)mContext).startActivity(intent);
                        ((AppCompatActivity)mContext).finish();*/
                        }

                        @Override
                        protected String doInBackground(Void... voids) {

                            PaymentRequest paymentRequest = new PaymentRequest();
                            paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                            paymentRequest.TransType = paymentRequest.ParseTransType("TRANSACTION ADJUSTMENT");
                            paymentRequest.ECRRefNum = "1";
                            paymentRequest.OrigRefNum = finalNum;
                            paymentRequest.TipAmt=String.valueOf(tipAmount);
                            poslink.PaymentRequest = paymentRequest;

                            try {
                                ProcessTransResult result = poslink.ProcessTrans();
                                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                                    Log.e("RESP", paymentResponse.ResultTxt);
                                    done = "yes";
                                } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                    //lblMsg.Text = "Action Timeout.";
                                    // Log.e("TimeOut", "Action Timeout.");
                                    msg = "Action TimeOut";
                                    done = msg;
                                } else {
                                    //Log.e("Result MSG", result.Msg);
                                    msg = result.Msg;
                                    done = msg;
                                    // lblMsg.Text = result.Msg;
                                }
                            } catch (Exception e) {
                                done = null;
                                e.printStackTrace();
                            }
                        /*ProcessTransResult result = poslink.ProcessTrans();
                        try {
                            if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                PaymentResponse paymentResponse = poslink.PaymentResponse;
                                Log.e("RESP", paymentResponse.ResultTxt);
                            } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                //lblMsg.Text = "Action Timeout.";
                                // Log.e("TimeOut", "Action Timeout.");
                                msg = "Action TimeOut";
                            } else {
                                //Log.e("Result MSG", result.Msg);
                                msg = result.Msg;
                                // lblMsg.Text = result.Msg;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            Log.e("PAX_RESP", msg);
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                            return done;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            if (s.equals("yes")) {
                                updateOrderAPI(String.valueOf(tipAmount), "Card");
                            } else {
                                Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
                            }

                        }
                    }.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                updateAPI();
            }
        }
    }

    public void setValues(){
        try {
            if(orderHistory.getName() != null && !orderHistory.getName().isEmpty()){
                txtName.append(orderHistory.getName());
            }else{   txtName.setVisibility(View.GONE);        }

            if(orderHistory.getMobile() != null && !orderHistory.getMobile().isEmpty()){
                txtMobile.append(orderHistory.getMobile());
            }else{   txtMobile.setVisibility(View.GONE);        }

            if(orderHistory.getEmail() != null && !orderHistory.getEmail().isEmpty()){
                txtEmail.append(orderHistory.getEmail());
            }else{   txtEmail.setVisibility(View.GONE);        }

            String datetime = orderHistory.getCreated_at();
            if(datetime != null && !datetime.isEmpty()){
                //2021-01-29 07:18:31
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
                Date time = localDateFormat.parse((datetime));
                txtTime.append(String.valueOf(new SimpleDateFormat("HH:mm").format(time)));

                SimpleDateFormat localDateFormatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date times = localDateFormatDate.parse((datetime));
                txtDate.append(String.valueOf(new SimpleDateFormat("MM-dd-yyyy").format(times)));

            }else {

            }



            txtOrdertotal.append("$"+orderHistory.getTotal_amount());
            txtTIP.append(" $"+ orderHistory.getTip_amount());
            if(orderHistory.getOrder_pay_type().equals("Cash_Card")){
                txtCashD.setVisibility(View.VISIBLE);
                txtcreditfees.setVisibility(View.VISIBLE);
                txtcash_amount.setVisibility(View.VISIBLE);
                txtcard_amount.setVisibility(View.VISIBLE);

                txtOrderType.setText("Cash/Card Order");
                txtCashD.append("$" + orderHistory.getCash_discount());
                txtcreditfees.append("$" +orderHistory.getCredit_fees());
                txtcash_amount.append("$" +orderHistory.getCash_amount());
                txtcard_amount.append("$" +orderHistory.getCard_amount());
            }else {
                if(orderHistory.getPayment_details().contains("Cash")) {
                    txtCashD.setVisibility(View.VISIBLE);
                    txtcreditfees.setVisibility(View.GONE);
                    txtcash_amount.setVisibility(View.GONE);
                    txtcard_amount.setVisibility(View.GONE);
                    txtOrderType.setText("Cash Order");
                    txtCashD.append("$" + orderHistory.getCash_discount());
                }else {
                    txtCashD.setVisibility(View.GONE);
                    txtcreditfees.setVisibility(View.GONE);
                    txtcash_amount.setVisibility(View.GONE);
                    txtcard_amount.setVisibility(View.GONE);
                    txtOrderType.setText("Card Order");
                }
            }

            txtTax.append("$"+orderHistory.getTax());
            txtSubTotal.append("$"+orderHistory.getOrder_total());

            mLayoutManager = new GridLayoutManager(getActivity(), 1);
            recyclerView.setLayoutManager(mLayoutManager);
           // pb.setVisibility(View.VISIBLE);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (orderHistory.getOrderItems().size()>0) {
            orderItemAdapter = new OrderItemAdapter(orderHistory.getOrderItems(), mContext);
            recyclerView.setAdapter(orderItemAdapter);
            //callProductsAPI();
        }

    }

    public void getOrderHistoryDetail(Integer orderHistoryID){
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/"+orderHistoryID,
                "orders", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
    public void updateAPI(){
        HashMap<String, String> map = new HashMap<>();
        map.put("order_status", "Cancelled");
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/"+orderHistoryID,
                "orders_update", map,Request.Method.PUT, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
  public void updateOrderAPI(String amnt,  String transType){
        HashMap<String, String> map = new HashMap<>();
        map.put("tip_amount", amnt);
        map.put("tip_trans_type", transType);
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/"+orderHistoryID,
                "order_update", map,Request.Method.PUT, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    private void initPOSLink() {
        POSLinkCreatorWrapper.createSync(getContext(), new AppThreadPool.FinishInMainThreadCallback<PosLink>() {
            @Override
            public void onFinish(PosLink result) {

                poslink = result;
                poslink.SetCommSetting(AppController.setupSetting());
                Log.d("MainActivity", "PosLink : " + poslink);
                Log.d("MainActivity", "PosLink comm: port " + poslink.GetCommSetting().getDestPort());
                Log.d("MainActivity", "PosLink comm timeout: " + poslink.GetCommSetting().getTimeOut());

                //paymentMethod();
            }
        });
    }
    private void printReceiptnew(String copyName) {

        try {
            POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();

            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_NAME).toUpperCase());
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS1));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS2));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_CONTACT));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();


            printDataFormatter.addContent("Transaction id");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("#"+orderHistory.getId());
            printDataFormatter.addLineSeparator();


            // System.out.println("Payment Details : " + orderResponse.getString("payment_details"));
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(orderHistory.getCreated_at().split(" ")[0]);
            printDataFormatter.addRightAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(orderHistory.getCreated_at().split(" ")[1]);
            printDataFormatter.addLineSeparator();

//            JSONObject paymentObj = new JSONObject(orderResponse.getString("payment_details").replaceAll("\\\\", ""));
//            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
//                try {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent(("Result"));
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent(("Approved"));
//                    printDataFormatter.addLineSeparator();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

            JSONObject paymentObj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                try {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(("Result"));
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent(("Approved"));
                    printDataFormatter.addLineSeparator();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //printDataFormatter.addBigFont();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Method:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent(paymentObj.getString("payment_type"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("Sale");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card No : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("account_num"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Type : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("card_type"));
                printDataFormatter.addLineSeparator();
            } else {
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Method:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent(paymentObj.getString("payment_type"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("Sale");
                printDataFormatter.addLineSeparator();
            }



            if(orderHistory.getOrder_pay_type().equals("Cash_Card")){
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent("Transaction Method:");
//                //printDataFormatter.addBigFont();
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("Cash/Card Order:");
//                printDataFormatter.addLineSeparator();
//
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent("Transaction Type:");
//                //printDataFormatter.addBigFont();
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("Sale");
//                printDataFormatter.addLineSeparator();
/*
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Credit Fees:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCredit_fees());
                printDataFormatter.addLineSeparator();*/

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Cash Paid:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCash_amount());
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Paid:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCard_amount());
                printDataFormatter.addLineSeparator();

            }else {
//                if(orderHistory.getPayment_details().contains("Cash")) {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Method:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Cash:");
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Type:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Sale");
//                    printDataFormatter.addLineSeparator();
//
//
//                }else {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Method:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Card:");
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Type:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Sale");
//                    printDataFormatter.addLineSeparator();
//
//                }

            }
            printDataFormatter.addLineSeparator();

            List<OrderItems> mList = new ArrayList<>();
            mList = orderHistory.getOrderItems();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent("Items:");
            printDataFormatter.addLineSeparator();
            for (int i = 0; i < mList.size(); i++) {
                printDataFormatter.addNormalFont();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(mList.get(i).getQuantity() + " X " + mList.get(i).getProduct_name());
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("$" + mList.get(i).getTotal_amount());
                printDataFormatter.addLineSeparator();
            }

            printDataFormatter.addHeader();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            printDataFormatter.addContent("Subtotal : ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + orderHistory.getOrder_total() + " ");
            printDataFormatter.addLineSeparator();

            printDataFormatter.addSmallFont();
            printDataFormatter.addContent("Cash Discount:");
            //printDataFormatter.addBigFont();
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + orderHistory.getCash_discount());
            printDataFormatter.addLineSeparator();

            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Tax (" + Preferences.get(mContext, Preferences.TAX) + "%) :");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getTax());
                printDataFormatter.addLineSeparator();
            }


            printDataFormatter.addHeader();

            printDataFormatter.addBigFont();
            printDataFormatter.addContent("Order Total : ");
            printDataFormatter.addBigFont();
            printDataFormatter.addRightAlign();
            // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
            printDataFormatter.addContent("$" + orderHistory.getTotal_amount() + " ");
            printDataFormatter.addLineSeparator();

            if (orderHistory.getOrder_pay_type().equals("Cash")) {
                printDataFormatter.addBigFont();
                printDataFormatter.addContent("Amount Paid : ");
                printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getAmount_paid() + " ");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addBigFont();
                printDataFormatter.addContent("Change Due : ");
                printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getChange_due() + " ");
                printDataFormatter.addLineSeparator();
            }else {

            }
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addTrailer();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(copyName);
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_FOOTER));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            Log.d("PrintFragment", printDataFormatter.build());
            POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
            posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
            posLinkPrinter.print(printDataFormatter.build(),this);

         /*   if(copyName.equals("Customer Copy"))
                if(Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                    printReceipt(orderResponse, "Merchant Copy");
                }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(ProcessResult processResult) {

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("orders")) {
            JSONObject result = null;
            try {
                result = new JSONObject(response);

                final JSONObject mProductList = result.getJSONObject("data");

                Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                orderHistory = gson.fromJson(mProductList.toString(), new TypeToken<OrderHistory>() {
                }.getType());

                setValues();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (tag_json_obj.equals("orders_update")){
        /* *//*   FragmentManager fm = ((FragmentActivity) getActivity()).getSupportFragmentManager();
            fm.popBackStack();*//*
            Fragment fragment = new AllOrders();
            if (fragment != null) {
                FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "AllProduct");
                ft.commit();
            }*/
            Intent intent=new Intent(mContext, MainActivity.class);
           /* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*/
            intent.putExtra("Fragment", "OrderHistory");
            ((AppCompatActivity)mContext).startActivity(intent);
            ((AppCompatActivity)mContext).finish();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

  /*  public void callProductsAPI() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI + "?limit=200",
                "products", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
*/

   /* @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("products")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mProductResponseList = gson.fromJson(mProductList.toString(), new TypeToken<List<Products>>() {
                    }.getType());
*/


              /*  } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/

}