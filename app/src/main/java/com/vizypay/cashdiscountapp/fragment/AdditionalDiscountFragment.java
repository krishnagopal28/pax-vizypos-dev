package com.vizypay.cashdiscountapp.fragment;

        import android.app.Dialog;
        import android.content.Context;
        import android.database.sqlite.SQLiteDatabase;
        import android.graphics.drawable.ColorDrawable;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.KeyEvent;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.Window;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ProgressBar;
        import android.widget.Spinner;
        import android.widget.TextView;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.fragment.app.Fragment;
        import androidx.fragment.app.FragmentTransaction;
        import androidx.recyclerview.widget.GridLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;

        import com.android.volley.Request;
        import com.android.volley.VolleyError;
        import com.google.gson.Gson;
        import com.google.gson.GsonBuilder;
        import com.google.gson.reflect.TypeToken;
        import com.vizypay.cashdiscountapp.R;
        import com.vizypay.cashdiscountapp.adapter.AllAdditionalDiscountAdapter;
        import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
        import com.vizypay.cashdiscountapp.utility.Constants;
        import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
        import com.vizypay.cashdiscountapp.utility.Preferences;
        import com.vizypay.cashdiscountapp.utility.Utility;
        import com.vizypay.cashdiscountapp.volley.ApiRequest;
        import com.vizypay.cashdiscountapp.volley.IApiResponse;

        import org.json.JSONArray;
        import org.json.JSONObject;

        import java.util.HashMap;
        import java.util.List;

public class AdditionalDiscountFragment extends Fragment implements IApiResponse, AllAdditionalDiscountAdapter.AllAdditionalDscountListener {


    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<AdditionalDiscount> mCategoryList;
    AllAdditionalDiscountAdapter allAdditionalDiscountAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    TextView txtMessage;
    Button btnTaxAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.all_items, container, false);
        // Setting ViewPager for each Tabs
        pb = view.findViewById(R.id.pb);
        txtMessage = view.findViewById(R.id.txtMessage);

        mContext = getActivity();
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        pb.setVisibility(View.VISIBLE);
        btnTaxAll=view.findViewById(R.id.btnTaxAll);
        btnTaxAll.setVisibility(View.VISIBLE);
        btnTaxAll.setText("Add New");
        btnTaxAll.setCompoundDrawables(null,null,null,null);
        btnTaxAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                additionalDiscountDialog();
            }
        });
       // view.findViewById(R.id.revertPriceLayout).setVisibility(View.GONE);

        callApi();
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new ItemCatalogFragment();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;

    }

    public void callApi() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.AdditionalDiscountGET + "?limit=200",
                "additional_discount", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equals("additional_discount_add")){
            callApi();
        }

        if (tag_json_obj.equals("additional_discount")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mCategoryList = gson.fromJson(mProductList.toString(), new TypeToken<List<AdditionalDiscount>>() {
                    }.getType());

                    allAdditionalDiscountAdapter = new AllAdditionalDiscountAdapter(mCategoryList, mContext, this);
                    recyclerView.setAdapter(allAdditionalDiscountAdapter);

                } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onCategoryUpdated() {
        callApi();
    }

    @Override
    public void onCategoryDeleted() {
        callApi();
    }

    public void callAdditionalDiscountADDAPI(String value, String name, String type){
        HashMap<String, String> map = new HashMap<>();
        map.put("name", name );
        map.put("type_value", value );
        map.put("type", type );
       // map.put("type_value", percent );
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.AdditionalDiscountAPIAdd,
                "additional_discount_add", map, Request.Method.POST,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void additionalDiscountDialog() {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_additional_discount, null, false);
            //dialog.setCanceledOnTouchOutside(false);
            // dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final EditText edtPercent = (EditText) dialog.findViewById(R.id.edtPercent);
            final EditText edtFixed = (EditText) dialog.findViewById(R.id.edtFixed);
            final Spinner spinType = (Spinner) dialog.findViewById(R.id.spinType);
            edtPercent.setVisibility(View.VISIBLE);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Additional Discount (In %)");
            //edtCategoryName.setText(Preferences.get(getActivity(), Preferences.CASD_Discount));
            edtCategoryName.setHint("Enter Discount Name");
            //edtPercent.setText(Preferences.get(getActivity(), Preferences.CASD_Discount));
            edtPercent.setHint("Enter Additional Discount %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();

                    if (edtCategoryName.getText().toString()!=null)
                        // Preferences.save(getActivity(), Preferences.CASD_Discount, edtCategoryName.getText().toString());
                        //  map.put("percent", edtCategoryName.getText().toString().trim());
                        //  map.put("type", );
                        if (spinType.getSelectedItemPosition()==0) {
                            callAdditionalDiscountADDAPI(edtFixed.getText().toString(), edtCategoryName.getText().toString().trim(), "Fixed");
                        }else {
                            callAdditionalDiscountADDAPI(edtPercent.getText().toString(), edtCategoryName.getText().toString().trim(), "Percent");
                        }
                    dialog.dismiss();
                }
            });

            spinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position==0){
                        edtFixed.setVisibility(View.VISIBLE);
                        edtPercent.setVisibility(View.GONE);
                    }else if (position==1){
                        edtPercent.setVisibility(View.VISIBLE);
                        edtFixed.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}