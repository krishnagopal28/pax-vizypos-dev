package com.vizypay.cashdiscountapp.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.pax.poslink.peripheries.DeviceModel;
import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Validation;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;


/**
 * Created by Richa on 10/03/17.
 */

public class MerchantSettingsFragment extends Fragment implements IApiResponse {
    EditText edtName, edtFooterText, edtFeesPAid, edtAddressLine1, edtAddressLine2, edtContact, edtapp_header, edtMID;
    TextView edtTip, edtTipsuggestion;
    SwitchCompat switchCompatTax, switchCompatMerchantCopy, switchNMinQty, switchStockTracking,switchTip, switchTipsuggestion,switchcustomeTipsuggestion;

    Button btnSubmit, btnTaxAll, btnLinkDevice;

    Context mContext;
    View view;
    String tax;
    String enb_dis;
    RelativeLayout tip_suggetion_rl,custome_tip_suggetion_rl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.merchant_settings, null);
        mContext = getActivity();
        getActivity().setTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));


        initComponents(view);

        callSettingsGETAPI();
        if (Preferences.get(mContext, Preferences.KEY_APP_NAME_EN).equals("1")) {
            btnTaxAll.setVisibility(View.VISIBLE);
        } else {
            btnTaxAll.setVisibility(View.INVISIBLE);
        }
        btnTaxAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTaxDialog();
            }
        });

        btnLinkDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDeviceInfoDialog();
            }
        });

        btnSubmit.setText("Save Settings");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chechValidation()) {

                    String myFormat = "yyyy-MM-dd"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

                    HashMap<String, Object> map = new HashMap<>();
                    JSONObject jsonObject = new JSONObject();

                    try {
                        jsonObject.put("app_header", edtapp_header.getText().toString().trim());
                        jsonObject.put("print_name", edtName.getText().toString().trim());
                        jsonObject.put("print_address_line1", edtAddressLine1.getText().toString().trim());
                        jsonObject.put("print_address_line2", edtAddressLine2.getText().toString().trim());
                        jsonObject.put("print_contact_no", edtContact.getText().toString().trim());
                        jsonObject.put("print_footer_text", edtFooterText.getText().toString().trim());
                        jsonObject.put("fees_paid", edtFeesPAid.getText().toString().trim());
                        //jsonObject.put("fees_paid", edtFeesPAid.getText().toString().trim());
                        jsonObject.put("merchant_copy_in_print", switchCompatMerchantCopy.isChecked() ? "1" : "0");
                        jsonObject.put("tip_enable", switchTip.isChecked() ? "1" : "0");
//                        jsonObject.put("tip_suggestion_enable", switchTipsuggestion.isChecked() ? "1" : "0");
                        jsonObject.put("tip_suggestion_enable", switchTipsuggestion.isChecked() ? "1" : "0");
                        jsonObject.put("tip_custom_enable", switchcustomeTipsuggestion.isChecked() ? "1" : "0");
                        jsonObject.put("stock_tracking", switchStockTracking.isChecked() ? "1" : "0");
                        jsonObject.put("stock_notification", switchNMinQty.isChecked() ? "1" : "0");
                        jsonObject.put("is_tax", switchCompatTax.isChecked() ? "1" : "0");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //map.put( jsonObject.toString());
                    callSettingsAPI(jsonObject);

                    //Toast.makeText(mContext, "Edited Successfully", Toast.LENGTH_LONG);

                }
                //uploadDataToServer();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new SettingsFragment();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }

    public void callSettingsAPI(JSONObject map) {
        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postJSONRequest1(Constants.BaseURL + Constants.SettingsAPI,
                "SettingsPost", map, Request.Method.POST, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void callSettingsGETAPI() {
        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.SettingsAPI,
                "SettingsGet", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
   /* public void callTaxAPI(String tax,int id) {
        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
       // btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.TaxAPI+"?tax="+tax+"&ids[]="+id,
                "product_tax", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }*/

    public boolean chechValidation() {
        boolean ret = true;
        if (!Validation.hasText(edtapp_header))
            ret = false;
        if (!Validation.hasText(edtName))
            ret = false;
        if (!Validation.hasText(edtFooterText))
            ret = false;
        //  if (!Validation.hasText(edtSKU))
        // ret = false;
        if (!Validation.hasText(edtAddressLine1)) {
            //Utility.openCustomDialog(getActivity(), getActivity().getResources().getString(R.string.address_req));
            ret = false;
        }
        if (!Validation.hasText(edtAddressLine2)) {
            //Utility.openCustomDialog(getActivity(), getActivity().getResources().getString(R.string.address_req));
            ret = false;
        }
        if (!Validation.hasText(edtContact)) {
            //Utility.openCustomDialog(getActivity(), getActivity().getResources().getString(R.string.address_req));
            ret = false;
        }

        return ret;
    }

    public void initComponents(View view) {
        try {
            //radiogroup = (RadioGroup) view.findViewById(R.id.radiogroup);
            edtContact = (EditText) view.findViewById(R.id.edtContactNo);
            edtapp_header = (EditText) view.findViewById(R.id.edtapp_header);
            edtAddressLine1 = (EditText) view.findViewById(R.id.edtAddressLine1);
            edtAddressLine2 = (EditText) view.findViewById(R.id.edtAddressLine2);
            edtName = (EditText) view.findViewById(R.id.edtName);
            edtFooterText = (EditText) view.findViewById(R.id.edtFooterText);
            edtFeesPAid = (EditText) view.findViewById(R.id.edtFeesPaid);
            edtMID = (EditText) view.findViewById(R.id.edtMID);
            edtTip = view.findViewById(R.id.edtTip);
            edtTipsuggestion = view.findViewById(R.id.edtTipsuggestion);
            tip_suggetion_rl = view.findViewById(R.id.tip_suggetion_rl);
            custome_tip_suggetion_rl = view.findViewById(R.id.custome_tip_suggetion_rl);


            edtFeesPAid.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    // if (!disabled) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(edtFeesPAid, InputMethodManager.SHOW_FORCED);
                    // }
                }
            });
            edtMID.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    // if (!disabled) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(edtMID, InputMethodManager.SHOW_FORCED);
                    // }
                }
            });

            switchCompatMerchantCopy = view.findViewById(R.id.switchMerchant);
            switchTip = view.findViewById(R.id.switchTip);
            switchTipsuggestion = view.findViewById(R.id.switchTipsuggestion);
            switchcustomeTipsuggestion = view.findViewById(R.id.switchcustomeTipsuggestion);

            switchTip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (switchTip.isChecked()){

                        switchTipsuggestion.setEnabled(true);
                        switchcustomeTipsuggestion.setEnabled(true);
                        tip_suggetion_rl.setVisibility(View.VISIBLE);

                        custome_tip_suggetion_rl.setVisibility(View.VISIBLE);

                    }else {

                        switchTipsuggestion.setEnabled(false);
                        switchcustomeTipsuggestion.setEnabled(false);
                        switchTipsuggestion.setChecked(false);
                        switchcustomeTipsuggestion.setChecked(false);
                        tip_suggetion_rl.setVisibility(View.GONE);
                        custome_tip_suggetion_rl.setVisibility(View.GONE);
                    }
                }
            });

            if (Preferences.get(mContext, Preferences.ADD_TIP).equals("0")) {
                switchTipsuggestion.setEnabled(false);
                tip_suggetion_rl.setVisibility(View.GONE);
                custome_tip_suggetion_rl.setVisibility(View.GONE);
                switchTipsuggestion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                        switchTipsuggestion.setChecked(false);
                    }
                });

            } else {
                switchTipsuggestion.setEnabled(true);
                tip_suggetion_rl.setVisibility(View.VISIBLE);
                custome_tip_suggetion_rl.setVisibility(View.VISIBLE);
                switchTipsuggestion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                        switchTipsuggestion.setChecked(true);
                    }
                });

            }

            if (Preferences.get(mContext, Preferences.ADD_TIP).equals("0")) {
                switchcustomeTipsuggestion.setEnabled(false);
                custome_tip_suggetion_rl.setVisibility(View.GONE);
                switchcustomeTipsuggestion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                        switchTipsuggestion.setChecked(false);
                    }
                });

            } else {
                switchcustomeTipsuggestion.setEnabled(true);
                custome_tip_suggetion_rl.setVisibility(View.VISIBLE);
                switchcustomeTipsuggestion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                        switchTipsuggestion.setChecked(true);
                    }
                });

            }


            System.out.println("Add_TIP" + Preferences.get(mContext, Preferences.ADD_TIP));
            System.out.println("TIP_SUGGETION" + Preferences.get(mContext, Preferences.TIP_SUGGESTION_ENABLE));


            switchCompatTax = view.findViewById(R.id.switchTax);
            switchNMinQty = view.findViewById(R.id.switchNMinQty);
            switchStockTracking = view.findViewById(R.id.switchStockTracking);

            btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
            btnLinkDevice = (Button) view.findViewById(R.id.btnLinkDevice);
            btnTaxAll = (Button) view.findViewById(R.id.btnTaxAll);
            //btnTaxAll.append(Preferences.get(mContext, Preferences.));

            btnTaxAll.setText("Tax: " + Preferences.get(mContext, Preferences.TAX) + "% ");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("SettingsPost")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.has("print_name")) {
                    Toast.makeText(mContext, "Saved Successfully", Toast.LENGTH_LONG);
                    // clearData();
                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Fragment fragment = new MainTabFragment();
                    if (fragment != null) {

                        ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                        //ft.addToBackStack("Brand");
                        ft.commit();
                    }
                    //
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                } else {
                    Toast.makeText(mContext, "Please try Again", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("SettingsGet")) {
            try {
                JSONObject result = new JSONObject(response);
                /*{"is_tax":1,"print_name":"Vizypay","print_address_line1":"144 29th ST. #208",
                "print_address_line2":"West Des Moines, IA 50266","print_contact_no":"8559994142",
                "print_footer_text":"THANK YOU!","merchant_copy_in_print":0}*/
               /*{"is_tax":1,"fees_paid":"3.85","print_name":"Vizypay",
               "print_address_line1":"144 29th ST. #208","print_address_line2":"West Des Moines, IA 50266",
               "print_contact_no":"8559994142","print_footer_text":"THANK YOU!","merchant_copy_in_print":1,
               "app_header":null}*/

                Preferences.save(mContext, Preferences.ISTAX, result.getString("is_tax").equals("null") ? "0.00" : result.getString("is_tax"));
                Preferences.save(mContext, Preferences.ISPRINT_MERCHANTCOPY, result.getString("merchant_copy_in_print"));
                Preferences.save(mContext, Preferences.PRINT_NAME, result.getString("print_name"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS1, result.getString("print_address_line1"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS2, result.getString("print_address_line2"));
                Preferences.save(mContext, Preferences.PRINT_CONTACT, result.getString("print_contact_no"));
                Preferences.save(mContext, Preferences.PRINT_FOOTER, result.getString("print_footer_text"));
                Preferences.save(mContext, Preferences.APP_HEADER, result.getString("app_header"));
                Preferences.save(mContext, Preferences.FEES_PAID, result.getString("fees_paid"));
                Preferences.save(mContext, Preferences.STOCK_TRACKING, result.getString("stock_tracking"));
                Preferences.save(mContext, Preferences.STOCK_NOTIFICATION, result.getString("stock_notification"));
                Preferences.save(mContext, Preferences.ADD_TIP, result.getString("tip_enable"));
                Preferences.save(mContext, Preferences.TIP_SUGGESTION_ENABLE, result.getJSONObject("user_details").getString("tip_suggestion_enable"));
                Preferences.save(mContext, Preferences.CUSTOME_TIP_SUGGESTION_ENABLE, result.getJSONObject("user_details").getString("tip_custom_enable"));
                String user_details = result.getString("user_details");
                String program_type = result.getJSONObject("user_details").getString("program_type");
                Preferences.save(mContext, Preferences.USER_DETAIL, user_details);
                Preferences.save(mContext, Preferences.PROGRAM_TYPE, program_type);

                ((MainActivity) getActivity()).setActionBarTitle(result.getString("app_header"));


                switchStockTracking.setChecked(result.getString("stock_tracking").equals("1") ? true : false);
                switchNMinQty.setChecked(result.getString("stock_notification").equals("1") ? true : false);
                switchCompatTax.setChecked(result.getString("is_tax").equals("1") ? true : false);
                switchCompatMerchantCopy.setChecked(result.getString("merchant_copy_in_print").equals("1") ? true : false);
                switchTip.setChecked(result.getString("tip_enable").equals("1") ? true : false);
                switchTipsuggestion.setChecked(result.getJSONObject("user_details").getString("tip_suggestion_enable").equals("1") ? true : false);
                switchcustomeTipsuggestion.setChecked(result.getJSONObject("user_details").getString("tip_custom_enable").equals("1") ? true : false);
                edtapp_header.setText(result.getString("app_header"));
                edtName.setText(result.getString("print_name"));
                edtFooterText.setText(result.getString("print_footer_text"));
                edtFeesPAid.setText(result.getString("fees_paid"));
                edtMID.setText(result.getString("merchant_number"));
                //  edtTip.setText(result.getString("tip_enable"));
                edtAddressLine1.setText(result.getString("print_address_line1"));
                edtAddressLine2.setText(result.getString("print_address_line2"));
                edtContact.setText(result.getString("print_contact_no"));
                view.findViewById(R.id.progressbar).setVisibility(View.GONE);

                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                    btnTaxAll.setVisibility(View.VISIBLE);
                } else {
                    btnTaxAll.setVisibility(View.INVISIBLE);
                }
                if (result.getString("stock_tracking").equals("1")) {
                    view.findViewById(R.id.rlSwitchNMinQty).setVisibility(View.VISIBLE);
                } else {
                    view.findViewById(R.id.rlSwitchNMinQty).setVisibility(View.GONE);
                }

                /*if(result.getString("type").equals("success")){
                    clearData();
                    Toast.makeText(mContext, "Saved Successfully", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }else {
                    Toast.makeText(mContext, "Please try Again", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals("product_tax")) {
            try {
                JSONObject result = new JSONObject(response);
                //   Toast.makeText(mContext, "Success", Toast.LENGTH_LONG).show();
                if (result.getString("type").equals("success")) {
                    Toast.makeText(mContext, "Success", Toast.LENGTH_LONG).show();

                    Preferences.save(mContext, Preferences.TAX, tax);
                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Fragment fragment = new MerchantSettingsFragment();
                    if (fragment != null) {

                        ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                        //ft.addToBackStack("Brand");
                        ft.commit();
                    }
                    //view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                } else {
                    Toast.makeText(mContext, "Try Later", Toast.LENGTH_LONG).show();
                    //view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("device_info")) {
            try {
                JSONObject result = new JSONObject(response);
                //   Toast.makeText(mContext, "Success", Toast.LENGTH_LONG).show();
                if (result.getJSONObject("device").has("id")) {
                    Toast.makeText(mContext, "Success", Toast.LENGTH_LONG).show();

                    Preferences.save(mContext, Preferences.DEVICE_MODEL, result.getJSONObject("device").getString("model"));
                    Preferences.save(mContext, Preferences.DEVICE_ID, result.getJSONObject("device").getString("device_id"));
                    Preferences.save(mContext, Preferences.DEVICE_SN, result.getJSONObject("device").getString("serial_num"));
                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Fragment fragment = new MerchantSettingsFragment();
                    if (fragment != null) {

                        ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                        //ft.addToBackStack("Brand");
                        ft.commit();
                    }
                    //view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                } else {
                    Toast.makeText(mContext, "Try Later", Toast.LENGTH_LONG).show();
                    //view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clearData() {
        edtName.setText(null);
        edtContact.setText(null);
        edtAddressLine1.setText(null);
        edtAddressLine2.setText(null);
        edtFooterText.setText(null);
        // edtFeesPAid.setText(null);
        FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
        Fragment fragment = new MerchantSettingsFragment();
        if (fragment != null) {

            ft.replace(R.id.frame_layout, fragment, "ItemAdd");
            //ft.addToBackStack("Brand");
            ft.commit();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void openTaxDialog() {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_tax, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txt = dialog.findViewById(R.id.text);
            txt.setText("Tax Percentage");
            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtTax = dialog.findViewById(R.id.edtTax);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // pb.setVisibility(View.VISIBLE);
                    tax = edtTax.getText().toString();
                    callTaxAPI(edtTax.getText().toString());
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void openDeviceInfoDialog() {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_device_info, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txt = dialog.findViewById(R.id.text);
            txt.setText("Link Device");

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtModel = dialog.findViewById(R.id.edtModel);
            edtModel.setText(Build.MODEL);
            edtModel.setEnabled(false);
            final EditText edtDeviceSN = dialog.findViewById(R.id.edtDeviceSN);
            edtDeviceSN.setText(DeviceModel.getSN(mContext));
            edtDeviceSN.setEnabled(false);
            final EditText edtDeviceID = dialog.findViewById(R.id.edtDeviceID);
            edtDeviceID.setText(Preferences.get(mContext, Preferences.DEVICE_ID));
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // pb.setVisibility(View.VISIBLE);
                    // tax=edtTax.getText().toString();
                    callDeviceInfoAPI(edtModel.getText().toString(), edtDeviceSN.getText().toString(), edtDeviceID.getText().toString());
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callTaxAPI(String tax) {
        // view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        // btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.TaxAPI + "?tax=" + tax,
                "product_tax", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void callDeviceInfoAPI(String model, String serial_num, String device_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("model", model);
        map.put("serial_num", serial_num);
        map.put("device_id", device_id);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.DeviceInfo,
                "device_info", map, Request.Method.POST, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity())
                .setActionBarTitle(Preferences.get(getActivity(), Preferences.APP_HEADER));

    }
}