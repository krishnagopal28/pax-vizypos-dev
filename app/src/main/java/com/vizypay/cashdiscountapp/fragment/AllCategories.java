package com.vizypay.cashdiscountapp.fragment;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.activity.SplashActivity;
import com.vizypay.cashdiscountapp.adapter.AllCategoryAdapter;
import com.vizypay.cashdiscountapp.adapter.CartListAdapter;
import com.vizypay.cashdiscountapp.model.Category;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class AllCategories extends Fragment implements IApiResponse, AllCategoryAdapter.CategoryAdapterListener {


    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<Category> mCategoryList;
    AllCategoryAdapter allCategoryAdapter;
    CartListAdapter cartListAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    TextView txtMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.all_items, container, false);
        // Setting ViewPager for each Tabs
        pb = view.findViewById(R.id.pb);
        txtMessage = view.findViewById(R.id.txtMessage);

        mContext = getActivity();
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        pb.setVisibility(View.VISIBLE);
        view.findViewById(R.id.revertPriceLayout).setVisibility(View.GONE);
        callCategoryApi();
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new ItemCatalogFragment();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;

    }

    public void callCategoryApi() {
        Log.e("TOKEN",  Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.CategoriesAPI + "?limit=200",
                "category", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));



    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("category")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mCategoryList = gson.fromJson(mProductList.toString(), new TypeToken<List<Category>>() {
                    }.getType());

                    allCategoryAdapter = new AllCategoryAdapter(mCategoryList, mContext, this);
                    recyclerView.setAdapter(allCategoryAdapter);

                } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {


    }

    @Override
    public void onCategoryUpdated() {
        callCategoryApi();
    }

    @Override
    public void onCategoryDeleted() {
        callCategoryApi();
    }
}
