package com.vizypay.cashdiscountapp.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.AllOrderHistoryAdapter;
import com.vizypay.cashdiscountapp.adapter.CartListAdapter;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AllOrders extends Fragment implements IApiResponse,  View.OnClickListener  {

    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<OrderHistory> mOrderList;
    AllOrderHistoryAdapter allOrderHistoryAdapter;

    CartListAdapter cartListAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    LinearLayout linearAllItems;
    TextView txtMessage, txtTipCount;

    EditText edtSearch;
    String from_date="", to_date="";
    SimpleDateFormat simpleDateFormat;
    TextView txtTotalSale, txtTotalCardSale, txtTotalCashSale, txtTotalFeesPaid, txtTotalTaxCollected, txtTotalTipCollected;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    Button btnSearch;
    EditText edtFromDate, edtToDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.all_orders, container, false);
        mContext = getActivity();
        // Setting ViewPager for each Tabs
        linearAllItems=view.findViewById(R.id.linearAllItems);
        linearAllItems.setBackgroundColor(Color.parseColor("#000000"));
        pb = view.findViewById(R.id.pb);
        txtMessage = view.findViewById(R.id.txtMessage);
        txtTipCount = view.findViewById(R.id.txtTipCount);
        btnSearch=view.findViewById(R.id.btnSearch);
        edtSearch=view.findViewById(R.id.edtSearch);
        edtFromDate=view.findViewById(R.id.fromDate);
        edtToDate=view.findViewById(R.id.toDate);
        simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        //linearDateRange=view.findViewById(R.id.linearDateRange);
        from_date=simpleDateFormat.format(new Date());
        to_date=simpleDateFormat.format(new Date());
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callOrdersAPI(from_date, to_date, edtSearch.getText().toString());
            }
        });
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setDateTimeField();
        edtToDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                toDatePickerDialog.getDatePicker().setMinDate(Utility.getLongMillFromeDateString(edtFromDate.getText().toString()));
            }
        });

        recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        pb.setVisibility(View.VISIBLE);
        callOrdersAPI(from_date, to_date, edtSearch.getText().toString());

        return view;

    }

    public void callOrdersAPI(String from_date, String to_date, String id) {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "?limit=200&from_date="+from_date+"&to_date="+to_date+"&order_id="+id+"&device_id="+Preferences.get(mContext, Preferences.DEVICE_ID)+"&serial_number="+Preferences.get(mContext,Preferences.DEVICE_SN),
                "orders", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("orders")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                txtTipCount.setText(result.getString("total_tip_count")+"/"+result.getString("total"));
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mOrderList = gson.fromJson(mProductList.toString(), new TypeToken<List<OrderHistory>>() {
                    }.getType());

                    allOrderHistoryAdapter = new AllOrderHistoryAdapter(mOrderList, mContext);
                    recyclerView.setAdapter(allOrderHistoryAdapter);

                } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
                recyclerView.setVisibility(View.GONE);
                txtMessage.setText("No Data Found");
                txtMessage.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {


    }

    private void setDateTimeField() {
        edtFromDate.setOnClickListener(this);
        edtToDate.setOnClickListener(this);

        Calendar newCalendarend = Calendar.getInstance();
        Calendar newCalendarfrom = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        /* Date End Date */
        newCalendarend.add(Calendar.DATE, 0);
        edtToDate.setText(dateFormat.format(newCalendarend.getTime()));
        /* Date From Date */
        // newCalendarfrom.add(Calendar.DATE, -1);
        newCalendarfrom.add(Calendar.DATE, 0);
        edtFromDate.setText(dateFormat.format(newCalendarfrom.getTime()));
        Log.e("Date Print====", dateFormat.format(newCalendarend.getTime()));

        fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edtFromDate.setText(dateFormatter.format(newDate.getTime()));
                from_date=dateFormatter.format(newDate.getTime());
            }

        },newCalendarfrom.get(Calendar.YEAR), newCalendarfrom.get(Calendar.MONTH), newCalendarfrom.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edtToDate.setText(dateFormatter.format(newDate.getTime()));
                to_date=dateFormatter.format(newDate.getTime());
            }

        },newCalendarend.get(Calendar.YEAR), newCalendarend.get(Calendar.MONTH), newCalendarend.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        toDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    @Override
    public void onClick(View v) {
        if(v == edtFromDate) {
            fromDatePickerDialog.show();
        } else if(v == edtToDate) {
            toDatePickerDialog.show();
        }
    }
}
