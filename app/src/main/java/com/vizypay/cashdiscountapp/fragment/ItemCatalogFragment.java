package com.vizypay.cashdiscountapp.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ItemCatalogFragment extends Fragment implements IApiResponse {

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4, floatingActionButton5, searchFAB;
    Dialog dialog;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.item_tab, container, false);
        listView=view.findViewById(R.id.listView);

        // callPriceAPI("2", "increase");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 1:
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        Fragment fragment = new AllItemsExpandable();
                        if (fragment != null) {
                            ft.replace(R.id.frame_layout, fragment, "AllItems");
                            ft.addToBackStack("Brand");
                            ft.commit();
                        }
                        break;
                    case 2:
                        FragmentTransaction ft1 = getActivity().getSupportFragmentManager().beginTransaction();
                        Fragment fragment1 = new AllCategories();
                        if (fragment1 != null) {
                            ft1.replace(R.id.frame_layout, fragment1, "AllCategories");
                            ft1.addToBackStack("Brand");
                            ft1.commit();
                        }
                        break;
                   /* case 2:
                      additionalDiscountDialog();
                    break;*/
                    case 0:
                        addNewPOPup();
                        break;
                    case 3:
                        FragmentTransaction ft2 = getActivity().getSupportFragmentManager().beginTransaction();
                        Fragment fragment2 = new AdditionalDiscountFragment();
                        if (fragment2 != null) {
                            ft2.replace(R.id.frame_layout, fragment2, "AllCategories");
                            ft2.addToBackStack("Brand");
                            ft2.commit();
                        }
                        break;
                }
            }
        });

        materialDesignFAM = (FloatingActionMenu) view.findViewById(R.id.material_design_android_floating_action_menu);
        materialDesignFAM.setVisibility(View.GONE);
        //materialDesignFAM.setBackgroundResource(R.drawable.my_list);
        floatingActionButton1 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton1.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton2 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton2.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton3 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton3.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton4 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton4.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton5 = (FloatingActionButton) view.findViewById(R.id.material_design_floating_action_menu_item5);
        floatingActionButton5.setBackgroundColor(Color.parseColor("#ffffff"));
        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment fragment = new MerchantSettingsFragment();
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "Settings");
                    ft.addToBackStack("ItemCatalog");
                    ft.commit();
                }
            }
        });

        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment fragment = new ItemFragment();
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                    ft.addToBackStack("Brand");
                    ft.commit();
                }
            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priceIncreaseDialog();
            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cashDiscountDialog();
            }
        });

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCategoryDialog();
            }
        });
        return view;
    }

    public void callCategoryAdd(HashMap<String, String> map,String token){
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.CategoriesAddAPI,
                "product_add", map, Request.Method.POST, token);

    }

    public void callPriceAPI(String percent, String type){
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.ProductsPriceAPI+"?percent="+percent+"&type="+type,
                "price_change", null, Request.Method.GET,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
     public void callDiscountAPI(String percent, String type){
         HashMap<String, String> map = new HashMap<>();
         map.put("discount", percent );
        ApiRequest apiRequest = new ApiRequest(getActivity(),this);
        apiRequest.postRequest(Constants.BaseURL+Constants.DiscountAPI,
                "discount", map, Request.Method.POST,  Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("product_add")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.getJSONObject("data").has("name")) {
                    /*{"data":{"price":"123","name":"test66","units_in_stok":"23","sku":"123456","created_by_id":3,"updated_at":"2020-05-14 10:33:00","created_at":"2020-05-14 10:33:00","id":33,"main_photo":null,"additional_photos":[],"media":[]}}*/
                    JSONObject res=result.getJSONObject("data");
                    String id=res.getString("id");
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please try later", Toast.LENGTH_SHORT).show();
                    //clearData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("price_change")){
            dialog.dismiss();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void addCategoryDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", edtCategoryName.getText().toString().trim());

                    String email = Preferences.get(getContext(), Preferences.EMAIL);
                    String password = Preferences.get(getContext(), Preferences.PASSWORD);
                    callLoginAPI(email,password,map);

                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void callLoginAPI(String email1, String password1,HashMap<String, String> map){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email1);
        params.put("password", password1);
        Log.d("URL", Constants.BaseURL+Constants.loginAPI);
        Log.d("Params", params.toString());
        JsonObjectRequest req = new JsonObjectRequest(Constants.BaseURL_1+Constants.loginAPI, new JSONObject(
                params), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.w("myApp", "status code..." + response);

                    if(response.has("success")){

                        Preferences.save(getContext(), Preferences.KEY_USER_TOKEN, response.getJSONObject("success").getString("token"));

                        String token = response.getJSONObject("success").getString("token");
                        callCategoryAdd(map,token);
                    } else if (response.has("error")) {
                        Log.e("res", response.optString("error"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w("error in response", "Error: " + error.getMessage());
                Toast.makeText(getContext(), "Invalid Credentials, Please try again.", Toast.LENGTH_SHORT).show();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }
        ){

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    String json = null;
                    try {
                        Log.d("parse jsonObject",volleyError.networkResponse.data.toString() );
                        String responseBody = new String(volleyError.networkResponse.data);
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.d("parse jsonObject", jsonObject.toString());




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return volleyError;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type", "application/json; charset=utf-8");
                Log.e("Header", params.toString());
                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(req);
    }

    public void priceIncreaseDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Price Increase (In %)");
            edtCategoryName.setHint("Enter Increase %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();

                    //  map.put("percent", edtCategoryName.getText().toString().trim());
                    //  map.put("type", );
                    callPriceAPI(edtCategoryName.getText().toString().trim(), "increase");
                    //dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void cashDiscountDialog() {
        try {
            // Create custom dialog object
            dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            final TextView txtHead =  dialog.findViewById(R.id.text);
            txtHead.setText("Cash Discount (In %)");
            edtCategoryName.setText(Preferences.get(getActivity(), Preferences.CASD_Discount));
            edtCategoryName.setHint("Enter Cash Discount %");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();

                    if (edtCategoryName.getText().toString()!=null)
                    Preferences.save(getActivity(), Preferences.CASD_Discount, edtCategoryName.getText().toString());
                    //  map.put("percent", edtCategoryName.getText().toString().trim());
                    //  map.put("type", );
                    callDiscountAPI(edtCategoryName.getText().toString().trim(), "increase");
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addNewPOPup(){
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnItem = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            btnItem.setText("Item");
            Button btnCategory = (Button) dialog.findViewById(R.id.dialogButtonCash);
            btnCategory.setText("Category");
            Button btnDiscount = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button dialogButtonCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            dialogButtonCardCash.setVisibility(View.GONE);
            btnDiscount.setVisibility(View.GONE);
          //  btnDiscount.setText("Discount");
            btnItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    Fragment fragment = new ItemFragment();
                    if (fragment != null) {

                        ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                        ft.addToBackStack("Brand");
                        ft.commit();
                    }
                    dialog.dismiss();
                }
            });
            btnCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   addCategoryDialog();
                    dialog.dismiss();
                }
            });
            final EditText edtName = dialog.findViewById(R.id.edtName);
            final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead =  dialog.findViewById(R.id.text);
           /* btnDiscount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cashDiscountDialog();
                    //callOrderAPI(map);
                    dialog.dismiss();
                }
            });*/
            btnDiscount.setText("Additional Discount");
            btnDiscount.setVisibility(View.GONE);
           /* btnDiscount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    additionalDiscountDialog();
                }
            });*/
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }




}