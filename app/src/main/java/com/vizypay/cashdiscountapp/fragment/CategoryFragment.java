package com.vizypay.cashdiscountapp.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.utility.AndroidMultiPartEntity;
import com.vizypay.cashdiscountapp.utility.BitmapUtils;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Validation;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.mime.content.FileBody;
//import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.ContextCompat.checkSelfPermission;


/**
 * Created by Richa on 10/03/17.
 */

public class CategoryFragment extends Fragment implements IApiResponse {
    EditText edtName, edtPrice, edtSKU, edtQuantity;

    Button btnSubmit;

    Context mContext;
    View view;
    Button btnChoose, btnCapture;
    ImageView image;
    Bitmap bitmap;
    Intent cameraIntent;
    String selectedPath;
    Uri filePath, picUri;
    String imageFilePath;
    int rotation = 0;
    Uri photoURI;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_item, null);
        mContext = getActivity();

        initComponents(view);

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), 1234);
            }
        });
        btnCapture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            5678);
                } else {
                    if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                1112);
                    } else {

                        openCameraIntent();
                       /* try {
                            cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            File file = getOutputMediaFile(1);
                            if (file!=null)
                            picUri = Uri.fromFile(file); // create
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri); // set the image file

                            startActivityForResult(cameraIntent, 9876);
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                    }
                }

                Toast.makeText(mContext, "Coming soon", Toast.LENGTH_LONG).show();


            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chechValidation()) {

                    String myFormat = "yyyy-MM-dd"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", edtName.getText().toString().trim());
                    map.put("price", edtPrice.getText().toString().trim());
                    map.put("sku", edtSKU.getText().toString().trim());
                    map.put("units_in_stok", edtQuantity.getText().toString().trim());
                    callProductAdd(map);
                    //uploadDataToServer();
                }

            }
        });


        return view;
    }

    public void callProductAdd(HashMap<String, String> map) {
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAddAPI,
                "product_add", map, Request.Method.POST, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public boolean chechValidation() {
        boolean ret = true;
        if (!Validation.hasText(edtName))
            ret = false;
        if (!Validation.hasText(edtSKU))
            ret = false;
        if (!Validation.hasText(edtPrice)) {
            //Utility.openCustomDialog(getActivity(), getActivity().getResources().getString(R.string.address_req));
            ret = false;
        }

        return ret;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5678) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
               /* try {

                    openCameraIntent();
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1112);
                } else {

                    openCameraIntent();

                }
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }

        }
        if (requestCode == 1111) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "write permission granted", Toast.LENGTH_LONG).show();
                //selectedPath = getImageUri(getActivity(), bitmap);

                //selectedPath = getRealPathFromURI(filePath);
                Glide.with(getActivity()).load(filePath).into(image);
                try {
                    Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath), mContext, (filePath));
                    selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "write permission denied", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == 1112) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "write permission granted", Toast.LENGTH_LONG).show();
                //selectedPath = getImageUri(getActivity(), bitmap);

                // Uri tempUri = getImageUri(getActivity(), bitmap);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
               /* try {
                    selectedPath = imageFilePath;
                }catch (Exception e){
                    e.printStackTrace();
                }*/

                openCameraIntent();
               /* cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                File file=getOutputMediaFile(1);
                picUri = Uri.fromFile(file); // create
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,picUri); // set the image file

                startActivityForResult(cameraIntent, 9876);*/
            } else {
                Toast.makeText(getActivity(), "write permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void initComponents(View view) {
        //radiogroup = (RadioGroup) view.findViewById(R.id.radiogroup);
        edtName = (EditText) view.findViewById(R.id.edtItemName);
        edtPrice = (EditText) view.findViewById(R.id.edtPrice);
        edtSKU = (EditText) view.findViewById(R.id.edtSKU);
        edtQuantity = (EditText) view.findViewById(R.id.edtQuantity);
        // gridPay = (GridView) view.findViewById(R.id.gridPayOptions);
        image = view.findViewById(R.id.imgImage);
        btnChoose = view.findViewById(R.id.btnChoose);
        btnCapture = view.findViewById(R.id.btnCapture);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("product_add")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.getJSONObject("data").has("price")) {
                    /*{"data":{"price":"123","name":"test66","units_in_stok":"23","sku":"123456","created_by_id":3,"updated_at":"2020-05-14 10:33:00","created_at":"2020-05-14 10:33:00","id":33,"main_photo":null,"additional_photos":[],"media":[]}}*/
                    JSONObject res=result.getJSONObject("data");
                    String id=res.getString("id");
                    if(selectedPath!=null)
//                    uploadFile(Constants.ProductsAddMAINPHOTOAPI+"/"+id);
                    Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                    clearData();
                } else {
                    Toast.makeText(mContext, "Please try later", Toast.LENGTH_SHORT).show();
                    clearData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       /* if (tag_json_obj.equals("payment_entry")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.getInt("api_status") == 1) {
                    Toast.makeText(mContext, result.getString("api_message"), Toast.LENGTH_SHORT).show();
                    clearData();
                } else {
                    Toast.makeText(mContext, result.getString("api_message"), Toast.LENGTH_SHORT).show();
                    clearData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

    }

    public void clearData() {
        edtName.setText(null);
        edtPrice.setText(null);
        edtSKU.setText(null);
        edtQuantity.setText(null);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1234 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                //getPathFromUri(getActivity(),filePath);
                // selectedPath=data.getStringArrayListExtra(IM);//getPathFromUri(getActivity(),filePath);
                //getting image from gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);


                if (checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE")
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1111);
                } else {
                    /* selectedPath = getRealPathFromURI(filePath);//getImageUri(getActivity(), bitmap);*/
                    Glide.with(getActivity()).load(filePath).into(image);
                    try {
                        Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath), mContext, (filePath));
                        selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


                //Setting image to ImageView
                //image.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 100) {

            if (checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE")
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1112);
            } else {
                Glide.with(getActivity()).load(imageFilePath).into(image);
                try {
                    Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoURI), mContext, Uri.parse(imageFilePath));
                    selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
               /* Bitmap image = BitmapUtils.decodeSampledBitmapFromFile(mContext, imageFilePath, Utility.getDisplayMetricsWidth(mContext),
                        300, false);*/

            // selectedPath=imageFilePath;
        }

        if (requestCode == 9876 && resultCode == Activity.RESULT_OK) {
            filePath = data.getData();
            //selectedPath=getPathFromUri(getActivity(),filePath);


            //  bitmap = (Bitmap) data.getExtras().get("data");
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE");
            if (checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE")
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1112);
            } else {
                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                //Uri tempUri = getImageUri(getActivity(), bitmap);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                try {
                    // selectedPath = (getRealPathFromURI(picUri));

                    Glide.with(getActivity()).load(picUri).into(image);
                    try {
                        Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri), mContext, (picUri));
                        selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    public static Bitmap rotateImageIfRequired(Bitmap img, Context context, Uri selectedImage) throws IOException {
/*
        if (selectedImage.getScheme().equals("content")) {
            String[] projection = { MediaStore.Images.ImageColumns.ORIENTATION };
            Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
            if (c.moveToFirst()) {
                final int rotation = c.getInt(0);
                c.close();
                return rotateImage(img, rotation);
            }
            return img;
        } else*/
        try {
            ExifInterface ei = new ExifInterface(selectedImage.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(img, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(img, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(img, 270);
                default:
                    return img;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (selectedImage.getScheme().equals("content")) {
                String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
                Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
                if (c.moveToFirst()) {
                    final int rotation = c.getInt(0);
                    c.close();
                    return rotateImage(img, rotation);
                }
                return img;
            } else
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    private Bitmap rotateBitmap(Bitmap source, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public File createImageFile(Context context) throws IOException {

        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void openCameraIntent() {
      /*  Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //pictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_USER);
        if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile(getActivity());
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getActivity(),getActivity().getPackageName() +".provider", photoFile);
                //photoURI = Uri.fromFile(photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //pictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
                startActivityForResult(pictureIntent,
                        100);
            }
        }*/
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //pictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_USER);
        if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile(getActivity());
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                // =FileProvider.getUriForFile(getActivity(),getActivity().getPackageName() +".provider", photoFile);
                photoURI = Uri.fromFile(photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //pictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
                startActivityForResult(pictureIntent,
                        100);
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void uploadDataToServer() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                btnSubmit.setEnabled(false);
            }

            @Override
            protected String doInBackground(Void... voids) {
//                return uploadFile(Constants.BaseURL + Constants.ProductsAddMAINPHOTOAPI);
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                /*{"api_status":1,"api_message":"success","api_authorization":"You are in debug mode !","id":5}*/
                try {
                    if (new JSONObject(s).getInt("api_status") == 1) {
                        try {
                            clearData();
                            image.setImageResource(R.mipmap.ic_launcher);
                            selectedPath = null;

                            Toast.makeText(mContext, new JSONObject(s).getString("api_message"), Toast.LENGTH_LONG).show();

                            {
                                /*android.support.v4.app.FragmentManager fm=((FragmentActivity) getActivity()).getSupportFragmentManager();
                                fm.popBackStack();
                                Bundle bundle = new Bundle();
                                bundle.putString("data", s);
                                fm.findFragmentByTag("VisitPayment").setArguments(bundle);
                                Preferences.save(mContext, "VISITS", s);*/

                                FragmentManager fm = ((FragmentActivity) getActivity()).getSupportFragmentManager();
                                fm.popBackStack();
                               /* Intent intent = new Intent();
                                intent.putExtra("data", s);

                                Fragment fragment = getTargetFragment();
                                fragment.onActivityResult(878, Activity.RESULT_OK, intent);
                                */
                               /* android.support.v4.app.FragmentManager fm=((FragmentActivity) getActivity()).getSupportFragmentManager();
                                fm.popBackStack();*/
                            } /*else {
                                if (Preferences.get(mContext, Preferences.KEY_VISITS).equals("YES")) {
                                    FragmentManager fm = ((FragmentActivity) getActivity()).getSupportFragmentManager();
                                    fm.popBackStack();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("data", s);
                                    fm.findFragmentByTag("VisitPayment").setArguments(bundle);
                                    Preferences.save(mContext, "VISITS_P", s);
                                } else {
                                    Fragment fragment = new PaymentHistoryFragment();
                                    //replacing the fragment
                                    if (fragment != null) {
                                        FragmentTransaction ft = ((FragmentActivity) getActivity()).getSupportFragmentManager().beginTransaction();
                                        //ft.addToBackStack("OfferF");
                                        ft.replace(R.id.framelayout, fragment, "PaymentHistory");
                                        ft.commit();
                                    }
                                }*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, new JSONObject(s).getString("api_message"), Toast.LENGTH_LONG).show();
                    }
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                    btnSubmit.setEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //super.onPostExecute(s);
            }
        }.execute();
    }

    public static String getPathFromUri(Context context, Uri contentUri) {
        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME};
        Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
            cursor.close();
            return filePath;
        } else {
            return contentUri.getPath();
        }
    }

//    private String uploadFile(String url) {
//        String responseString;
//        HttpPost httppost;
//        HttpClient httpclient = new DefaultHttpClient();//MySSLSocketFactoryUtil.getNewHttpClient();
//        httppost = new HttpPost(url);
//        // Log.e(UploadFileToServerUtils.class.getSimpleName(), "Post Url: " + url);
//
//        try {
//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
//                    new AndroidMultiPartEntity.ProgressListener() {
//
//                        @Override
//                        public void transferred(long num) {
//                            //publishProgress((int) ((num / (float) totalSize) * 100));
//                        }
//                    });
//
//            if (selectedPath != null) {
////                entity.addPart("main_photo", new FileBody(new File(selectedPath)));
//            }
//
//           /* entity.addPart("name", new StringBody(edtName.getText().toString().trim()));
//            entity.addPart("sku", new StringBody(edtSKU.getText().toString().trim()));
//            entity.addPart("price", new StringBody(edtPrice.getText().toString().trim()));
//            entity.addPart("units_in_stok", new StringBody(edtQuantity.getText().toString().trim()));*/
//            // entity.addPart("units_in_stok", new StringBody(edtRemark.getText().toString().trim()));
//
//            //totalSize = entity.getContentLength();
////            httppost.setEntity(entity);
//            httppost.setHeader("Authorization", "Bearer " + Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
//            Log.e("POST IMAGE", "Post Url: " + url + " ::: " + entity.toString());
//            // Making server call
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            entity.writeTo(bytes);
//            HttpResponse response = httpclient.execute(httppost);
//            HttpEntity r_entity = response.getEntity();
//
//            int statusCode = response.getStatusLine().getStatusCode();
//            if (statusCode == 200) {
//                // Server response
//                responseString = EntityUtils.toString(r_entity);
//                image.setImageResource(R.mipmap.ic_launcher);
//                selectedPath = null;
//                Log.e("RSP", responseString);
//
//            } else {
//                responseString = "Error occurred! Http Status Code: "
//                        + statusCode;
//            }
//
//        } catch (Exception | OutOfMemoryError e) {
//            //mProgressDialog.dismiss();
//            //mIsError = true;
//            responseString = e.toString();
//        }
//        return responseString;
//    }
}