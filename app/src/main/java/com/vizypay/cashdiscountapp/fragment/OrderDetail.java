package com.vizypay.cashdiscountapp.fragment;

import static androidx.core.content.ContextCompat.checkSelfPermission;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.graphics.BitmapCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.pax.poslink.PaymentRequest;
import com.pax.poslink.PaymentResponse;
import com.pax.poslink.PosLink;
import com.pax.poslink.ProcessTransResult;
import com.pax.poslink.peripheries.DeviceModel;
import com.pax.poslink.peripheries.POSLinkPrinter;
import com.pax.poslink.peripheries.ProcessResult;

import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.OrderItemAdapter;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.utility.ApiServices;
import com.vizypay.cashdiscountapp.utility.AppThreadPool;
import com.vizypay.cashdiscountapp.utility.BitmapUtils;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.POSLinkCreatorWrapper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.SketchSheetView;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Build;
import android.widget.ImageView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderDetail extends Fragment implements POSLinkPrinter.PrintListener, IApiResponse {


    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<OrderItems> orderItems;
    OrderItemAdapter orderItemAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    // TextView txtMessage;
    Bundle bundle;
    protected PosLink poslink;
    OrderHistory orderHistory;
    TextView txtName,txtMobile, txtEmail, txtOrdertotal, txtSubTotal, txtCashD_lbl, txtCashD, txtTax, txtOrderType;
    TextView txtcreditfees,txtcash_amount,txtcard_amount,txtDate,txtTime, txtTIP;
    Button btn_printreceipt, btnCancelTransaction, btnAddTip,btnviewDegital,btnviewDegital2;
    LinearLayout layoutcreditfees, layouttxtTIP, layoutcash_amount, layoutcard_amount, linearCashDiscount;
    int orderHistoryID;
    boolean validEmail = false;

    Uri filePath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.all_items, container, false);
        mContext = getActivity();
        bundle=getArguments();
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if ((bundle!=null)){
            orderHistory = (OrderHistory) bundle.get("Order");
            orderHistoryID =  bundle.getInt("OrderID");
            Preferences.save(mContext, Preferences.ORDER_ID, String.valueOf(orderHistoryID));
        }

        initPOSLink();
        // Setting ViewPager for each Tabs
        pb = view.findViewById(R.id.pb);
        //txtMessage = view.findViewById(R.id.txtMessage);
        view.findViewById(R.id.linearHeader).setVisibility(View.VISIBLE);
        view.findViewById(R.id.linearItemsHeader).setVisibility(View.VISIBLE);

        txtName=view.findViewById(R.id.txtName);
        txtMobile=view.findViewById(R.id.txtMobile);
        txtEmail=view.findViewById(R.id.txtEmail);
        txtOrdertotal=view.findViewById(R.id.txtOrderTotal);
        txtSubTotal=view.findViewById(R.id.txtSubTotal);
        txtCashD_lbl = view.findViewById(R.id.txtCashD_lbl);
        txtCashD=view.findViewById(R.id.txtCashD);
        txtTIP=view.findViewById(R.id.txtTIP);
        txtTax=view.findViewById(R.id.txtTax);
        txtOrderType=view.findViewById(R.id.txtOrderType);
        txtcreditfees=view.findViewById(R.id.txtcreditfees);
        txtcash_amount=view.findViewById(R.id.txtcash_amount);
        txtcard_amount=view.findViewById(R.id.txtcard_amount);
        txtDate =view.findViewById(R.id.txtDate);
        txtTime =view.findViewById(R.id.txtTime);
        btn_printreceipt = view.findViewById(R.id.btn_printreceipt);
        btnCancelTransaction = view.findViewById(R.id.btnCancelTransaction);
        btnAddTip = view.findViewById(R.id.btnAddTip);
//        btnviewDegital = view.findViewById(R.id.btnviewDegital);
//        btnviewDegital2 = view.findViewById(R.id.btnviewDegital2);
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        layoutcreditfees = view.findViewById(R.id.layoutcreditfees);
        layouttxtTIP = view.findViewById(R.id.layouttxtTIP);
        layoutcash_amount  = view.findViewById(R.id.layoutcash_amount);
        linearCashDiscount  = view.findViewById(R.id.linearCashDiscount);
        layoutcard_amount  = view.findViewById(R.id.layoutcard_amount);
        recyclerView.setVisibility(View.VISIBLE);
        callSettingsGETAPI();

        getOrderHistoryDetail(orderHistoryID);

        btnCancelTransaction.setText("Void");
        btnCancelTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cancelTrans();
                showAlertDialog(mContext, "Void Transaction", "Are you sure you want to void transaction?");
            }
        });

//        btnviewDegital.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Fragment fragment = new Order_Detail_View();
//                // AskOptionDialog().show();
//                FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
//                //Bundle bundle=new Bundle();
//                //bundle.putSerializable("Order", mList.get(position));
//                //bundle.putInt("OrderID", mList.get(position).getId());
//                //fragment.setArguments(bundle);
//                ft.replace(R.id.frame_layout, fragment, "hisory");
//                ft.addToBackStack("history1");
//                ft.commit();
//
//            }
//        });

//        btnviewDegital2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Fragment fragment = new Order_Detail_View();
//                // AskOptionDialog().show();
//                FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
//                //Bundle bundle=new Bundle();
//                //bundle.putSerializable("Order", mList.get(position));
//                //bundle.putInt("OrderID", mList.get(position).getId());
//                //fragment.setArguments(bundle);
//                ft.replace(R.id.frame_layout, fragment, "hisory");
//                ft.addToBackStack("history1");
//                ft.commit();
//
//            }
//        });

        btnAddTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tipType=btnAddTip.getText().toString().equals("Edit Tip")?"edit":"add";
                switch(orderHistory.getOrder_pay_type()){
                    case "Card":
                        openTipsPopup("Card", orderHistory, tipType);
                        break;
                    case "Cash":
                        openTipsPopup("Cash", orderHistory, tipType);
                        break;
                    case "Cash_Card":
                        paymentPopupTIPS("TIPS", tipType);
                        break;
                    default:
                        break;
                }
            }
        });
        btn_printreceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printReceiptnew(txtOrderType.getText().toString());
            }
        });
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new AllOrders();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;

    }



    public void showAlertDialog(final Context oContext, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                dialog.dismiss();
                                cancelTrans();
                                //((Activity)oContext).finish();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void paymentPopup(final String order_pay_type, final String ad_id, final int type) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnCard = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button btnCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            btnCardCash.setVisibility(View.GONE);
            Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
            if(type==3){
                btnCardCash.setVisibility(View.GONE);
            }else {
                btnCardCash.setVisibility(View.VISIBLE);
            }
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // isCash = 0;
                    dialog.dismiss();
                }
            });
            dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateAPI();
                    //  isCash = 1;
                   /* if(type==3){
                        showAlertDialog(mContext, "Cash Refund", "Are you sure you want to refund amount: $ "+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "cash");
                      *//*  trans_type=1;
                        completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, "Refund");*//*
                    }else {
                        trans_type=0;
                        completeCashOrder(order_pay_type, ad_id, type);
                    }*/
                    dialog.dismiss();
                }
            });
            // final EditText edtName = dialog.findViewById(R.id.edtName);
            // final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            // final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);
          /*  btnCardCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type==3){
                        double total= Double.valueOf(edtQuickSale.getText().toString().replace("$", ""));
                        openSummaryPopupCashCreditRefund(total);
                        //showAlertDialog(mContext, "Cash Refund", "Are you sure you want to refund amount: $ "+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "cash");
                      *//*  trans_type=1;
                        completeOrder("Cash", 3, "0", 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, 0, 0, Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]","")), 0, "Refund");*//*
                    }else {
                        double total= Double.valueOf(txtCartTotalPrice.getText().toString().replace("$", ""));
                        //Double.valueOf(edtQuickSale.getText().toString().replaceAll("[$,]","")
                        openSummaryPopupCashCredit("Cash/Credit Order", total, total, 0.00, 0.00, total + discFixedC);
                    }

                    dialog.dismiss();
                }
            });*/
            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // isCash = 0;
                    /*if(type==3){
                        showAlertDialog(mContext, "Credit Refund", "Are you sure you want to refund amount: $"+ Double.parseDouble(edtQuickSale.getText().toString().replaceAll("[$,]",""))+"?", "card");
                    }else {
                        trans_type=0;
                        double creditfee_value = Double.parseDouble(Preferences.get(mContext, Preferences.CREDIT_FEES)); // credit fees value get 1.04

                        double subAmount=Double.parseDouble(txtCartTotalPrice.getText().toString().replaceAll("[$,]",""));
                        Double creditFees=(subAmount*creditfee_value)-subAmount;

                        paymentMethod(order_pay_type, type, ad_id, 0, 0.00, 0.00, 0.00,
                                0.00, 0.00, 0.00, creditFees);
                        //callOrderAPI(map);
                    }*/
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paymentPopupTIPS(final String order_pay_type, final String tipType) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_payment_popup, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnCard = (Button) dialog.findViewById(R.id.dialogButtonCard);
            Button btnCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            btnCardCash.setVisibility(View.GONE);
            Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
            {
                btnCardCash.setVisibility(View.GONE);
            }
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // isCash = 0;
                    dialog.dismiss();
                }
            });
            dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openTipsPopup("Cash", orderHistory, tipType);

                    dialog.dismiss();
                }
            });
            // final EditText edtName = dialog.findViewById(R.id.edtName);
            // final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            // final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);

            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openTipsPopup("Card", orderHistory, tipType);
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void orderReceiptOption() {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_receipt_option, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnEmail = (Button) dialog.findViewById(R.id.dialogButtonEmail);
            Button btnSMS = (Button) dialog.findViewById(R.id.dialogButtonSMS);
            Button btnBarcode = (Button) dialog.findViewById(R.id.dialogButtonBarcode);
            Button btnView = (Button) dialog.findViewById(R.id.dialogButtonView);
            Button btnPrintReceipt = (Button) dialog.findViewById(R.id.dialogButtonPrintReceipt);
            Button btnUploadSignature = (Button) dialog.findViewById(R.id.dialogButtonUploadSignature);
            Button btnClose = (Button) dialog.findViewById(R.id.dialogButtonClose);


            if (Build.MODEL.startsWith("A6")) {
                btnPrintReceipt.setVisibility(View.GONE);
            }else {
                btnPrintReceipt.setVisibility(View.VISIBLE);
            }

//            if (orderHistory.getOrder_pay_type().equals("Card")){
//                btnUploadSignature.setVisibility(View.GONE);
//            }else {
//
//                btnUploadSignature.setVisibility(View.VISIBLE);
//            }

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });



            //Button btnCardCash = (Button) dialog.findViewById(R.id.dialogButtonCardCash);
            //btnCardCash.setVisibility(View.GONE);
            //Button dialogButtonCash = (Button) dialog.findViewById(R.id.dialogButtonCash);
            //{
            //    btnCardCash.setVisibility(View.GONE);
            //}

            btnEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openReceiptEmailPopup();
                    //dialog.dismiss();
                }
            });
            btnSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent intent=new Intent(mContext,MainActivity.class);
                    PendingIntent pi= PendingIntent.getActivity(mContext, 0, intent,0);

                    //Get the SmsManager instance and call the sendTextMessage method to send message
                    SmsManager sms= SmsManager.getDefault();
                    sms.sendTextMessage("5154806310", null, "test from pax device", pi,null);
                    //Utility.openSignatureDialog(mContext, "", false);
                    dialog.dismiss();*//*

                    Twilio.init(Constants.TWILIO_ACCOUNT_SID, Constants.TWILIO_TOKEN);
                    Message message = Message.creator(
                            new com.twilio.type.PhoneNumber("+918817518511"),
                            new com.twilio.type.PhoneNumber("+918817518511"),
                            "Hi there")
                            .create();


                    System.out.println(message.getSid());*/

                    openReceiptSMSPopup();

                }
            });
            btnBarcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openReceiptBarcodePopup();
                    //dialog.dismiss();
                }
            });
            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    if (Build.MODEL.startsWith("A6")){
                        //Intent intent = new Intent(mContext, Order_Detail_View.class);
                        //startActivity(intent);

                        Fragment fragment = new Order_Detail_View();
                        // AskOptionDialog().show();
                        FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                        //Bundle bundle=new Bundle();
                        //bundle.putSerializable("Order", mList.get(position));
                        //bundle.putInt("OrderID", mList.get(position).getId());
                        //fragment.setArguments(bundle);
                        ft.replace(R.id.frame_layout, fragment, "hisory");
                        ft.addToBackStack("history1");
                        ft.commit();

                    }else {

                        String url = orderHistory.getReceipt_url();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        // Note the Chooser below. If no applications match,
                        // Android displays a system message.So here there is no need for try-catch.
                        try {
                            startActivity(Intent.createChooser(intent, "Choose browser"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.dismiss();
                }
            });

            btnPrintReceipt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PrintRecipet(txtOrderType.getText().toString());
                }
            });

            btnUploadSignature.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                5678);
                    } else {
                        if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    1112);
                        } else {
                            Paint paint;
                            final SketchSheetView[] view1 = new SketchSheetView[1];
                            Path path2;
                            try {
                                // Create custom dialog object
                                final Dialog dialog = new Dialog(mContext);
                                // hide to default title for Dialog
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                                LayoutInflater inflater = (LayoutInflater) mContext
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                view = inflater.inflate(R.layout.signature_dialog, null, false);
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.setCancelable(false);
                                dialog.setContentView(view);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                                //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                                final RelativeLayout relativeLayout = dialog.findViewById(R.id.relativeLayout);

                                view1[0] = new SketchSheetView(mContext);
                                Preferences.save(mContext, Preferences.KEY_SIGN_DONE, "false");
                                relativeLayout.addView(view1[0], new ViewGroup.LayoutParams(
                                        RelativeLayout.LayoutParams.MATCH_PARENT,
                                        RelativeLayout.LayoutParams.MATCH_PARENT));
                                //Preferences.save(context, Preferences.KEY_SIGN_DONE, "true");
                                view1[0].setOnHoverListener(new View.OnHoverListener() {
                                    @Override
                                    public boolean onHover(View v, MotionEvent event) {
                                        Preferences.save(mContext, Preferences.KEY_SIGN_DONE, "true");
                                        return false;

                                    }
                                });
                                Button btn = dialog.findViewById(R.id.dialogButtonOK);
                                Button btnCancel = dialog.findViewById(R.id.btnCancel);
                                btnCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        if (true) {
                                            dialog.dismiss();
                                        } else {
                                        }
                                    }
                                });
                                Button btnReset = dialog.findViewById(R.id.btnReset);
                                btnReset.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        Preferences.save(mContext, Preferences.KEY_SIGN_DONE, "false");
                                        //final SketchSheetView view2 = new SketchSheetView(context);

                                        relativeLayout.removeView(view1[0]);

                                        view1[0] = new SketchSheetView(mContext);
                                        relativeLayout.addView(view1[0], new ViewGroup.LayoutParams(
                                                RelativeLayout.LayoutParams.MATCH_PARENT,
                                                RelativeLayout.LayoutParams.MATCH_PARENT));
                                    }
                                });
                                btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //Log.e("drar",view1[0].length+"");
                                        if (Preferences.get(mContext,  Preferences.KEY_SIGN_DONE).equals("Yes")) {
                                            view1[0].setDrawingCacheEnabled(true);
                                            Bitmap bitmap = view1[0].getDrawingCache();



                                            Log.e("bitmap--", String.valueOf(view1[0].bitmap));
                                            Log.e("bitmap11--", String.valueOf(view1[0].paint));
                                            if (bitmap != null) {
                                                Log.e("bitmap_size", String.valueOf(BitmapCompat.getAllocationByteCount(view1[0].bitmap)));

                                                String selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, view1[0].getDrawingCache(true), ".jpg");
                                                try {
                                                    if (selectedPath.equals(null)) {
                                                        Toast.makeText(mContext, "Please Sign to continue....", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        dialog.dismiss();

                                                        UploaodSignature();
                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(mContext, "Please Sign to continue...", Toast.LENGTH_SHORT).show();
                                                }
                                                //Preferences.save(context, Preferences.KEY_SIGN_DONE, "No");

                                            } else {
                                                Toast.makeText(mContext, "Please Sign to continue..", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(mContext, "Please Sign to continue.", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });
                                // Display the dialog
                                dialog.show();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                        }
                    }

                }
            });

            /*dialogButtonCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openTipsPopup("Cash", orderHistory, tipType);

                    dialog.dismiss();
                }
            });
            // final EditText edtName = dialog.findViewById(R.id.edtName);
            // final EditText edtMobile = dialog.findViewById(R.id.edtMobile);
            // final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final TextView txtHead = dialog.findViewById(R.id.text);

            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openTipsPopup("Card", orderHistory, tipType);
                    dialog.dismiss();
                }
            });*/

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void UploaodSignature() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_PICK);
//        startActivityForResult(Intent.createChooser(intent, "Select Image"), 1234);


        String email = Preferences.get(mContext, Preferences.EMAIL);
        String password = Preferences.get(mContext, Preferences.PASSWORD);
        callLoginAPI(email,password);


//        filePath = (Preferences.get(mContext, SIGNATURE_FILE_PATH));
//        Log.e("filepath:::",filePath+"");
//        try {
//            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(filePath));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    public void callLoginAPI(String email1, String password1){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email1);
        params.put("password", password1);
        Log.d("URL", Constants.BaseURL+Constants.loginAPI);
        Log.d("Params", params.toString());
        JsonObjectRequest req = new JsonObjectRequest(Constants.BaseURL_1+Constants.loginAPI, new JSONObject(
                params), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.w("myApp", "status code..." + response);

                    if(response.has("success")){

                        Preferences.save(mContext, Preferences.KEY_USER_TOKEN, response.getJSONObject("success").getString("token"));

                        String token = response.getJSONObject("success").getString("token");
                        uploadSignatureVolley(Constants.BaseURL + Constants.SIGNATURE_PHOTO,token);
                    } else if (response.has("error")) {
                        Log.e("res", response.optString("error"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w("error in response", "Error: " + error.getMessage());
                Toast.makeText(mContext, "Invalid Credentials, Please try again.", Toast.LENGTH_SHORT).show();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }
        ){

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    String json = null;
                    try {
                        Log.d("parse jsonObject",volleyError.networkResponse.data.toString() );
                        String responseBody = new String(volleyError.networkResponse.data);
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.d("parse jsonObject", jsonObject.toString());




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return volleyError;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type", "application/json; charset=utf-8");
                Log.e("Header", params.toString());
                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(req);
    }

    private void uploadSignatureVolley(String url,String token) {

        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(mContext);
        String previouslyEncodedImage = shre.getString("image_data", "");
        filePath = Uri.fromFile(new File(previouslyEncodedImage));
        String filePath2 = getRealPathFromURIPath(filePath, mContext);
        File file1 = new File(filePath2);
        Log.d("file1::", file1+"");

        String mimeType = null;
        if (ContentResolver.SCHEME_CONTENT.equals(filePath.getScheme())) {
            ContentResolver cr = mContext.getContentResolver();
            mimeType = cr.getType(filePath);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(filePath
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());

            Log.d("mimeType::", mimeType+"");
        }


        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("signature_photo", file1.getName(), requestFile);


        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10,TimeUnit.MINUTES)
                .writeTimeout(10,TimeUnit.MINUTES).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BaseURL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiServices uploadImage = retrofit.create(ApiServices.class);
        Integer id = Integer.valueOf(Preferences.get(mContext, Preferences.ORDER_ID));
        String bearar = "Bearer"+" "+ token;
        String Cookie = "XSRF-TOKEN=eyJpdiI6IlMzOEVpVnM5Sjc2NURlTWlRaHFtOEE9PSIsInZhbHVlIjoiVTlJbEs0NkM1VnY2RTFIeW1kd2R2WXROeXJxZmJcL3JYa0FUaDdaTDZhQnpPQ0JVM0VSUmxMWTNUZldOa1RYY0IiLCJtYWMiOiJhMjBhOTM3ZTk3NzgzZDljMzJiNzRmM2E4Zjg0MGY2NWU1N2I2ZDc4ZGE5ZTkzNzdjMjdhNWIxNjc2N2Q3NmU5In0%3D; vizypos_merchant_portal_session=eyJpdiI6InJ6dnI5eWFLaVl3aXJHK3o5cXh1aGc9PSIsInZhbHVlIjoiQXphbEt5UlV5VVhVWHNGMHRVd3JSNENDWmtHNjdpdXNxamZ5SFhGRWI2cmhqOVRXRzFFbE9zTVZcLzNSNjhaTFciLCJtYWMiOiIzZjYzOWJiZWNjNGZmNzI1NjdhYjFjODhlZDM3ODliNzAxNjYxYmM1N2JmNTFkMWNiZGE5OGY4ODVkZDgxNTIwIn0%3D";
        Call<ResponseBody> fileUpload = uploadImage.uploadFile(bearar,fileToUpload, Integer.valueOf(id));//"fileToUpload",

        Log.d("id::", id+"");
        fileUpload.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.d("responceture::", response+"");
                try {
                    if (response.isSuccessful()){
                        Toast.makeText(mContext, "Singature Upload Sucessfully", Toast.LENGTH_SHORT).show();
                    }
                    String res = response.body().toString();
                    JSONObject josn = new JSONObject(res);
                    Log.d("responcefdv::::", josn+"");
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Log.d("uploadimage", "Error occur " + t.getMessage());
                }
            }
        });

    }

    private String getRealPathFromURIPath(Uri contentURI, Context mContext) {
        String result;
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    private void PrintRecipet(String copyName) {

        try {
            POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();

            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_NAME).toUpperCase());
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS1));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS2));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_CONTACT));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            printDataFormatter.addContent("Transaction id");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("#"+orderHistory.getId());
            printDataFormatter.addLineSeparator();


            // System.out.println("Payment Details : " + orderResponse.getString("payment_details"));
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(orderHistory.getCreated_at().split(" ")[0]);
            printDataFormatter.addRightAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(orderHistory.getCreated_at().split(" ")[1]);
            printDataFormatter.addLineSeparator();

//            JSONObject paymentObj = new JSONObject(orderResponse.getString("payment_details").replaceAll("\\\\", ""));
//            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
//                try {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent(("Result"));
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent(("Approved"));
//                    printDataFormatter.addLineSeparator();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

            JSONObject paymentObj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                try {
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(("Result"));
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent(("Approved"));
                    printDataFormatter.addLineSeparator();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //printDataFormatter.addBigFont();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Method:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent(paymentObj.getString("payment_type"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("Sale");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card No : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("account_num"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Type : ");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent(paymentObj.getString("card_type"));
                printDataFormatter.addLineSeparator();
            } else {
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Method:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent(paymentObj.getString("payment_type"));
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Transaction Type:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("Sale");
                printDataFormatter.addLineSeparator();
            }



            if(orderHistory.getOrder_pay_type().equals("Cash_Card")){
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent("Transaction Method:");
//                //printDataFormatter.addBigFont();
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("Cash/Card Order:");
//                printDataFormatter.addLineSeparator();
//
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent("Transaction Type:");
//                //printDataFormatter.addBigFont();
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("Sale");
//                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Credit Fees:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCredit_fees());
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Cash Paid:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCash_amount());
                printDataFormatter.addLineSeparator();

                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Card Paid:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCard_amount());
                printDataFormatter.addLineSeparator();

            }else {
//                if(orderHistory.getPayment_details().contains("Cash")) {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Method:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Cash:");
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Type:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Sale");
//                    printDataFormatter.addLineSeparator();
//
//
//                }else {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Method:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Card:");
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Type:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Sale");
//                    printDataFormatter.addLineSeparator();
//
//                }

            }
            printDataFormatter.addLineSeparator();

            List<OrderItems> mList = new ArrayList<>();
            mList = orderHistory.getOrderItems();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent("Items:");
            printDataFormatter.addLineSeparator();
            if(mList.size() > 0) {
                for (int i = 0; i < mList.size(); i++) {
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent(mList.get(i).getQuantity() + " X " + mList.get(i).getProduct_name());
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addSmallFont();
                    printDataFormatter.addContent("$" + mList.get(i).getTotal_amount());
                    printDataFormatter.addLineSeparator();
                }
            }else {
                printDataFormatter.addNormalFont();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Quick Sale (Charge)");
                printDataFormatter.addRightAlign();
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderHistory.getOrder_total())));
                printDataFormatter.addLineSeparator();
            }

            printDataFormatter.addHeader();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            printDataFormatter.addContent("Subtotal : ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + orderHistory.getOrder_total() + " ");
            printDataFormatter.addLineSeparator();


            if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")){
                //System.out.println("if orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                txtCashD.setVisibility(View.GONE);
                txtCashD_lbl.setVisibility(View.GONE);

            }else {
                //System.out.println("else orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                txtCashD.setVisibility(View.VISIBLE);
                txtCashD_lbl.setVisibility(View.VISIBLE);
            }
            if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")){
                //System.out.println("if orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));

            }else {
                //System.out.println("else orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Cash Discount:");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getCash_discount());
                printDataFormatter.addLineSeparator();
            }
            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
                if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")) {
                    printDataFormatter.addContent("NCC (" + Preferences.get(mContext, Preferences.NON_CASH_CHARGE) + "%) :");
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderHistory.getNcc())) + " ");
                    printDataFormatter.addLineSeparator();
                }
            }

            if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
                printDataFormatter.addSmallFont();
                printDataFormatter.addContent("Tax (" + Preferences.get(mContext, Preferences.TAX) + "%) :");
                //printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getTax());
                printDataFormatter.addLineSeparator();
            }


            printDataFormatter.addHeader();

            printDataFormatter.addBigFont();
            printDataFormatter.addContent("Order Total : ");
            printDataFormatter.addBigFont();
            printDataFormatter.addRightAlign();
            // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
            printDataFormatter.addContent("$" + orderHistory.getTotal_amount() + " ");
            printDataFormatter.addLineSeparator();

            if (orderHistory.getOrder_pay_type().equals("Cash")) {
                printDataFormatter.addBigFont();
                printDataFormatter.addContent("Amount Paid : ");
                printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getAmount_paid() + " ");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addBigFont();
                printDataFormatter.addContent("Change Due : ");
                printDataFormatter.addBigFont();
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("$" + orderHistory.getChange_due() + " ");
                printDataFormatter.addLineSeparator();
            }else {

            }

            if(orderHistory.getTip_amount().equals(null) || orderHistory.getTip_amount().equals("0.00") || orderHistory.getTip_amount().isEmpty()){

            }else {
                if(Preferences.get(mContext, Preferences.ADD_TIP).equals("1")) {
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addContent("Tip Amount : ");
                    printDataFormatter.addNormalFont();
                    printDataFormatter.addRightAlign();
                    printDataFormatter.addContent("$" + orderHistory.getTip_amount() + " ");
                    printDataFormatter.addLineSeparator();
                }
            }
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addTrailer();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(copyName);
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_FOOTER));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            Log.d("PrintFragment", printDataFormatter.build());
            POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
            posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
            posLinkPrinter.print(printDataFormatter.build(),this);

            if(copyName.equals("Customer Copy"))
                if(Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                    //saz  printReceipt(orderResponse, "Merchant Copy");
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openReceiptSMSPopup() {

        try {
            //System.out.println("orderHistoryID--"+orderHistoryID);
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_receipt_sms, null, false);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txt=dialog.findViewById(R.id.text);
            txt.setText("Customer Detail");

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final EditText edtCustname = dialog.findViewById(R.id.edtName);
            final EditText edtCustMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtComnt = dialog.findViewById(R.id.edtCommnt);



            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtCustMobile.getText().toString().isEmpty()){
                        edtCustMobile.setError("Please enter mobile number.");
                        edtCustMobile.requestFocus();
                    }else {
                        dialog.dismiss();

                        String to_number = edtCustMobile.getText().toString().trim();
                        DefaultHttpClient httpclient = new DefaultHttpClient();

                        HttpPost httppost = new HttpPost(
                                "https://api.twilio.com/2010-04-01/Accounts/"+Constants.TWILIO_ACCOUNT_SID+"/SMS/Messages.json");
                        String base64EncodedCredentials = "Basic "
                                + Base64.encodeToString(
                                (Constants.TWILIO_ACCOUNT_SID + ":" + Constants
                                        .TWILIO_TOKEN).getBytes(),
                                Base64.NO_WRAP);

                        httppost.setHeader("Authorization",
                                base64EncodedCredentials);
                        try {

                            List<BasicNameValuePair> nameValuePairs = new ArrayList<>();
                            nameValuePairs.add(new BasicNameValuePair("From",
                                    "+18455521804"));
                            nameValuePairs.add(new BasicNameValuePair("To",
                                    to_number));
                            nameValuePairs.add(new BasicNameValuePair("Body",
                                    "Here is your rerceipt "+orderHistory.getReceipt_url()+"\n"+"" +
                                            "Please click on the link"));

                            httppost.setEntity(new UrlEncodedFormEntity(
                                    nameValuePairs));

                            // Execute HTTP Post Request
                            HttpResponse response = httpclient.execute(httppost);
                            HttpEntity entity = response.getEntity();

                            System.out.println("Entity post is: "
                                    + EntityUtils.toString(entity));

                            pb.setVisibility(View.VISIBLE);

                            String status_code = String.valueOf(response.getStatusLine().getStatusCode());

                            System.out.println("status::::::"+response.getStatusLine().getStatusCode());

                            if (status_code.equalsIgnoreCase("201")){

                                pb.setVisibility(View.GONE);
                                opensucessDAilog("sucess");
                                Toast.makeText(mContext, "SMS sent sucessfully", Toast.LENGTH_SHORT).show();
                            }else {

                                pb.setVisibility(View.GONE);
                                opensucessDAilog("Fail");
                                Toast.makeText(mContext, "SMS not sent", Toast.LENGTH_SHORT).show();
                            }

                        } catch (ClientProtocolException e) {

                        } catch (IOException e) {

                        }


                    }

                    }

            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void openReceiptEmailPopup(){
        try {
            //System.out.println("orderHistoryID--"+orderHistoryID);
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_receipt_email, null, false);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txt=dialog.findViewById(R.id.text);
            txt.setText("Customer Detail");

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
            final EditText edtCustname = dialog.findViewById(R.id.edtName);
            final EditText edtCustMobile = dialog.findViewById(R.id.edtMobile);
            final EditText edtComnt = dialog.findViewById(R.id.edtCommnt);
            final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
                    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                            "\\@" +
                            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                            "(" +
                            "\\." +
                            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                            ")+"
            );
            edtEmail.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if (EMAIL_ADDRESS_PATTERN.matcher(edtEmail.getText().toString()).matches() && s.length() > 0)
                    {
                        //Toast.makeText(mContext,"valid email address",Toast.LENGTH_SHORT).show();
                        // or
                        //textView.setText("valid email");
                        validEmail = true;
                        edtEmail.setError(null);
                    }
                    else
                    {
                        validEmail = false;
                        edtEmail.setError("Invalid email address");
                        // Toast.makeText(mContext,"Invalid email address",Toast.LENGTH_SHORT).show();
                        //or
                        //textView.setText("invalid email");
                    }
                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // other stuffs
                }
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // other stuffs
                }
            });

            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtEmail.getText().toString().isEmpty() || !validEmail){
                        edtEmail.setError("Please enter valid email address.");
                    }else {
                        dialog.dismiss();
                        HashMap<String, String> map = new HashMap<>();
                        map.put("type", "email");
                        map.put("order_id", String.valueOf(orderHistoryID));
                        map.put("email", edtEmail.getText().toString());
                        map.put("customer_name", edtCustname.getText().toString());
                        map.put("message", edtComnt.getText().toString());
                        CallOrderRecEmail(map);
                    }
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void openReceiptBarcodePopup(){
        try {
            //System.out.println("orderHistoryID--"+orderHistoryID);
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_order_receipt_barcode, null, false);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            //TextView txt=dialog.findViewById(R.id.text);
            //txt.setText("Customer Detail");
            final ImageView imageView = dialog.findViewById(R.id.imageView);
            Button btnOk = (Button) dialog.findViewById(R.id.button);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                    try {
                        BitMatrix bitMatrix = multiFormatWriter.encode(orderHistory.getReceipt_url(), BarcodeFormat.QR_CODE, imageView.getWidth(), imageView.getHeight());
                        Bitmap bitmap = Bitmap.createBitmap(imageView.getWidth(), imageView.getHeight(), Bitmap.Config.RGB_565);
                        for (int i = 0; i<imageView.getWidth(); i++){
                            for (int j = 0; j<imageView.getHeight(); j++){
                                bitmap.setPixel(i,j,bitMatrix.get(i,j)? Color.BLACK:Color.WHITE);
                            }
                        }
                        imageView.setImageBitmap(bitmap);
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }
                }
            });


            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void CallOrderRecEmail(HashMap<String, String> map) {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ORDER_RECEIPT,
                "Order_receiptEmail", map, Request.Method.POST, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
    //@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void openTipsPopup(final String order_pay_type, OrderHistory orderHistory, String tipType ) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_cash_tips, null, false);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            TextView txtAmnt= dialog.findViewById(R.id.textAMT);
            TextView txtTipAmnt= dialog.findViewById(R.id.textTipAMT);
            final TextView txtChange= dialog.findViewById(R.id.textChange);
            Button btnCalculate = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button btn1 = (Button) dialog.findViewById(R.id.dialogButton1);
            Button btn2 = (Button) dialog.findViewById(R.id.dialogButton2);
            Button btn3 = (Button) dialog.findViewById(R.id.dialogButton3);
            Button btn4 = (Button) dialog.findViewById(R.id.dialogButton4);
            Button btnCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            Button btnSubmit = (Button) dialog.findViewById(R.id.dialogButtonSubmit);
            final EditText editText= dialog.findViewById(R.id.edtPaid);
            final EditText editTextFixed= dialog.findViewById(R.id.edtPaidFixed);
            final SwitchCompat switchCompat= dialog.findViewById(R.id.switchTipType);
            final LinearLayout linearPercent= dialog.findViewById(R.id.LinearPercent);
            final LinearLayout linearFixed= dialog.findViewById(R.id.LinearFixed);
            final Double amount=Double.valueOf(Utility.decimal2Values_new(Double.valueOf(orderHistory.getTotal_amount())-Double.valueOf(orderHistory.getTip_amount())));
            final Double[] newAmount = {0.00};
            editTextFixed.setText(null);
            if (Preferences.get(mContext, Preferences.TIP_SUGGESTION_ENABLE).equals("1") && Preferences.get(mContext, Preferences.CUSTOME_TIP_SUGGESTION_ENABLE).equals("0")) {

                editTextFixed.setText("$0.00");
                newAmount[0] = 0.00;
                linearFixed.setVisibility(View.GONE);
                linearPercent.setVisibility(View.VISIBLE);
            } else if (Preferences.get(mContext, Preferences.TIP_SUGGESTION_ENABLE).equals("0") && Preferences.get(mContext, Preferences.CUSTOME_TIP_SUGGESTION_ENABLE).equals("1")) {


                editTextFixed.setText("$0.00");
                newAmount[0] = 0.00;
                linearFixed.setVisibility(View.VISIBLE);
                linearPercent.setVisibility(View.GONE);
            } else {

                newAmount[0] = 0.00;
                editTextFixed.setText("$0.00");
                linearPercent.setVisibility(View.VISIBLE);
                linearFixed.setVisibility(View.VISIBLE);
            }
            if(tipType.equals("edit")){
                txtTipAmnt.setVisibility(View.VISIBLE);
            }else {
                txtTipAmnt.setVisibility(View.GONE);
            }
           /* editText.setEllipsize(TextUtils.TruncateAt.END);
            editText.setMovementMethod(null);
            // editText.setShowSoftInputOnFocus(false);
            editText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    editText.setSelection(editText.getText().toString().length());
                    return false;
                }
            });*/
           /* editText.setSelection(editText.getText().toString().length());
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    editText.setSelection(editText.getText().toString().length());
                }
            });

            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    editText.setSelection(editText.getText().toString().length());
                    if (editText.getText().toString().equals("$0.00")) {
                        editText.setSelection(editText.getText().toString().length());
                        editText.setText(editText.getText().toString().replace("$", "").replace(".00", ""));
                    }
                }

                private String current = "";

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals(current)) {
                        editText.removeTextChangedListener(this);

                        String cleanString = s.toString().replaceAll("[$,.]", "");

                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));

                        current = formatted;
                        editText.setText(formatted);
                       // editText.setText(editText.getText().toString().replace("$", ""));
                        editText.setSelection(formatted.length());
                        editText.setText(editText.getText().toString().replace("$", "").replace(".00", ""));

                        editText.addTextChangedListener(this);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });*/
          /*  editText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    editText.setCursorVisible(true);
                    editText.setSelection(editText.getText().toString().length());
                    return false;
                }
            });*/
            editTextFixed.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    editTextFixed.setCursorVisible(true);
                    editTextFixed.setSelection(editTextFixed.getText().toString().length());
                    return false;
                }
            });
            editTextFixed.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            editTextFixed.setSelection(editTextFixed.getText().toString().length());
                        }
                    }
            );
           /* editText.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            editText.setSelection(editText.getText().toString().length());
                        }
                    }
            );*/
            editTextFixed.setEllipsize(TextUtils.TruncateAt.END);
            editTextFixed.setMovementMethod(null);
            //  editTextFixed.setShowSoftInputOnFocus(false);
            editTextFixed.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    editTextFixed.setSelection(editTextFixed.getText().toString().length());
                    return false;
                }
            });
            editTextFixed.setSelection(editTextFixed.getText().toString().length());
            editTextFixed.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    editTextFixed.setSelection(editTextFixed.getText().toString().length());
                }
            });

            editTextFixed.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    editTextFixed.setSelection(editTextFixed.getText().toString().length());
                    if (editTextFixed.getText().toString().equals("$0.00")) {
                        editTextFixed.setSelection(editTextFixed.getText().toString().length());
                    }
                }

                private String current = "";

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals(current)) {
                        editTextFixed.removeTextChangedListener(this);

                        String cleanString = s.toString().replaceAll("[$,.]", "");

                        double parsed = Double.parseDouble(cleanString);
                        String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));

                        current = formatted;
                        editTextFixed.setText(formatted);
                        editTextFixed.setSelection(formatted.length());

                        editTextFixed.addTextChangedListener(this);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            txtTipAmnt.setText("Tip: $"+orderHistory.getTip_amount());
//            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked){
//                        editTextFixed.setText("$0.00");
//                        newAmount[0] = 0.00;
//                        linearFixed.setVisibility(View.GONE);
//                        linearPercent.setVisibility(View.VISIBLE);
//                    }else {
//                        newAmount[0] = 0.00;
//                        editTextFixed.setText("$0.00");
//                        linearPercent.setVisibility(View.GONE);
//                        linearFixed.setVisibility(View.VISIBLE);
//                    }
//                }
//            });

            txtAmnt.setText("$"+Utility.decimal2Values_new(amount));
            btnCalculate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    newAmount[0]=0.00;
                    try{
                        newAmount[0] =((amount*Double.valueOf(editText.getText().toString()))/100);
                    }catch (Exception e){

                    }
                    if(newAmount[0]<=0.005 ) {
                        txtChange.setText("New Amount: $" + Utility.decimal3Values(amount + newAmount[0]));
                    }else {
                        txtChange.setText("New Amount: $" + Utility.decimal2Values(amount + newAmount[0]));
                    }
                }
            });
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*15)/100);
                    if(newAmount[0]<=0.005) {
                        txtChange.setText("New Amount: $" + Utility.decimal3Values(amount + newAmount[0]));
                    }else {
                        txtChange.setText("New Amount: $" + Utility.decimal2Values((amount + newAmount[0])));
                    }
                    // txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*18)/100);
                    if(newAmount[0]<=0.005 ) {
                        txtChange.setText("New Amount: $" + Utility.decimal3Values(amount + newAmount[0]));
                    }else {
                        txtChange.setText("New Amount: $" + Utility.decimal2Values(amount + newAmount[0]));
                    }
                    //txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*20)/100);

                    if(newAmount[0]<=0.005) {
                        txtChange.setText("New Amount: $" + Utility.decimal3Values(amount + newAmount[0]));
                    }else {
                        txtChange.setText("New Amount: $" + Utility.decimal2Values(amount + newAmount[0]));
                    }
                    //txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                }
            });
            btn4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newAmount[0]=0.00;
                    newAmount[0] =((amount*25)/100);
                    Log.e("25%", Utility.decimal2Values(amount + newAmount[0])+"");
                    //txtChange.setText("New Amount: $"+Utility.decimal2Values(amount+newAmount[0]));
                    if(newAmount[0]<=0.005) {
                        txtChange.setText("New Amount: $" + Utility.decimal3Values(amount + newAmount[0]));
                    }else {
                        txtChange.setText("New Amount: $" + Utility.decimal2Values(amount + newAmount[0]));
                    }
                }
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(editTextFixed.getVisibility()== View.VISIBLE) {
                        try {
                            newAmount[0] = Double.valueOf(editTextFixed.getText().toString().replace("$", ""));
                        } catch (Exception e) {

                        }
                    }
                    //if (txtChange.getText().toString().contains("$"))

                    if (newAmount[0]>0.00) {
                        if (order_pay_type.equals("Card")) {
                            double new_CFees=(amount*Double.valueOf(Preferences.get(mContext, Preferences.CREDIT_FEES)))-amount;
                            updateTipToPAXServer(Utility.decimal2Values_new(newAmount[0]), amount + newAmount[0], new_CFees);
                        } else if (order_pay_type.equals("Cash")) {
                            updateOrderAPI(Utility.decimal2Values_new(newAmount[0]), order_pay_type, null);
                        } else {
                            updateOrderAPI(Utility.decimal2Values_new(newAmount[0]), "Cash", null);
                        }
                        dialog.dismiss();
                    }else {
                        Toast.makeText(mContext, "Please enter valid tip amount to proceed!", Toast.LENGTH_LONG).show();
                    }


                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateTipToPAXServer(final String tipAmount, double newAmount, final double newCreditFees) {
        new AsyncTask<Void, Void , String>(){

            @Override
            protected String doInBackground(Void... voids) {
                addTipAmount(tipAmount, newCreditFees);
                return null;
            }
        }.execute();
    }

    public void cancelTrans(){
        String num = null;
        //if (!orderHistory.getOrder_pay_type().equals("Cash"))
        {
            try {
                num = orderHistory.getTransaction_no();
//                Log.e("TransNo", orderHistory.getTransaction_no());
            } catch (Exception e) {
                try {
                    JSONObject obj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
                    num = obj.getString("ref_num");
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                    //updateAPI();
                }
                e.printStackTrace();
            }

            if (num!=null) {
                try {
                    final String finalNum = num;
                    new AsyncTask<Void, Void, String>() {
                        String msg, done = null;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            /*       Intent intent=new Intent(mContext, MainActivity.class);
                             *//* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*//*
                        intent.putExtra("Fragment", "OrderHistory");
                        ((AppCompatActivity)mContext).startActivity(intent);
                        ((AppCompatActivity)mContext).finish();*/
                        }

                        @Override
                        protected String doInBackground(Void... voids) {

                            PaymentRequest paymentRequest = new PaymentRequest();
                            paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                            paymentRequest.TransType = paymentRequest.ParseTransType("VOID");
                            paymentRequest.ECRRefNum = "1";
                            paymentRequest.OrigRefNum = finalNum;
                            poslink.PaymentRequest = paymentRequest;

                            try {
                                ProcessTransResult result = poslink.ProcessTrans();
                                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                                    Log.e("RESP", paymentResponse.ResultTxt);
                                    done = "yes";
                                } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                    //lblMsg.Text = "Action Timeout.";
                                    // Log.e("TimeOut", "Action Timeout.");
                                    msg = "Action TimeOut";
                                    done = msg;
                                } else {
                                    //Log.e("Result MSG", result.Msg);
                                    msg = result.Msg;
                                    done = msg;
                                    // lblMsg.Text = result.Msg;
                                }
                            } catch (Exception e) {
                                done = null;
                                e.printStackTrace();
                            }
                        /*ProcessTransResult result = poslink.ProcessTrans();
                        try {
                            if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                PaymentResponse paymentResponse = poslink.PaymentResponse;
                                Log.e("RESP", paymentResponse.ResultTxt);
                            } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                //lblMsg.Text = "Action Timeout.";
                                // Log.e("TimeOut", "Action Timeout.");
                                msg = "Action TimeOut";
                            } else {
                                //Log.e("Result MSG", result.Msg);
                                msg = result.Msg;
                                // lblMsg.Text = result.Msg;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            Log.e("PAX_RESP", msg);
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                            return done;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            if (s.equals("yes")) {
                                updateAPI();
                            } else {
                                Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
                            }

                        }
                    }.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                updateAPI();
            }
        }
    }

    public void addTipAmount(final String tipAmount, final Double newCreditFeess){
        String num = null;
        //if (!orderHistory.getOrder_pay_type().equals("Cash"))
        {
            try {
                num = orderHistory.getTransaction_no();
//                Log.e("TransNo", orderHistory.getTransaction_no());
            } catch (Exception e) {
                try {
                    JSONObject obj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
                    num = obj.getString("ref_num");
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                    //updateAPI();
                }
                e.printStackTrace();
            }

            if (num!=null) {
                try {
                    final String finalNum = num;
                    new AsyncTask<Void, Void, String>() {
                        String msg, done = null;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            /*       Intent intent=new Intent(mContext, MainActivity.class);
                             *//* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*//*
                        intent.putExtra("Fragment", "OrderHistory");
                        ((AppCompatActivity)mContext).startActivity(intent);
                        ((AppCompatActivity)mContext).finish();*/
                        }

                        @Override
                        protected String doInBackground(Void... voids) {

                            PaymentRequest paymentRequest = new PaymentRequest();
                            paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
                            paymentRequest.TransType = paymentRequest.ParseTransType("ADJUST");
                            paymentRequest.ECRRefNum = "1";
                            paymentRequest.OrigRefNum = finalNum;
                            paymentRequest.Amount=String.valueOf(tipAmount);
                            poslink.PaymentRequest = paymentRequest;

                            try {
                                ProcessTransResult result = poslink.ProcessTrans();
                                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                    PaymentResponse paymentResponse = poslink.PaymentResponse;
                                    Log.e("RESP", paymentResponse.ResultTxt);
                                    done = "yes";
                                } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                    //lblMsg.Text = "Action Timeout.";
                                    // Log.e("TimeOut", "Action Timeout.");
                                    msg = "Action TimeOut";
                                    done = msg;
                                } else {
                                    //Log.e("Result MSG", result.Msg);
                                    msg = result.Msg;
                                    done = msg;
                                    // lblMsg.Text = result.Msg;
                                }
                            } catch (Exception e) {
                                done = null;
                                e.printStackTrace();
                            }
                        /*ProcessTransResult result = poslink.ProcessTrans();
                        try {
                            if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                                PaymentResponse paymentResponse = poslink.PaymentResponse;
                                Log.e("RESP", paymentResponse.ResultTxt);
                            } else if (result.Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
                                //lblMsg.Text = "Action Timeout.";
                                // Log.e("TimeOut", "Action Timeout.");
                                msg = "Action TimeOut";
                            } else {
                                //Log.e("Result MSG", result.Msg);
                                msg = result.Msg;
                                // lblMsg.Text = result.Msg;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            Log.e("PAX_RESP", msg);
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                            return done;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            if (s.equals("yes")) {
                                updateOrderAPI(String.valueOf(tipAmount), "Card", String.valueOf(newCreditFeess));
                            } else {
                                Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
                            }

                        }
                    }.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                updateAPI();
            }
        }
    }

    public void setValues(){
        try {
            if(orderHistory.getName() != null && !orderHistory.getName().isEmpty()){
                txtName.append(orderHistory.getName());
            }else{   txtName.setVisibility(View.GONE);        }

            if(orderHistory.getMobile() != null && !orderHistory.getMobile().isEmpty()){
                txtMobile.append(orderHistory.getMobile());
            }else{   txtMobile.setVisibility(View.GONE);        }

            if(orderHistory.getEmail() != null && !orderHistory.getEmail().isEmpty()){
                txtEmail.append(orderHistory.getEmail());
            }else{   txtEmail.setVisibility(View.GONE);        }

            String datetime = orderHistory.getCreated_at();
            if(datetime != null && !datetime.isEmpty()){
                //2021-01-29 07:18:31
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyy-MM-dd h:mm:ss");
                Date time = localDateFormat.parse((datetime));
                txtTime.append(String.valueOf(new SimpleDateFormat("h:mm").format(time)));

                SimpleDateFormat localDateFormatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date times = localDateFormatDate.parse((datetime));
                txtDate.append(String.valueOf(new SimpleDateFormat("MM-dd-yyyy").format(times)));

            }else {

            }



            // txtOrdertotal.append("$"+orderHistory.getTotal_amount());
            try {
                // if (orderHistory.getTip_amount().equals(null) || orderHistory.getTip_amount().equals("0.00") || orderHistory.getTip_amount().isEmpty()) {
                txtOrdertotal.append("$"+orderHistory.getTotal_amount());
               /* } else {
                    double orderTot = Double.parseDouble(orderHistory.getTotal_amount()) + Double.parseDouble(orderHistory.getTip_amount());
                    txtOrdertotal.append("$"+String.format("%.2f", orderTot));
                }*/
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                if (orderHistory.getTip_amount().equals(null) || orderHistory.getTip_amount().equals("0.00") || orderHistory.getTip_amount().isEmpty()) {
                    btnAddTip.setVisibility(View.VISIBLE);
//                    btnviewDegital.setVisibility(View.VISIBLE);
                    layouttxtTIP.setVisibility(View.GONE);
//                    btnviewDegital2.setVisibility(View.GONE);
                } else {
                    btnAddTip.setText("Edit Tip");
                    //btnAddTip.setVisibility(View.GONE);
                    layouttxtTIP.setVisibility(View.VISIBLE);
                    txtTIP.append("$" + orderHistory.getTip_amount());
                    if(orderHistory.getPayment_details().contains("Card")){
                        txtOrdertotal.setText("$"+Utility.decimal2Values(Double.valueOf(orderHistory.getTotal_amount())));
                    }

                    if(orderHistory.getOrder_status().equals("Voided")|| orderHistory.getOrder_status().equals("Refund")){
                        btnAddTip.setVisibility(View.GONE);
//                        btnviewDegital.setVisibility(View.GONE);
//                        btnviewDegital2.setVisibility(View.VISIBLE);
                    } if(orderHistory.getOrder_status().equals("Voided")){
                        btnCancelTransaction.setVisibility(View.GONE);
                    }

                }
            }catch (Exception e){
                e.printStackTrace();
            }
            if(Preferences.get(mContext, Preferences.ADD_TIP).equals("0")) {
                btnAddTip.setVisibility(View.GONE);
//                btnviewDegital.setVisibility(View.GONE);
                layouttxtTIP.setVisibility(View.GONE);
//                btnviewDegital2.setVisibility(View.VISIBLE);
            }else {
                if (orderHistory.getOrder_pay_type().equals("Cash_Card")){
                    btnAddTip.setVisibility(View.GONE);
//                    btnviewDegital.setVisibility(View.GONE);
//                    btnviewDegital2.setVisibility(View.VISIBLE);
                    layouttxtTIP.setVisibility(View.GONE);
                }
                if(orderHistory.getOrder_status().equals("Voided")|| orderHistory.getOrder_status().equals("Refund")){
                    btnAddTip.setVisibility(View.GONE);
//                    btnviewDegital.setVisibility(View.GONE);
//                    btnviewDegital2.setVisibility(View.VISIBLE);

                }
            }
            if(orderHistory.getOrder_pay_type().equals("Cash_Card")){
                btnAddTip.setVisibility(View.GONE);
//                btnviewDegital.setVisibility(View.GONE);
//                btnviewDegital2.setVisibility(View.VISIBLE);
                layouttxtTIP.setVisibility(View.GONE);
                txtCashD.setVisibility(View.VISIBLE);
                layoutcreditfees.setVisibility(View.VISIBLE);
                layoutcash_amount.setVisibility(View.VISIBLE);
                layoutcard_amount.setVisibility(View.VISIBLE);
                txtOrderType.setText("Cash/Card Order");
                txtCashD.append("($" + orderHistory.getCash_discount()+")");
                txtcreditfees.append("($" +orderHistory.getCredit_fees().replace("-","")+")");
                txtcash_amount.append("$" +orderHistory.getCash_amount());
                txtcard_amount.append("$" +orderHistory.getCard_amount());
            }else {
                if(orderHistory.getOrder_status().equals("Voided")|| orderHistory.getOrder_status().equals("Refund")){
                    btnAddTip.setVisibility(View.GONE);
//                    btnviewDegital.setVisibility(View.GONE);
//                    btnviewDegital2.setVisibility(View.VISIBLE);

                }
                if(orderHistory.getPayment_details().contains("Cash")) {
                    txtCashD.setVisibility(View.VISIBLE);
                    layoutcreditfees.setVisibility(View.GONE);
                    layoutcash_amount.setVisibility(View.GONE);
                    layoutcard_amount.setVisibility(View.GONE);
                    txtOrderType.setText("Cash Order");
                    txtCashD.append("($" + orderHistory.getCash_discount()+")");
                }else {
                    txtCashD.setVisibility(View.GONE);
                    layoutcreditfees.setVisibility(View.GONE);
                    layoutcash_amount.setVisibility(View.GONE);
                    layoutcard_amount.setVisibility(View.GONE);
                    txtOrderType.setText("Card Order");
                    linearCashDiscount.setVisibility(View.GONE);
                }
            }
            if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")){
                //System.out.println("if orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                txtCashD.setVisibility(View.GONE);
                txtCashD_lbl.setVisibility(View.GONE);

            }else {
                //System.out.println("else orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
                txtCashD.setVisibility(View.VISIBLE);
                txtCashD_lbl.setVisibility(View.VISIBLE);
            }


            txtTax.append("$"+orderHistory.getTax());
            txtSubTotal.append("$"+orderHistory.getOrder_total());

            mLayoutManager = new GridLayoutManager(getActivity(), 1);
            recyclerView.setLayoutManager(mLayoutManager);
            // pb.setVisibility(View.VISIBLE);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (orderHistory.getOrderItems().size()>0) {
            orderItemAdapter = new OrderItemAdapter(orderHistory.getOrderItems(), mContext);
            recyclerView.setAdapter(orderItemAdapter);
            //callProductsAPI();
        }
        if(orderHistory.getOrder_status().equals("Voided")|| orderHistory.getOrder_status().equals("Refund")){
            btnAddTip.setVisibility(View.GONE);
//            btnviewDegital.setVisibility(View.GONE);
//            btnviewDegital2.setVisibility(View.VISIBLE);

        }
        if(orderHistory.getOrder_status().equals("Voided")){
            btnCancelTransaction.setVisibility(View.GONE);
        }
        try{
            if (orderHistory.getOrder_pay_type().equals("Cash_Card")){
                btnAddTip.setVisibility(View.GONE);
//                btnviewDegital.setVisibility(View.GONE);
//                btnviewDegital2.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void callSettingsGETAPI() {
        //  view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        //btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.SettingsAPI,
                "SettingsGet", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
    public void getOrderHistoryDetail(Integer orderHistoryID){
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/"+orderHistoryID,
                "orders", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
    public void updateAPI(){
        HashMap<String, String> map = new HashMap<>();
        map.put("order_status", "Cancelled");
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/"+orderHistoryID,
                "orders_update", map,Request.Method.PUT, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
    public void updateOrderAPI(String amnt,  String transType, String newCreditFees){
        HashMap<String, String> map = new HashMap<>();
        map.put("tip_amount", amnt);
        map.put("tip_trans_type", transType);
        if(newCreditFees!=null) {
            map.put("credit_fees", newCreditFees);
        }
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "/"+orderHistoryID,
                "order_update", map,Request.Method.PUT, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    private void initPOSLink() {
        POSLinkCreatorWrapper.createSync(getContext(), new AppThreadPool.FinishInMainThreadCallback<PosLink>() {
            @Override
            public void onFinish(PosLink result) {

                poslink = result;
                poslink.SetCommSetting(AppController.setupSetting());
                Log.d("MainActivity", "PosLink : " + poslink);
                Log.d("MainActivity", "PosLink comm: port " + poslink.GetCommSetting().getDestPort());
                Log.d("MainActivity", "PosLink comm timeout: " + poslink.GetCommSetting().getTimeOut());

                //paymentMethod();
            }
        });
    }
    private void printReceiptnew(String copyName) {
        orderReceiptOption();
//        if (Build.MODEL.startsWith("A6")){
//
//        }  else {
//            try {
//                POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addHeader();
//                printDataFormatter.addBigFont();
//
//                printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_NAME).toUpperCase());
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS1));
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_ADDRESS2));
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_CONTACT));
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//
//                printDataFormatter.addContent("Transaction id");
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("#"+orderHistory.getId());
//                printDataFormatter.addLineSeparator();
//
//
//                // System.out.println("Payment Details : " + orderResponse.getString("payment_details"));
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent(orderHistory.getCreated_at().split(" ")[0]);
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent(orderHistory.getCreated_at().split(" ")[1]);
//                printDataFormatter.addLineSeparator();
//
////            JSONObject paymentObj = new JSONObject(orderResponse.getString("payment_details").replaceAll("\\\\", ""));
////            if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
////                try {
////                    printDataFormatter.addSmallFont();
////                    printDataFormatter.addContent(("Result"));
////                    printDataFormatter.addRightAlign();
////                    printDataFormatter.addContent(("Approved"));
////                    printDataFormatter.addLineSeparator();
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
//
//                JSONObject paymentObj = new JSONObject(orderHistory.getPayment_details().replaceAll("\\\\", ""));
//                if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
//                    try {
//                        printDataFormatter.addSmallFont();
//                        printDataFormatter.addContent(("Result"));
//                        printDataFormatter.addRightAlign();
//                        printDataFormatter.addContent(("Approved"));
//                        printDataFormatter.addLineSeparator();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Method:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent(paymentObj.getString("payment_type"));
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Type:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Sale");
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Card No : ");
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent(paymentObj.getString("account_num"));
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Card Type : ");
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent(paymentObj.getString("card_type"));
//                    printDataFormatter.addLineSeparator();
//                } else {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Method:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent(paymentObj.getString("payment_type"));
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Transaction Type:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("Sale");
//                    printDataFormatter.addLineSeparator();
//                }
//
//
//
//                if(orderHistory.getOrder_pay_type().equals("Cash_Card")){
////                printDataFormatter.addSmallFont();
////                printDataFormatter.addContent("Transaction Method:");
////                //printDataFormatter.addBigFont();
////                printDataFormatter.addRightAlign();
////                printDataFormatter.addContent("Cash/Card Order:");
////                printDataFormatter.addLineSeparator();
////
////                printDataFormatter.addSmallFont();
////                printDataFormatter.addContent("Transaction Type:");
////                //printDataFormatter.addBigFont();
////                printDataFormatter.addRightAlign();
////                printDataFormatter.addContent("Sale");
////                printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Credit Fees:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getCredit_fees());
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Cash Paid:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getCash_amount());
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Card Paid:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getCard_amount());
//                    printDataFormatter.addLineSeparator();
//
//                }else {
////                if(orderHistory.getPayment_details().contains("Cash")) {
////                    printDataFormatter.addSmallFont();
////                    printDataFormatter.addContent("Transaction Method:");
////                    //printDataFormatter.addBigFont();
////                    printDataFormatter.addRightAlign();
////                    printDataFormatter.addContent("Cash:");
////                    printDataFormatter.addLineSeparator();
////
////                    printDataFormatter.addSmallFont();
////                    printDataFormatter.addContent("Transaction Type:");
////                    //printDataFormatter.addBigFont();
////                    printDataFormatter.addRightAlign();
////                    printDataFormatter.addContent("Sale");
////                    printDataFormatter.addLineSeparator();
////
////
////                }else {
////                    printDataFormatter.addSmallFont();
////                    printDataFormatter.addContent("Transaction Method:");
////                    //printDataFormatter.addBigFont();
////                    printDataFormatter.addRightAlign();
////                    printDataFormatter.addContent("Card:");
////                    printDataFormatter.addLineSeparator();
////
////                    printDataFormatter.addSmallFont();
////                    printDataFormatter.addContent("Transaction Type:");
////                    //printDataFormatter.addBigFont();
////                    printDataFormatter.addRightAlign();
////                    printDataFormatter.addContent("Sale");
////                    printDataFormatter.addLineSeparator();
////
////                }
//
//                }
//                printDataFormatter.addLineSeparator();
//
//                List<OrderItems> mList = new ArrayList<>();
//                mList = orderHistory.getOrderItems();
//                printDataFormatter.addHeader();
//                printDataFormatter.addBigFont();
//                printDataFormatter.addSmallFont();
//                printDataFormatter.addContent("Items:");
//                printDataFormatter.addLineSeparator();
//                if(mList.size() > 0) {
//                    for (int i = 0; i < mList.size(); i++) {
//                        printDataFormatter.addNormalFont();
//                        printDataFormatter.addSmallFont();
//                        printDataFormatter.addContent(mList.get(i).getQuantity() + " X " + mList.get(i).getProduct_name());
//                        printDataFormatter.addRightAlign();
//                        printDataFormatter.addSmallFont();
//                        printDataFormatter.addContent("$" + mList.get(i).getTotal_amount());
//                        printDataFormatter.addLineSeparator();
//                    }
//                }else {
//                    printDataFormatter.addNormalFont();
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Quick Sale (Charge)");
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderHistory.getOrder_total())));
//                    printDataFormatter.addLineSeparator();
//                }
//
//                printDataFormatter.addHeader();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//
//                printDataFormatter.addContent("Subtotal : ");
//                printDataFormatter.addRightAlign();
//                printDataFormatter.addContent("$" + orderHistory.getOrder_total() + " ");
//                printDataFormatter.addLineSeparator();
//
//
//                if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")){
//                    //System.out.println("if orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
//                    txtCashD.setVisibility(View.GONE);
//                    txtCashD_lbl.setVisibility(View.GONE);
//
//                }else {
//                    //System.out.println("else orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
//                    txtCashD.setVisibility(View.VISIBLE);
//                    txtCashD_lbl.setVisibility(View.VISIBLE);
//                }
//                if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")){
//                    //System.out.println("if orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
//
//                }else {
//                    //System.out.println("else orderDetail PROGRAM_TYPE--"+Preferences.get(mContext, Preferences.PROGRAM_TYPE));
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Cash Discount:");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getCash_discount());
//                    printDataFormatter.addLineSeparator();
//                }
//                if (paymentObj.getString("payment_type").equalsIgnoreCase("Card")) {
//                    if (Preferences.get(mContext, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0")) {
//                        printDataFormatter.addContent("NCC (" + Preferences.get(mContext, Preferences.NON_CASH_CHARGE) + "%) :");
//                        printDataFormatter.addRightAlign();
//                        printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(orderHistory.getNcc())) + " ");
//                        printDataFormatter.addLineSeparator();
//                    }
//                }
//
//                if (Preferences.get(mContext, Preferences.ISTAX).equals("1")) {
//                    printDataFormatter.addSmallFont();
//                    printDataFormatter.addContent("Tax (" + Preferences.get(mContext, Preferences.TAX) + "%) :");
//                    //printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getTax());
//                    printDataFormatter.addLineSeparator();
//                }
//
//
//                printDataFormatter.addHeader();
//
//                printDataFormatter.addBigFont();
//                printDataFormatter.addContent("Order Total : ");
//                printDataFormatter.addBigFont();
//                printDataFormatter.addRightAlign();
//                // printDataFormatter.addContent("$"+Utility.decimal2Values(Double.valueOf(orderResponse.getString("total_amount")))+" ");
//                printDataFormatter.addContent("$" + orderHistory.getTotal_amount() + " ");
//                printDataFormatter.addLineSeparator();
//
//                if (orderHistory.getOrder_pay_type().equals("Cash")) {
//                    printDataFormatter.addBigFont();
//                    printDataFormatter.addContent("Amount Paid : ");
//                    printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getAmount_paid() + " ");
//                    printDataFormatter.addLineSeparator();
//
//                    printDataFormatter.addBigFont();
//                    printDataFormatter.addContent("Change Due : ");
//                    printDataFormatter.addBigFont();
//                    printDataFormatter.addRightAlign();
//                    printDataFormatter.addContent("$" + orderHistory.getChange_due() + " ");
//                    printDataFormatter.addLineSeparator();
//                }else {
//
//                }
//
//                if(orderHistory.getTip_amount().equals(null) || orderHistory.getTip_amount().equals("0.00") || orderHistory.getTip_amount().isEmpty()){
//
//                }else {
//                    if(Preferences.get(mContext, Preferences.ADD_TIP).equals("1")) {
//                        printDataFormatter.addNormalFont();
//                        printDataFormatter.addContent("Tip Amount : ");
//                        printDataFormatter.addNormalFont();
//                        printDataFormatter.addRightAlign();
//                        printDataFormatter.addContent("$" + orderHistory.getTip_amount() + " ");
//                        printDataFormatter.addLineSeparator();
//                    }
//                }
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addTrailer();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addContent(copyName);
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addCenterAlign();
//                printDataFormatter.addContent(Preferences.get(mContext, Preferences.PRINT_FOOTER));
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//                printDataFormatter.addLineSeparator();
//
//                Log.d("PrintFragment", printDataFormatter.build());
//                POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
//                posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
//                posLinkPrinter.print(printDataFormatter.build(),this);
//
//                if(copyName.equals("Customer Copy"))
//                    if(Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
//                        //saz  printReceipt(orderResponse, "Merchant Copy");
//                    }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(ProcessResult processResult) {

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("SettingsGet"))  {
            try {
                JSONObject result = new JSONObject(response);
                /*{"is_tax":1,"print_name":"Vizypay","print_address_line1":"144 29th ST. #208",
                "print_address_line2":"West Des Moines, IA 50266","print_contact_no":"8559994142",
                "print_footer_text":"THANK YOU!","merchant_copy_in_print":0}*/

                Preferences.save(mContext, Preferences.ISTAX, result.getString("is_tax").equals("null") ? "0.00" : result.getString("is_tax"));
                Preferences.save(mContext, Preferences.ISPRINT_MERCHANTCOPY, result.getString("merchant_copy_in_print"));
                Preferences.save(mContext, Preferences.PRINT_NAME, result.getString("print_name"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS1, result.getString("print_address_line1"));
                Preferences.save(mContext, Preferences.PRINT_ADDRESS2, result.getString("print_address_line2"));
                Preferences.save(mContext, Preferences.PRINT_CONTACT, result.getString("print_contact_no"));
                Preferences.save(mContext, Preferences.PRINT_FOOTER, result.getString("print_footer_text"));
                Preferences.save(mContext, Preferences.APP_HEADER, result.getString("app_header"));
//                Preferences.save(mContext, Preferences.CREDIT_FEES, result.getString("credit_fees"));
                Preferences.save(mContext, Preferences.FEES_PAID, result.getString("fees_paid"));
                Preferences.save(mContext, Preferences.STOCK_TRACKING, result.getString("stock_tracking"));
                Preferences.save(mContext, Preferences.STOCK_NOTIFICATION, result.getString("stock_notification"));
                Preferences.save(mContext, Preferences.ADD_TIP, result.getString("tip_enable"));
                Preferences.save(mContext, Preferences.TIP_SUGGESTION_ENABLE, result.getJSONObject("user_details").getString("tip_suggestion_enable"));
                Preferences.save(mContext, Preferences.CUSTOME_TIP_SUGGESTION_ENABLE, result.getJSONObject("user_details").getString("tip_custom_enable"));
                String user_details = result.getString("user_details");
                String program_type = result.getJSONObject("user_details").getString("program_type");
                Preferences.save(mContext, Preferences.USER_DETAIL, user_details);
                Preferences.save(mContext, Preferences.PROGRAM_TYPE, program_type);
                Preferences.save(getActivity(), Preferences.NON_CASH_CHARGE, result.getJSONObject("user_details").getString("ncc_percent"));

                //Log.e("user_details==",Preferences.get(getContext(), Preferences.USER_DETAIL));
                //Log.e("program_type==",Preferences.get(getContext(), Preferences.PROGRAM_TYPE));


               /* switchCompatTax.setChecked(result.getString("is_tax").equals("1")?true:false);
                switchCompatMerchantCopy.setChecked(result.getString("merchant_copy_in_print").equals("1")?true:false);
                edtName.setText(result.getString("print_name"));
                edtFooterText.setText(result.getString("print_footer_text"));
                edtAddressLine1.setText(result.getString("print_address_line1"));
                edtAddressLine2.setText(result.getString("print_address_line2"));
                edtContact.setText(result.getString("print_contact_no"));
                view.findViewById(R.id.progressbar).setVisibility(View.GONE);*/
                /*if(result.getString("type").equals("success")){
                    clearData();
                    Toast.makeText(mContext, "Saved Successfully", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }else {
                    Toast.makeText(mContext, "Please try Again", Toast.LENGTH_LONG);
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals("orders")) {
            JSONObject result = null;
            Log.e("OrdersHistory", response+"");
            try {
                result = new JSONObject(response);

                final JSONObject mProductList = result.getJSONObject("data");

                Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                orderHistory = gson.fromJson(mProductList.toString(), new TypeToken<OrderHistory>() {
                }.getType());

                setValues();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (tag_json_obj.equals("orders_update")){
            /* *//*   FragmentManager fm = ((FragmentActivity) getActivity()).getSupportFragmentManager();
            fm.popBackStack();*//*
            Fragment fragment = new AllOrders();
            if (fragment != null) {
                FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "AllProduct");
                ft.commit();
            }*/
            Intent intent=new Intent(mContext, MainActivity.class);
           /* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*/
            intent.putExtra("Fragment", "OrderHistory");
            ((AppCompatActivity)mContext).startActivity(intent);
            ((AppCompatActivity)mContext).finish();
        }
        if (tag_json_obj.equals("order_update")){
            /* *//*   FragmentManager fm = ((FragmentActivity) getActivity()).getSupportFragmentManager();
            fm.popBackStack();*//*
            Fragment fragment = new AllOrders();
            if (fragment != null) {
                FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "AllProduct");
                ft.commit();
            }*/
            if(orderHistory.getTip_amount().equals(null) || orderHistory.getTip_amount().equals("0.00") || orderHistory.getTip_amount().isEmpty()){
                Toast.makeText(mContext, "Tip Added", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(mContext, "Tip Edited", Toast.LENGTH_LONG).show();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent(mContext, MainActivity.class);
           /* Bundle bundle=new Bundle();
            bundle.putString("Fragment", "OrderHistory");*/
                    intent.putExtra("Fragment", "OrderHistory");
                    ((AppCompatActivity)mContext).startActivity(intent);
                    ((AppCompatActivity)mContext).finish();
                }
            }, 300);

        }
        if (tag_json_obj.equals("Order_receiptEmail")){
            JSONObject result = null;
            try {
                result = new JSONObject(response);
                final JSONObject Receipt_Email = result.getJSONObject("data");
                Log.e("Receipt_Email", response);

                if (result.has("data")){

                    String sucess = "sucess";
                    opensucessDAilog(sucess);
                }else {

                    String sucess = "Fail";
                    opensucessDAilog(sucess);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void opensucessDAilog(String sucess) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_email_sms_sent, null, false);
        dialog.setContentView(view);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ImageView img_sucess = dialog.findViewById(R.id.img_sucess);
        ImageView img_fail = dialog.findViewById(R.id.img_fail);
        TextView tv_sucess = dialog.findViewById(R.id.tv_sucess);
        TextView tv_failiure = dialog.findViewById(R.id.tv_failiure);
        Button dialogButtonOKl = dialog.findViewById(R.id.dialogButtonOKl);

        if (sucess.equals("sucess")){
            img_fail.setVisibility(View.GONE);
            tv_failiure.setVisibility(View.GONE);
            img_sucess.setVisibility(View.VISIBLE);
            tv_sucess.setVisibility(View.VISIBLE);
        }else if (sucess.equals("Fail")){
            img_sucess.setVisibility(View.GONE);
            tv_sucess.setVisibility(View.GONE);
            img_fail.setVisibility(View.VISIBLE);
            tv_failiure.setVisibility(View.VISIBLE);
        }

        dialogButtonOKl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

  /*  public void callProductsAPI() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI + "?limit=200",
                "products", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }
*/

   /* @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("products")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mProductResponseList = gson.fromJson(mProductList.toString(), new TypeToken<List<Products>>() {
                    }.getType());
*/


              /*  } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/

}