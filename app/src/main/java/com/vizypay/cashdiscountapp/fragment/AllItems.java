package com.vizypay.cashdiscountapp.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.AllProductAdapter;
import com.vizypay.cashdiscountapp.adapter.CartListAdapter;
import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class AllItems extends Fragment implements IApiResponse {


    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<Products> mProductResponseList;
    AllProductAdapter allProductAdapter;
    CartListAdapter cartListAdapter;
    private GridLayoutManager mLayoutManager;
    ProgressBar pb;
    List<AdditionalDiscount> madiscountResponseList;
    TextView txtMessage;
    Button mRevertAllPriceBtn, btnTaxForAll;
   // Spinner spinAdditionalDiscount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.all_items, container, false);
        final String program_type = Preferences.get(getContext(), Preferences.PROGRAM_TYPE);
        // Setting ViewPager for each Tabs
        pb = view.findViewById(R.id.pb);
        txtMessage = view.findViewById(R.id.txtMessage);

        mContext = getActivity();
        mRevertAllPriceBtn = view.findViewById(R.id.btnRevertAll);
        btnTaxForAll = view.findViewById(R.id.btnTaxAll);
        //spinAdditionalDiscount = view.findViewById(R.id.spinAdditionalDiscount);
        //spinAdditionalDiscount.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        pb.setVisibility(View.VISIBLE);

        mRevertAllPriceBtn.setVisibility(View.GONE);


        if (Preferences.get(mContext, Preferences.KEY_APP_NAME_EN).equals("1")) {
            btnTaxForAll.setVisibility(View.VISIBLE);
        } else {
            btnTaxForAll.setVisibility(View.INVISIBLE);
        }
        btnTaxForAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTaxDialog();
            }
        });
        mRevertAllPriceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Revert Price")
                        .setMessage("Do you really want to revert all price?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                callRevertApi();

                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
        callProductsAPI();


        return view;

    }


    public void callProductsAPI() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI + "?limit=200",
                "products", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }

    public void callAdditionalDiscount() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.AdditionalDiscountGET + "?limit=200",
                "additionalD", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }


    public void callRevertApi() {
        mRevertAllPriceBtn.setVisibility(View.GONE);

        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsPriceAPI + "?type=revert",
                "revert_all", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("additionalD")) {

            try {
                JSONObject result = new JSONObject(response);
                // JSONArray array=result.getJSONArray("data");

                final JSONArray mADiscountList = result.getJSONArray("data");
                if (mADiscountList.length() > 0) {
                    // recyclerView.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    madiscountResponseList = gson.fromJson(mADiscountList.toString(), new TypeToken<List<AdditionalDiscount>>() {}.getType());

                   // AdditionalDiscountSpinAdapter additionalDiscountSpinAdapter = new AdditionalDiscountSpinAdapter(mContext, madiscountResponseList);
                    //spinAdditionalDiscount.setAdapter(additionalDiscountSpinAdapter);

                    AdditionalDiscount categoryList = new AdditionalDiscount();
                    categoryList.setName("Select Category");
                    categoryList.setId(0);
                  //  spinAdditionalDiscount.setAdapter(additionalDiscountSpinAdapter );
                  /*  productListAdapter = new ProductListAdapter( mProductResponseList, mContext, txtCartTotalPrice, mCartList);
                    recyclerView.setAdapter(productListAdapter);*/
                } else {
                    // txtMessage.setText("No Data Found");
                    // txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        if (tag_json_obj.equals("product_tax")) {
            try {
                JSONObject result = new JSONObject(response);
                //   Toast.makeText(mContext, "Success", Toast.LENGTH_LONG).show();
                if (result.getString("type").equals("success")) {
                    Toast.makeText(mContext, "Success", Toast.LENGTH_LONG).show();

                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Fragment fragment = new AllItems();
                    if (fragment != null) {

                        ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                        //ft.addToBackStack("Brand");
                        ft.commit();
                    }
                    //view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                } else {
                    Toast.makeText(mContext, "Try Later", Toast.LENGTH_LONG).show();
                    //view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("products")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);

                    if (Preferences.get(getContext(), Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(getContext(), Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0") )
                       { mRevertAllPriceBtn.setVisibility(View.GONE); Log.e("if PROGRAM_TYPE--", Preferences.get(getContext(), Preferences.PROGRAM_TYPE));}
                    else
                    {mRevertAllPriceBtn.setVisibility(View.VISIBLE); Log.e("else  PROGRAM_TYPE--", Preferences.get(getContext(), Preferences.PROGRAM_TYPE));}

                    txtMessage.setVisibility(View.GONE);
                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    mProductResponseList = gson.fromJson(mProductList.toString(), new TypeToken<List<Products>>() {
                    }.getType());

                    allProductAdapter = new AllProductAdapter(mProductResponseList, mContext);
                    recyclerView.setAdapter(allProductAdapter);

                } else {
                    txtMessage.setText("No Data Found");
                    txtMessage.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (tag_json_obj.equals("revert_all")) {


            Log.e("Revert RES", response);

            JSONObject result = null;
            try {
                result = new JSONObject(response);

                if (result.has("message")) {
                    Toast.makeText(mContext, result.getString("message"), Toast.LENGTH_SHORT).show();
                }

                callProductsAPI();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {


    }

    private void openTaxDialog() {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_tax, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtTax = dialog.findViewById(R.id.edtTax);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pb.setVisibility(View.VISIBLE);
                    callTaxAPI(edtTax.getText().toString());
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callTaxAPI(String tax) {
        // view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        // btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.TaxAPI + "?tax=" + tax,
                "product_tax", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));
    }
}
