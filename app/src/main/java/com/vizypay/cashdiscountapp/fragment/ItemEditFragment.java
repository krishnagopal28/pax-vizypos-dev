package com.vizypay.cashdiscountapp.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.stmt.query.In;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.Category;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.utility.ApiServices;
import com.vizypay.cashdiscountapp.utility.BitmapUtils;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.utility.Validation;
import com.vizypay.cashdiscountapp.utility.VolleyMultipartRequest;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.apache.http.util.TextUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.ContextCompat.checkSelfPermission;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Richa on 10/03/17.
 */

public class ItemEditFragment extends Fragment implements IApiResponse,AdapterView.OnItemSelectedListener  {
    EditText edtName, edtPrice, edtSKU, edtQuantity, edtTax;

    Button btnSubmit;

    Context mContext;
    View view;
    Button btnChoose, btnCapture;
    ImageView image;
    Bitmap bitmap;
    Intent cameraIntent;
    String selectedPath;
    File file1;
    Uri filePath, picUri;
    String imageFilePath;
    int rotation = 0;
    Uri photoURI;
    Bundle bundle;
    Products products;
    Spinner spinner;
    List<Category> mCategoryList;
    int selectedCategoryPos = -1;
    private EditText edtCPrice, edtMQuantity;
    // private EditText edtUnit;
    String filePath2;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_item, null);
        mContext = getActivity();

        bundle=getArguments();
        if(bundle!=null){
            products= (Products) bundle.getSerializable("Product");
        }
        initComponents(view);

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), 1234);
            }
        });
        btnCapture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            5678);
                } else {
                    if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                1112);
                    } else {

                        openCameraIntent();
                    }
                }



            }
        });


        btnSubmit.setText("Save");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!edtTax.getText().toString().isEmpty()) {
                        callTaxAPI(edtTax.getText().toString(), products.getId());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (chechValidation()) {

                    String myFormat = "yyyy-MM-dd"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", edtName.getText().toString().trim());
                    map.put("price", edtPrice.getText().toString().trim());
                    map.put("sku", edtSKU.getText().toString().trim());
                    map.put("units_in_stok", edtQuantity.getText().toString().trim().equals("null")?"0": edtQuantity.getText().toString().trim());
                    map.put("category_id",String.valueOf(mCategoryList.get(selectedCategoryPos).getId()));
                    map.put("cost",edtCPrice.getText().toString().trim().equals("null")?"0": edtQuantity.getText().toString().trim());
                    map.put("minimum_qty",edtMQuantity.getText().toString().trim());

                    callProductEdit(map, products.getId());

                    Toast.makeText(mContext, "Edited Successfully", Toast.LENGTH_LONG);

                }
                //uploadDataToServer();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Fragment fragment = new AllItemsExpandable();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();

                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }

    public void callProductEdit(HashMap<String, String> map,int id) {
        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI+"/"+id,
                "product_edit", map, Request.Method.PUT, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }
    public void callTaxAPI(String tax,int id) {
        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        // btnSubmit.setEnabled(false);
        ApiRequest apiRequest = new ApiRequest(getActivity(), this);
        apiRequest.postRequest(Constants.BaseURL + Constants.TaxAPI+"?tax="+tax+"&ids[]="+id,
                "product_tax", null, Request.Method.GET, Preferences.get(getActivity(), Preferences.KEY_USER_TOKEN));

    }

    public boolean chechValidation() {
        boolean ret = true;
        if (!Validation.hasText(edtName))
            ret = false;
        //  if (!Validation.hasText(edtSKU))
        // ret = false;
        if (!Validation.hasText(edtPrice)) {
            //Utility.openCustomDialog(getActivity(), getActivity().getResources().getString(R.string.address_req));
            ret = false;
        }

        if(selectedCategoryPos<=0){
            Toast.makeText(getActivity(), "Please select category.", Toast.LENGTH_LONG).show();
            ret = false;
        }

        return ret;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5678) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();

                if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1112);
                } else {

                    openCameraIntent();

                }
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == 1111) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "write permission granted", Toast.LENGTH_LONG).show();
                Glide.with(getActivity()).load(filePath).into(image);
                file1 = new File(String.valueOf(filePath));
                try {
                    Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath), mContext, (filePath));
//                    selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "write permission denied", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 1112) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "write permission granted", Toast.LENGTH_LONG).show();

                openCameraIntent();

            } else {
                Toast.makeText(getActivity(), "write permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void initComponents(View view) {
        try {
            //radiogroup = (RadioGroup) view.findViewById(R.id.radiogroup);
            edtName = (EditText) view.findViewById(R.id.edtItemName);

            edtPrice = (EditText) view.findViewById(R.id.edtPrice);

            edtSKU = (EditText) view.findViewById(R.id.edtSKU);

            edtCPrice = (EditText) view.findViewById(R.id.edtCPrice);
            edtCPrice.setVisibility(View.VISIBLE);
            //edtSKU = (EditText) view.findViewById(R.id.edtSKU);
            //edtQuantity = (EditText) view.findViewById(R.id.edtQuantity);
            edtMQuantity = (EditText) view.findViewById(R.id.edtMQuantity);
            edtMQuantity.setVisibility(View.VISIBLE);
            edtQuantity = (EditText) view.findViewById(R.id.edtQuantity);
            // edtUnit = (EditText) view.findViewById(R.id.edtUnit);
            edtTax = (EditText) view.findViewById(R.id.edtTax);
            //edtQuantity.setVisibility(View.GONE);
            // gridPay = (GridView) view.findViewById(R.id.gridPayOptions);
            image = view.findViewById(R.id.imgImage);
            btnChoose = view.findViewById(R.id.btnChoose);
            btnCapture = view.findViewById(R.id.btnCapture);
            btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

            edtTax.setText(products.getTax()+"");
            edtName.setText(products.getName()+"");
            edtPrice.setText(products.getPrice()+"");
            edtSKU.setText(products.getSku());
            edtCPrice.setText(products.getCost()+"");
            edtQuantity.setText(products.getUnits_in_stok()+"");
            edtMQuantity.setText(products.getMinimum_qty()+"");

            if(products.getMain_photo()!=null) {
                try {
                    if (!products.getMain_photo().getUrl().equals(""))
                        Glide.with(getActivity()).load(Constants.BaseURLImage + products.getMain_photo().getUrl()).into(image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            spinner = view.findViewById(R.id.categorySpinner);

            // Spinner click listener
            spinner.setOnItemSelectedListener(this);
            setCategorySpinnerAdapter(null);

            callCategoryApi();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("category")) {
            Log.e("RES", response);
            try {
                JSONObject result = new JSONObject(response);
                final JSONArray mProductList = result.getJSONArray("data");
                if (mProductList.length() > 0) {

                    Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                            .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                    List<Category> categoryList = gson.fromJson(mProductList.toString(), new TypeToken<List<Category>>() {
                    }.getType());;


                    setCategorySpinnerAdapter(categoryList);
                    setCategoryPosition(categoryList);



                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        if (tag_json_obj.equals("product_edit")) {
            try {
                JSONObject result = new JSONObject(response);
                if (result.getJSONObject("data").has("price")) {
                    /*{"data":{"price":"123","name":"test66","units_in_stok":"23","sku":"123456","created_by_id":3,"updated_at":"2020-05-14 10:33:00","created_at":"2020-05-14 10:33:00","id":33,"main_photo":null,"additional_photos":[],"media":[]}}*/
                    JSONObject res=result.getJSONObject("data");
                    Integer id= Integer.valueOf(res.getString("id"));
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                    btnSubmit.setEnabled(true);
                    if(filePath!=null ) {
//                        Toast.makeText(mContext, "Product updated successfully. Uploading image to server.", Toast.LENGTH_SHORT).show();

                        String email = Preferences.get(mContext, Preferences.EMAIL);
                        String password = Preferences.get(mContext, Preferences.PASSWORD);
                        callLoginAPI(email,password,id);


                        Log.e("URL::", Constants.BaseURL + Constants.ProductsAddMAINPHOTOAPI + "/" + id);
                    }else if (imageFilePath!= null){

                        String email = Preferences.get(mContext, Preferences.EMAIL);
                        String password = Preferences.get(mContext, Preferences.PASSWORD);
                        callLoginAPI(email,password,id);
                    } else {
                        Toast.makeText(mContext, "Product updated successfully", Toast.LENGTH_SHORT).show();
                        clearData();
                    }
                } else {
                    Toast.makeText(mContext, "Error while updating product. Please try later", Toast.LENGTH_SHORT).show();
                    clearData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals("product_tax")) {
            try {
                JSONObject result = new JSONObject(response);
                if(result.getString("type").equals("success")){
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }else {
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void callLoginAPI(String email1, String password1,Integer id){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email1);
        params.put("password", password1);
        Log.d("URL", Constants.BaseURL+Constants.loginAPI);
        Log.d("Params", params.toString());
        JsonObjectRequest req = new JsonObjectRequest(Constants.BaseURL_1+Constants.loginAPI, new JSONObject(
                params), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.w("myApp", "status code..." + response);

                    if(response.has("success")){

                        Preferences.save(mContext, Preferences.KEY_USER_TOKEN, response.getJSONObject("success").getString("token"));

                        String token = response.getJSONObject("success").getString("token");
                        Preferences.save(mContext, Preferences.KEY_USER_TOKEN, response.getJSONObject("success").getString("token"));
                        view.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                        uploadImageVolley(id,token);
                    } else if (response.has("error")) {
                        Log.e("res", response.optString("error"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w("error in response", "Error: " + error.getMessage());
                Toast.makeText(mContext, "Invalid Credentials, Please try again.", Toast.LENGTH_SHORT).show();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }
        ){

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    String json = null;
                    try {
                        Log.d("parse jsonObject",volleyError.networkResponse.data.toString() );
                        String responseBody = new String(volleyError.networkResponse.data);
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.d("parse jsonObject", jsonObject.toString());




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return volleyError;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type", "application/json; charset=utf-8");
                Log.e("Header", params.toString());
                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(req);
    }

    private String getRealPathFromURIPath(Uri contentURI, Context mContext) {
        String result;
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public void clearData() {
        edtName.setText(null);
        edtPrice.setText(null);
        edtSKU.setText(null);
        edtQuantity.setText(null);
        FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
        Fragment fragment = new AllItemsExpandable();
        if (fragment != null) {

            ft.replace(R.id.frame_layout, fragment, "ItemAdd");
            //ft.addToBackStack("Brand");
            ft.commit();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1234 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);


                if (checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE")
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1111);
                } else {
                    Glide.with(getActivity()).load(filePath).into(image);
                    file1 = new File(String.valueOf(filePath));
                    try {
                        Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath), mContext, (filePath));
//                        selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == 100) {

            if (checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE")
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1112);
            } else {
                Glide.with(getActivity()).load(imageFilePath).into(image);
                file1 = new File(String.valueOf(imageFilePath));
                try {
                    Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoURI), mContext, Uri.parse(imageFilePath));
                    bitmap = image;
//                    selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == 9876 && resultCode == Activity.RESULT_OK) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (checkSelfPermission(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE")
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1112);
            } else {

                try {
                    // selectedPath = (getRealPathFromURI(picUri));

                    Glide.with(getActivity()).load(picUri).into(image);
                    file1 = new File(String.valueOf(picUri));
                    try {
                        Bitmap image = rotateImageIfRequired(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri), mContext, (picUri));
//                        selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(mContext, image, ".jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    public static Bitmap rotateImageIfRequired(Bitmap img, Context context, Uri selectedImage) throws IOException {
/*
        if (selectedImage.getScheme().equals("content")) {
            String[] projection = { MediaStore.Images.ImageColumns.ORIENTATION };
            Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
            if (c.moveToFirst()) {
                final int rotation = c.getInt(0);
                c.close();
                return rotateImage(img, rotation);
            }
            return img;
        } else*/
//        try {
//            ExifInterface ei = new ExifInterface(selectedImage.getPath());
//            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    return rotateImage(img, 90);
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    return rotateImage(img, 180);
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    return rotateImage(img, 270);
//                default:
//                    return img;
//            }
//        } catch (Exception e) {
////            e.printStackTrace();
//        if (selectedImage.getScheme().equals("content")) {
//            String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
//            Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
//            if (c.moveToFirst()) {
//                final int rotation = c.getInt(0);
//                c.close();
//                return rotateImage(img, rotation);
//            }
//            return img;
//        } else
            return img;
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    private Bitmap rotateBitmap(Bitmap source, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public File createImageFile(Context context) throws IOException {

        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void openCameraIntent() {

        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //pictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_USER);
        if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile(getActivity());
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                // =FileProvider.getUriForFile(getActivity(),getActivity().getPackageName() +".provider", photoFile);
                photoURI = Uri.fromFile(photoFile);
               /* try {
                    selectedPath = photoURI.getPath();
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //pictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
                startActivityForResult(pictureIntent,
                        100);
            }
        }
    }


    private void uploadImageVolley(Integer id,String token) {

//        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(mContext);
//        String previouslyEncodedImage = shre.getString("image_data", "");
//        filePath = Uri.fromFile(new File(previouslyEncodedImage));
        if (imageFilePath != null){
            filePath2 = getRealPathFromURIPath(Uri.parse(imageFilePath), mContext);
            file1 = new File(filePath2);
        }else {
            filePath2 = getRealPathFromURIPath(filePath, mContext);
            file1 = new File(filePath2);
        }

        Log.d("file1::", file1 + "");
//        String mimeType = null;
//        if (ContentResolver.SCHEME_CONTENT.equals(filePath.getScheme())) {
//            ContentResolver cr = mContext.getContentResolver();
//            mimeType = cr.getType(filePath);
//        } else {
//            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(filePath
//                    .toString());
//            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
//                    fileExtension.toLowerCase());
//
//            Log.d("mimeType::", mimeType+"");
//        }


        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("main_photo", file1.getName(), requestFile);


        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10,TimeUnit.MINUTES)
                .writeTimeout(10,TimeUnit.MINUTES).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BaseURL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiServices uploadImage = retrofit.create(ApiServices.class);
//        Integer id = Integer.valueOf(Preferences.get(mContext, Preferences.ORDER_ID));
        String bearar = "Bearer"+" "+ token;
        String Cookie = "XSRF-TOKEN=eyJpdiI6IlMzOEVpVnM5Sjc2NURlTWlRaHFtOEE9PSIsInZhbHVlIjoiVTlJbEs0NkM1VnY2RTFIeW1kd2R2WXROeXJxZmJcL3JYa0FUaDdaTDZhQnpPQ0JVM0VSUmxMWTNUZldOa1RYY0IiLCJtYWMiOiJhMjBhOTM3ZTk3NzgzZDljMzJiNzRmM2E4Zjg0MGY2NWU1N2I2ZDc4ZGE5ZTkzNzdjMjdhNWIxNjc2N2Q3NmU5In0%3D; vizypos_merchant_portal_session=eyJpdiI6InJ6dnI5eWFLaVl3aXJHK3o5cXh1aGc9PSIsInZhbHVlIjoiQXphbEt5UlV5VVhVWHNGMHRVd3JSNENDWmtHNjdpdXNxamZ5SFhGRWI2cmhqOVRXRzFFbE9zTVZcLzNSNjhaTFciLCJtYWMiOiIzZjYzOWJiZWNjNGZmNzI1NjdhYjFjODhlZDM3ODliNzAxNjYxYmM1N2JmNTFkMWNiZGE5OGY4ODVkZDgxNTIwIn0%3D";
        Call<ResponseBody> fileUpload = uploadImage.uploadFileItem(id,bearar,fileToUpload);//"fileToUpload",

        Log.d("id::", id+"");
        fileUpload.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.code() == 200){
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                    Fragment fragment = new AllItemsExpandable();
                    // AskOptionDialog().show();
                    FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                    //Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    //bundle.putInt("OrderID", mList.get(position).getId());
                    //fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();
                }else {
                    Toast.makeText(mContext, "Something went wrong. Please try again after some time.", Toast.LENGTH_LONG).show();
                    view.findViewById(R.id.progressbar).setVisibility(View.GONE);
                }
                Log.d("responcesignature::", response+"");
                try {
                    String res = response.body().toString();
                    JSONObject josn = new JSONObject(res);
                    Log.d("responcefdv::::", josn+"");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Log.d("uploadimage", "Error occur " + t.getMessage());
                }
            }
        });

    }
    public void callCategoryApi() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.CategoriesAPI + "?limit=200",
                "category", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        selectedCategoryPos = pos;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void setCategorySpinnerAdapter(List<Category> categoryList){
        mCategoryList = new ArrayList<>() ;

        Category category = new Category();
        category.setId(-1);
        category.setName("Select Category");
        mCategoryList.add(category);

        if(categoryList!=null){
            mCategoryList.addAll(categoryList);

        }
        // Creating adapter for spinner
        ArrayAdapter<Category> dataAdapter = new ArrayAdapter<Category>(getActivity(), android.R.layout.simple_spinner_item, mCategoryList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    private void setCategoryPosition(List<Category> categoryList){
        String categoryId = products.getCategory_id();
        if(categoryId!=null && !TextUtils.isBlank(categoryId)){

            int i=0;
            for (Category category : categoryList){
                i++;
                if(category.getId()== Integer.parseInt(categoryId)){
                    spinner.setSelection(i);

                    break;
                }
            }

        }
    }
}