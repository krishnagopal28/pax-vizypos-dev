package com.vizypay.cashdiscountapp.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.adapter.OrderItemAdapter;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.DatabaseHelper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReportFragment extends Fragment implements IApiResponse,  View.OnClickListener {


    RecyclerView recyclerView, recyclerViewCart;
    Context mContext;
    List<OrderItems> orderItems;
    OrderItemAdapter orderItemAdapter;
    private GridLayoutManager mLayoutManager;
    LinearLayout linearDateRange, linearTip;
    EditText edtFromDate, edtToDate;
    ProgressBar pb;
    // TextView txtMessage;
    Bundle bundle;
    String from_date, to_date;
    SimpleDateFormat simpleDateFormat;
    Spinner spinner;
    TextView txtTotalSale,txtTotalCardSale,txtTotalCashSale, txtTotalFeesPaid, txtTotalTaxCollected, txtTotalTipCollected;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    Button btnSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        mContext=getActivity();
        SQLiteDatabase db = helper.getWritableDatabase();
        View view = inflater.inflate(R.layout.report_fragment, container, false);
        // Setting ViewPager for each Tabs
        pb = view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);
        //txtMessage = view.findViewById(R.id.txtMessage);
//        view.findViewById(R.id.linearHeader).setVisibility(View.VISIBLE);
        //      view.findViewById(R.id.linearItemsHeader).setVisibility(View.VISIBLE);

        spinner=view.findViewById(R.id.spinnerReport);
        btnSearch=view.findViewById(R.id.btnSearch);
        txtTotalSale=view.findViewById(R.id.txtTotalSales);
        edtFromDate=view.findViewById(R.id.fromDate);
        edtToDate=view.findViewById(R.id.toDate);
        linearDateRange=view.findViewById(R.id.linearDateRange);
        txtTotalCardSale=view.findViewById(R.id.txtTotalCardSales);
        txtTotalCashSale=view.findViewById(R.id.txtTotalCashSales);
        txtTotalFeesPaid=view.findViewById(R.id.txtFeesPaid);
        txtTotalTaxCollected=view.findViewById(R.id.txtTaxCollected);
        linearTip=view.findViewById(R.id.linearTip);
        txtTotalTipCollected=view.findViewById(R.id.txtTIP);
        simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        //final Date from_date=new Date();
        //Date to_date=new Date();
        from_date=simpleDateFormat.format(new Date());
        to_date=simpleDateFormat.format(new Date());
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callReportAPI(from_date, to_date);
            }
        });


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setDateTimeField();
        spinner.setSelection(0);

        callReportAPI(from_date, to_date);
        if(Preferences.get(mContext, Preferences.ADD_TIP).equals("1")){
            linearTip.setVisibility(View.VISIBLE);
        }else {
            linearTip.setVisibility(View.GONE);
        }
        linearDateRange.setVisibility(View.VISIBLE);
        edtToDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                toDatePickerDialog.getDatePicker().setMinDate(Utility.getLongMillFromeDateString(edtFromDate.getText().toString()));
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0){
                    linearDateRange.setVisibility(View.GONE);
                    from_date=simpleDateFormat.format(new Date());
                    to_date=simpleDateFormat.format(new Date());
                }else if(i==1){
                    linearDateRange.setVisibility(View.GONE);
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -7);
                    Date todate1 = cal.getTime();
                    from_date=simpleDateFormat.format(todate1);
                    to_date=simpleDateFormat.format(new Date());
                }else if(i==2){
                    linearDateRange.setVisibility(View.GONE);
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -30);
                    Date todate1 = cal.getTime();
                    to_date=simpleDateFormat.format(new Date());
                    from_date=simpleDateFormat.format(todate1);
                }else if(i==3){
                    linearDateRange.setVisibility(View.VISIBLE);
                    from_date=edtFromDate.getText().toString();
                    to_date=edtToDate.getText().toString();
                }
                callReportAPI(from_date, to_date);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*mContext = getActivity();
        bundle=getArguments();
        if ((bundle!=null)){
            orderHistory= (OrderHistory) bundle.get("Order");
        }*/

       /* try {
            txtName.append(orderHistory.getName());
            txtEmail.append(orderHistory.getEmail());
            txtMobile.append(orderHistory.getMobile());
        }catch (Exception e){
            e.printStackTrace();
        }*/

       /* recyclerView = view.findViewById(R.id.recycleView);
        recyclerViewCart = view.findViewById(R.id.recycleViewCart);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);*/
        pb.setVisibility(View.VISIBLE);

   /*     orderItemAdapter = new OrderItemAdapter(orderHistory.getOrderItems(), mContext);
        recyclerView.setAdapter(orderItemAdapter);*/
        //callProductsAPI();

        return view;

    }

    public void callReportAPI(String from_date, String to_date) {
        Log.e("TEST",Constants.BaseURL + Constants.REPORTAPI + "?from_date="+from_date+"&to_date="+to_date );
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.REPORTAPI + "?from_date="+from_date+"&to_date="+to_date,
                "products", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));


    }
   /* public void callReportAPI() {
        ApiRequest apiRequest = new ApiRequest(mContext, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.REPORTAPI + "?",
                "products", Request.Method.GET, Preferences.get(mContext, Preferences.KEY_USER_TOKEN));
    }*/

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("products")) {
            Log.e("RES", response);
            try {
                pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);

                txtTotalSale.setText("$"+result.getString("total_sales"));
                txtTotalCardSale.setText("$"+result.getString("card_sales"));
                txtTotalCashSale.setText("$"+result.getString("cash_sales"));
                txtTotalFeesPaid.setText("$"+result.getString("fees_paid"));
                txtTotalTaxCollected.setText("$"+result.getString("total_tax"));
                txtTotalTipCollected.setText("$"+result.getString("total_tip_amount"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    @Override
    public void onClick(View v) {
        if(v == edtFromDate) {
            fromDatePickerDialog.show();
        } else if(v == edtToDate) {
            toDatePickerDialog.show();
        }
    }
    private void setDateTimeField() {
        edtFromDate.setOnClickListener(this);
        edtToDate.setOnClickListener(this);

        Calendar newCalendarend = Calendar.getInstance();
        Calendar newCalendarfrom = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        /* Date End Date */
        newCalendarend.add(Calendar.DATE, 0);
        edtToDate.setText(dateFormat.format(newCalendarend.getTime()));
        /* Date From Date */
        // newCalendarfrom.add(Calendar.DATE, -1);
        newCalendarfrom.add(Calendar.DATE, 0);
        edtFromDate.setText(dateFormat.format(newCalendarfrom.getTime()));
        Log.e("Date Print====", dateFormat.format(newCalendarend.getTime()));

        fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edtFromDate.setText(dateFormatter.format(newDate.getTime()));
                from_date=dateFormatter.format(newDate.getTime());
            }

        },newCalendarfrom.get(Calendar.YEAR), newCalendarfrom.get(Calendar.MONTH), newCalendarfrom.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edtToDate.setText(dateFormatter.format(newDate.getTime()));
                to_date=dateFormatter.format(newDate.getTime());
            }

        },newCalendarend.get(Calendar.YEAR), newCalendarend.get(Calendar.MONTH), newCalendarend.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        toDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

}
