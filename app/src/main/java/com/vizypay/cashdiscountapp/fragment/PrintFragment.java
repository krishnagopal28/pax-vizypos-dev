package com.vizypay.cashdiscountapp.fragment;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.pax.poslink.peripheries.POSLinkPrinter;
import com.pax.poslink.peripheries.ProcessResult;
import com.vizypay.cashdiscountapp.R;


public class PrintFragment extends Fragment  implements POSLinkPrinter.PrintListener {


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.print_fragment, null);

        Bitmap viewBitmap = getBitmapFromView(view);

        //  POSLinkPrinter.getInstance(getActivity()).print(viewBitmap,this);

        POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
        printDataFormatter.addCenterAlign();
        printDataFormatter.addHeader();
        printDataFormatter.addContent("Vizypay");
        printDataFormatter.addLineSeparator();
        printDataFormatter.addLineSeparator();

        printDataFormatter.addContent("2020-09-16");
        printDataFormatter.addContent(" 10:11:00");
        printDataFormatter.addLineSeparator();
        printDataFormatter.addNormalFont();
        printDataFormatter.addContent("Item 1");
        printDataFormatter.addLineSeparator();
        printDataFormatter.addContent("Item 2");
        printDataFormatter.addLineSeparator();
        printDataFormatter.addContent("Item 3");
        printDataFormatter.addLineSeparator();
        printDataFormatter.addBigFont();
        printDataFormatter.addContent("Total : ");
        printDataFormatter.addInvert();
        printDataFormatter.addTrailer();
        printDataFormatter.addContent("Trailer 1");

        Log.d("PrintFragment",printDataFormatter.build());
        POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(getActivity());
        posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
        posLinkPrinter.print( printDataFormatter.build(),this);
        return view;
    }


    public static Bitmap getBitmapFromView(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return bitmap;
    }

    @Override
    public void onSuccess() {

        Log.e("PrintFragment","Printed successfully. ");

    }

    @Override
    public void onError(ProcessResult processResult) {
        Log.e("PrintFragment","Print Error. "+processResult.getMessage());

    }
}