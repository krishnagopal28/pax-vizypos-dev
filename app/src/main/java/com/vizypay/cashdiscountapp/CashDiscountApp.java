package com.vizypay.cashdiscountapp;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

import com.pax.poslink.CommSetting;
import com.pax.poslink.POSLinkAndroid;
import com.pax.poslink.broadpos.BroadPOSReceiverHelper;
import com.pax.poslink.broadpos.ReceiverResult;

import com.pax.poslink.util.LogStaticWrapper;


public class CashDiscountApp extends Application {

    private static CashDiscountApp instance;

    public static CashDiscountApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        try {
            Context applicationContext = getApplicationContext();
//            IDAL dal = NeptuneDiamondUser.getInstance().getDal(applicationContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }



    private void init() {

        CommSetting commSetting = setupSetting(getApplicationContext());
        POSLinkAndroid.init(getApplicationContext(), commSetting);
        android.util.Log.i("DEBUG", "Start Application");

        BroadPOSReceiverHelper.getInstance(this).setReceiverListener(new BroadPOSReceiverHelper.ReceiverListener() {
            @Override
            public void onReceiverFromBroadPOS(ReceiverResult receiverResult) {
              //  UIUtil.showToast(getApplicationContext(), receiverResult.getCode() + ": " + receiverResult.getMessage(), Toast.LENGTH_LONG);
                LogStaticWrapper.getLog().v("Message Form BroadPOS: " + receiverResult.getCode() + ", " + receiverResult.getMessage());
            }
        });
        LogStaticWrapper.getLog().v("DeviceMode:" + Build.MODEL);
    }

    private static CommSetting setupSetting(Context context) {
        CommSetting commSetting =  new CommSetting();

        if (Build.MODEL.startsWith("E")) {
            commSetting.setType(CommSetting.USB);
        } else if (Build.MODEL.startsWith("A9")){
            commSetting.setType(CommSetting.AIDL);
        }else if (Build.MODEL.startsWith("A8")){
            commSetting.setType(CommSetting.AIDL);
        } else if (Build.MODEL.startsWith("A6")){
            commSetting.setType(CommSetting.AIDL);
        }else {
            commSetting.setType(CommSetting.TCP);
        }

        commSetting.setTimeOut("60000");
        commSetting.setSerialPort("COM1");
        //commSetting.setSerialPort("Ethernet");
        commSetting.setBaudRate("9600");
        commSetting.setDestIP("172.16.20.15");
        commSetting.setDestPort("10009");
        commSetting.setMacAddr("");
        commSetting.setEnableProxy(false);



        return commSetting;
    }



    /**
     * Determine if the current application is in debug state
     */
    public static boolean isApkInDebug(Context context) {
        try {
            ApplicationInfo info = context.getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if it is a test version
     */
    public static boolean isApkForTest() {
        String versionName = BuildConfig.VERSION_NAME;
        CharSequence c = "T";
        return versionName.contains(c);
    }
}
