/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 *
 */
package com.vizypay.cashdiscountapp.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AppConstant {
    public static final String ORDER_PACKAGE = "";
    private static Context mContext;
    private String oauthToken, oauth_secret;
    private static String locale;
    public static Locale mLocale;
    private JSONObject mBody, mUserDetail;
    private boolean isRetryOption = false, mIsLoginSignUpRequest = false;
    public ProgressDialog pDialog;
    public Map<String, String> postParams, mAuthParams, mRequestParams;
    int mStatusCode;
    public static final int REQUEST_TIMEOUT_MS = 10000;
    public static final float BACK_OF_MULTIPLIER = 2.0f;
    public static final int NO_OF_RETRY_ATTEMPTS = 3;
    // GridView image padding
    public static final int GRID_PADDING = 2; // in dp
    // Number of items shown per page
    // by default 20 but user can configure this
    public static final int LIMIT = 20;
    public static int NUM_OF_COLUMNS_FOR_VIEW_PAGE = 3;
    public static int NUM_OF_COLUMNS_FOR_PHOTO_GRID = 2;
    public static final int FEATURED_CONTENT_LIMIT = 5;
    // GridView image padding
    public static final int STICKERS_GRID_PADDING = 8; // in dp
    // Default url for data access
    public static final String DEFAULT_URL = "http://madooh.com/api/rest/";
    /*X-Authorization-Token: 856b298a254d720fae4e40509a3723a0
X-Authorization-Time:
User-Agent:*/
    public static final String oauth_consumer_key = "856b298a254d720fae4e40509a3723a0";
    public static final String tag_json_obj = "json_obj_req";
    public static boolean isLocationEnable = false;
    public static String mLocationType = "";
    public static int isDeviceLocationEnable = 0, isDeviceLocationChange = 1;
    public static final String ACTION_PAYMENT_PROCESSED = "com.clover.intent.action.PAYMENT_PROCESSED";

    public AppConstant(Context context) {
        mContext = context;
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("please wait" + "…");
        pDialog.setCancelable(false);
        initializeVariable();
    }
    /**
     * Method to initialize class member variables.
     */
    public void initializeVariable() {
        mAuthParams = new HashMap<>();
        mRequestParams = new HashMap<>();

        /*X-Authorization-Token: 856b298a254d720fae4e40509a3723a0
X-Authorization-Time:
User-Agent:*/
        mAuthParams.put("X-Authorization-Token", oauth_consumer_key);
        mAuthParams.put("X-Authorization-Time", String.valueOf(Calendar.getInstance().getTime()));

       /* if (checkManifestPermission(android.Manifest.permission.READ_PHONE_STATE)) {
            TelephonyManager  tm=(TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
            mRequestParams.put("device_id", tm.getDeviceId());
        }*/
    }
    public boolean isLoggedOutUser() {
        return oauthToken == null;
    }

    public Map<String, String> getAuthenticationParams() {
        return mAuthParams;
    }
    public Map<String, String> getRequestParams() {
        return mRequestParams;
    }
    /**
     * Get request execution for getting the response from server.
     *
     * @param url              Url on which the request will be executed
     * @param responseListener Interface used to listen response events
     */
    public void getJsonResponseFromUrl(String url, final OnResponseListener responseListener) {
        manageResponse(url, Request.Method.GET, null, responseListener);
    }
    /**
     * Used to execute post request on server without looking for response.
     *
     * @param url    Url on which the request will be executed
     * @param params Parameters required for executing the post request on server
     */
    public void postJsonRequest(String url, Map<String, String> params, final OnResponseListener responseListener) {
        manageResponse(url, Request.Method.POST, params, responseListener);
    }
    /**
     * Used to execute post request on server without looking for response and params.
     *
     * @param url    Url on which the request will be executed
     */
    public void postJsonRequest(String url) {
        manageResponse(url, Request.Method.POST, new HashMap<String, String>(), null);
    }
    /**
     * Used to execute post request on server without params.
     *
     * @param url    Url on which the request will be executed
     * @param responseListener Interface used to listen response events
     */
    public void postJsonRequestWithoutParams(String url, final OnResponseListener responseListener) {
        manageResponse(url, Request.Method.POST, new HashMap<String, String>(), responseListener);
    }
    /**
     * Used to execute post request on server..
     *
     * @param url              Url on which the request will be executed
     * @param params           Parameters required for executing the post request on server
     * @param responseListener Interface used to listen response events
     */
    public void postLoginSignUpRequest(String url, Map<String, String> params,
                                       final OnResponseListener responseListener) {
        mIsLoginSignUpRequest = true;
        manageResponse(url, Request.Method.POST, params, responseListener);
    }
    /**
     * Used to execute post request on server..
     *
     * @param url              Url on which the request will be executed
     * @param params           Parameters required for executing the post request on server
     * @param responseListener Interface used to listen response events
     */
    public void postJsonResponseForUrl(String url, Map<String, String> params,
                                       final OnResponseListener responseListener) {
        manageResponse(url, Request.Method.POST, params, responseListener);
    }
    /**
     * Used to execute delete request on server..
     *
     * @param params           parameters required for request execution
     * @param url              Url on which the request will be executed
     * @param responseListener Interface used to listen response events
     */
    public void deleteResponseForUrl(String url, Map<String, String> params,
                                     final OnResponseListener responseListener) {
        manageResponse(url, Request.Method.DELETE, params, responseListener);
    }
    /**
     * Used to update song tally count.
     *
     * @param params           Parameters required for executing the put request on server
     * @param url              Url on which the request will be executed
     * @param responseListener Interface used to listen response events
     */
    public void putResponseForUrl(String url, Map<String, String> params,
                                  final OnResponseListener responseListener) {
        manageResponse(url, Request.Method.PUT, params, responseListener);
    }
    /**
     * Method to manage response for GET, POST, DELETE, and PUT Request.
     *
     * @param url                Url of calling service.
     * @param method             Type of method.
     * @param params             Post Params.
     * @param onResponseListener Interface used to listen response events
     */
    public void manageResponse(String url, final int method, Map<String, String> params,
                               final OnResponseListener onResponseListener) {
        try {
           /* if (!url.contains("graph")) {

                url = buildQueryString(url, mAuthParams);
                // Put Language Params, location params, and version params in Params
               // url = buildQueryString(url, mRequestParams);
            }*/
            try {
               // url = buildQueryString(url, mAuthParams);
                postParams = params;
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.d("URL", url);
            Log.d("Params", postParams.toString());
            StringRequest request = new StringRequest(method, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        if (response != null && !response.isEmpty()) {
                            try {
                                onResponseListener.onTaskCompleted(new JSONObject(response));
                                Log.d("RESP", response);
                               /* JSONObject json = new JSONObject(response);
                                mStatusCode = json.optInt("status_code");
                                mBody = json.optJSONObject("body");
                                if (mStatusCode != 0 && isRequestSuccessful(mStatusCode) && mBody == null) {
                                    JSONArray bodyJsonArray = json.optJSONArray("body");
                                    mBody = convertToJsonObject(bodyJsonArray);
                                    if (mBody != null && mBody.length() == 0 && json.optString("body") != null) {
                                        mBody = json;
                                    }
                                }
                                if (mStatusCode == 406) {
                                    eraseUserDatabase();
                                } else if (isRequestSuccessful(mStatusCode) && onResponseListener != null) {
                                    onResponseListener.onTaskCompleted(new JSONObject(response));
                                } else if (onResponseListener != null) {
                                    String message = json.optString("message");
                                    String errorCode = json.optString("error_code");
                                    if (errorCode != null && (errorCode.equals("email_not_verified") ||
                                            errorCode.equals("not_approved") || errorCode.equals("subscription_fail"))) {
                                        message = json.optString("error_code");
                                    }
                                    onResponseListener.onErrorInExecutingTask(message, isRetryOption);
                                }
*/
                            }catch (Exception e){
                                e.printStackTrace();
                                onResponseListener.onTaskCompleted(new JSONObject(response));

                            }
                        } else {
                            if (onResponseListener != null) {
                                onResponseListener.onErrorInExecutingTask("retry", isRetryOption);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (onResponseListener != null) {
                            onResponseListener.onErrorInExecutingTask("Parse Error", isRetryOption);
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (onResponseListener != null) {
                        onResponseListener.onErrorInExecutingTask(displayVolleyError(error), isRetryOption);
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    if (mIsLoginSignUpRequest) {
                        String registrationId ="";// FirebaseInstanceId.getInstance().getToken();
                        if (registrationId != null && !registrationId.isEmpty()) {
                            postParams.put("registration_id", registrationId);
                            postParams.put("device_uuid", getDeviceUUID());
                        }
                    }
                    return postParams;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return mAuthParams;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    }
                    return volleyError;
                }
            };
            //Setting timeout to 0 to fix multiple post request at once
            if (method == Request.Method.POST) {
                request.setRetryPolicy(new DefaultRetryPolicy(
                        0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            } else {
                request.setRetryPolicy(new DefaultRetryPolicy(
                        REQUEST_TIMEOUT_MS,
                        NO_OF_RETRY_ATTEMPTS,
                        BACK_OF_MULTIPLIER));
            }
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(request, tag_json_obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isRequestSuccessful(int statusCode) {
        switch (statusCode) {
            case 200:
                return true;
            case 201:
            case 204:
                return true;
            default:
                return false;
        }
    }
    //Method for converting JSONArray to JSONObject
    public JSONObject convertToJsonObject(JSONArray jsonArray) {
        JSONObject newJsonObject = new JSONObject();
        try {
            newJsonObject.put("response", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newJsonObject;
    }

    //Used to display volley errors on every page using the error instance
    public String displayVolleyError(VolleyError error) {
        if (error instanceof TimeoutError) {
            isRetryOption = true;
            return mContext.getResources().getString(R.string.time_our_error) + "…";
        } else if (error instanceof NoConnectionError) {
            isRetryOption = true;
            return mContext.getResources().getString(R.string.network_connectivity_error);
        } else if (error instanceof AuthFailureError) {
            return mContext.getResources().getString(R.string.authentication_failure);
        } else if (error instanceof ServerError) {
            return mContext.getResources().getString(R.string.server_error);
        } else if (error instanceof NetworkError) {
            return mContext.getResources().getString(R.string.network_error);
        } else if (error instanceof ParseError) {
            return mContext.getResources().getString(R.string.parse_error);
        } else {
            return mContext.getResources().getString(R.string.please_retry_option);
        }
    }
    /*
     * getting screen width
     */
    @SuppressWarnings("deprecation")
    public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) mContext
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) {
            // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }
    /**
     * Used to build url string.
     *
     * @param queryParams query parameters to be added in url
     * @param requestUrl  - url in which the parameters will be added
     */
    public String buildQueryString(String requestUrl, Map<String, String> queryParams) {
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            requestUrl = Uri.parse(requestUrl)
                    .buildUpon()
                    .appendQueryParameter(key, value)
                    .build().toString();
        }
        return requestUrl;
    }
    public void showProgressDialog() {
        try {
            if (!pDialog.isShowing())
                pDialog.show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
    public void hideProgressDialog() {
        try {
            if (pDialog.isShowing())
                pDialog.dismiss();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
    public void hideKeyboard() {
        View view = ((Activity) mContext).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public void hideKeyboardInDialog(View view) {
        if (view != null) {
            InputMethodManager im = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public void showKeyboard() {
        View view = ((Activity) mContext).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }



    public static String getDeviceUUID() {
        return Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }
}
