package com.vizypay.cashdiscountapp.utility;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.graphics.BitmapCompat;

import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.vizypay.cashdiscountapp.R;

//import org.apache.xerces.dom.DocumentImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Utility {

    public static class EmptyStringToNumberTypeAdapter extends TypeAdapter<Number> {
        @Override
        public void write(JsonWriter jsonWriter, Number number) throws IOException {
            if (number == null) {
                jsonWriter.nullValue();
                return;
            }
            jsonWriter.value(number);
        }

        @Override
        public Number read(JsonReader jsonReader) throws IOException {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            try {
                String value = jsonReader.nextString();
                if ("".equals(value)) {
                    return 0;
                }
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }
    }

   /* public static String decimal2Values(double price) {
        try {
            // double productPrice = qua * price;
            // double roundOff = (double) Math.round(a * 100) / 100;
            return String.format(Locale.US,"%.2f", price);
        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price);
        }
    }*/
   public static String decimal2Values(double price) {
       try {
           // double productPrice = qua * price;
           // double roundOff = (double) Math.round(a * 100) / 100;

           Double rounded= new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            return String.format("%.2f", price);
          // return String.valueOf(rounded);
          // return String.valueOf(rounded);
       } catch (Exception ex) {
           ex.printStackTrace();
           return String.valueOf(price==0.0?0.00:price);
       }
   }

    public static String decimal2Values1(double price) {
        try {
            // double productPrice = qua * price;
            // double roundOff = (double) Math.round(a * 100) / 100;
            System.out.println("price===="+price);
            Double rounded= new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            System.out.println("rounded===="+rounded);
            //return String.format("%.2f", price);
            System.out.println("rounded-----===="+Math.floor(rounded));
            return String.valueOf(rounded);


            // return String.valueOf(rounded);
        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price==0.0?0.00:price);
        }
    } public static String decimal2Values2(double price) {
        try {
             //double productPrice = a * price;
             double roundOff = (double) Math.round(price * 100) / 100;

            //Double rounded= new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            //return String.format("%.2f", price);
            return String.valueOf(roundOff);
            // return String.valueOf(rounded);
        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price==0.0?0.00:price);
        }
    }
public static String decimal2Values3(double price) {
        try {
             //double productPrice = a * price;
             double roundOff = (double) Math.round(price * 100) / 100;

            //Double rounded= new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            //return String.format("%.2f", price);
            return String.valueOf(roundOff);
            // return String.valueOf(rounded);
        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price==0.0?0.00:price);
        }
    }

    public static String decimal3Values(double price) {
        try {
            // double productPrice = qua * price;
            // double roundOff = (double) Math.round(a * 100) / 100;
            return String.format("%.3f", price);
        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price);
        }
    }
    public static String decimal2Values_new(double price) {
        try {
            // double productPrice = qua * price;
            // double roundOff = (double) Math.round(a * 100) / 100;

           Double price1 = Double.parseDouble(String.format("%.3f", price));
            Double price2 = Double.parseDouble(String.format("%.2f", price1));


            return String.format("%.2f", price);

        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price==0.0?0.00:price);
        }
    }


    public static String decimal2Values_new_cashd(double price) {
        try {
            // double productPrice = qua * price;
            // double roundOff = (double) Math.round(a * 100) / 100;

            Double price1 = Double.parseDouble(String.format("%.3f", price));
            Double price2 = Double.parseDouble(String.format("%.2f", price1));


            return String.format("%.2f", price2);

        } catch (Exception ex) {
            ex.printStackTrace();
            return String.valueOf(price==0.0?0.00:price);
        }
    }

    public static long getLongMillFromeDateString(String string_date) {

        long milliseconds = System.currentTimeMillis();
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date d = f.parse(string_date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliseconds;
    }

    public static Date getDateTimeFromTimestamp(Long value1) {
        Long value=value1;
        TimeZone timeZone = TimeZone.getTimeZone("ISO");
        long offset = timeZone.getOffset(value);
        if (offset < 0) {
            value -= offset;
        } else {
            value += offset;
        }
        Date dateTime = new Date(value);
//        SimpleDateFormat sd=new SimpleDateFormat("")
        return dateTime;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void openSignatureDialog(final Context context, final String id, final boolean isHistory) {
        Paint paint;
        final SketchSheetView[] view1 = new SketchSheetView[1];
        Path path2;
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(context);
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View view = inflater.inflate(R.layout.signature_dialog, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            final RelativeLayout relativeLayout = dialog.findViewById(R.id.relativeLayout);

            view1[0] = new SketchSheetView(context);
            Preferences.save(context, Preferences.KEY_SIGN_DONE, "false");
            relativeLayout.addView(view1[0], new ViewGroup.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT));
            //Preferences.save(context, Preferences.KEY_SIGN_DONE, "true");
            view1[0].setOnHoverListener(new View.OnHoverListener() {
                @Override
                public boolean onHover(View v, MotionEvent event) {
                    Preferences.save(context, Preferences.KEY_SIGN_DONE, "true");
                    return false;

                }
            });
            Button btn = dialog.findViewById(R.id.dialogButtonOK);
            Button btnCancel = dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (isHistory) {
                        dialog.dismiss();
                    } else {
                    }
                }
            });
            Button btnReset = dialog.findViewById(R.id.btnReset);
            btnReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Preferences.save(context, Preferences.KEY_SIGN_DONE, "false");
                    //final SketchSheetView view2 = new SketchSheetView(context);

                    relativeLayout.removeView(view1[0]);

                    view1[0] = new SketchSheetView(context);
                    relativeLayout.addView(view1[0], new ViewGroup.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                }
            });
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.e("drar",view1[0].length+"");
                    if (Preferences.get(context, "Sign").equals("true")) {
                        view1[0].setDrawingCacheEnabled(true);
                        Bitmap bitmap = view1[0].getDrawingCache();


                        Log.e("bitmap--", String.valueOf(view1[0].bitmap));
                        Log.e("bitmap11--", String.valueOf(view1[0].paint));
                        if (bitmap != null) {
                            Log.e("bitmap_size", String.valueOf(BitmapCompat.getAllocationByteCount(view1[0].bitmap)));

                            String selectedPath = BitmapUtils.storeImageOnLocalPathFromUrl(context, view1[0].getDrawingCache(true), ".jpg");
                            try {
                                if (selectedPath.equals(null)) {
                                    Toast.makeText(context, "Please Sign to continue....", Toast.LENGTH_SHORT).show();
                                } else {
                                    dialog.dismiss();
                                }
                            } catch (Exception e) {
                                Toast.makeText(context, "Please Sign to continue...", Toast.LENGTH_SHORT).show();
                            }
                            //Preferences.save(context, Preferences.KEY_SIGN_DONE, "No");

                        } else {
                            Toast.makeText(context, "Please Sign to continue..", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(context, "Please Sign to continue.", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void openCustomDialog(final Context context, String msg) {
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(context);
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.custom_warning, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            TextView txtNoData = (TextView) dialog.findViewById(R.id.text);
            txtNoData.setText(msg);

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
