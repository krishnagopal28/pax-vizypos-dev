package com.vizypay.cashdiscountapp.utility;

import android.content.Context;
import android.content.SharedPreferences;


public class Preferences {

    public static final String APP_PREF = "PAX_Vizypay_Pref";
    public static SharedPreferences sp;
    public static String KEY_USER_ID = "userId";
    public static String KEY_USER_TOKEN = "userToken";
    public static String KEY_USER_TYPE = "keyusertype";
    public static String KEY_SALES_ID = "keycustomerid";
    public static String KEY_DEALER_ID = "keydealerid";
    public static String KEY_DEALER_NAME = "keydealername";
    public static String KEY_DEALER_MODULE = "keydealermodule";
    public static String KEY_SALES_Mobile = "key_salesman_number";

    public static String IS_CHILD_SELECTED="ischildselected";
    public static String KEY_MASTER_OBJECT="masterobject";

    public static String LOCATION_START="locationstart";
    public static String LOCATION_END="locationend";


    public static String BackgroundLocation="backgroundlocation";
    public static String LOCATION_LAST_SYNC_DATA="locationlastsyncdata";
    public static String LOCATION_KM_SETTING="location_km_setting";
    public static String LOCATION_LAST_KM="location_last_km";

    public static String IS_SALES_AUTHENTICATE = "salesauthenticate";
    //public static String IS_SALES_AUTHENTICATE = "salesauthenticate";
    public static String KEY_USER_NAME = "keyname";
    public static String KEY_CART_FIRST = "keycartfirst";
    public static String KEY_USER_ADDRESS = "keyAddress";
    public static String KEY_USER_MOBILE = "keycontatct";
    public static String KEY_STORE = "storename";
    public static String KEY_STORE_MOBILE = "storecontact";
    public static String KEY_SYNC_DATE = "syncdate";
    public static String KEY_SETTING = "setting";
    public static String KEY_MASTER_SETTING = "mastersetting";
    public static String KEY_DOMAINNAME = "domainName";
    public static String KEY_FIRST_TIME = "isFIrstTIme";
    public static String KEY_LANGUAGE = "keyylanguage";
    public static String KEY_DEVICE_ID = "oemLogo";
    public static String KEY_REMARK = "keyReamek";
    public static String DELIVERY_OPTION = "keyDelivery";
    public static String DELIVERY_PAYMNT_OPTION = "keypaymentotpion";
    public static String KEY_SHOP_NAME_EN = "keyshopnameen";
    public static String KEY_SHOP_NAME_HI = "keyshopnamehi";
    public static String KEY_APP_NAME_EN = "keyappnameen";
    public static String KEY_APP_NAME_HI = "keyappnamehi";

    public static String KEY_VISITS="is_visit";
    public static String KEY_VISITS_MODULE="visits_module";
    public static String KEY_SIGN_MODULE="sign_module";
    public static String KEY_SIGN_DONE="sign_done";

    public static String KEY_FONT="font_type";
    public static String KEY_KYC_SETTING="KYC_setting";
    public static String KEY_KYC="KYC_module";
    public static String KEY_KYC_STATUS="KYC_status";
    public static String KEY_KYC_SUBMITTED="KYC_submitted";
    public static String KEY_GRID_SPAN="grid_span";
    public static String KEY_SALESMAN_PRIVILEGES="salesman_privileges";

    public static String KEY_OFFER_TEASER="offer_teaser";
    public static String KEY_OFFER_TEASER_DATA="offer_teaser_data";

    public static String SHOWCASE_PRICE="showcasePrice";
    public static String CASD_Discount="cash_discount";
    public static String NON_CASH_CHARGE="ncc";
    public static String ISTAX="isTax";
    public static String TAX="tax";
    public static String DEVICE_SN="device_sn";
    public static String DEVICE_ID="device_id";
    public static String DEVICE_MODEL="device_model";
    public static String QSaleID="QSaleID";
    public static String PRINT_NAME="print_name";
    public static String PRINT_ADDRESS1="print_address1";
    public static String PRINT_ADDRESS2="print_address2";
    public static String PRINT_CONTACT="print_contact";
    public static String PRINT_FOOTER="print_footer";
    public static String ISPRINT_MERCHANTCOPY="isPrintMerchantCopy";
    public static String CREDIT_FEES ="credit_fees";
    public static String EMAIL ="email";
    public static String PASSWORD ="password";
    public static String FEES_PAID ="fees_paid";
    public static String STOCK_TRACKING ="stock_tracking";
    public static String STOCK_NOTIFICATION ="stock_notification";
    public static String ADD_TIP ="add_tip";
    public static String TIP_SUGGESTION_ENABLE ="tip_suggestion_enable";
    public static String CUSTOME_TIP_SUGGESTION_ENABLE ="custome_tip_suggestion_enable";
    public static String PriceInc ="price_increase";
    public static String APP_HEADER ="app_header";
    public static String USER_DETAIL ="user_details";
    public static String PROGRAM_TYPE ="program_type";
    public static String ORDER_ID ="order_id";
    public static String SIGNATURE_FILE_PATH ="signature_file_path";

    public static void save(Context context, String key, String value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String get(Context context , String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        String userId = sp.getString(key, "");
        return userId;
    }

    public static void saveInt(Context context, String key, int value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public static int getInt(Context context , String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        int userId = sp.getInt(key,0);
        return userId;
    }

    public static void saveBool(Context context, String key, Boolean value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static Boolean getBool(Context context , String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        Boolean userId = sp.getBoolean(key,true);
        return userId;
    }

    public static void clearPreference(Context context) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.commit();
    }

    public static void remove(Context context, String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.remove(key);
        edit.commit();
    }


}
