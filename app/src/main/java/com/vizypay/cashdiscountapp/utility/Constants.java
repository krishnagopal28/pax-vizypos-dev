package com.vizypay.cashdiscountapp.utility;

public class Constants {

// public static String BaseURL_1="https://paxapp.vizypay.com/api/";
public static String BaseURL_1="https://dev-paxapp.vizypay.com/api/";
 public static String API_VERSION="v2/";
 // public static String BaseURL="https://paxapp.vizypay.com/api/";
 public static String BaseURL=BaseURL_1+API_VERSION;
 public static String BaseURLImage="https://dev-paxapp.vizypay.com/";



 public static String ProductsAPI="products";
 public static String ProductsWithCategoryAPI="categories/all";
 public static String TaxAPI="tax";
 public static String DeviceInfo="linkdevice";
 public static String CategoriesAPI="categories";
 public static String ProductsAddAPI="products";
 public static String ProductsPriceAPI="product-price";
 public static String DiscountAPI="discount";
 public static String AdditionalDiscountAPIAdd="additional_discounts";
 public static String CategoriesAddAPI="categories";
 public static String OrdersAddAPI="orders";
 public static String OrdersAPI="orders";
 public static String ProductsAddMAINPHOTOAPI="products/mainphoto";
 public static String loginAPI="login";
 public static String AllCategoriesAPI="categories/all";
 public static String SettingsAPI="merchant-setting";
 public static String GetTax="gettax";
 public static String REPORTAPI="account-overview";
 public static String BATCH_CLOSE="batch";
 public static String AdditionalDiscountGET="additional_discounts";
 //public static String AdditionalDiscountGET="additional_discounts";
 public static String ORDER_RECEIPT="order-receipt";

 public static String SIGNATURE_PHOTO="orders/signature";

 public static String TWILIO_ACCOUNT_SID="AC16ac83b24b42416ac51652b2ccd3d67a";
 public static String TWILIO_TOKEN="b889eca6aeb9c645a50c8e4de60f5670";



}