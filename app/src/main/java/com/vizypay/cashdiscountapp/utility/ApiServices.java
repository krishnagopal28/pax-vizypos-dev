package com.vizypay.cashdiscountapp.utility;

import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiServices {

    @Multipart
    @POST("orders/signature")
    Call<ResponseBody> uploadFile(@Header("Authorization") String Authorization,
                                  @Part MultipartBody.Part file,
                                  @Part("order_id") Integer order_id); // @Part MultipartBody.Part file,


    @Multipart
    @POST("products/mainphoto/{id}")
    Call<ResponseBody> uploadFileItem(@Path(value = "id", encoded = true) Integer id,
                                      @Header("Authorization") String Authorization,
                                      @Part MultipartBody.Part file);
//    @Multipart
//    @POST("picture_orders_add")
//    Call<ResponseBody> uploadFileItem(@Part("cms users id") Integer cmsusrsid,
//                                      @Part("remark") String remark,
//                                      @Part("latitude") String latitude,
//                                      @Part("longitude") String longitude,
//                                      @Part MultipartBody.Part file);


}
//    @Header("Content-Type") String contenttype,