package com.vizypay.cashdiscountapp.adapter;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.fragment.AllItems;
import com.vizypay.cashdiscountapp.fragment.ItemEditFragment;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class AllProductAdapter extends RecyclerView.Adapter<AllProductAdapter.DataObjectHolder> implements IApiResponse {
    Context context;
    TextView txt;
    List<Products> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
    // TextView txtCartTotal;

    public AllProductAdapter(List<Products> getList, Context context) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        //  this.txtCartTotal=txtCartTotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_product, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position){
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;
        position1=position;

        holder.txtName.setText(mList.get(position).getName());
        holder.txtPrice.setText(context.getString(R.string.currency)+mList.get(position).getPrice());
        if(Preferences.get(context, Preferences.STOCK_TRACKING).equals("1")) {
            holder.txtUnit.setVisibility(View.VISIBLE);
        }else {
            holder.txtUnit.setVisibility(View.GONE);
        }
        holder.txtUnit.setText("Stock: " +mList.get(position).getUnits_in_stok());
        if(mList.get(position).getMain_photo()!=null) {
            try {
                if (!mList.get(position).getMain_photo().getUrl().equals(""))
                    Glide.with(context).load("https://paxapp.vizypay.com/" + mList.get(position).getMain_photo().getUrl()).into(holder.imgNoti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                Fragment fragment = new ItemEditFragment();
                Bundle bundle=new Bundle();
                bundle.putSerializable("Product", mList.get(position));
                fragment.setArguments(bundle);
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                    //ft.addToBackStack("Brand");
                    ft.commit();
                }
            }
        });

        holder.btnRevert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRevertApi(mList.get(position).getId());
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog(context, "Delete", "Are you sure you want to delete this item?");

            }
        });

        try {
            orderItemRepository=new OrderItemRepository(context);
          /*  if(orderItemRepository.containsId(mList.get(position).getId())>=1){
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void callDeleteApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI +"/"+id,
                "delete", Request.Method.DELETE, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }
    public void callRevertApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsPriceAPI +"?type=revert&ids[]="+id,
                "delete", Request.Method.GET, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("delete")) {
            Log.e("RES", response);
           Fragment fragment = new AllItems();
            if (fragment != null) {
                FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "AllProduct");
                ft.commit();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    public void showAlertDialog(final Context oContext, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                callDeleteApi(mList.get(position1).getId());

                                mList.remove(mList.get(position1));
                                notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName, txtUnit;
        TextView txtPrice;
        Button btnEdit, btnDelete, btnRevert;
        ImageView imgNoti;
        RelativeLayout relativeProductRow;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtUnit = (TextView) itemView.findViewById(R.id.txtUnit);
            btnDelete=itemView.findViewById(R.id.btnDelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);
            btnRevert=itemView.findViewById(R.id.btnRevert);
            relativeProductRow=itemView.findViewById(R.id.relativeProductRow);
            imgNoti=itemView.findViewById(R.id.imgLogo);

        }
    }


}