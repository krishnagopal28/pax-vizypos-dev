package com.vizypay.cashdiscountapp.adapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pax.poslink.BatchRequest;
import com.pax.poslink.BatchResponse;
import com.pax.poslink.PosLink;
import com.pax.poslink.ProcessTransResult;
import com.pax.poslink.peripheries.POSLinkPrinter;
import com.pax.poslink.peripheries.ProcessResult;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.fragment.AllOrders;
import com.vizypay.cashdiscountapp.fragment.ItemCatalogFragment;
import com.vizypay.cashdiscountapp.fragment.MainTabFragment;
import com.vizypay.cashdiscountapp.fragment.ReportFragment;
import com.vizypay.cashdiscountapp.fragment.SettingsFragment;
import com.vizypay.cashdiscountapp.model.BatchClose;
import com.vizypay.cashdiscountapp.model.MenuPojo;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.utility.AppThreadPool;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.POSLinkCreatorWrapper;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

//import okhttp3.internal.Util;

import static com.vizypay.cashdiscountapp.volley.AppController.getContext;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.DataObjectHolder> implements IApiResponse, POSLinkPrinter.PrintListener {
    Context context;
    TextView txt;
    List<MenuPojo> mList = new ArrayList<>();
    int per;
    int position1;
    private PosLink poslink;
    private BatchResponse resp;
    BatchClose batchClose;
    int lBatchNo, total_tip_count;
    boolean isAllTipsAdded;

    public HomeAdapter(List<MenuPojo> getList, Context context) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        //  this.txtCartTotal=txtCartTotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_grid_row, parent, false);
        initPOSLink();
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;

        position1 = position;

        callOrdersAPI("","","");
/*   menuPojo=new MenuPojo(R.drawable.register, "Register");
        menuPojo=new MenuPojo(R.drawable.reports, "Reports");
        menuPojo=new MenuPojo(R.drawable.translog, "Trans Log");
        menuPojo=new MenuPojo(R.drawable.inventory, "Inventory");
        menuPojo=new MenuPojo(R.drawable.settings, "Settings");*/
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment;
                switch (position) {

                    case 0:
                        fragment = new MainTabFragment();
                        if (fragment != null) {
                            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_layout, fragment, "MainScreen");
                            ft.addToBackStack("history");
                            ft.commit();
                        }
                        break;
                    case 1:
                        fragment = new ReportFragment();
                        if (fragment != null) {
                            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_layout, fragment, "MainScreen");
                            ft.addToBackStack("history");
                            ft.commit();
                        }
                        break;
                    case 2:
                        fragment = new AllOrders();
                        if (fragment != null) {
                            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_layout, fragment, "MainScreen");
                            ft.addToBackStack("history");
                            ft.commit();
                        }
                        break;
                    case 3:
                        fragment = new ItemCatalogFragment();
                        if (fragment != null) {
                            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_layout, fragment, "MainScreen");
                            ft.addToBackStack("history");
                            ft.commit();
                        }
                        break;
                    case 4:
                        fragment = new SettingsFragment();
                        if (fragment != null) {
                            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_layout, fragment, "MainScreen");
                            ft.addToBackStack("history");
                            ft.commit();
                        }
                        break;
                    case 5:
                        holder.relativeLayout.setEnabled(false);
                        if(total_tip_count==0){
                            showAlertDialogNoTrans(context, "Batch Close", "There is no transaction for batch close.");
                        }else {
                            if (Preferences.get(context, Preferences.ADD_TIP).equals("1")) {
                                if (!isAllTipsAdded) {
                                    openTipsPopup("Tips Warning", "Oh No!!!! You have unsettled tip transactions! Please settle in activity before batching out!", holder);
                                } else {
                                    showAlertDialog(context, "Batch Close", "Are you sure you want to close batch?", holder);
                                }
                            } else {
                                showAlertDialog(context, "Batch Close", "Are you sure you want to close batch?", holder);
                            }
                        }
                        break;
                    default:
                        fragment = new MainTabFragment();
                        if (fragment != null) {
                            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_layout, fragment, "MainScreen");
                            ft.addToBackStack("history");
                            ft.commit();
                        }
                        break;
                }
            }
        });
        holder.txtName.setText(mList.get(position).getName());
        holder.imgNoti.setImageResource(mList.get(position).getImgResID());

    }


    public void showAlertDialog(final Context oContext, String title, String message, final DataObjectHolder holder) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                //dialog.cancel();


                                if (!Preferences.get(context, Preferences.DEVICE_ID).isEmpty()) {
                                    holder.pb.setVisibility(View.VISIBLE);
                                    batchClose(holder);
                                } else {
                                    Toast.makeText(context, "Please link device to close batch", Toast.LENGTH_SHORT).show();
                                }

                                //((Activity)oContext).finish();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                holder.relativeLayout.setEnabled(true);
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showAlertDialogNoTrans(final Context oContext, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Okay",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.dismiss();
                                //((Activity)oContext).finish();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void batchClose(final DataObjectHolder holder) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                holder.relativeLayout.setEnabled(false);
            }

            @Override
            protected String doInBackground(Void... voids) {
                BatchRequest request = new BatchRequest();
                poslink.BatchRequest = request;
                request.TransType = request.ParseTransType("BATCHCLOSE");
                request.EDCType = request.ParseEDCType("CREDIT");
                ProcessTransResult result = poslink.ProcessTrans();
                //  ProcessResult result = poslink.BatchResponse();
                if (result.Code == ProcessTransResult.ProcessTransResultCode.OK) {
                    return result.toString();
                } else {
                    Log.e("Else", result.Msg);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    if (s != null) {
                        resp = poslink.BatchResponse;
                        Log.e("BAtch", resp.Message + "*" + resp.TORResponseInfo.OrigAmount + "**" + "-___-" + resp.SAFTotalAmount + "__" + resp.CreditAmount);
                        Log.e("BAtchCloseResp", resp.toString());
                        String DateT;
                        try {
                                       /*  DateT = (Utility.getDateTimeFromTimestamp(Long.valueOf(resp.Timestamp))).toString();
                                           // SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                            // ;
                                           // DateFormat inputFormatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                            Date date1 = inputFormatter1.parse(DateT);

                                            DateFormat outputFormatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                            String output1 = outputFormatter1.format(DateT); //*/

                            if (lBatchNo != Integer.parseInt(resp.BatchNum))
                                callBatchAPI(resp.BatchNum, resp.Timestamp, resp.HostResponse);
                        } catch (Exception e) {
                            DateT = "";
                            callBatchAPI("0", "", resp.ResultTxt);

                        }
                    } else {
                        resp = null;
                        resp = poslink.BatchResponse;
                        callBatchAPI("0", "", resp.ResultTxt);
                        Toast.makeText(context, resp.BatchNum, Toast.LENGTH_LONG).show();

                        Log.e("Else", null);
                    }
                } catch (Exception e) {
                    callBatchAPI("0", "", "");
                    e.printStackTrace();
                }
                holder.relativeLayout.setEnabled(true);
                holder.pb.setVisibility(View.GONE);
            }
        }.execute();

    }

    public void callBatchAPI(String batchNumber, String batchDate, String json) {
        lBatchNo = Integer.parseInt(batchNumber);
        HashMap<String, String> map = new HashMap<>();
        /*batch_number:06421104
batch_date:2021-03-08
batch_json:""*/
        map.put("batch_number", batchNumber);
        map.put("batch_date", batchDate);
        map.put("batch_json", json);
        map.put("serial_number", Preferences.get(context, Preferences.DEVICE_SN));
        map.put("device_id", Preferences.get(context, Preferences.DEVICE_ID));
        // map.put("type_value", percent );
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.BATCH_CLOSE,
                "batch_close", map, Request.Method.POST, Preferences.get(context, Preferences.KEY_USER_TOKEN));

    }

    @Override
    public void onSuccess() {

        Log.e("PrintFragment", "Printed successfully. ");

    }

    @Override
    public void onError(ProcessResult processResult) {
        Log.e("PrintFragment", "Print Error. " + processResult.getMessage());

    }

    private void batchClosePrint(BatchResponse resp, BatchClose object, String cashAmount, String transCount, String batchDate, String tip, JSONObject obj) {
        try {
            Double cash = Double.valueOf(cashAmount);
            // Double credit=Double.valueOf(resp.CreditAmount)/100;
            // Double credit=Double.valueOf(obj.getString("card_amount"));
            double cash_card = (Double.valueOf(obj.getString("cash_card_amount")));
            double card_cash = (Double.valueOf(obj.getString("card_cash_amount")));
            cash = cash + cash_card;

            POSLinkPrinter.PrintDataFormatter printDataFormatter = new POSLinkPrinter.PrintDataFormatter();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addHeader();
            printDataFormatter.addBigFont();

            printDataFormatter.addContent(Preferences.get(context, Preferences.PRINT_NAME).toUpperCase());
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(context, Preferences.PRINT_ADDRESS1));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(context, Preferences.PRINT_ADDRESS2));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addSmallFont();
            printDataFormatter.addContent(Preferences.get(context, Preferences.PRINT_CONTACT));
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Daily Summary Report");
            printDataFormatter.addHeader();

            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("         Date:   ");
            printDataFormatter.addLeftAlign();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = format.parse(batchDate);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
            format.parse(batchDate);
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent(format2.format(date1).toString());
            printDataFormatter.addHeader();

            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Sales Report");
            printDataFormatter.addHeader();

            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Sales");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Amount");
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent("Transaction");
            printDataFormatter.addLineSeparator();


            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Card");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new((Double.valueOf(batchClose.getCard_amount())+Double.parseDouble(batchClose.getCard_cash_amount()))));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(""+(Integer.parseInt(batchClose.getCard_trans())+Integer.parseInt(batchClose.getCash_card_trans())));
            printDataFormatter.addLineSeparator();


            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Cash");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new((Double.parseDouble(batchClose.getCash_amount())+Double.parseDouble(batchClose.getCash_card_amount()))));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(""+(Integer.parseInt(batchClose.getCash_trans())+Integer.parseInt(batchClose.getCash_card_trans())));
            printDataFormatter.addLineSeparator();


          /*  printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Split");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getCard_cash_amount()) + Double.parseDouble(batchClose.getCash_card_trans())));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(Integer.parseInt(batchClose.getCash_card_trans()) + "");
            printDataFormatter.addLineSeparator();
*/
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Net Sales:  ");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getNet_sales())));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(Integer.parseInt(batchClose.getGross_trans()) + "");
            printDataFormatter.addLineSeparator();

            if (Preferences.get(context, Preferences.ADD_TIP).equals("1")) {

                printDataFormatter.addLineSeparator();
                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("Tip Report");
                printDataFormatter.addHeader();


                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("Amount");
                printDataFormatter.addRightAlign();
                //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
                printDataFormatter.addContent("Transaction");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addLeftAlign();
                printDataFormatter.addContent("Card");
                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getTotal_card_tip_amount())));
                printDataFormatter.addRightAlign();
                //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
                printDataFormatter.addContent(batchClose.getTotal_card_tip_count() + "");
                printDataFormatter.addLineSeparator();

                printDataFormatter.addLeftAlign();
                printDataFormatter.addContent("Cash");
                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getTotal_cash_tip_amount())));
                printDataFormatter.addRightAlign();
                //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
                printDataFormatter.addContent(batchClose.getTotal_cash_tip_count() + "");
                printDataFormatter.addLineSeparator();


                printDataFormatter.addLineSeparator();

                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("Total Tips:  ");
                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getTotal_tip_amount())));
                printDataFormatter.addRightAlign();
                //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
                printDataFormatter.addContent(batchClose.getTotal_tip_count() + "");
                printDataFormatter.addLineSeparator();
            }


            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Discount Report");
            printDataFormatter.addHeader();


            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Amount");
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent("Transaction");
            printDataFormatter.addLineSeparator();

            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Add Disc");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getAd_discount())));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(Integer.parseInt(batchClose.getAd_discount_trans()) + "");
            printDataFormatter.addLineSeparator();


            if (!Preferences.get(context, Preferences.PROGRAM_TYPE).equals("Traditional")) {
                printDataFormatter.addLeftAlign();
                printDataFormatter.addContent("Cash Disc");
                printDataFormatter.addCenterAlign();
                printDataFormatter.addContent("$" + Utility.decimal2Values(Double.parseDouble(batchClose.getCash_discount())));
                printDataFormatter.addRightAlign();
                //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
                printDataFormatter.addContent(Integer.parseInt(batchClose.getCash_discount_trans()) + "");

                printDataFormatter.addLineSeparator();
            }
            printDataFormatter.addLineSeparator();

            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Total Disc:  ");
            printDataFormatter.addCenterAlign();
            if(Preferences.get(context, Preferences.PROGRAM_TYPE).equals("Traditional")){
                printDataFormatter.addContent("$" + Utility.decimal2Values_new((Double.parseDouble(batchClose.getAd_discount()))));
            } else {
                printDataFormatter.addContent("$" + Utility.decimal2Values_new((Double.parseDouble(batchClose.getCash_discount()) + Double.parseDouble(batchClose.getAd_discount()))));
            }
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent((Integer.parseInt(batchClose.getCash_discount_trans())+Integer.parseInt(batchClose.getAd_discount_trans())) + "");

            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Voids/Refunds");
            printDataFormatter.addHeader();


            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Amount");
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent("Transaction");
            printDataFormatter.addLineSeparator();

            printDataFormatter.addLineSeparator();
            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Card");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + batchClose.getCard_ref_voi_amount());
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(batchClose.getCard_ref_voi_trans() + "");

            printDataFormatter.addLineSeparator();
            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Cash");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + batchClose.getCash_ref_voi_amount());
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(batchClose.getCash_ref_voi_trans() + "");

           /* printDataFormatter.addLineSeparator();
            printDataFormatter.addLeftAlign();
            printDataFormatter.addContent("Split");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getSplit_ref_voi_amount())));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(batchClose.getSplit_ref_voi_trans() + "");*/

            double netRef = Double.parseDouble(batchClose.getCard_ref_voi_amount()) + Double.parseDouble(batchClose.getCash_ref_voi_amount()) + Double.parseDouble(batchClose.getSplit_ref_voi_amount());
            int netRefTrans = Integer.parseInt(batchClose.getCard_ref_voi_trans()) + Integer.parseInt(batchClose.getCash_ref_voi_trans()) + Integer.parseInt(batchClose.getSplit_ref_voi_trans());

            printDataFormatter.addLineSeparator();
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("Total:   ");
            printDataFormatter.addCenterAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(netRef));
            printDataFormatter.addRightAlign();
            //printDataFormatter.addContent(String.valueOf((Integer.valueOf(resp.CreditCount)+Integer.valueOf(transCount))));
            printDataFormatter.addContent(netRefTrans + "");
            printDataFormatter.addLineSeparator();
            printDataFormatter.addHeader();

            printDataFormatter.addLineSeparator();
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("Net Sales: ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.parseDouble(batchClose.getTotal_sales())));
            printDataFormatter.addLineSeparator();

            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("Card Sales: ");
            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("$" + Utility.decimal2Values_new((Double.parseDouble(batchClose.getTotal_sales()))));
            printDataFormatter.addLineSeparator();

            printDataFormatter.addRightAlign();
            printDataFormatter.addContent("Total Tax: ");
            printDataFormatter.addRightAlign();
            //double feesP = ((Double.valueOf(batchClose.getCard_amount()) + Double.valueOf(batchClose.getCard_cash_amount())) * (Double.valueOf(Preferences.get(context, Preferences.FEES_PAID)))) / 100;
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(Double.valueOf(batchClose.getTax())));
            printDataFormatter.addLineSeparator();

            if (!Preferences.get(context, Preferences.PROGRAM_TYPE).equals("Traditional")) {
                printDataFormatter.addRightAlign();
                printDataFormatter.addContent("Fees Paid: ");
                printDataFormatter.addRightAlign();
                // double feesP = ((Double.valueOf(batchClose.getCard_amount()) + Double.valueOf(batchClose.getCard_cash_amount())) * (Double.valueOf(Preferences.get(context, Preferences.FEES_PAID)))) / 100;
                double feesP = (Double.valueOf(batchClose.getFees_paid_calculated()) < 0.009 && Double.valueOf(batchClose.getFees_paid_calculated()) != 0.00 ? 0.01 : Double.valueOf(batchClose.getFees_paid_calculated()));
                printDataFormatter.addContent("$" + Utility.decimal2Values_new(feesP));
                printDataFormatter.addLineSeparator();
            }

            printDataFormatter.addRightAlign();
            if (!Preferences.get(context, Preferences.PROGRAM_TYPE).equals("Traditional")) {
            printDataFormatter.addContent("Est.Deposit: ");
            printDataFormatter.addRightAlign();
            double estD = (Double.valueOf(batchClose.getEstimated_deposit()));
            //double estD = ((Double.valueOf(batchClose.getCard_amount()) + Double.valueOf(batchClose.getCard_cash_amount())) - feesP);
            estD=((Double.parseDouble(batchClose.getTotal_card_volume()))-Double.valueOf(batchClose.getFees_paid_calculated()));
            printDataFormatter.addContent("$" + Utility.decimal2Values_new(estD));
            // printDataFormatter.addContent("$" + Utility.decimal2Values_new(estD >= 0.005 ? estD : 0.00));
            printDataFormatter.addLineSeparator();
            }

            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();
            printDataFormatter.addLineSeparator();

            Log.d("PrintBatchClose", printDataFormatter.build());
            POSLinkPrinter posLinkPrinter=POSLinkPrinter.getInstance(context);
            posLinkPrinter.setPrintWidth(POSLinkPrinter.RecommendWidth.A920_RECOMMEND_WIDTH);
            posLinkPrinter.print(printDataFormatter.build(), this);

         /*   if(copyName.equals("Customer Copy"))
                if(Preferences.get(mContext, Preferences.ISPRINT_MERCHANTCOPY).equals("1")) {
                    printReceipt(orderResponse, "Merchant Copy");
                }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void callOrdersAPI(String from_date, String to_date, String id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.OrdersAPI + "?limit=200&from_date="+from_date+"&to_date="+to_date+"&order_id="+id+"&device_id="+Preferences.get(context, Preferences.DEVICE_ID)+"&serial_number="+Preferences.get(context,Preferences.DEVICE_SN),
                "orders", Request.Method.GET, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }



    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("delete")) {
            Log.e("RES", response);
        }
        if (tag_json_obj.equals("orders")) {
            Log.e("RES", response);
            try {
                //pb.setVisibility(View.GONE);
                JSONObject result = new JSONObject(response);
                {
                    total_tip_count=Integer.parseInt(result.getString("total"));
                    if (Integer.parseInt(result.getString("total_tip_count")) == Integer.parseInt(result.getString("total_cash_card")) ||  Integer.parseInt(result.getString("total_cash_card"))==0) {
                        isAllTipsAdded = true;
                    } else {
                        isAllTipsAdded = false;
                    }
                }
            }catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (tag_json_obj.equals("batch_close")) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(response).getJSONObject("data");

                Gson gson = new GsonBuilder().registerTypeAdapter(int.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Integer.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(double.class, new Utility.EmptyStringToNumberTypeAdapter())
                        .registerTypeAdapter(Double.class, new Utility.EmptyStringToNumberTypeAdapter()).create();

                batchClose = gson.fromJson(obj.toString(), new TypeToken<BatchClose>() {
                }.getType());

                if (obj.has("id"))
                    try {

                        if (resp != null) {

                            batchClosePrint(resp, batchClose, obj.getString("cash_amount"), obj.getString("cash_trans"), obj.getString("batch_date"), obj.getString("total_tip_amount"), obj);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void openTipsPopup(String title, String message,final DataObjectHolder holder) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("View Activity",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.dismiss();
                               /* Preferences.clearPreference(getActivity());
                                Intent intent=new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                finish();*/

                                Fragment fragment = new AllOrders();
                                // AskOptionDialog().show();
                                FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                                //Bundle bundle=new Bundle();
                                //bundle.putSerializable("Order", mList.get(position));
                                //bundle.putInt("OrderID", mList.get(position).getId());
                                //fragment.setArguments(bundle);
                                ft.replace(R.id.frame_layout, fragment, "hisory");
                                ft.addToBackStack("history1");
                                ft.commit();

                                //((Activity)oContext).finish();
                            }
                        }).setNegativeButton("Close Batch",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.dismiss();
                        try {

                            batchClose(holder);
                            //showAlertDialog(context, "Batch Close", "Are you sure you want to close batch?", holder);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //((Activity)oContext).finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }



    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;
        Button btnEdit, btnDelete, btnRevert;
        ImageView imgNoti;
        RelativeLayout relativeLayout;
        ProgressBar pb;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtMenuName);
            relativeLayout=itemView.findViewById(R.id.relativeMenuRow);
            imgNoti=itemView.findViewById(R.id.imgMenu);
            pb=itemView.findViewById(R.id.pb);


        }
    }
    private void initPOSLink() {
        POSLinkCreatorWrapper.createSync(getContext(), new AppThreadPool.FinishInMainThreadCallback<PosLink>() {
            @Override
            public void onFinish(PosLink result) {

                poslink = result;
                poslink.SetCommSetting(AppController.setupSetting());
                Log.d("MainActivity", "PosLink : "+poslink);
                Log.d("MainActivity", "PosLink comm: port "+poslink.GetCommSetting().getDestPort());
                Log.d("MainActivity", "PosLink comm timeout: "+poslink.GetCommSetting().getTimeOut());

                //  paymentMethod();
                //  paymentMethod();
                //  paymentMethod();
                //  paymentMethod();

            }
        });
    }

}