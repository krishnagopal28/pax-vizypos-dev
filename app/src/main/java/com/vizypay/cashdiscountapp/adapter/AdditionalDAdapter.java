package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
import com.vizypay.cashdiscountapp.utility.CustomView;

import java.util.ArrayList;
import java.util.List;

public class AdditionalDAdapter extends BaseAdapter {
    private String[] strings;
    Context context;
    public List<Integer> selectedPositions = new ArrayList<>();
    public List<Integer> selectedIDs = new ArrayList<>();
    List<AdditionalDiscount> additionalDiscounts = new ArrayList<>();

    public AdditionalDAdapter(List<AdditionalDiscount> additionalDiscount, Context context) {
        this.strings = strings;
        this.additionalDiscounts=additionalDiscount;
        this.context=context;
    }

    @Override
    public int getCount() {
        return additionalDiscounts.size();
    }

    @Override
    public Object getItem(int position) {
        return additionalDiscounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return additionalDiscounts.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomView customView = (convertView == null) ?
                new CustomView(context) : (CustomView) convertView;
        customView.display(additionalDiscounts.get(position).getName(), selectedPositions.contains(position));
        return customView;
    }
}
