package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Utility;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.vizypay.cashdiscountapp.fragment.MainTabFragment.newPercentTotal;

//import org.jsoup.nodes.Element;

/**
 * Created by Krishna.
 */
public class Cash_CardSummaryAdditionalDiscountAdapter extends RecyclerView.Adapter<Cash_CardSummaryAdditionalDiscountAdapter.DataObjectHolder> {
    Context context;
    TextView txt;
    List<AdditionalDiscount> mList = new ArrayList<>();
    int per;
    int position1;


    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
   // TextView txtCartTotal;


    double subTotal;
   // List<AdditionalDiscountAdapter> mCartList;

    public Cash_CardSummaryAdditionalDiscountAdapter(List<AdditionalDiscount> getList, Context context, OrderItemRepository orderItemRepository, double subtotal) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        this.orderItemRepository=orderItemRepository;
       // this.txtCartTotal=txtCartTotal;

        this.subTotal=subtotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cash_cardsummary_add_discount, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position){
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;


       if(mList.get(position).getType().equals("Fixed")) {
            // holder.txtPrice.setText(""+context.getString(R.string.currency) + mList.get(position).getType_value());
            holder.txtPrice.setText("-"+context.getString(R.string.currency) + Utility.decimal2Values(Double.valueOf(mList.get(position).getType_value().replace("-",""))));
           holder.txtName.setText(mList.get(position).getName()+"  "+"(-"+context.getString(R.string.currency) + Utility.decimal2Values(Double.valueOf(mList.get(position).getType_value().replace("-","")))+")");
           try {
               orderItemRepository.updatePriceById(mList.get(position).getItems().getProduct_id(),Double.valueOf(mList.get(position).getType_value().replace("-","")));
           } catch (SQLException throwables) {
               throwables.printStackTrace();
           }
       }else {
            holder.txtPrice.setText( "-$"+Utility.decimal2Values(((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100)));
           /*OrderItems items=mList.get(position).getItems();
           items.setItem_price(Utility.decimal2Values(((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100)));
           items.setTotal_amount(Utility.decimal2Values(((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100)));
            orderItemRepository.update(items);*/
           try {
               orderItemRepository.updatePriceById(mList.get(position).getItems().getProduct_id(),(((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100)) );
           } catch (SQLException throwables) {
               throwables.printStackTrace();
           }
           newPercentTotal=newPercentTotal+((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100);
           // holder.txtPrice.setText( mList.get(position).getType_value()+"%");
           holder.txtName.setText(mList.get(position).getName()+"  "+"("+ Utility.decimal2Values(Double.valueOf(mList.get(position).getType_value().replace("-","")))+"%)");

       }
       // holder.txtPrice.setText(mList.get(position).getType_value());
    }
    public double getAllPercent(){

        return newPercentTotal;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtCashD);
            txtPrice = (TextView) itemView.findViewById(R.id.txtCashDV);
        }
    }

}