package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import java.util.ArrayList;
import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.DataObjectHolder> implements IApiResponse {
    Context context;
    TextView txt;
    List<OrderItems> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
   // TextView txtCartTotal;

    public OrderItemAdapter(List<OrderItems> getList, Context context) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
      //  this.txtCartTotal=txtCartTotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_items, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position){
      //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
       // this.position=position;
        position1=position;

        holder.txtProductID.setText(mList.get(position).getProduct_id()+"");
        holder.txtName.setText(""+mList.get(position).getProduct_name());
        holder.txtQty.setText(""+mList.get(position).getQuantity());
        holder.txtPrice.setText("$"+mList.get(position).getItem_price());
        holder.txttotal.setText("$"+mList.get(position).getTotal_amount());

    }

    public void callDeleteApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI +"/"+id,
                "delete", Request.Method.DELETE, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("delete")) {
            Log.e("RES", response);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtProductID,txtName,txtQty,txtPrice,txttotal;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtame);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtProductID=itemView.findViewById(R.id.txtID);
            txtQty=itemView.findViewById(R.id.txtQty);
            txttotal=itemView.findViewById(R.id.txtTotal);
        }
    }
}