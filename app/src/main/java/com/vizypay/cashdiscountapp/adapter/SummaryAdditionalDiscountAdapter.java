package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Utility;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class SummaryAdditionalDiscountAdapter extends RecyclerView.Adapter<SummaryAdditionalDiscountAdapter.DataObjectHolder> {
    Context context;
    TextView txt;
    List<AdditionalDiscount> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
   // TextView txtCartTotal;


    double subTotal;
   // List<AdditionalDiscountAdapter> mCartList;

    public SummaryAdditionalDiscountAdapter(List<AdditionalDiscount> getList, Context context, OrderItemRepository orderItemRepository,double subtotal) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        this.orderItemRepository=orderItemRepository;
       // this.txtCartTotal=txtCartTotal;

        this.subTotal=subtotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.summary_add_discount, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position){
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;


        holder.txtName.setText(mList.get(position).getName()+"");
        if(mList.get(position).getType().equals("Fixed")) {
            // holder.txtPrice.setText(""+context.getString(R.string.currency) + mList.get(position).getType_value());
            holder.txtPrice.setText("-"+context.getString(R.string.currency) + Utility.decimal2Values(Double.valueOf(mList.get(position).getType_value().replace("-",""))));
            try {
                orderItemRepository.updatePriceById(mList.get(position).getItems().getProduct_id(),Double.valueOf(mList.get(position).getType_value().replace("-","")) );
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }else {
            try {
                orderItemRepository.updatePriceById(mList.get(position).getItems().getProduct_id(),((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100) );
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            holder.txtPrice.setText( "-$"+Utility.decimal2Values(((subTotal*Double.valueOf(mList.get(position).getType_value().replace("-","")))/100)));
           // holder.txtPrice.setText( mList.get(position).getType_value()+"%");
        }
       // holder.txtPrice.setText(mList.get(position).getType_value());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtCashD);
            txtPrice = (TextView) itemView.findViewById(R.id.txtCashDV);
        }
    }

}