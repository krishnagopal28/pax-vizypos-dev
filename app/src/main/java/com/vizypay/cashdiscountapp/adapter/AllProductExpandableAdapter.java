package com.vizypay.cashdiscountapp.adapter;

import static com.vizypay.cashdiscountapp.utility.Constants.BaseURLImage;

import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.fragment.AllItemsExpandable;
import com.vizypay.cashdiscountapp.fragment.ItemEditFragment;
import com.vizypay.cashdiscountapp.model.CategoryWithProducts;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Richa on 08/03/17.
 */

public class AllProductExpandableAdapter extends BaseExpandableListAdapter implements IApiResponse {


    boolean ret = false;

    Context context;
    List<CategoryWithProducts> arrayList;
    OrderItemRepository orderItemRepository;

    public AllProductExpandableAdapter(Context context, List<CategoryWithProducts> mainCatList) {
        this.context = context;
        arrayList = mainCatList;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return arrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return arrayList.get(groupPosition).getListOfProducts().size();
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return arrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return arrayList.get(groupPosition).getListOfProducts().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_group_view, null);
        TextView text = (TextView) convertView.findViewById(R.id.txtName);
        text.setText(arrayList.get(groupPosition).getName());
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_all_product, null);
        ViewHolder holder=new ViewHolder(convertView);
        final Products products=arrayList.get(groupPosition).getListOfProducts().get(childPosition);
        holder.txtName.setText(arrayList.get(groupPosition).getListOfProducts().get(childPosition).getName());
        holder.txtPrice.setText(products.getPrice());
        if(products.getMain_photo()!=null) {
            try {
                if (!products.getMain_photo().getUrl().equals(""))
                    Glide.with(context).load(BaseURLImage+ products.getMain_photo().getUrl()).into(holder.imgNoti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(Preferences.get(context, Preferences.STOCK_TRACKING).equals("1")) {
            holder.txtUnit.setVisibility(View.VISIBLE);
        }else {
            holder.txtUnit.setVisibility(View.GONE);
        }

        if (Preferences.get(context, Preferences.PROGRAM_TYPE).equalsIgnoreCase("Traditional") || Preferences.get(context, Preferences.PROGRAM_TYPE).equalsIgnoreCase("CDP 1.0") )
        { holder.btnRevert.setVisibility(View.GONE); Log.e("if PROGRAM_TYPE--", Preferences.get(context, Preferences.PROGRAM_TYPE));}
        else
        { holder.btnRevert.setVisibility(View.VISIBLE); Log.e("else  PROGRAM_TYPE--", Preferences.get(context, Preferences.PROGRAM_TYPE));}

        holder.txtUnit.setText("Stock: " +products.getUnits_in_stok());
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                Fragment fragment = new ItemEditFragment();
                Bundle bundle=new Bundle();
                bundle.putSerializable("Product", arrayList.get(groupPosition).getListOfProducts().get(childPosition));
                fragment.setArguments(bundle);
                if (fragment != null) {

                    ft.replace(R.id.frame_layout, fragment, "ItemAdd");
                    //ft.addToBackStack("Brand");
                    ft.commit();
                }
            }
        });

        holder.btnRevert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRevertApi(products.getId());
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog(context, "Delete", "Are you sure you want to delete this item?", products);

            }
        });

        try {
            orderItemRepository=new OrderItemRepository(context);
          /*  if(orderItemRepository.containsId(arrayList.get(groupPosition).getId())>=1){
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return convertView;
    }
    public void callDeleteApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI +"/"+id,
                "delete", Request.Method.DELETE, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }
    public void callRevertApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsPriceAPI +"?type=revert&ids[]="+id,
                "delete", Request.Method.GET, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("delete")) {
            Log.e("RES", response);
            Fragment fragment = new AllItemsExpandable();
            if (fragment != null) {
                FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "AllProduct");
                ft.commit();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    public void showAlertDialog(final Context oContext, String title, String message, final Products products) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                callDeleteApi(products.getId());

                                arrayList.remove(products);
                                notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public static class ViewHolder {
        //CardView cv;
        TextView txtName, txtUnit;
        TextView txtPrice;
        Button btnEdit, btnDelete, btnRevert;
        ImageView imgNoti;
        RelativeLayout relativeProductRow;


        ViewHolder(View itemView) {
            // cv = (CardView) itemView.findViewById(R.id.productCard);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtUnit = (TextView) itemView.findViewById(R.id.txtUnit);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            btnDelete=itemView.findViewById(R.id.btnDelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);
            btnRevert=itemView.findViewById(R.id.btnRevert);
            relativeProductRow=itemView.findViewById(R.id.relativeProductRow);
            imgNoti=itemView.findViewById(R.id.imgLogo);
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}