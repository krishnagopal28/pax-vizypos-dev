package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.fragment.OrderDetail;
import com.vizypay.cashdiscountapp.model.OrderHistory;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class AllOrderHistoryAdapter extends RecyclerView.Adapter<AllOrderHistoryAdapter.DataObjectHolder> implements IApiResponse {
    Context context;
    TextView txt;
    List<OrderHistory> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
    // TextView txtCartTotal;

    public AllOrderHistoryAdapter(List<OrderHistory> getList, Context context) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        //  this.txtCartTotal=txtCartTotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_history, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;
        position1 = position;
        try {
            holder.txtStatus.setText(mList.get(position).getOrder_status());
        } catch (Exception e) {
            holder.txtOrderBy.setText("NA");
            //e.printStackTrace();
        }
        try {
            holder.txtOrderBy.setText(mList.get(position).getName().equals(null) ? "NA" : mList.get(position).getName());
        } catch (Exception e) {
            holder.txtOrderBy.setText("NA");
            //e.printStackTrace();
        }
        try {
            holder.txtOrderID.setText("#"+mList.get(position).getId());
        } catch (Exception e) {
            holder.txtOrderBy.setText("NA");
            //e.printStackTrace();
        }
        try {
            holder.txtTotalItems.setText(mList.get(position).getTotal_item_count()+"");
        } catch (Exception e) {
           // e.printStackTrace();
        }

        try {
            //2021-01-29 07:18:31
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyy-MM-dd h:mm:ss");
            Date time = localDateFormat.parse((mList.get(position).getCreated_at()));
            holder.txtTime.setText("Time: "+String.valueOf(new SimpleDateFormat("h:mm aa").format(time)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(Preferences.get(context, Preferences.ADD_TIP).equals("1")){
                holder.txtTip.setText("$"+mList.get(position).getTip_amount());
                holder.layoutTIP.setVisibility(View.VISIBLE);
        }else {
            holder.layoutTIP.setVisibility(View.GONE);
        }
 try {
            //2021-01-29 07:18:31
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date time = localDateFormat.parse((mList.get(position).getCreated_at()));
            holder.txtDate.setText("Date: "+String.valueOf(new SimpleDateFormat("MM-dd-yyyy").format(time)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            holder.txtTotalAmount.setText("$" + mList.get(position).getTotal_amount());
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new OrderDetail();
                if (fragment != null) {
                    FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                    Bundle bundle=new Bundle();
                    //bundle.putSerializable("Order", mList.get(position));
                    bundle.putInt("OrderID", Integer.parseInt(mList.get(position).getId()));
                    fragment.setArguments(bundle);
                    ft.replace(R.id.frame_layout, fragment, "hisory");
                    ft.addToBackStack("history1");
                    ft.commit();
                }
            }
        });

        try {
            orderItemRepository = new OrderItemRepository(context);
          /*  if(orderItemRepository.containsId(mList.get(position).getId())>=1){
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void callDeleteApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI + "/" + id,
                "delete", Request.Method.DELETE, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("delete")) {
            Log.e("RES", response);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtOrderBy, txtOrderID, txtStatus, txtTip;
        TextView txtTotalItems, txtTime, txtDate;
        TextView txtTotalAmount;
        Button btnDetail;
        ImageView imgNoti;
        RelativeLayout relativeProductRow;
        LinearLayout layoutTIP;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtTip = (TextView) itemView.findViewById(R.id.txtTIP);
            txtOrderID = (TextView) itemView.findViewById(R.id.txtOrderId);
            txtOrderBy = (TextView) itemView.findViewById(R.id.txtOrderBy);
            txtTotalItems = (TextView) itemView.findViewById(R.id.txtTotalItems);
            txtTotalAmount = (TextView) itemView.findViewById(R.id.txtTotalAmount);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtTime.setText("");
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtDate.setText("");

            txtTotalAmount = (TextView) itemView.findViewById(R.id.txtTotalAmount);
            btnDetail = itemView.findViewById(R.id.btnDetail);
            relativeProductRow = itemView.findViewById(R.id.relativeProductRow);
            layoutTIP = itemView.findViewById(R.id.layoutTIP);
            imgNoti = itemView.findViewById(R.id.imgLogo);

        }
    }
}