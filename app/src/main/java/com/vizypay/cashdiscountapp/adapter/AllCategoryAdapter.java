package com.vizypay.cashdiscountapp.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.Category;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class AllCategoryAdapter extends RecyclerView.Adapter<AllCategoryAdapter.DataObjectHolder> implements IApiResponse {
    Context context;
    TextView txt;
    List<Category> mList = new ArrayList<>();
    int per;
    int position1;
    Dialog dialog;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
    CategoryAdapterListener categoryAdapterListener;
    // TextView txtCartTotal;

    public AllCategoryAdapter(List<Category> getList, Context context, CategoryAdapterListener listener) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        this.categoryAdapterListener = listener;
        //  this.txtCartTotal=txtCartTotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_product, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;

        holder.txtName.setText(mList.get(position).getName());
        holder.txtPrice.setVisibility(View.GONE);
        // holder.txtPrice.setText(mList.get(position).getPrice());
      /*  if(mList.get(position).getMain_photo()!=null) {
            try {
                if (!mList.get(position).getMain_photo().getUrl().equals(""))
                    Glide.with(context).load("https://paxapp.vizypay.com/" + mList.get(position).getMain_photo().getUrl()).into(holder.imgNoti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position1 = position;
                editCategoryDialog(mList.get(position).getName());


            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position1 = position;
                showAlertDialog(context, "Delete", "Are you sure you want to delete this category?");
            }
        });

        try {
            orderItemRepository = new OrderItemRepository(context);
          /*  if(orderItemRepository.containsId(mList.get(position).getId())>=1){
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showAlertDialog(final Context oContext, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                HashMap<String, String> map = new HashMap<>();
                                callCategoryDelete(map);
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void editCategoryDialog(String name) {
        try {
            // Create custom dialog object
            dialog = new Dialog(context);
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.add_category, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            Button btnOk = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final EditText edtCategoryName = (EditText) dialog.findViewById(R.id.edtCayegoryName);
            edtCategoryName.setText(name);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", edtCategoryName.getText().toString().trim());
                    callCategoryEdit(map);
                    dialog.dismiss();

                }
            });

            // Display the dialog
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callCategoryEdit(HashMap<String, String> map) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.CategoriesAddAPI + "/" + mList.get(position1).getId(),
                "category_edit", map, Request.Method.PUT, Preferences.get(context, Preferences.KEY_USER_TOKEN));

    }

    public void callCategoryDelete(HashMap<String, String> map) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.CategoriesAddAPI + "/" + mList.get(position1).getId(),
                "category_delete", map, Request.Method.DELETE, Preferences.get(context, Preferences.KEY_USER_TOKEN));

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals("category_edit")) {
            JSONObject result = null;
            try {
                result = new JSONObject(response);

                if (result.has("data")) {
                    categoryAdapterListener.onCategoryUpdated();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (tag_json_obj.equals("category_delete")) {

            categoryAdapterListener.onCategoryDeleted();

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;
        Button btnEdit, btnDelete, btnRevert;
        ImageView imgNoti;
        RelativeLayout relativeProductRow;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnRevert = itemView.findViewById(R.id.btnRevert);
            btnRevert.setVisibility(View.GONE);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            relativeProductRow = itemView.findViewById(R.id.relativeProductRow);
            imgNoti = itemView.findViewById(R.id.imgLogo);
            imgNoti.setVisibility(View.GONE);
        }
    }

    public interface CategoryAdapterListener {
        void onCategoryUpdated();

        void onCategoryDeleted();

    }
}