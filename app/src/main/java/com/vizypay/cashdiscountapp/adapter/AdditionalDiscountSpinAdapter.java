package com.vizypay.cashdiscountapp.adapter;

        import android.content.Context;
        import android.content.res.Configuration;
        import android.database.DataSetObserver;
        import android.graphics.Color;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.SpinnerAdapter;
        import android.widget.TextView;


        import com.vizypay.cashdiscountapp.model.AdditionalDiscount;

        import java.util.List;

/**
 *
 * @author Richa
 *
 *
 */

public class AdditionalDiscountSpinAdapter implements SpinnerAdapter {

    Context context;
    List<AdditionalDiscount> alCategory;

    public AdditionalDiscountSpinAdapter(Context context, List<AdditionalDiscount> alState) {
        super();
        this.context = context;
        this.alCategory = alState;

    }
    @Override
    public int getCount() {
        return alCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_dropdown_item, null);
        textview.setAllCaps(false);
        textview.setTextColor(Color.parseColor("#333333"));
        setTextViewSizeByScreen(textview);
        textview.setCompoundDrawablePadding(25);
       // textview.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.category),null,null,null);
        textview.setText(alCategory.get(position).getName());
        textview.setBackgroundColor(Color.parseColor("#00000000"));
        return textview;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_dropdown_item, null);
        textview.setTextColor(Color.parseColor("#333333"));
        setTextViewSizeByScreen(textview);
        textview.setAllCaps(false);
        textview.setText(alCategory.get(position).getName());
        textview.setBackgroundColor(Color.parseColor("#00000000"));
        return textview;
    }



    //Note:-Create this two method getIDFromIndex and getIndexByID
    public String getIDFromIndex(int Index) {
        return String.valueOf(alCategory.get(Index).getId());
    }

    public String getNameFromIndex(int Index) {
        return  alCategory.get(Index).getName();
    }

    public int getIndexByID(int ID) {
        for(int i=0;i<alCategory.size();i++){
            if(String.valueOf(alCategory.get(i).getId()).equals(String.valueOf(ID))){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }
    //text size according to screen resolution
    public void setTextViewSizeByScreen(TextView txtView) {
        try{
            int screenSize =context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
            switch(screenSize) {
                case Configuration.SCREENLAYOUT_SIZE_LARGE:
                    txtView.setTextSize(30);
                    break;
                case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                    txtView.setTextSize(18);
                    break;
                case Configuration.SCREENLAYOUT_SIZE_SMALL:
                    txtView.setTextSize(14);
                    break;
                default:
                    break;
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
