package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.readystatesoftware.viewbadger.BadgeView;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.fragment.AllItemsExpandable;
import com.vizypay.cashdiscountapp.model.CategoryWithProducts;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;
import com.vizypay.cashdiscountapp.volley.ApiRequest;
import com.vizypay.cashdiscountapp.volley.IApiResponse;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Richa on 08/03/17.
 */

public class ExpProductListAdapter extends BaseExpandableListAdapter implements IApiResponse {


    boolean ret = false;

    Context context;
    List<CategoryWithProducts> arrayList;
    OrderItemRepository orderItemRepository;

    TextView txtCartTotal;
    List<OrderItems> mCartList;
    TextView txt;

    //public ExpProductListAdapter(Context context, List<CategoryWithProducts> mainCatList) {
    public ExpProductListAdapter(List<CategoryWithProducts> getList, Context context, TextView txtCartTotal, List<OrderItems> cartList) {
        this.context = context;
        // arrayList = mainCatList;
        this.arrayList = getList;
        this.context = context;
        this.txt = txt;
        this.txtCartTotal=txtCartTotal;
        this.mCartList=cartList;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return arrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return arrayList.get(groupPosition).getListOfProducts().size();
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return arrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return arrayList.get(groupPosition).getListOfProducts().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_group_view, null);
        TextView text = (TextView) convertView.findViewById(R.id.txtName);
        text.setText(arrayList.get(groupPosition).getName());
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_product_list, null);
        final ViewHolder holder=new ViewHolder(convertView);
        final Products products=arrayList.get(groupPosition).getListOfProducts().get(childPosition);
        try{
            orderItemRepository=new OrderItemRepository(context);
        }catch (Exception e){
            e.printStackTrace();
        }



        holder.txtName.setText(products.getName());
        if(Preferences.get(context, Preferences.STOCK_TRACKING).equals("1")) {
            holder.txtUnit.setVisibility(View.VISIBLE);
        }else {
            holder.txtUnit.setVisibility(View.GONE);
        }
        holder.txtUnit.setText("Stock: "+products.getUnits_in_stok());
        holder.txtPrice.setText(context.getString(R.string.currency)+products.getPrice());

        try {
            holder.badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
            //badge.setBadgeMargin(-110,0);
            holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(products.getId())));
            holder.badge.show();
            // Log.e("badgesss", arrayList.get(position).getMenuName()+" ** "+arrayList.get(position).getMyCount());
           /* if(!arrayList.get(position).getMyCount().equals("")){
                badge.show();
                if(arrayList.get(position).getMyCount().equals("0")) {
                    badge.hide();
                }
            }else{
                badge.hide();
            }*/
        }catch (Exception e){
            e.printStackTrace();
        }
        if(products.getMain_photo()!=null) {
            try {
                if (!products.getMain_photo().getUrl().equals(""))
                    Glide.with(context).load("https://paxapp.vizypay.com/" + products.getMain_photo().getUrl()).into(holder.imgNoti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Preferences.save(context, Preferences.TAX, String.valueOf(products.getTax()));
        try {
            orderItemRepository=new OrderItemRepository(context);
            if(orderItemRepository.containsId(String.valueOf(products.getId()))>=1){
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        holder.relativeProductRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (Preferences.get(context, Preferences.STOCK_TRACKING).equals("1")) {
                        if (Integer.parseInt(products.getUnits_in_stok()) > 0) {

                            if (orderItemRepository.getQuantityById(products.getId())<Integer.parseInt(products.getUnits_in_stok())) {

                                /*if (Integer.parseInt(products.getMinimum_qty()) > 0) {
                                    try {
                                        orderItemRepository = new OrderItemRepository(context);

                                        if (orderItemRepository.containsId(String.valueOf(products.getId())) > 0) {
                                            if (Integer.parseInt(products.getUnits_in_stok()) > orderItemRepository.getQuantityById(products.getId())) {
                                                orderItemRepository.updateQuantityById(String.valueOf(products.getId()), (orderItemRepository.getQuantityById(products.getId()) + 1), (orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getPrice()));
                                            } else {
                                                Toast.makeText(context, "Stock not Available", Toast.LENGTH_LONG).show();
                                            }
                                        } else {

                                            OrderItems orderItems = new OrderItems();
                                            orderItems.setProduct_id(String.valueOf(products.getId()));
                                            orderItems.setItem_price(products.getPrice());
                                            orderItems.setProduct_name(products.getName());
                                            orderItems.setTax(products.getTax());
                                            orderItems.setIsDPercent("false");
                                            orderItems.setIsDiscount("false");
                                            orderItems.setMin_qty(String.valueOf(products.getMinimum_qty()));
                                            //orderItems.setAd_id("");
                                            orderItems.setUnit_in_stok(String.valueOf(products.getUnits_in_stok()));
                                            orderItems.setQuantity(String.valueOf(products.getMinimum_qty()));
                                            orderItems.setTotal_amount(String.valueOf(Integer.parseInt(products.getMinimum_qty()) * Double.valueOf(products.getPrice())));
                                            orderItemRepository.create(orderItems);
                                        }
                                        txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                                        holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));

                                        holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(products.getId())));

                                        // notifyItemChanged(position);

                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                } else*/ {
                                    try {
                                        orderItemRepository = new OrderItemRepository(context);

                                        if (orderItemRepository.containsId(String.valueOf(products.getId())) > 0) {
                                            orderItemRepository.updateQuantityById(String.valueOf(products.getId()), (orderItemRepository.getQuantityById(products.getId()) + 1), (orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getPrice()),(orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getCost()),(orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getProfit()));
                                        } else {
                                            OrderItems orderItems = new OrderItems();
                                            orderItems.setProduct_id(String.valueOf(products.getId()));
                                            orderItems.setItem_price(products.getPrice());
                                            orderItems.setItem_cost(products.getCost());
                                            orderItems.setItem_profit(products.getProfit());
                                            orderItems.setItem_total_cost(products.getCost());
                                            orderItems.setItem_total_profit(products.getProfit());
                                            orderItems.setProduct_name(products.getName());
                                            orderItems.setTax(products.getTax());
                                            orderItems.setIsDPercent("false");
                                            orderItems.setIsDiscount("false");
                                            orderItems.setUnit_in_stok(String.valueOf(products.getUnits_in_stok()));
                                            //orderItems.setAd_id("");
                                            orderItems.setMin_qty(String.valueOf(products.getMinimum_qty()));
                                            orderItems.setQuantity(String.valueOf(1));
                                            orderItems.setTotal_amount(String.valueOf(Integer.parseInt("1") * Double.valueOf(products.getPrice())));
                                            orderItemRepository.create(orderItems);
                                        }
                                        txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                                        holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));

                                        holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(products.getId())));

                                        // notifyItemChanged(position);

                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }else {
                                Toast.makeText(context, "Stock Not Available", Toast.LENGTH_SHORT).show();
                            }
                            // orderItemRepository.updateQuantityById(String.valueOf(products.getId()), (orderItemRepository.getQuantityById(products.getId()) + 1), (orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getPrice()));
                        } else {
                            Toast.makeText(context, "Stock not Available", Toast.LENGTH_LONG).show();
                        }
                    } else {


                        try {
                            orderItemRepository = new OrderItemRepository(context);

                            if (orderItemRepository.containsId(String.valueOf(products.getId())) > 0) {
                                orderItemRepository.updateQuantityById(String.valueOf(products.getId()), (orderItemRepository.getQuantityById(products.getId()) + 1), (orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getPrice()), (orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getCost()), (orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getProfit()));
                            } else {
                                OrderItems orderItems = new OrderItems();
                                orderItems.setProduct_id(String.valueOf(products.getId()));
                                orderItems.setItem_price(products.getPrice());
                                orderItems.setItem_cost(products.getCost());
                                orderItems.setItem_profit(products.getProfit());
                                orderItems.setItem_total_cost(products.getCost());
                                orderItems.setItem_total_profit(products.getProfit());
                                orderItems.setProduct_name(products.getName());
                                orderItems.setTax(products.getTax());
                                orderItems.setIsDPercent("false");
                                orderItems.setIsDiscount("false");
                                //orderItems.setAd_id("");
                                orderItems.setUnit_in_stok(String.valueOf(products.getUnits_in_stok()));
                                orderItems.setMin_qty(String.valueOf(products.getMinimum_qty()));
                                orderItems.setQuantity(String.valueOf(orderItemRepository.getQuantityById(products.getId()) + 1));
                                orderItems.setTotal_amount(String.valueOf((orderItemRepository.getQuantityById(products.getId()) + 1) * Double.valueOf(products.getPrice())));
                                orderItemRepository.create(orderItems);
                            }
                            txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                            holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));

                            holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(products.getId())));

                            // notifyItemChanged(position);

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        return convertView;
    }
    public void callDeleteApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsAPI +"/"+id,
                "delete", Request.Method.DELETE, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }
    public void callRevertApi(int id) {
        ApiRequest apiRequest = new ApiRequest(context, this);
        apiRequest.postRequest(Constants.BaseURL + Constants.ProductsPriceAPI +"?type=revert&ids[]="+id,
                "delete", Request.Method.GET, Preferences.get(context, Preferences.KEY_USER_TOKEN));
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals("delete")) {
            Log.e("RES", response);
            Fragment fragment = new AllItemsExpandable();
            if (fragment != null) {
                FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment, "AllProduct");
                ft.commit();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    public void showAlertDialog(final Context oContext, String title, String message, final Products products) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                oContext);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(
                        message)
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                callDeleteApi(products.getId());

                                arrayList.remove(products);
                                notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public class ViewHolder {
        //CardView cv;
        TextView txtName;
        TextView txtPrice;
        ImageView imgNoti;
        ImageView imgCart;
        RelativeLayout relativeProductRow;
        BadgeView badge;

        TextView txtUnit;


        ViewHolder(View itemView) {
            // cv = (CardView) itemView.findViewById(R.id.productCard);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtUnit = (TextView) itemView.findViewById(R.id.txtUnit);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            relativeProductRow=itemView.findViewById(R.id.relativeProductRow);
            imgNoti=itemView.findViewById(R.id.imgLogo);
            imgCart=itemView.findViewById(R.id.imgCart);

            badge = new BadgeView(context, imgCart);


        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}