package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.OrderItems;

import java.util.ArrayList;

public class Order_Item_Adapter extends RecyclerView.Adapter<Order_Item_Adapter.ViewHolder> {

    ArrayList<OrderItems> orderItems;
    Context context;

    OrderItems detail;;
    public Order_Item_Adapter(Context applicationContext, ArrayList<OrderItems> orderItemsnew) {
        orderItems = orderItemsnew;
        context = applicationContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        detail = orderItems.get(i);

        viewHolder.product_price.setText(detail.getTotal_amount());
        viewHolder.tv_product_name.setText(detail.getProduct_name());
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      TextView tv_product_name,product_price;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            product_price = itemView.findViewById(R.id.product_price);
        }
    }
}
