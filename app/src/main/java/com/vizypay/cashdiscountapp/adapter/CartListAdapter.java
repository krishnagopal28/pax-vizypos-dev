package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.vizypay.cashdiscountapp.fragment.MainTabFragment.buttonCalculation;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.DataObjectHolder> {
    Context context;
    TextView txt;
    List<OrderItems> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
    TextView txtCartTotal;

    public CartListAdapter(List<OrderItems> getList, Context context, TextView txtCartTotal) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        this.txtCartTotal=txtCartTotal;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cart_list, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position){
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;

        try{
            orderItemRepository=new OrderItemRepository(context);
        }catch (Exception e){
            e.printStackTrace();
        }
        if(mList.get(position).getProduct_id().contains("Q_") ||mList.get(position).getProduct_id().contains("-")){
            holder.editButton.setVisibility(View.INVISIBLE);
        }else {
            holder.editButton.setVisibility(View.VISIBLE);
        }

        holder.txtName.setText(mList.get(position).getProduct_name());
        try {
            if (orderItemRepository.getISDiscountPercentByID(String.valueOf(mList.get(position).getProduct_id())).equals("true")){
                holder.txtPrice.setText(Utility.decimal2Values(Double.valueOf(mList.get(position).getTotal_amount()))+"%");
            }else {
                try{
                    holder.txtPrice.setText(context.getResources().getString(R.string.currency)+Utility.decimal2Values(Double.valueOf(mList.get(position).getTotal_amount())));
                } catch(NumberFormatException ex){
                }
                // holder.txtPrice.setText(context.getResources().getString(R.string.currency)+Utility.decimal2Values(Double.valueOf(mList.get(position).getTotal_amount())));
            }
        } catch (SQLException throwables) {
            holder.txtPrice.setText(context.getResources().getString(R.string.currency)+Utility.decimal2Values(Double.valueOf(mList.get(position).getTotal_amount())));
            throwables.printStackTrace();
        }

        holder.txtCartQty.setText(mList.get(position).getQuantity());

        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (Preferences.get(context, Preferences.STOCK_TRACKING).equals("1")) {
                        if (orderItemRepository.getQuantityById(Integer.parseInt(mList.get(position).getProduct_id())) < Integer.parseInt(mList.get(position).getUnit_in_stok())){
                            holder.txtCartQty.setText("" + (Integer.parseInt(holder.txtCartQty.getText().toString()) + 1));

                            try {
                                orderItemRepository.updateQuantityById(String.valueOf(mList.get(position).getProduct_id()), (Integer.parseInt(holder.txtCartQty.getText().toString().trim())), (Double.parseDouble(mList.get(position).getItem_price())) * Integer.parseInt(holder.txtCartQty.getText().toString().trim()),(Double.parseDouble(mList.get(position).getItem_cost())) * Integer.parseInt(holder.txtCartQty.getText().toString().trim()),(Double.parseDouble(mList.get(position).getItem_profit())) * Integer.parseInt(holder.txtCartQty.getText().toString().trim()));
                                holder.txtPrice.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getPriceById(String.valueOf(mList.get(position).getProduct_id()))));
                                txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                                buttonCalculation(context, txtCartTotal);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(context, "Stock not Available", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        // orderItemRepository.getQuantityById(mList.get(position).getProduct_id());
                        holder.txtCartQty.setText("" + (Integer.parseInt(holder.txtCartQty.getText().toString()) + 1));

                        try {
                            orderItemRepository.updateQuantityById(String.valueOf(mList.get(position).getProduct_id()), (Integer.parseInt(holder.txtCartQty.getText().toString().trim())), (Double.parseDouble(mList.get(position).getItem_price())) * Integer.parseInt(holder.txtCartQty.getText().toString().trim()), (Double.parseDouble(mList.get(position).getItem_cost())) * Integer.parseInt(holder.txtCartQty.getText().toString().trim()), (Double.parseDouble(mList.get(position).getItem_profit())) * Integer.parseInt(holder.txtCartQty.getText().toString().trim()));
                            holder.txtPrice.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getPriceById(String.valueOf(mList.get(position).getProduct_id()))));
                            txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                            buttonCalculation(context, txtCartTotal);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                // notifyDataSetChanged();
            }
        });

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (Preferences.get(context, Preferences.STOCK_TRACKING).equals("1")) {
                        //if (orderItemRepository.getMinQuantityById(Integer.parseInt(mList.get(position).getProduct_id())) > 0) {
                        {
                            //if (Integer.parseInt(holder.txtCartQty.getText().toString()) > orderItemRepository.getMinQuantityById(Integer.parseInt(mList.get(position).getProduct_id()))) {
                            if (Integer.parseInt(holder.txtCartQty.getText().toString()) > 1) {
                                // orderItemRepository.getQuantityById(mList.get(position).getProduct_id());
                                holder.txtCartQty.setText("" + (Integer.parseInt(holder.txtCartQty.getText().toString()) - 1));

                                try {
                                    orderItemRepository.updateQuantityById(mList.get(position).getProduct_id(), (Integer.parseInt(holder.txtCartQty.getText().toString())), (Double.parseDouble(mList.get(position).getItem_price())) * Integer.parseInt(holder.txtCartQty.getText().toString()),(Double.parseDouble(mList.get(position).getItem_cost())) * Integer.parseInt(holder.txtCartQty.getText().toString()), (Double.parseDouble(mList.get(position).getItem_profit())) * Integer.parseInt(holder.txtCartQty.getText().toString()));
                                    holder.txtPrice.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getPriceById(mList.get(position).getProduct_id())));
                                    txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                                    buttonCalculation(context, txtCartTotal);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                //   notifyDataSetChanged();
                            }
                        } /*else {

                            if (Integer.parseInt(holder.txtCartQty.getText().toString()) > 1) {
                                // orderItemRepository.getQuantityById(mList.get(position).getProduct_id());
                                holder.txtCartQty.setText("" + (Integer.parseInt(holder.txtCartQty.getText().toString()) - 1));

                                try {
                                    orderItemRepository.updateQuantityById(mList.get(position).getProduct_id(), (Integer.parseInt(holder.txtCartQty.getText().toString())), (Double.parseDouble(mList.get(position).getItem_price())) * Integer.parseInt(holder.txtCartQty.getText().toString()),(Double.parseDouble(mList.get(position).getItem_cost())) * Integer.parseInt(holder.txtCartQty.getText().toString()), (Double.parseDouble(mList.get(position).getItem_profit())) * Integer.parseInt(holder.txtCartQty.getText().toString()));
                                    holder.txtPrice.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getPriceById(mList.get(position).getProduct_id())));
                                    txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                                    buttonCalculation(context, txtCartTotal);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                //   notifyDataSetChanged();
                            }
                        }*/
                    }else {
                        if (Integer.parseInt(holder.txtCartQty.getText().toString()) > 1) {
                            // orderItemRepository.getQuantityById(mList.get(position).getProduct_id());
                            holder.txtCartQty.setText("" + (Integer.parseInt(holder.txtCartQty.getText().toString()) - 1));

                            try {
                                orderItemRepository.updateQuantityById(mList.get(position).getProduct_id(), (Integer.parseInt(holder.txtCartQty.getText().toString())), (Double.parseDouble(mList.get(position).getItem_price())) * Integer.parseInt(holder.txtCartQty.getText().toString()),(Double.parseDouble(mList.get(position).getItem_cost())) * Integer.parseInt(holder.txtCartQty.getText().toString()), (Double.parseDouble(mList.get(position).getItem_profit())) * Integer.parseInt(holder.txtCartQty.getText().toString()));
                                holder.txtPrice.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getPriceById(mList.get(position).getProduct_id())));
                                txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                                buttonCalculation(context, txtCartTotal);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            //   notifyDataSetChanged();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        });

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    orderItemRepository.deleteById(String.valueOf(mList.get(position).getProduct_id()));
                    mList.remove(mList.get(position));
                    notifyDataSetChanged();
                    txtCartTotal.setText(context.getResources().getString(R.string.currency)+Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                    buttonCalculation(context, txtCartTotal);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;
        TextView txtCartQty;
        ImageView imgNoti;
        Button btnPlus, btnMinus, btnRemove;
        RelativeLayout relativeProductRow;
        LinearLayout editButton;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtCartQty = (TextView) itemView.findViewById(R.id.txtCartQty);
            btnMinus=itemView.findViewById(R.id.btnMinus);
            btnRemove=itemView.findViewById(R.id.btnRemove);
            btnPlus=itemView.findViewById(R.id.btnPlus);
            relativeProductRow=itemView.findViewById(R.id.relativeProductRow);
            imgNoti=itemView.findViewById(R.id.imgLogo);
            editButton=itemView.findViewById(R.id.editButton);

        }
    }
}