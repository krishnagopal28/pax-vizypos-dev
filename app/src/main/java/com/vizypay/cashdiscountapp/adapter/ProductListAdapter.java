package com.vizypay.cashdiscountapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.readystatesoftware.viewbadger.BadgeView;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.model.Products;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Utility;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.DataObjectHolder> {
    Context context;
    TextView txt;
    List<Products> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
    TextView txtCartTotal;
    List<OrderItems> mCartList;

    public ProductListAdapter(List<Products> getList, Context context, TextView txtCartTotal, List<OrderItems> cartList) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        this.txtCartTotal=txtCartTotal;
        this.mCartList=cartList;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product_list, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position){
      //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
       // this.position=position;

        try{
            orderItemRepository=new OrderItemRepository(context);
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.txtName.setText(mList.get(position).getName());
        holder.txtUnit.setVisibility(View.VISIBLE);
        holder.txtUnit.setText("Available Stock: "+mList.get(position).getUnits_in_stok());
        holder.txtPrice.setText(context.getString(R.string.currency)+mList.get(position).getPrice());

        try {
            holder.badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
            //badge.setBadgeMargin(-110,0);
            holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(mList.get(position).getId())));
            holder.badge.show();
           // Log.e("badgesss", arrayList.get(position).getMenuName()+" ** "+arrayList.get(position).getMyCount());
           /* if(!arrayList.get(position).getMyCount().equals("")){
                badge.show();
                if(arrayList.get(position).getMyCount().equals("0")) {
                    badge.hide();
                }
            }else{
                badge.hide();
            }*/
        }catch (Exception e){
            e.printStackTrace();
        }
        if(mList.get(position).getMain_photo()!=null) {
            try {
                if (!mList.get(position).getMain_photo().getUrl().equals(""))
                    Glide.with(context).load("https://paxapp.vizypay.com/" + mList.get(position).getMain_photo().getUrl()).into(holder.imgNoti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Preferences.save(context, Preferences.TAX, String.valueOf(mList.get(position).getTax()));
        try {
            orderItemRepository=new OrderItemRepository(context);
            if(orderItemRepository.containsId(String.valueOf(mList.get(position).getId()))>=1){
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        holder.relativeProductRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    orderItemRepository = new OrderItemRepository(context);

                    if(orderItemRepository.containsId(String.valueOf(mList.get(position).getId()))>0){
                        orderItemRepository.updateQuantityById(String.valueOf(mList.get(position).getId()), (orderItemRepository.getQuantityById(mList.get(position).getId())+1), (orderItemRepository.getQuantityById(mList.get(position).getId())+1)*Double.valueOf(mList.get(position).getPrice()), (orderItemRepository.getQuantityById(mList.get(position).getId())+1)*Double.valueOf(mList.get(position).getCost()), (orderItemRepository.getQuantityById(mList.get(position).getId())+1)*Double.valueOf(mList.get(position).getProfit()));
                    }else {
                        OrderItems orderItems=new OrderItems();
                        orderItems.setProduct_id(String.valueOf(mList.get(position).getId()));
                        orderItems.setItem_price(mList.get(position).getPrice());
                        orderItems.setProduct_name(mList.get(position).getName());
                        orderItems.setTax(mList.get(position).getTax());
                        orderItems.setIsDPercent("false");
                        orderItems.setIsDiscount("false");
                        orderItems.setMin_qty(String.valueOf(mList.get(position).getMinimum_qty()));
                        //orderItems.setAd_id("");
                        orderItems.setUnit_in_stok(String.valueOf(mList.get(position).getUnits_in_stok()));
                        orderItems.setQuantity(String.valueOf(orderItemRepository.getQuantityById(mList.get(position).getId())+1));
                        orderItems.setTotal_amount(String.valueOf((orderItemRepository.getQuantityById(mList.get(position).getId())+1)*Double.valueOf(mList.get(position).getPrice())));
                        orderItemRepository.create(orderItems);
                    }
                    txtCartTotal.setText(context.getResources().getString(R.string.currency)+ Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                    holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));

                    holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(mList.get(position).getId())));

                   // notifyItemChanged(position);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName, txtUnit;
        TextView txtPrice;
        ImageView imgNoti;
        ImageView imgCart;
        RelativeLayout relativeProductRow;
        BadgeView badge;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtUnit = (TextView) itemView.findViewById(R.id.txtUnit);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            relativeProductRow=itemView.findViewById(R.id.relativeProductRow);
            imgNoti=itemView.findViewById(R.id.imgLogo);
            imgCart=itemView.findViewById(R.id.imgCart);

            badge = new BadgeView(context, imgCart);


        }
    }

}