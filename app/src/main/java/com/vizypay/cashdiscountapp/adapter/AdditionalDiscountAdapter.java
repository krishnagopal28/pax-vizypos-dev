package com.vizypay.cashdiscountapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.ProgressBar;
        import android.widget.RelativeLayout;
        import android.widget.TextView;

        import androidx.recyclerview.widget.RecyclerView;

import com.readystatesoftware.viewbadger.BadgeView;
        import com.vizypay.cashdiscountapp.R;
        import com.vizypay.cashdiscountapp.model.AdditionalDiscount;
        import com.vizypay.cashdiscountapp.model.OrderItems;
import com.vizypay.cashdiscountapp.repository.OrderItemRepository;
        import com.vizypay.cashdiscountapp.utility.Preferences;
        import com.vizypay.cashdiscountapp.utility.Utility;

        import java.sql.SQLException;
        import java.util.ArrayList;
        import java.util.List;

//import org.jsoup.nodes.Element;

/**
 * Created by Richa.
 */
public class AdditionalDiscountAdapter extends RecyclerView.Adapter<AdditionalDiscountAdapter.DataObjectHolder> {
    Context context;
    TextView txt;
    List<AdditionalDiscount> mList = new ArrayList<>();
    int per;
    int position1;

    ProgressBar progressBarTop;
    OrderItemRepository orderItemRepository;
    TextView txtCartTotal;

    double subTotalNoDiscount;
   // List<AdditionalDiscountAdapter> mCartList;

    public AdditionalDiscountAdapter(List<AdditionalDiscount> getList, Context context, TextView txtCartTotal) {
        this.mList = getList;
        this.context = context;
        this.txt = txt;
        this.txtCartTotal=txtCartTotal;

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_discount_list, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, @SuppressLint("RecyclerView") final int position){
        //  progressBarTop = (ProgressBar) ((AppCompatActivity)context).findViewById(R.id.pb1);
        // this.position=position;

        try{
            orderItemRepository=new OrderItemRepository(context);
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.txtName.setText(mList.get(position).getName());
        if(mList.get(position).getType().equals("Fixed")) {
            holder.txtPrice.setText(context.getString(R.string.currency) + mList.get(position).getType_value());
        }else {
            holder.txtPrice.setText( mList.get(position).getType_value()+"%");
        }
       // holder.txtPrice.setText(mList.get(position).getType_value());

        try {
            holder.badge.setVisibility(View.GONE);
            holder.badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
            //badge.setBadgeMargin(-110,0);
            holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(mList.get(position).getId())));
            //holder.badge.show();
            // Log.e("badgesss", arrayList.get(position).getMenuName()+" ** "+arrayList.get(position).getMyCount());
           /* if(!arrayList.get(position).getMyCount().equals("")){
                badge.show();
                if(arrayList.get(position).getMyCount().equals("0")) {
                    badge.hide();
                }
            }else{
                badge.hide();
            }*/
        }catch (Exception e){
            e.printStackTrace();
        }
       /* if(mList.get(position).getMain_photo()!=null) {
            try {
                if (!mList.get(position).getMain_photo().getUrl().equals(""))
                    Glide.with(context).load("https://paxapp.vizypay.com/" + mList.get(position).getMain_photo().getUrl()).into(holder.imgNoti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        holder.imgNoti.setVisibility(View.GONE);
       // Preferences.save(context, Preferences.TAX, String.valueOf(mList.get(position).getTax()));
        try {
            orderItemRepository=new OrderItemRepository(context);
            if(orderItemRepository.containsId(("-"+(mList.get(position).getId())))>=1){
                holder.txtPlusMinus.setText("-");
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));
            }else {
                holder.txtPlusMinus.setText("+");
                holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        holder.relativeProductRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    orderItemRepository = new OrderItemRepository(context);

                    if(orderItemRepository.containsId(String.valueOf("-"+mList.get(position).getId()))>0) {
                       {
                            holder.txtPlusMinus.setText("+");
                            orderItemRepository.deleteById("-"+mList.get(position).getId());
                            txtCartTotal.setText(context.getResources().getString(R.string.currency)+ Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                            holder.relativeProductRow.setBackgroundColor(Color.parseColor("#ffffff"));
                        }
                    }else {
                        holder.txtPlusMinus.setText("-");
                   /*   //  orderItemRepository.updateQuantityById(mList.get(position).getId(), (orderItemRepository.getQuantityById(mList.get(position).getId())+1), (orderItemRepository.getQuantityById(mList.get(position).getId())+1)*Double.valueOf(mList.get(position).getPrice()));
                    }else {*/
                        OrderItems orderItems = new OrderItems();
                        orderItems.setProduct_id("-" + mList.get(position).getId());
//                        Log.e("null","execption_vlaue"+Float.parseFloat(Preferences.get(context,Preferences.CASD_Discount)));

                        subTotalNoDiscount =orderItemRepository.getSubTotal();
                        Float cashD=Float.valueOf(Utility.decimal2Values((Float.parseFloat(Preferences.get(context, Preferences.CASD_Discount)))*(orderItemRepository.getSubTotal())/(100)));
                        subTotalNoDiscount=subTotalNoDiscount-cashD;
                        if (mList.get(position).getType().equals("Fixed")) {
                            orderItems.setItem_price("-" + mList.get(position).getType_value());
                            //orderItems.setItem_price("-"+String.valueOf(Utility.decimal2Values(subTotalNoDiscount-Double.valueOf(mList.get(position).getType_value()))));
                        } else {
                           // orderItems.setItem_price("-"+String.valueOf(Utility.decimal2Values((subTotalNoDiscount*Double.valueOf(mList.get(position).getType_value()))/100)));
                            orderItems.setItem_price("-" + mList.get(position).getType_value());
                        }
                        if (mList.get(position).getType().equals("Fixed")){
                            orderItems.setIsDPercent("false");
                        } else {
                            orderItems.setIsDPercent("true");
                        }
                        orderItems.setAd_id(String.valueOf(mList.get(position).getId()).replace("-", ""));
                        orderItems.setIsDiscount("true");
                        orderItems.setProduct_name(mList.get(position).getName());
                        orderItems.setTax(0);
                        orderItems.setQuantity("1");
                        // orderItems.setTotal_amount(Double.valueOf(mList.get(position).getType_value()));
                        if (mList.get(position).getType().equals("Fixed")) {
                            orderItems.setTotal_amount("-" + mList.get(position).getType_value());
                           // orderItems.setTotal_amount("-"+String.valueOf(Utility.decimal2Values(subTotalNoDiscount-Double.valueOf(mList.get(position).getType_value()))));
                        } else {
                            //orderItems.setTotal_amount("-"+String.valueOf(Utility.decimal2Values((subTotalNoDiscount*Double.valueOf(mList.get(position).getType_value()))/100)));
                            orderItems.setTotal_amount("-" + mList.get(position).getType_value());

                        }
                        orderItemRepository.create(orderItems);
                        txtCartTotal.setText(context.getResources().getString(R.string.currency) + Utility.decimal2Values(orderItemRepository.getTotalPrice()));
                        holder.relativeProductRow.setBackgroundColor(Color.parseColor("#e0f2f1"));

                        //  holder.badge.setText(String.valueOf(orderItemRepository.getQuantityById(mList.get(position).getId())));
                    }

                    // notifyItemChanged(position);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPlusMinus;
        TextView txtPrice;
        ImageView imgNoti;
        ImageView imgCart;
        RelativeLayout relativeProductRow;
        BadgeView badge;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtPlusMinus = (TextView) itemView.findViewById(R.id.txtPlusMinus);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            relativeProductRow=itemView.findViewById(R.id.relativeProductRow);
            imgNoti=itemView.findViewById(R.id.imgLogo);
            imgCart=itemView.findViewById(R.id.imgCart);

            badge = new BadgeView(context, imgCart);
        }
    }

}