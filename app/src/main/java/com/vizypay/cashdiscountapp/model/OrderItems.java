package com.vizypay.cashdiscountapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="OrderItems")
public class OrderItems {

    @DatabaseField(columnName="id")
    String id;
    @DatabaseField(columnName="product_id")
    String product_id;
    @DatabaseField(columnName="product_name")
    String product_name;
    @DatabaseField(columnName="quantity")
    String quantity;
     @DatabaseField(columnName="min_qty")
    String min_qty;
     @DatabaseField(columnName="unit_in_stok")
    String unit_in_stok;
    @DatabaseField(columnName="item_price")
    String item_price;
    @DatabaseField(columnName="total_amount")
    String total_amount;
    @DatabaseField(columnName="new_total_amount")
    String new_total_amount;
    @DatabaseField(columnName="quickSale")
    Double quickSale;
    @DatabaseField(columnName="isDPercent")
    String isDPercent;
    @DatabaseField(columnName="isDiscount")
    String isDiscount;
    @DatabaseField(columnName="ad_id")
    String ad_id;
    @DatabaseField(columnName="item_cost")
    String item_cost;
    @DatabaseField(columnName="item_profit")
    String item_profit;
    @DatabaseField(columnName="item_total_profit")
    String item_total_profit;
    @DatabaseField(columnName="item_total_cost")
    String item_total_cost;



 @DatabaseField(columnName="tax")
    double tax;

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTax() {
        return tax;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getItem_price() {
        return item_price;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public Double getQuickSale() {
        return quickSale;
    }

    public void setQuickSale(Double quickSale) {
        this.quickSale = quickSale;
    }

    public String getIsDPercent() {
        return isDPercent;
    }

    public void setIsDPercent(String isDPercent) {
        this.isDPercent = isDPercent;
    }

    public String getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(String isDiscount) {
        this.isDiscount = isDiscount;
    }

    public String getAd_id() {
        return ad_id;
    }

    public void setAd_id(String ad_id) {
        this.ad_id = ad_id;
    }

    public String getNew_total_amount() {
        return new_total_amount;
    }

    public void setNew_total_amount(String new_total_amount) {
        this.new_total_amount = new_total_amount;
    }

    public String getMin_qty() {
        return min_qty;
    }

    public void setMin_qty(String min_qty) {
        this.min_qty = min_qty;
    }

    public String getUnit_in_stok() {
        return unit_in_stok;
    }

    public void setUnit_in_stok(String unit_in_stok) {
        this.unit_in_stok = unit_in_stok;
    }

    public String getItem_cost() {
        return item_cost;
    }

    public void setItem_cost(String item_cost) {
        this.item_cost = item_cost;
    }

    public String getItem_total_profit() {
        return item_total_profit;
    }

    public void setItem_total_profit(String item_total_profit) {
        this.item_total_profit = item_total_profit;
    }

    public String getItem_total_cost() {
        return item_total_cost;
    }

    public void setItem_total_cost(String item_total_cost) {
        this.item_total_cost = item_total_cost;
    }

    public String getItem_profit() {
        return item_profit;
    }

    public void setItem_profit(String item_profit) {
        this.item_profit = item_profit;
    }
}
