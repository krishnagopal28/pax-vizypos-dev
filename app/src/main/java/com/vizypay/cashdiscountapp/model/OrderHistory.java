package com.vizypay.cashdiscountapp.model;

import com.vizypay.cashdiscountapp.utility.Signature_photo;

import java.util.List;

public class OrderHistory {
    //""payment_details":"{\"payment_type\":\"Cash\"}""
//    int id;
    String name;
    String mobile;
    String email;
    String order_total;
    String cash_discount;
    String ncc_amount;
    String tax;
    String total_amount;
    String payment_details;
    String order_status;
    String created_at;
    // created by 24-02-2021
    String order_pay_type;
    String credit_fees;
    String cash_amount;
    String card_amount;
    String amount_paid;
    String change_due;
    String tip_amount;
    String transaction_no;
    String receipt_url;
    String total_items_count;
    String CardNumber;
    String CardName;
    String CardDiscount;
    String id;

    List<OrderItems> order_items;
    Signature_photo signature_photo;
//
//    public List<Signature_photo> getSignature_photo() {
//        return signature_photo;
//    }
//
//    public void setSignature_photo(List<Signature_photo> signature_photo) {
//        this.signature_photo = signature_photo;
//    }



//    public int getId() {
//        return id;
//    }


    public Signature_photo getSignaturephoto() {
        return signature_photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getOrder_total() {
        return order_total;
    }

    public String getCash_discount() {
        return cash_discount;
    }

    public String getTax() {
        return tax;
    }
    public String getNcc() {
        return ncc_amount;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public String getOrder_status() {
        return order_status;
    }

    public List<OrderItems> getOrderItems() {
        return order_items;
    }



    public String getPayment_details() {
        return payment_details;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getOrder_pay_type() {  return order_pay_type;   }

    public String getCredit_fees() {   return credit_fees;   }

    public String getCash_amount() {   return cash_amount;   }

    public String getCard_amount() {   return card_amount;   }

    public String getAmount_paid() {
        return amount_paid;
    }

    public String getChange_due() {
        return change_due;
    }

    public String getTransaction_no() {
        return transaction_no;
    }

    public String getTip_amount() {
        return tip_amount;
    }
    public String getReceipt_url() {
        return receipt_url;
    }



    public String getTotal_item_count() {
        return total_items_count;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public String getCardName() {
        return CardName;
    }

    public void setCardName(String cardName) {
        CardName = cardName;
    }

    public String getCardDiscount() {
        return CardDiscount;
    }

    public void setCardDiscount(String cardDiscount) {
        CardDiscount = cardDiscount;
    }



}