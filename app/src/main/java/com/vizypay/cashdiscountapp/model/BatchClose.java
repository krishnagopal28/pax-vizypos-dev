package com.vizypay.cashdiscountapp.model;

import java.io.Serializable;

public class BatchClose implements Serializable {
    /*"batch_json": "0",
        "batch_date": "2021-03-19 14:09:34",
        "batch_number": "7",
        "merchant_number": "519917920300027",
        "created_by_id": 4,
        "updated_at": "2021-03-19 14:09:37",
        "created_at": "2021-03-19 14:09:37",
        "id": 128,
        "net_sales": 4.6499999999999995,
        "gross_volume": 4.85,
        "gross_trans": 5,
        "ad_discount": 0,
        "ad_discount_trans": 0,
        "cash_discount": 0.2,
        "cash_discount_trans": 5,
        "cash_amount": 4.08,
        "cash_trans": 3,
        "card_amount": 0.01,
        "card_trans": 1,
        "cash_card_trans": 1,
        "cash_card_amount": 0.95,
        "card_cash_amount": 0.07,
        "card_ref_voi_amount": 0,
        "cash_ref_voi_amount": 0,
        "split_ref_voi_amount": 0,
        "card_ref_voi_trans": 0,
        "cash_ref_voi_trans": 0,
        "split_ref_voi_trans": 0,
        "tax": 0.3006,
        "total_sales": 4.6499999999999995,
        "net_sales_trans": 5*/
    int id;
    String net_sales;
    String gross_volume;
    String gross_trans;
    String ad_discount;
    String ad_discount_trans;
    String cash_discount;
    String cash_discount_trans;
    String cash_amount;
    String card_trans;
    String cash_card_trans;
    String cash_card_amount;
    String card_cash_amount;
    String card_ref_voi_amount;
    String cash_ref_voi_amount;
    String split_ref_voi_amount;
    String card_ref_voi_trans;
    String cash_ref_voi_trans;
    String split_ref_voi_trans;
    String tax;
    String total_sales;
    String cash_trans;
    String batch_date;
    String net_sales_trans;
    String total_tip_amount;
    String total_tip_count;

    String  total_cash_tip_amount;
    String  total_cash_tip_count;
    String  total_card_tip_amount;
    String total_card_tip_count;

    String card_amount;
    String total_card_volume;
    String estimated_deposit;
    String fees_paid_calculated;


    public int getId() {
        return id;
    }

    public String getNet_sales() {
        return net_sales;
    }

    public String getGross_volume() {
        return gross_volume;
    }

    public String getGross_trans() {
        return gross_trans;
    }

    public String getAd_discount() {
        return ad_discount;
    }

    public String getAd_discount_trans() {
        return ad_discount_trans;
    }

    public String getCash_discount() {
        return cash_discount;
    }

    public String getCash_discount_trans() {
        return cash_discount_trans;
    }

    public String getCash_amount() {
        return cash_amount;
    }

    public String getCard_trans() {
        return card_trans;
    }

    public String getCash_card_trans() {
        return cash_card_trans;
    }

    public String getCash_card_amount() {
        return cash_card_amount;
    }

    public String getCard_cash_amount() {
        return card_cash_amount;
    }

    public String getCard_ref_voi_amount() {
        return card_ref_voi_amount;
    }

    public String getCash_ref_voi_amount() {
        return cash_ref_voi_amount;
    }

    public String getSplit_ref_voi_amount() {
        return split_ref_voi_amount;
    }

    public String getCard_ref_voi_trans() {
        return card_ref_voi_trans;
    }

    public String getCash_ref_voi_trans() {
        return cash_ref_voi_trans;
    }

    public String getSplit_ref_voi_trans() {
        return split_ref_voi_trans;
    }

    public String getTax() {
        return tax;
    }

    public String getTotal_sales() {
        return total_sales;
    }

    public String getCash_trans() {
        return cash_trans;
    }

    public String getBatch_date() {
        return batch_date;
    }

    public String getNet_sales_trans() {
        return net_sales_trans;
    }

    public String getCard_amount() {
        return card_amount;
    }

    public String getTotal_tip_amount() {
        return total_tip_amount;
    }

    public String getTotal_tip_count() {
        return total_tip_count;
    }

    public String getTotal_cash_tip_amount() {
        return total_cash_tip_amount;
    }

    public String getTotal_cash_tip_count() {
        return total_cash_tip_count;
    }

    public String getTotal_card_tip_amount() {
        return total_card_tip_amount;
    }

    public String getTotal_card_tip_count() {
        return total_card_tip_count;
    }

    public String getTotal_card_volume() {
        return total_card_volume;
    }

    public String getEstimated_deposit() {
        return estimated_deposit;
    }
    public String getFees_paid_calculated() {
        return fees_paid_calculated;
    }
}
