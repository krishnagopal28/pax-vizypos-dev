package com.vizypay.cashdiscountapp.model;

public class Categories {

    int id;
    String name;
    String short_code;
    String color;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShort_code() {
        return short_code;
    }

    public String getColor() {
        return color;
    }
}
