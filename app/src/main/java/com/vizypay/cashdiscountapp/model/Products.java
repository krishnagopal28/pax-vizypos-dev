package com.vizypay.cashdiscountapp.model;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Products implements Serializable {
    int id;
    String name;
    String sku;
    String description;

    String price;
    String created_at;
    String deleted_at;
    String created_by_id;
    String category_id;
    String units_in_stok;
    String pos_data;
    String backup_price;
    String minimum_qty;
    String cost;
    String profit;
    double tax;
    @Nullable
    MainPhoto main_photo;

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTax() {
        return tax;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public void setCreated_by_id(String created_by_id) {
        this.created_by_id = created_by_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public void setUnits_in_stok(String units_in_stok) {
        this.units_in_stok = units_in_stok;
    }

    public void setPos_data(String pos_data) {
        this.pos_data = pos_data;
    }

    public void setBackup_price(String backup_price) {
        this.backup_price = backup_price;
    }

    public void setMain_photo(@Nullable MainPhoto main_photo) {
        this.main_photo = main_photo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSku() {
        return sku;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public String getCreated_by_id() {
        return created_by_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getUnits_in_stok() {
        return units_in_stok;
    }

    public String getPos_data() {
        return pos_data;
    }

    public MainPhoto getMain_photo() {
        return main_photo;
    }

    public String getBackup_price() {
        return backup_price;
    }

    public String getMinimum_qty() {
        return minimum_qty;
    }

    public void setMinimum_qty(String minimum_qty) {
        this.minimum_qty = minimum_qty;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getProfit() {
        return profit;
    }


/*public String getMain_photo() {
        return main_photo;
    }*/
}
