package com.vizypay.cashdiscountapp.model;

import java.io.Serializable;

public class MainPhoto implements Serializable {

    /*"id": 1,
                "model_type": "App\\Product",
                "model_id": 16,
                "collection_name": "main_photo",
                "name": "5e870047d2edd_1",
                "file_name": "5e870047d2edd_1.jpg",
                "mime_type": "image/jpeg",
                "disk": "public",
                "size": 138100,
                "manipulations": [],
                "custom_properties": {
                    "generated_conversions": {
                        "thumb": true
                    }
                },
                "responsive_images": [],
                "order_column": 1,
                "created_at": "2020-04-03 03:52:50",
                "updated_at": "2020-04-03 03:52:54",
                "url": "/storage/app/public/1/5e870047d2edd_1.jpg",
                "thumbnail": "/storage/app/public/1/conversions/5e870047d2edd_1-thumb.jpg"*/
    int id;
    int model_id;
    String model_type;
    String collection_name;
    String name;
    String file_name;
    String mime_type;
    String disk;
    String size;
    String url;
    String thumbnail;

    public int getId() {
        return id;
    }

    public int getModel_id() {
        return model_id;
    }

    public String getModel_type() {
        return model_type;
    }

    public String getCollection_name() {
        return collection_name;
    }

    public String getName() {
        return name;
    }

    public String getFile_name() {
        return file_name;
    }

    public String getMime_type() {
        return mime_type;
    }

    public String getDisk() {
        return disk;
    }

    public String getSize() {
        return size;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
