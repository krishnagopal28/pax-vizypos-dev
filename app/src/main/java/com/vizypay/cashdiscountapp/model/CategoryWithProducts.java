package com.vizypay.cashdiscountapp.model;

import java.io.Serializable;
import java.util.List;

public class CategoryWithProducts implements Serializable {

    int id;
    String name;
    String short_code;
    String color;
    List<Products> products;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_code() {
        return short_code;
    }

    public void setShort_code(String short_code) {
        this.short_code = short_code;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Products> getListOfProducts() {
        return products;
    }

    public void setListOfProducts(List<Products> listOfProducts) {
        this.products = listOfProducts;
    }

    /*public String getMain_photo() {
        return main_photo;
    }*/
}
