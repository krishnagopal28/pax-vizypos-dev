package com.vizypay.cashdiscountapp.model;

public class Category {

    /*  "id": 3,
            "name": "Cate APi Test Edit",
            "short_code": "cate-api-test-edit",
            "parent_id": null,
            "created_by_id": 3,
            "color": "",*/
    int id;
    String name;
    String short_code;
    String color;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShort_code(String short_code) {
        this.short_code = short_code;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShort_code() {
        return short_code;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return name;
    }
}
