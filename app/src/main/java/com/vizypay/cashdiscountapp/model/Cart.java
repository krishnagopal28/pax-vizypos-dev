package com.vizypay.cashdiscountapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="Cart")
public class Cart {

  /*	"name":"Test",
	"mobile":"9999999999",
	"email":"test@test.com",
	"order_total":"100.25",
	"cash_discount":"0.00",
	"card_discount":"0.00",
	"tax":"10.55",
	"total_amount":"110.80",*/
    @DatabaseField(columnName="totalItems")
    int totalItems;
    @DatabaseField(columnName="totalPrice")
    int totalPrice;
    @DatabaseField(columnName="orderItems")
    OrderItems orderItems;
    @DatabaseField(columnName = "quickSale")
    double quickSale;

    public int getTotalItems() {
        return totalItems;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public OrderItems getOrderItems() {
        return orderItems;
    }

  public double getQuickSale() {
    return quickSale;
  }

  public void setQuickSale(double quickSale) {
    this.quickSale = quickSale;
  }
}
