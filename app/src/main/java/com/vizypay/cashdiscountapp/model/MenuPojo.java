package com.vizypay.cashdiscountapp.model;

public class MenuPojo {
    int imgResID;
    String name;

    public MenuPojo(int imgResID, String name) {
        this.imgResID = imgResID;
        this.name = name;
    }

    public int getImgResID() {
        return imgResID;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
