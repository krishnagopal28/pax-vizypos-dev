package com.vizypay.cashdiscountapp.model;

import java.io.Serializable;

public class AdditionalDiscount implements Serializable {

    /*"id": 1,
            "name": "API Test 2%",
            "type": "Percent",
            "type_value": "2.00",
            "created_at": "2020-11-26 12:54:41",
            "updated_at": "2020-11-26 16:32:59",
            "deleted_at": null,
            "created_by_id": 6,*/
    int id;
    String name;
    String type;
    String type_value;

    String created_by_id;
    OrderItems items;

    public AdditionalDiscount() {
    }

    public AdditionalDiscount(String name, String type_value, String type, OrderItems items) {
        this.name = name;
        this.type_value = type_value;
        this.type = type;
        this.items=items;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setType_value(String type_value) {
        this.type_value = type_value;
    }

    public void setCreated_by_id(String created_by_id) {
        this.created_by_id = created_by_id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getType_value() {
        return type_value;
    }

    public String getCreated_by_id() {
        return created_by_id;
    }

    public OrderItems getItems() {
        return items;
    }

    public void setItems(OrderItems items) {
        this.items = items;
    }
}
