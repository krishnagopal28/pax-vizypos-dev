package com.vizypay.cashdiscountapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.utility.Constants;
import com.vizypay.cashdiscountapp.utility.Preferences;
import com.vizypay.cashdiscountapp.utility.Validation;
import com.vizypay.cashdiscountapp.volley.AppController;
import com.vizypay.cashdiscountapp.volley.IApiResponse;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements IApiResponse {

    EditText edtEmail, edtPassword;
    Button btnLogin;
    Context mContext;
    String responseServer;
    String email, password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext=this;
        edtEmail= findViewById(R.id.username);
        edtPassword= findViewById(R.id.password);
        btnLogin= findViewById(R.id.login);
        btnLogin.setEnabled(true);
        Preferences.save(LoginActivity.this, Preferences.PriceInc, "4");
        /*edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isValid()){
                    btnLogin.setEnabled(true);
                }else {
                    btnLogin.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
        /*edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isValid()){
                    btnLogin.setEnabled(true);
                }else {
                    btnLogin.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValid())
                callLoginAPI(edtEmail.getText().toString(), edtPassword.getText().toString());

            }
        });

    }


    public void callLoginAPI(String email1, String password1){
        email=email1;
        password=password1;
/*

        HashMap<String, String> map=new HashMap<>();
        map.put("email", email);
        map.put("password", password);

        ApiRequest apiRequest = new ApiRequest(mContext,this);
        apiRequest.postRequest(Constants.BaseURL+Constants.loginAPI,
                "userlogin", map, Request.Method.POST);
*/

       /* try{
            final AppConstant mAppConst=new AppConstant(mContext);
            mAppConst.showProgressDialog();
            Map<String, String> map=new HashMap<>();
            map.put("email", email);
            map.put("password", password);

            *//*params.put("name",PreferencesUtils.getCloverMerchantName());
            params.put("email","support@vizypay.com");
            params.put("address",PreferencesUtils.getCloverMerchantAddress());
            params.put("phone",PreferencesUtils.getCloverMerchantPhone());
            params.put("message",message);*//*
            mAppConst.postJsonRequest(Constants.BaseURL+Constants.loginAPI, map, new OnResponseListener() {
                        @Override
                        public void onTaskCompleted(JSONObject object) {
                            mAppConst.hideProgressDialog();

                            try {
                              //  JSONObject object=new JSONObject(response);
                                if(object.has("success")){
                                    Toast.makeText(mContext, "Successfully logged in", Toast.LENGTH_LONG).show();
                                    Preferences.save(mContext, Preferences.KEY_USER_TOKEN, object.getJSONObject("success").getString("token"));
                                    Intent intent=new Intent(mContext, MainActivity.class);
                                    startActivityForResult(intent, 0003);
                                } else if (object.has("error")) {
                                    Log.e("res", object.optString("error"));
                                    //Toast.makeText(mContext, object.optString("error"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                            mAppConst.hideProgressDialog();
                           // dialog.dismiss();
                        }
                    }
            );
        }catch (Exception e){
            e.printStackTrace();
        }*/

      /*  try{
            AsyncT asyncT = new AsyncT();
            asyncT.execute();
        }catch (Exception e){
            e.printStackTrace();
        }*/
    /*    Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("name", "Richa");
        paramsReq.put("device_id", "123");
        paramsReq.put("mobile_number", "8817518511");
      {
            paramsReq.put("store_mobile_number", "7999493928");
            paramsReq.put("qrcode", "");
        }
       // paramsReq.put("mac_address", Utility.getMacAddr());

        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postRequest("https://www.appvyapar.com/api/customers_add", "customers_add", paramsReq, Request.Method.POST,"");
*/


            // Post params to be sent to the server
             HashMap<String, String> params = new HashMap<String, String>();
            params.put("email", email1);
            params.put("password", password1);
            Log.d("URL", Constants.BaseURL+Constants.loginAPI);
            Log.d("Params", params.toString());

           // Log.d("Method", Constants.BaseURL+Constants.loginAPI);

            JsonObjectRequest req = new JsonObjectRequest(Constants.BaseURL_1+Constants.loginAPI, new JSONObject(
                    params), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {


                        Log.w("myApp",
                                "status code..." + response);

                        if(response.has("success")){
                            Toast.makeText(mContext, "Successfully logged in", Toast.LENGTH_LONG).show();
                            Preferences.save(mContext, Preferences.KEY_USER_TOKEN, response.getJSONObject("success").getString("token"));

                            Preferences.save(mContext, Preferences.QSaleID, String.valueOf(00));
                            Preferences.save(mContext, Preferences.CREDIT_FEES, "1.04");
                            Preferences.save(mContext, Preferences.EMAIL, email1);
                            Preferences.save(mContext, Preferences.PASSWORD, password1);

                            Intent intent=new Intent(mContext, MainActivity.class);
                            startActivityForResult(intent, 0003);
                            finish();
                        } else if (response.has("error")) {
                            Log.e("res", response.optString("error"));
                            //Toast.makeText(mContext, object.optString("error"), Toast.LENGTH_LONG).show();
                        }

                        // VolleyLog.v("Response:%n %s", response.toString(4));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.w("error in response", "Error: " + error.getMessage());
                    Toast.makeText(mContext, "Invalid Credentials, Please try again.", Toast.LENGTH_SHORT).show();
                    clearData();
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                }
            }
            ){

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;
                        try {
                            Log.d("parse jsonObject",volleyError.networkResponse.data.toString() );
                            String responseBody = new String(volleyError.networkResponse.data);
                           // displayMessage(responseBody);
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.d("parse jsonObject", jsonObject.toString());




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    //long unixTime = System.currentTimeMillis() / 1000L;
                    //  String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("content-type", "application/json; charset=utf-8");
                  //  params.put("X-Requested-WithXML", "application/json; charset=utf-8");
                    //params.put("Content-Length", getBodyContentType().);
                   // params.put("Host", "http://localhost");
                   // params.put("User-Agent", "android");


               /*     long unixTime = System.currentTimeMillis() / 1000L;
                    String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put("X-Authorization-Token", token);
                    params.put("X-Authorization-Time", unixTime + "");
                    params.put("User-Agent", "android");
                    Log.e("Header", params.toString());
*/
                    Log.e("Header", params.toString());
                    // params.put("Authorization", "Bearer "+token);
                    return params;
                }

            };

            // add the request object to the queue to be executed
            AppController.getInstance().addToRequestQueue(req);
      /*  try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                    new AndroidMultiPartEntity.ProgressListener() {

                        @Override
                        public void transferred(long num) {
                            //publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });
            String myFormat = "dd-MM-yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            if (selectedPath != null) {
                entity.addPart("image", new FileBody(new File(selectedPath)));
            }
            try {
                entity.addPart("date", new StringBody(new SimpleDateFormat("yyyy-MM-dd").format(sdf.parse(edtDate.getText().toString())) + ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.addPart("amount", new StringBody(edtAmount.getText().toString().trim()));
            entity.addPart("payment_mode", new StringBody(Preferences.get(mContext, Preferences.DELIVERY_PAYMNT_OPTION)));
            entity.addPart("salesman_id", new StringBody(Preferences.get(mContext, Preferences.KEY_SALES_ID)));
            entity.addPart("payment_detail", new StringBody(edtRemark.getText().toString().trim()));
            if (Preferences.get(mContext, Preferences.KEY_USER_TYPE).equals("8"))
                entity.addPart("initiated_by", new StringBody("S"));
            else
                entity.addPart("initiated_by", new StringBody("C"));
            entity.addPart("status", new StringBody("0"));
            entity.addPart("customer_id", new StringBody(Preferences.get(mContext, Preferences.KEY_USER_ID)));

            //totalSize = entity.getContentLength();
            httppost.setEntity(entity);
            Log.e("POST IMAGE", "Post Url: " + url + " ::: " + entity.toString());
            // Making server call
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            entity.writeTo(bytes);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);

                Log.e("RSP", responseString);

            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }

        } catch (Exception | OutOfMemoryError e) {
            //mProgressDialog.dismiss();
            //mIsError = true;
            responseString = e.toString();
        }*/
    }

    public void clearData(){
        edtEmail.setText(null);
        edtPassword.setText(null);
    }

    public boolean isValid(){
        boolean ret=true;
        if(!Validation.isEmailAddressOnClick(edtEmail,true)){
            ret=false;
        }
        if(!Validation.hasText(edtPassword)){
            ret=false;
        }
        if(edtPassword.getText().toString().length()<6){
            ret=false;
            edtPassword.setError("Atleast 6 characters");
        }
        return ret;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if(tag_json_obj.equals("userlogin")){
            try {
                JSONObject object=new JSONObject(response);
                if(object.has("success")){
                    Toast.makeText(mContext, "Successfully logged in", Toast.LENGTH_LONG).show();
                    Preferences.save(mContext, Preferences.KEY_USER_TOKEN, object.getJSONObject("success").getString("token"));
                    Intent intent=new Intent(mContext, MainActivity.class);
                    startActivityForResult(intent, 0003);
                } else if (object.has("error")) {
                    Log.e("res", object.optString("error"));
                    //Toast.makeText(mContext, object.optString("error"), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


    /* Inner class to get response */
    class AsyncT extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constants.BaseURL+Constants.loginAPI);

            try {

                JSONObject jsonobj = new JSONObject();

                jsonobj.put("email", email);
                jsonobj.put("password", password);

              /*  List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("", jsonobj.toString()));

                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());*/
                StringEntity se = new StringEntity( jsonobj.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                // Use UrlEncodedFormEntity to send in proper format which we need
                httppost.setEntity(se);

                // Execute HTTP Post Request
                org.apache.http.HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                responseServer = str.getStringFromInputStream(inputStream);
                Log.e("response", "response -----" + responseServer);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

           /* txt.setText(responseServer);*/
        }
    }

    public static class InputStreamToStringExample {

        public static void main(String[] args) throws IOException {

            // intilize an InputStream
            InputStream is =
                    new ByteArrayInputStream("file content..blah blah".getBytes());

            String result = getStringFromInputStream(is);

            System.out.println(result);
            System.out.println("Done");

        }

        // convert InputStream to String
        private static String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return sb.toString();
        }

    }
}