package com.vizypay.cashdiscountapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.vizypay.cashdiscountapp.MainActivity;
import com.vizypay.cashdiscountapp.R;
import com.vizypay.cashdiscountapp.utility.Preferences;

public class SplashActivity extends AppCompatActivity {

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (Preferences.get(SplashActivity.this, Preferences.KEY_USER_TOKEN).equals("")) {
                    intent=new Intent(SplashActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 0002);
                    finish();

                } else {
                  //  Preferences.save(SplashActivity.this, Preferences.KEY_USER_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjhiOTFjZDUxNTUyZjBlMGI0ZDAyOWIxZGRhY2YwZjUyY2U5OGMyOTUzZmJhNDUyMzM5NDZlZmIyYTk2ZDg3YjY3OWVmMGZjNjFjYjk0NGQ2In0.eyJhdWQiOiIxIiwianRpIjoiOGI5MWNkNTE1NTJmMGUwYjRkMDI5YjFkZGFjZjBmNTJjZTk4YzI5NTNmYmE0NTIzMzk0NmVmYjJhOTZkODdiNjc5ZWYwZmM2MWNiOTQ0ZDYiLCJpYXQiOjE1ODgyMjk4MTAsIm5iZiI6MTU4ODIyOTgxMCwiZXhwIjoxNjE5NzY1ODEwLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.KD3Imlv3kDXB0RkxPe9oT6WuEc0UoiIhz5iegnXaDK22LHszJp0WeSZkrI0YOfckPM8nnlIRiZPXGWz0uHQXMn_ZAtzoj35zJPCNz_J_tqFEsCZ-WMnmGSNsQcuzjRARQcl6nSNtPKHp26Ckled87j3KtEKBZWqYHdyXep_8dg84E26dZgb96UCXdZkjiQxhBoAsr3NpPhjvciGfOgnqrpDMd7mp7RLpVUuDZ44IYyvnObpylGIPtsDKMcZYxSVpZR_vyWL5sFYxmJlGz6L4EfufmZdVnaptZag0ToyOF-BHcGfRsaCcSgEWxWEWFi7MeTc77bCOyJSDUpIqRR6s1o0kx2ng8LNFot9FsQHJqdpRH-0AAXcs_3HK17ZvcntwNua4GZCqXsrkhTbTMr35dEriUUQIP6D9r7yBtKVrtDYIulCzX3tl7jco9VCK9k-0iqpmBf-iqKYkhpeihtRIIkFclAp5yCHMp7p5mq-5OYdvqMAiHwdLov6uMSRXVLCd1EqJRiMMLs6Lyf_IiWjKkJgN5GwBSxEhrg6zxEwK39kY0ZkLhuF4fP0WZQPUa5TdVvWQcGIgiI29XoVNCJyxLKCiFOZ-08G6a4P_zIRe4GLE3_RAqBGRYtnsaGRDvG8dzHToN7_TuFZLbHBbZALvsMjorJqgWPywtyfettrIZ4g");
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivityForResult(intent, 0001);
                    finish();
                }

            }
        }, 3000);
    }
}
