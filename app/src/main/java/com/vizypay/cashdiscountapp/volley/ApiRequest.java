package com.vizypay.cashdiscountapp.volley;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class ApiRequest {

    Context oContext;
    IApiResponse apiResponse;


    public ApiRequest(Context oContext, IApiResponse apiResponse) {
        this.oContext = oContext;
        this.apiResponse = apiResponse;
    }

    public void postRequest(final String url, final String tag_json_obj, int method, final String token) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            Log.d("URL", url);

           /* final ProgressDialog pDialog = new ProgressDialog(oContext);
            pDialog.setCancelable(false);
            pDialog.setMessage(oContext.getResources().getString(R.string.loading));
            pDialog.show();*/
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                  /*  if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    error.printStackTrace();
                    VolleyLog.e("Push Response Error:", "Error: " + error.getMessage());
                   /* if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                }
            }) {
                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    long unixTime = System.currentTimeMillis() / 1000L;
                   // String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                   // params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer "+token);
                 /*   params.put("X-Authorization-Time", unixTime + "");
                    params.put("User-Agent", "android");*/
                    Log.e("Headers", params.toString());
                    return params;
                }
            };

            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {
            AlertManager.showToast(oContext,"No Internet");
        }
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String trimMessage(JSONObject obj, String key) {
        String trimmedString = null;
        try {
            // JSONObject obj = new JSONObject(json);
            JSONObject errorObj = obj.getJSONObject("error");
            trimmedString = errorObj.getString(key);
            int statusCode = errorObj.getInt("code");
            if (trimmedString.equalsIgnoreCase("Token has expired") || trimmedString.equalsIgnoreCase("Unauthorized.")) {
                //Preferences.logOut(oContext);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    public void displayMessage(final String toastString) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                AlertManager.showToast(oContext, toastString);
            }
        });
    }

    public void postRequest(final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method, final String token) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            //System.out.println("URL::::" + url);
            //System.out.println("params::::" + paramsReq);
            //System.out.println("method::" + method);
            Random r = new Random();
          /*  final ProgressDialog pDialog = new ProgressDialog(oContext);
            pDialog.setCancelable(false);
            pDialog.setMessage(oContext.getResources().getString(R.string.loading));
            pDialog.show();*/
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    //Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                    try {
                        //Log.d("Push Response POST", response.toString() + tag_json_obj.toString());
                     /*   if (pDialog != null && pDialog.isShowing())
                            pDialog.hide();*/
                    } catch (Exception e) {

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
               /*     if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    return paramsReq;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            apiResponse.onResultReceived(responseBody, tag_json_obj);
                           /* Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                 //   long unixTime = System.currentTimeMillis() / 1000L;
                   // String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    //params.put("content-type", "; charset=utf-8");
                    params.put("Authorization", "Bearer "+token);

                    Log.e("Header", params.toString());
                  /*  params.put("X-Authorization-Time", unixTime + "");
                    params.put("User-Agent", "android");*/

                    /*long unixTime = System.currentTimeMillis() / 1000L;
                    String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put("X-Authorization-Token", token);
                    params.put("X-Authorization-Time", unixTime + "");
                    params.put("User-Agent", "android");
                    Log.e("Header", params.toString());*/
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {
            VolleyError error = new VolleyError();
            apiResponse.onErrorResponse(error);
            AlertManager.showToast(oContext, "No Internet");
        }
    }

    public void postJSONRequest(final String url, final String tag_json_obj, final Map<String, Object> paramsReq, int method, final String token) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            System.out.println("URL::::" + url);
            System.out.println("params::::" + paramsReq);
            System.out.println("method::" + method);
            Random r = new Random();
            final JsonObjectRequest strReq = new JsonObjectRequest(method, url, new JSONObject(paramsReq), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("JSON_RESP", response.toString());

                    apiResponse.onResultReceived(response.toString(), tag_json_obj);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);
                        // Print Error!
                        VolleyLog.d("JSON_ERROR", "Error: " + error.getMessage()+" _  "+jsonError);
                    }


                    apiResponse.onErrorResponse(error);
                }
            } ) {


                /*@Override
                protected Map<String, String> getParams() {

                    return super.getParams() ;
                }*/

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {

                            String responseBody = new String(volleyError.networkResponse.data);
                            //JSONObject jsonObject = new JSONObject(responseBody);
                            Log.d("ERR", responseBody);
                            apiResponse.onResultReceived(responseBody, tag_json_obj);
                           /* Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {


                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer "+token);
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");


                    Log.e("Header", params.toString());
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {
            VolleyError error = new VolleyError();
            apiResponse.onErrorResponse(error);
            AlertManager.showToast(oContext, "No Internet");
        }
    }

 public void postJSONRequest1(final String url, final String tag_json_obj, final JSONObject paramsReq, int method, final String token) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            System.out.println("URL::::" + url);
            System.out.println("params::::" + paramsReq);
            System.out.println("method::" + method);
            Random r = new Random();
            final JsonObjectRequest strReq = new JsonObjectRequest(method, url, paramsReq, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("JSON_RESP", response.toString());

                    apiResponse.onResultReceived(response.toString(), tag_json_obj);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);
                        // Print Error!
                        VolleyLog.d("JSON_ERROR", "Error: " + error.getMessage()+" _  "+jsonError);
                    }


                    apiResponse.onErrorResponse(error);
                }
            } ) {


                /*@Override
                protected Map<String, String> getParams() {

                    return super.getParams() ;
                }*/

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {

                            String responseBody = new String(volleyError.networkResponse.data);
                            //JSONObject jsonObject = new JSONObject(responseBody);
                            Log.d("ERR", responseBody);
                            apiResponse.onResultReceived(responseBody, tag_json_obj);
                           /* Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {


                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer "+token);
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");


                    Log.e("Header", params.toString());
                    return params;
                }
            };
            //  strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {
            VolleyError error = new VolleyError();
            apiResponse.onErrorResponse(error);
            AlertManager.showToast(oContext, "No Internet");
        }
    }


    public void postRequest(final int page, final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            System.out.println("URL::::" + url);
            System.out.println("paramsReq::::" + paramsReq);

/*
            final ProgressDialog pDialog = new ProgressDialog(oContext);
            pDialog.setCancelable(false);
            pDialog.setMessage(oContext.getResources().getString(R.string.loading));
*/

            if (page == 0) {
              /*  if (pDialog != null) {
                    //pDialog.setMessage(array[i1]);
                    //pDialog.show();
                }*/
            }
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);


                 /*   if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                /*    if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                    //displayMessage(error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return paramsReq;
                }


                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;
                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            displayMessage(responseBody);
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    long unixTime = System.currentTimeMillis() / 1000L;
                  //  String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                   // params.put("Authorization", "Bearer "+token);
                    return params;
                }

            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 1000, 5, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq);
        } else {
            VolleyError error = new VolleyError();
            apiResponse.onErrorResponse(error);
          //  AlertManager.showToast(oContext, oContext.getString(R.string.InternetNot));
        }

    }


    public void postRequestBackground(final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            System.out.println("Url:" + url);


            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    long unixTime = System.currentTimeMillis() / 1000L;
                    String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put("X-Authorization-Token", token);
                    params.put("X-Authorization-Time", unixTime + "");
                    params.put("User-Agent", "android");
                    Log.i("paramsss", params.toString());
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return volleyError;
                }
            };
            System.out.println("paramsReq:: " + paramsReq);
            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 100, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq);
        }
    }

    public void postRequestWithImage(final byte[] data, final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            System.out.println("Url:" + url);


            MultipartRequest request=new MultipartRequest(Request.Method.POST, url,new Response.Listener<NetworkResponse>() {

                @Override
                public void onResponse(NetworkResponse response) {
                    Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(new String(response.data), tag_json_obj);
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                }
            } ){
               /* @Override
                protected Map<String, String> getParams() {
                    return paramsReq;
                }
*/
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    long unixTime = System.currentTimeMillis() / 1000L;
                    String token = md5("c774928ce1d865cc124332279dfb656f" + unixTime + "android");
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put("X-Authorization-Token", token);
                    params.put("X-Authorization-Time", unixTime + "");
                    params.put("User-Agent", "android");
                    Log.i("paramsss", params.toString());
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return volleyError;
                }

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    // file name could found file base or direct access from real path
                    // for now just get bitmap data from ImageView
                    params.put("image", new DataPart("image", data));
                    //params.put("cover", new DataPart("file_cover.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mCoverImage.getDrawable()), "image/jpeg"));
                    System.out.println("paramsReq:: " + paramsReq+" data: "+params);
                    return params;
                }
            };
            System.out.println("paramsReq:: " + paramsReq);
            request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 100, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(request);
        }
    }
}