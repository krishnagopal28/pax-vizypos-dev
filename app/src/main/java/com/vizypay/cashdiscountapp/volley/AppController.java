package com.vizypay.cashdiscountapp.volley;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;

import androidx.multidex.BuildConfig;
import androidx.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.pax.poslink.POSLinkAndroid;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.pm.ApplicationInfo;
import android.os.Build;

import com.pax.poslink.CommSetting;
import com.pax.poslink.broadpos.BroadPOSReceiverHelper;
import com.pax.poslink.broadpos.ReceiverResult;

import com.pax.poslink.util.LogStaticWrapper;


public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static AppController mInstance;
    private static Context mContext;
    Typeface typeface;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext=this;
        init();

        //POSLinkAndroid.init(getApplicationContext());
        //Typeface typeface = ResourcesCompat.getFont(this, R.font.abril_fatface);
    }

    /*@Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
        MultiDex.install(this);
    }*/

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(String tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static Context getContext(){
        return mContext;
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;

    public static <T> ArrayList<T> getGsonArray(String s, Class<T[]> clazz) {
        T[] arr = new Gson().fromJson(s, clazz);
        return new ArrayList<T>(Arrays.asList(arr));
    }

    public static <T> Object getGsonObject(String s, Class<T> clazz) {
        Object object = new Gson().fromJson(s, clazz);
        return object;
    }

    public static  <T> JsonArray getGsonJsonArray(ArrayList<T> clazz) {
        Gson gson = new GsonBuilder().create();
        JsonArray myCustomArray = gson.toJsonTree(clazz).getAsJsonArray();
        return myCustomArray;
    }


    private void init() {

        CommSetting commSetting = setupSetting();
        POSLinkAndroid.init(getApplicationContext(), commSetting);
        android.util.Log.i("DEBUG", "Start Application");

        BroadPOSReceiverHelper.getInstance(this).setReceiverListener(new BroadPOSReceiverHelper.ReceiverListener() {
            @Override
            public void onReceiverFromBroadPOS(ReceiverResult receiverResult) {
                //  UIUtil.showToast(getApplicationContext(), receiverResult.getCode() + ": " + receiverResult.getMessage(), Toast.LENGTH_LONG);
                LogStaticWrapper.getLog().v("Message Form BroadPOS: " + receiverResult.getCode() + ", " + receiverResult.getMessage());
            }
        });
        LogStaticWrapper.getLog().v("DeviceMode:" + Build.MODEL);
        android.util.Log.d("AppController", "DeviceMode:" + Build.MODEL);

    }

    public static CommSetting setupSetting() {
        CommSetting commSetting =  new CommSetting();

        if (Build.MODEL.startsWith("E")) {
            commSetting.setType(CommSetting.USB);
        } else if (Build.MODEL.startsWith("A9")){
            commSetting.setType(CommSetting.AIDL);
        }else if (Build.MODEL.startsWith("A8")){
            commSetting.setType(CommSetting.AIDL);
        } else if (Build.MODEL.startsWith("A6")){
            commSetting.setType(CommSetting.AIDL);
        }else {
            commSetting.setType(CommSetting.AIDL);
        }

        commSetting.setTimeOut("60000");
        commSetting.setSerialPort("COM1");
        //commSetting.setSerialPort("Ethernet");
        commSetting.setBaudRate("9600");
        commSetting.setDestIP("172.16.20.15");
        commSetting.setDestPort("10009");
        commSetting.setMacAddr("");
        commSetting.setEnableProxy(false);



        return commSetting;
    }



    /**
     * Determine if the current application is in debug state
     */
    public static boolean isApkInDebug(Context context) {
        try {
            ApplicationInfo info = context.getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if it is a test version
     */
    public static boolean isApkForTest() {
        String versionName = BuildConfig.VERSION_NAME;
        CharSequence c = "T";
        return versionName.contains(c);
    }
}