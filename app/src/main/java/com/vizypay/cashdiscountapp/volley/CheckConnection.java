package com.vizypay.cashdiscountapp.volley;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckConnection {
    static NetworkInfo WifiInformation, mobileInformation, ethernetinfo;
    static ConnectivityManager connection_manager;
    private static boolean isNetworkAvaliable = false;

    public static boolean isNetworkAvailable(Context cxt) {

        connection_manager = (ConnectivityManager) cxt.getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiInformation = connection_manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mobileInformation = connection_manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        ethernetinfo = connection_manager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);

        if (WifiInformation.isConnected() || mobileInformation.isConnected() || ethernetinfo.isConnected()) {
            isNetworkAvaliable = true;
        } else {
            isNetworkAvaliable = false;
        }
        return isNetworkAvaliable;
    }
}

